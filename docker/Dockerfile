FROM node:14.17.5-alpine as build

# Env
ARG NODE_ENVIRONMENT=production
ENV TIME_ZONE="America/Chicago"
ENV NODE_ENV=$NODE_ENVIRONMENT
ENV HTTP_PORT=3000

# Set the timezone in docker
RUN apk --update add tzdata git \
   && eval "cp /usr/share/zoneinfo/$TIME_ZONE /etc/localtime" \
   && echo "$TIME_ZONE" > /etc/timezone \
   && apk del tzdata

RUN apk add python3 g++ make

# UI
WORKDIR /var/app
COPY ui/package.json ui/yarn.lock ui/jsconfig.json ui/.eslintrc ./ui/
WORKDIR /var/app/ui
RUN yarn install --network-timeout 1000000 --frozen-lockfile
COPY ui/src /var/app/ui/src
COPY ui/public /var/app/ui/public
RUN yarn build

# Server
WORKDIR /var/app
COPY server/package.json server/yarn.lock server/tsconfig.json server/tsconfig.build.json ./server/
WORKDIR /var/app/server
RUN yarn install --network-timeout 1000000 --frozen-lockfile
COPY server/src /var/app/server/src
RUN yarn build

RUN if [[ $NODE_ENV == "production" ]]; then \
    rm -rf /var/app/ui/src && \
    rm -rf /var/app/ui/public && \
    rm -rf /var/app/server/src; \
    fi;

WORKDIR /var/app/server
CMD [ "yarn", "start:prod" ]

EXPOSE 3000
