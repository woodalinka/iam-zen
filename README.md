![Iam Zen](https://s3.amazonaws.com/assets.cryptexlabs.com/iam-zen-wide-light-bg.png)

## <p align="center">Identity and Access Management Server</p>
<p align="center">Simple Identity and Access Management Server with admin UI</p>

## Key features
- User management
- Group management
- Policy management
- Permissions management
- Role management
- App management
- Organization management (TODO)
- Identity linking
- Authorization of above zen features

## UI Screen Shots

<img width="400" src="https://s3.amazonaws.com/assets.cryptexlabs.com/iam-zen-group-screen-shot.png" alt="IAM Zen light Screen Shot">
<img width="400" src="https://s3.amazonaws.com/assets.cryptexlabs.com/iam-zen-groups-dark-screen-shot.png" alt="IAM Zen dark Screen Shot">

### Out of scope
- User profile management. This server is only intended to identify a particular user by user id and not include profile attributes such as first and last name.
- Authentication or Authorization of non-zen permissions. While this server can enable creation of an access token and external identities, this server does not check to see if that token is legitimate. Also will not check authorization of non-zen functions.

## Documentation
- [Supported entities and attributes](./docs/attributes.md)
- [Configuration options](./docs/configuration.md)
- [Server development environment configuration](./app/server)
- [UI development environment configuration](./app/ui)
- [Authentication flows](./docs/authentication-flows.md)

## Development

### Running

#### Docker
```shell
cd docker
cp docker-compose.override.elasticsearch.example.yml docker-compose.override.yml
docker-compose up
```

### Swagger docs
http://localhost:3000/docs/v1

### Admin UI
http://localhost:3000/ui

## Merge requests
Merge requests are welcome. Please open an issue before starting to work on a merge request. If you are working on a bug fix you can create the merge request before getting a thumbs up to add a feature. If you are adding a feature please wait for a thumbs up before creating a merge request. Please document and unit test your changes.

## Code of Conduct
You will be treated with dignity and respect and your thoughts and ideas will not be ignored or brushed aside like most open source projects. Even if we do not ultimately agree with something you want to do we will give it thoughtful consideration and respond kindly.

## Key technologies
- Server: [NestJS](https://nestjs.com/)
- Admin UI: [ReactJS](https://reactjs.org/)

## License
[Apache 2.0](./LICENSE.txt)

## [Contributors](./CONTRIBUTORS.md)
