# IAM Zen Environment variables

## Environment variables

| Name | Optionality | Description | Default | Options/examples |
| ---- | ----------- | ----------- | ------- | ---------------- |
| `AUTHENTICATION_SERVICE_TYPE` | optional | Type of server used for authentication. If | `authf` | `authf`, `local` (todo) |
| `DATABASE_TYPE` | optional | Type of database used | `elasticsearch` | `elasticsearch`, `sqlite` (todo) |
| `USER_REGISTRATION_DISABLED` | optional | Whether or not to disable user registration | `false` | `true` |
| `FORGOT_PASSWORD_DISABLED` | optional | Whether or not to disable user registration | `false` | `true` |
| `AUTOLOAD_IAM_ZEN_SERVER_CONFIG` | optional | Whether or not to autoload configuration specified in `IAM_ZEN_SERVER_CONFIG_PATH` and `IAM_ZEN_SERVER_CONFIG_PATTERN` into the database when the server starts | `true` | `false` |
| `IAM_ZEN_SERVER_CONFIG_HOT_RELOAD` | optional | Whether or not to update the server configuration in memory when file system changes occur | `true` | `false` |