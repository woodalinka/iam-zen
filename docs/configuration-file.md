# IAM Zen configuration file

Configuration file will automatically create entities in the provided database when the server is launched. 

## Configuration

Example
```yaml
token:
  expirationPolicy:
    access:
      length: 900
    refresh:
      length: 2592000

identityLink:
  cognito:
    poolId: us-east-1:20845286-9960-40f6-a05a-d852bbe57076
    developerProviderName: com.cryptexlabs.iam-zen

users:
  - username: admin
    id: 278c6ec0-777c-43ad-83db-18b7cdfe2314
    groups:
      - cf24881f-0983-40e2-a6d1-2e560863e89e
    authentication:
      type: basic
      data:
        username: admin
        password: password

permissions:
  - id: ec7d1b2f-a23c-4b8a-86fc-1a9276c4ff88
    value: iam-zen:::user:self:update
    description: User can change their authentication details and username
    name: Self managed user
  - id: ae37255e-199b-4137-bece-023266d904d6
    value: iam-zen:::user:self:delete
    description: User can delete themselves
    name: Self deleting users
  - id: 7da13f60-8536-4795-ade3-373aea49d1f7
    value: iam-zen:any:any:any
    name: IAM Zen Admin user
    description: Admin user can do anything

policies:
  - name: default
    description: The default policy for all users
    id: ac5c44eb-501d-4b53-bcca-9aac58b438de
    permissions:
      - ec7d1b2f-a23c-4b8a-86fc-1a9276c4ff88
      - ae37255e-199b-4137-bece-023266d904d6
  - name: admin
    description: Administration policy
    id: ec570e6e-caaf-43f6-ac6b-cea8b7949124
    permissions:
      - 7da13f60-8536-4795-ade3-373aea49d1f7

groups:
  - name: default
    id: 613b6c12-d9c1-47cf-b401-0b5b03ef33d0
    policies:
      - ac5c44eb-501d-4b53-bcca-9aac58b438de
  - name: admin
    id: cf24881f-0983-40e2-a6d1-2e560863e89e
    policies:
      - ec570e6e-caaf-43f6-ac6b-cea8b7949124

apps:
  - name: Widget
    id: 1a2ec631-208d-4157-a741-3be6334cae6b
    description: Automatic widgetinator
    roles:
      - db1c7afb-0e59-496f-90e4-07c96757dd8c
    authentications:
      - type: api
        data:
          apiKey: daf7cb4d-a463-43a2-b5ee-1ecf65bb26e6
          secret: 12d2d44b1e904fd583c43a5b5f9df3357a8b7992

roles:
  - name: Widget Role
    id: db1c7afb-0e59-496f-90e4-07c96757dd8c
    description: Role for widget app
    policies:
      - ec570e6e-caaf-43f6-ac6b-cea8b7949124

# These are attributes that will be applied by default to all users that are created
newUser:
  groups:
    # These are group ids that will be added to all new users
    - 613b6c12-d9c1-47cf-b401-0b5b03ef33d0
```

## Immutability
Entities defined in this configuration are immutable. New entities can be created that reference these entities but these entities cannot be edited manually or through the API. 
The only way to edit the entities defined in this configuration file is to edit the configuration file that defines the entities.
