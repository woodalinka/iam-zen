import React, { useState } from 'react';
import { VscWarning } from 'react-icons/vsc';
import { Tooltip } from 'reactstrap';

const WarnTooltipIcon = ({ id, iconSize }) => {
  const [tooltipOpen, setTooltipOpen] = useState(false);

  return (
    <>
      <VscWarning id={id} size={iconSize} className="modal-checkbox-icon" />
      <Tooltip
        toggle={() => setTooltipOpen(!tooltipOpen)}
        target={id}
        isOpen={tooltipOpen}
      >
        A group with this name already exists
      </Tooltip>
    </>
  );
};

export default WarnTooltipIcon;
