import React, { useState } from 'react';
import { Tooltip } from 'reactstrap';

const TooltipIcon = ({ id, icon, item }) => {
  const [tooltipOpen, setTooltipOpen] = useState(false);

  return (
    <span>
      <i id={`tooltip_${id}`} className={`icon-m ${icon}`}>
        &nbsp;
      </i>
      <Tooltip
        placement={item.placement}
        isOpen={tooltipOpen}
        target={`tooltip_${id}`}
        toggle={() => setTooltipOpen(!tooltipOpen)}
      >
        {item.body}
      </Tooltip>
    </span>
  );
};
export default TooltipIcon;
