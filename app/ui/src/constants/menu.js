import { adminRoot } from './defaultValues';

const data = [
  // {
  //   id: 'home',
  //   icon: 'iconsminds-home',
  //   label: 'menu.dashboards',
  //   to: `${adminRoot}/dashboards/default`,
  // },
  {
    id: 'users',
    icon: 'iconsminds-user',
    label: 'menu.users',
    to: `${adminRoot}/user`,
  },
  {
    id: 'groups',
    icon: 'simple-icon-people',
    label: 'menu.groups',
    to: `${adminRoot}/group`,
  },
  {
    id: 'policies',
    icon: 'iconsminds-file',
    label: 'menu.policies',
    to: `${adminRoot}/policy`,
  },
  {
    id: 'permissions',
    icon: 'iconsminds-key',
    label: 'menu.permissions',
    to: `${adminRoot}/permission`,
  },
  {
    id: 'apps',
    icon: 'iconsminds-reverbnation',
    label: 'menu.blank-page',
    to: `${adminRoot}/app`,
  },
  {
    id: 'roles',
    icon: 'iconsminds-id-card',
    label: 'menu.roles',
    to: `${adminRoot}/role`,
  },
];
export default data;
