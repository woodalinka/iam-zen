import axios from 'axios';
import ApiUtil from '../redux/api-util';

export default class RoleApi {
  static async saveRole(roleId, name, description, appIds, policyIds) {
    const payload = {
      name,
      description,
      apps: appIds,
      policies: policyIds,
    };

    const headers = await ApiUtil.getAuthorizedPayloadApiHeaders(
      'cryptexlabs.iam-zen.role.created'
    );
    await axios.put(`/role/${roleId}`, payload, {
      headers,
      baseURL: ApiUtil.apiBasePath,
    });
  }

  static async getRoles(searchRoleName) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();

    const response = await axios.get('/role', {
      headers,
      baseURL: ApiUtil.apiBasePath,
      params: {
        page: 1,
        pageSize: 10,
        searchName: searchRoleName,
      },
    });

    return response.data.data;
  }

  static async addPoliciesToRoles(roleIds, policyIds) {
    const headers = await ApiUtil.getAuthorizedPayloadApiHeaders(
      'cryptexlabs.iam-zen.role.policies.added'
    );
    for (const roleId of roleIds) {
      await axios.post(`/role/${roleId}/policy`, policyIds, {
        headers,
        baseURL: ApiUtil.apiBasePath,
      });
    }
  }

  static async removePoliciesFromRoles(roleIds, policyIds) {
    const headers = await ApiUtil.getAuthorizedPayloadApiHeaders(
      'cryptexlabs.iam-zen.role.policies.added'
    );
    for (const roleId of roleIds) {
      await axios.delete(`/role/${roleId}/policy`, {
        headers,
        params: {
          ids: policyIds,
        },
        baseURL: ApiUtil.apiBasePath,
      });
    }
  }

  static async getRole(id) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();

    const response = await axios.get(`/role/${id}`, {
      headers,
      baseURL: ApiUtil.apiBasePath,
    });

    return response.data.data;
  }

  static async updateRole(roleId, name, description) {
    const payload = {
      name,
      description,
    };
    const headers = await ApiUtil.getAuthorizedPayloadApiHeaders(
      'cryptexlabs.iam-zen.role.updated'
    );
    await axios.patch(`/role/${roleId}`, payload, {
      headers,
      baseURL: ApiUtil.apiBasePath,
    });
  }
}
