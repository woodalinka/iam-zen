import axios from 'axios';
import ApiUtil from '../redux/api-util';

export default class PermissionApi {
  static async getPermissions(searchPermissionName) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();
    const response = await axios.get('/permission', {
      headers,
      baseURL: ApiUtil.apiBasePath,
      params: {
        page: 1,
        pageSize: 10,
        searchName: searchPermissionName,
      },
    });

    return response.data.data;
  }

  static async savePermission(
    permissionId,
    name,
    description,
    value,
    policyIds
  ) {
    let payload = {
      name,
      description,
      value,
    };

    payload = {
      ...payload,
      policies: policyIds,
    };

    const headers = await ApiUtil.getAuthorizedPayloadApiHeaders(
      'cryptexlabs.iam-zen.permission.created'
    );
    await axios.put(`/permission/${permissionId}`, payload, {
      headers,
      baseURL: ApiUtil.apiBasePath,
    });
  }

  static async permissionExists(permissionValue) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();

    const response = await axios.get('/permission/any/exists', {
      headers,
      baseURL: ApiUtil.apiBasePath,
      params: {
        value: permissionValue,
      },
    });

    return response.data.data;
  }

  static async deletePermissions(permissionIds) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();

    await axios.delete(`/permission`, {
      headers,
      baseURL: ApiUtil.apiBasePath,
      params: {
        ids: permissionIds,
      },
    });
  }

  static async getPermission(id) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();

    const response = await axios.get(`/permission/${id}`, {
      headers,
      baseURL: ApiUtil.apiBasePath,
    });

    return response.data.data;
  }

  static async updatePermission(permissionId, name, description, value) {
    const payload = {
      name,
      description,
      value,
    };
    const headers = await ApiUtil.getAuthorizedPayloadApiHeaders(
      'cryptexlabs.iam-zen.app.updated'
    );
    await axios.patch(`/permission/${permissionId}`, payload, {
      headers,
      baseURL: ApiUtil.apiBasePath,
    });
  }
}
