import axios from 'axios';
import ApiUtil from '../redux/api-util';

export default class PolicyApi {
  static async savePolicy(
    policyId,
    name,
    description,
    groupIds,
    permissionIds,
    roleIds
  ) {
    const payload = {
      name,
      description,
      groups: groupIds,
      permissions: permissionIds,
      roles: roleIds,
    };

    const headers = await ApiUtil.getAuthorizedPayloadApiHeaders(
      'cryptexlabs.iam-zen.policy.created'
    );

    await axios.put(`/policy/${policyId}`, payload, {
      headers,
      baseURL: ApiUtil.apiBasePath,
    });
  }

  static async getPaginatedPolicies(searchPolicyName, page, pageSize) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();
    const response = await axios.get('/policy', {
      headers,
      baseURL: ApiUtil.apiBasePath,
      params: {
        page,
        pageSize,
        searchName: searchPolicyName,
      },
    });

    return response.data;
  }

  static async getPolicies(searchPolicy) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();

    const response = await axios.get('/policy', {
      headers,
      baseURL: ApiUtil.apiBasePath,
      params: {
        page: 1,
        pageSize: 10,
        searchName: searchPolicy,
      },
    });

    return response.data.data;
  }

  static async addPermissionsToPolicies(policyIds, permissionIds) {
    const headers = await ApiUtil.getAuthorizedPayloadApiHeaders(
      'cryptexlabs.iam-zen.policy.permissions.added'
    );
    for (const policyId of policyIds) {
      await axios.post(`/policy/${policyId}/permission`, permissionIds, {
        headers,
        baseURL: ApiUtil.apiBasePath,
      });
    }
  }

  static async removePermissionsFromPolicies(policyIds, permissionIds) {
    const headers = await ApiUtil.getAuthorizedPayloadApiHeaders(
      'cryptexlabs.iam-zen.policy.permissions.added'
    );
    for (const policyId of policyIds) {
      await axios.delete(`/policy/${policyId}/permission`, {
        headers,
        params: {
          ids: permissionIds,
        },
        baseURL: ApiUtil.apiBasePath,
      });
    }
  }

  static async getPolicy(id) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();

    const response = await axios.get(`/policy/${id}`, {
      headers,
      baseURL: ApiUtil.apiBasePath,
    });

    return response.data.data;
  }

  static async updatePolicy(policyId, name, description) {
    const payload = {
      name,
      description,
    };
    const headers = await ApiUtil.getAuthorizedPayloadApiHeaders(
      'cryptexlabs.iam-zen.policy.updated'
    );
    await axios.patch(`/policy/${policyId}`, payload, {
      headers,
      baseURL: ApiUtil.apiBasePath,
    });
  }
}
