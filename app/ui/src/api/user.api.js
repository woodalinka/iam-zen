import axios from 'axios';
import ApiUtil from '../redux/api-util';

export default class UserApi {
  static async createUser(userId, username, password, groupIds) {
    let payload = {
      username,
    };
    payload = {
      ...payload,
      groups: groupIds,
    };
    if (password && password.trim()) {
      payload = {
        ...payload,
        authentication: {
          data: {
            username,
            password,
          },
          type: 'basic',
        },
      };
    }
    const headers = await ApiUtil.getAuthorizedPayloadApiHeaders(
      'cryptexlabs.iam-zen.user.created'
    );
    const response = await axios.post(`/user/${userId}`, payload, {
      headers,
      baseURL: ApiUtil.apiBasePath,
    });
  }

  static async getUsers(searchUsername) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();

    const response = await axios.get('/user', {
      headers,
      baseURL: ApiUtil.apiBasePath,
      params: {
        page: 1,
        pageSize: 10,
        searchName: searchUsername,
      },
    });

    return response.data.data;
  }

  static async userExists(username) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();
    const response = await axios.get('/user/any/exists', {
      headers,
      baseURL: ApiUtil.apiBasePath,
      params: {
        username,
      },
    });
    return response.data.data;
  }

  static async addGroupsToUsers(userIds, groupIds) {
    const headers = await ApiUtil.getAuthorizedPayloadApiHeaders(
      'cryptexlabs.iam-zen.user.groups.added'
    );
    for (const userId of userIds) {
      await axios.post(`/user/${userId}/group`, groupIds, {
        headers,
        baseURL: ApiUtil.apiBasePath,
      });
    }
  }

  static async removeGroupsFromUsers(userIds, groupIds) {
    const headers = await ApiUtil.getAuthorizedPayloadApiHeaders(
      'cryptexlabs.iam-zen.user.groups.added'
    );
    for (const userId of userIds) {
      await axios.delete(`/user/${userId}/group`, {
        headers,
        params: {
          ids: groupIds,
        },
        baseURL: ApiUtil.apiBasePath,
      });
    }
  }

  static async getUser(id) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();

    const response = await axios.get(`/user/${id}`, {
      headers,
      baseURL: ApiUtil.apiBasePath,
    });

    return response.data.data;
  }

  static async updateUser(userId, username, password) {
    const payload = {
      username,
      password,
    };
    const headers = await ApiUtil.getAuthorizedPayloadApiHeaders(
      'cryptexlabs.iam-zen.user.updated'
    );
    await axios.patch(`/user/${userId}`, payload, {
      headers,
      baseURL: ApiUtil.apiBasePath,
    });
  }
}
