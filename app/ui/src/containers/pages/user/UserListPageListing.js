import React from 'react';
import { Row, Spinner } from 'reactstrap';
import Pagination from '../Pagination';
import ContextMenuContainer from '../ContextMenuContainer';
import UserListView from './UserListView';

function collect(props) {
  return { data: props.data };
}

const UserListPageListing = ({
  items,
  selectedItems,
  onCheckItem,
  currentPage,
  totalPage,
  onContextMenuClick,
  onContextMenu,
  onChangePage,
  isLoading,
  intl,
  canCheckItem = true,
}) => {
  if (!isLoading) {
    return (
      <Row>
        {items.map((user) => {
          return (
            <UserListView
              key={user.id}
              user={user}
              isSelect={selectedItems.includes(user.id)}
              onCheckItem={onCheckItem}
              canCheckItem={canCheckItem}
              collect={collect}
              intl={intl}
            />
          );
        })}
        <Pagination
          currentPage={currentPage}
          totalPage={totalPage}
          onChangePage={(i) => onChangePage(i)}
        />
        <ContextMenuContainer
          onContextMenuClick={onContextMenuClick}
          onContextMenu={onContextMenu}
        />
      </Row>
    );
  }

  return (
    <div style={{ display: 'flex', justifyContent: 'center' }}>
      <Spinner color="primary" />;
    </div>
  );
};

export default React.memo(UserListPageListing);
