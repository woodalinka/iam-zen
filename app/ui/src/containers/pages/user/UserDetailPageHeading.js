/* eslint-disable react/no-array-index-key */
import React, { useState } from 'react';
import { Button, Input, Row } from 'reactstrap';
import { injectIntl } from 'react-intl';
import classnames from 'classnames';
import TextareaAutosize from 'react-textarea-autosize';

import { Colxx, Separator } from 'components/common/CustomBootstrap';
import IntlMessages from 'helpers/IntlMessages';

import Breadcrumb from '../../navs/Breadcrumb';
import UserApi from '../../../api/user.api';
import TooltipIcon from '../../../components/common/TooltipIcon';

const UserDetailPageHeading = ({ intl, match, immutable, userName }) => {
  const [isEditing, setIsEditing] = useState(false);
  const [newUserName, setNewUserName] = useState(userName);
  const [newUserPassword, setNewUserPassword] = useState('');

  const onSaveClicked = async () => {
    await UserApi.updateUser(match.params.userId, newUserName, newUserPassword);
    setIsEditing(false);
  };

  const onCancelClicked = () => {
    setNewUserName(userName);
    setNewUserPassword('');
    setIsEditing(false);
  };

  return (
    <>
      <Row>
        <Colxx xxs="12">
          <div className="mb-2">
            {!isEditing && (
              <h1>
                {!newUserName && <IntlMessages id="user.user" />}
                {newUserName}
              </h1>
            )}
            {isEditing && (
              <Input
                className="col-sm-4 d-sm-inline mb-lg-0 mb-2"
                value={newUserName}
                onChange={(event) => {
                  setNewUserName(event.target.value);
                }}
              />
            )}

            <div
              className={classnames('text-zero top-right-button-container', {
                'd-inline': immutable,
              })}
            >
              {immutable && (
                <div style={{ marginTop: 10 }} className="float-right">
                  <TooltipIcon
                    icon="simple-icon-lock"
                    item={{
                      body: intl.formatMessage({
                        id: 'tip.immutable.permission',
                      }),
                      placement: 'top',
                    }}
                    id="user-immutable-icon"
                  />
                </div>
              )}

              {!immutable && (
                <>
                  {!isEditing && (
                    <Button
                      onClick={() => setIsEditing(true)}
                      color="primary"
                      size="lg"
                      className="top-right-button"
                    >
                      <IntlMessages id="pages.edit" />
                    </Button>
                  )}
                  {isEditing && (
                    <>
                      <Button
                        onClick={onSaveClicked}
                        color="primary"
                        size="lg"
                        className="mr-1 top-right-button"
                      >
                        <IntlMessages id="pages.save" />
                      </Button>
                      <Button
                        onClick={onCancelClicked}
                        outline
                        color="secondary"
                        size="lg"
                        className="top-right-button"
                      >
                        <IntlMessages id="pages.cancel" />
                      </Button>
                    </>
                  )}
                </>
              )}
            </div>

            <div className="col-sm-4 d-sm-inline">
              <Breadcrumb match={match} />
            </div>
          </div>

          <Separator className="mb-3" />
        </Colxx>
      </Row>

      <Row style={{ marginBottom: 20 }}>
        <Colxx xxs="12">
          {isEditing && (
            <Input
              className="col-sm-4"
              placeholder="New password"
              value={newUserPassword}
              onChange={(event) => {
                setNewUserPassword(event.target.value);
              }}
            />
          )}
        </Colxx>
      </Row>
    </>
  );
};

export default injectIntl(UserDetailPageHeading);
