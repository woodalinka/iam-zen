import { FcCancel, FcCheckmark } from 'react-icons/fc';
import React, { useEffect, useState } from 'react';
import {
  Button,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from 'reactstrap';
import IntlMessages from 'helpers/IntlMessages';
import Multiselect from 'multiselect-react-dropdown';
import { injectIntl } from 'react-intl';
import UserApi from '../../../api/user.api';

const AddUsersToGroupModal = ({
  modalOpen,
  toggleModal,
  intl,
  onGroupsUpdated,
  groupId,
}) => {
  const [searchUserName, setSearchUserName] = useState('');

  const initialState = {
    users: [],
    usersSelected: [],
  };
  const [state, setState] = useState(initialState);

  const resetState = () => {
    let initialStateToUse = {
      ...initialState,
    };
    if (searchUserName || searchUserName.trim() === '') {
      initialStateToUse = {
        ...initialStateToUse,
        users: state.users,
      };
    }

    setSearchUserName('');
    setState(initialStateToUse);
  };

  useEffect(() => {
    const fetchData = async () => {};
    fetchData().then();
  }, []);

  useEffect(async () => {
    const apiUsers = await UserApi.getUsers(searchUserName);
    setState({
      ...state,
      users: apiUsers.map((item) => ({
        id: item.id,
        name: item.username,
      })),
    });
  }, [searchUserName]);

  const iconSize = 22;

  const onAddButtonClicked = async () => {
    console.log(state.usersSelected);
    if (state.usersSelected.length > 0) {
      await UserApi.addGroupsToUsers(
        state.usersSelected.map((user) => user.id),
        [groupId]
      );
      await resetState();
      onGroupsUpdated();
      toggleModal();
    }
  };

  const setUsersSelected = (usersSelected) => {
    setState({
      ...state,
      usersSelected,
    });
  };

  return (
    <Modal isOpen={modalOpen} toggle={toggleModal} backdrop="static">
      <ModalHeader toggle={toggleModal}>
        <IntlMessages id="pages.add-users-to-groups-modal-title" />
      </ModalHeader>
      <ModalBody>
        <div className="modal-checkbox">
          <div className="modal-checkbox-container">
            <div className="modal-checkbox-wrapper">
              {state.userExists === 'exists' && (
                <FcCancel size={iconSize} className="modal-checkbox-icon" />
              )}
              {state.userExists === 'not-exists' && (
                <FcCheckmark size={iconSize} className="modal-checkbox-icon" />
              )}
              {state.userExists === 'unknown' && (
                <div
                  style={{ width: iconSize }}
                  className="modal-checkbox-icon"
                >
                  &#8203;
                </div>
              )}
            </div>
          </div>
        </div>

        <Label className="mt-4">
          <IntlMessages id="dashboards.users" />
        </Label>
        <Multiselect
          style={{ color: 'black' }}
          avoidHighlightFirstOption={true}
          options={state.users}
          selectedValues={state.usersSelected}
          onSearch={setSearchUserName}
          onSelect={setUsersSelected}
          onRemove={setUsersSelected}
          displayValue="name"
        />
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" outline onClick={toggleModal}>
          <IntlMessages id="pages.cancel" />
        </Button>
        <Button
          color="primary"
          disabled={state.userExists === 'exists'}
          onClick={onAddButtonClicked}
        >
          <IntlMessages id="pages.add" />
        </Button>{' '}
      </ModalFooter>
    </Modal>
  );
};

export default injectIntl(AddUsersToGroupModal);
