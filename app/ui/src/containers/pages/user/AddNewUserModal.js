import { v4 as uuidv4 } from 'uuid';
import { FcCancel, FcCheckmark } from 'react-icons/fc';
import React, { useEffect, useState } from 'react';
import {
  Button,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from 'reactstrap';
import IntlMessages from 'helpers/IntlMessages';
import Multiselect from 'multiselect-react-dropdown';
import { injectIntl } from 'react-intl';
import UserApi from '../../../api/user.api';
import GroupApi from '../../../api/group.api';

const AddNewUserModal = ({ modalOpen, toggleModal, intl, onCreateUser }) => {
  const [userName, setUserName] = useState('');
  const [userPassword, setUserPassword] = useState('');
  const [searchGroupName, setSearchGroupName] = useState('');

  const initialState = {
    groups: [],
    groupsSelected: [],
  };
  const [state, setState] = useState(initialState);

  const resetState = () => {
    setUserName('');

    let initialStateToUse = {
      ...initialState,
    };
    if (searchGroupName || searchGroupName.trim() === '') {
      initialStateToUse = {
        ...initialStateToUse,
        groups: state.groups,
      };
    }

    setSearchGroupName('');
    setState(initialStateToUse);
  };

  useEffect(() => {
    const fetchData = async () => {
      if (!userName || !userName.trim()) {
        setState({
          ...state,
          userExists: 'unknown',
        });
        return;
      }

      const exists = await UserApi.userExists(userName);

      setState({
        ...state,
        userExists: exists ? 'exists' : 'not-exists',
      });
    };
    fetchData().then();
  }, [userName]);

  useEffect(async () => {
    const apiGroups = await GroupApi.getGroups(searchGroupName);
    setState({
      ...state,
      groups: apiGroups.map((item) => ({
        id: item.id,
        name: item.name,
      })),
    });
  }, [searchGroupName]);

  const iconSize = 22;

  const createUser = async (userId) => {
    await UserApi.createUser(
      userId,
      userName,
      userPassword,
      state.groupsSelected.map((group) => group.id)
    );
  };

  const onCreateButtonClicked = async () => {
    const userId = uuidv4();
    await createUser(userId);
    await toggleModal();
    await onCreateUser();
    await resetState();
  };

  const setGroupsSelected = (groupsSelected) => {
    setState({
      ...state,
      groupsSelected,
    });
  };

  return (
    <Modal
      isOpen={modalOpen}
      toggle={toggleModal}
      wrapClassName="modal-right"
      backdrop="static"
    >
      <ModalHeader toggle={toggleModal}>
        <IntlMessages id="pages.add-new-user-modal-title" />
      </ModalHeader>
      <ModalBody>
        <Label>
          <IntlMessages id="user.username" />
        </Label>
        <div className="modal-checkbox">
          <Input
            value={userName}
            onChange={(event) => {
              setUserName(event.target.value);
            }}
          />
          <div className="modal-checkbox-container">
            <div className="modal-checkbox-wrapper">
              {state.userExists === 'exists' && (
                <FcCancel size={iconSize} className="modal-checkbox-icon" />
              )}
              {state.userExists === 'not-exists' && (
                <FcCheckmark size={iconSize} className="modal-checkbox-icon" />
              )}
              {state.userExists === 'unknown' && (
                <div
                  style={{ width: iconSize }}
                  className="modal-checkbox-icon"
                >
                  &#8203;
                </div>
              )}
            </div>
          </div>
        </div>

        <Label className="mt-4">
          <IntlMessages id="user.password" />
        </Label>
        <div className="modal-checkbox">
          <Input
            type="password"
            placeholder={intl.formatMessage({
              id: 'user.create.password.placeholder',
            })}
            value={userPassword}
            onChange={(event) => {
              setState({
                ...state,
                userPassword: event.target.value,
              });
            }}
          />
        </div>

        <Label className="mt-4">
          <IntlMessages id="dashboards.groups" />
        </Label>
        <Multiselect
          style={{ color: 'black' }}
          avoidHighlightFirstOption={true}
          options={state.groups}
          selectedValues={state.groupsSelected}
          onSearch={setSearchGroupName}
          onSelect={setGroupsSelected}
          onRemove={setGroupsSelected}
          displayValue="name"
        />
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" outline onClick={toggleModal}>
          <IntlMessages id="pages.cancel" />
        </Button>
        <Button
          color="primary"
          disabled={state.userExists === 'exists'}
          onClick={onCreateButtonClicked}
        >
          <IntlMessages id="pages.submit" />
        </Button>{' '}
      </ModalFooter>
    </Modal>
  );
};

export default injectIntl(AddNewUserModal);
