import React from 'react';
import { Card, CustomInput } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import classnames from 'classnames';
import { ContextMenuTrigger } from 'react-contextmenu';
import { Colxx } from 'components/common/CustomBootstrap';
import TooltipIcon from '../../../components/common/TooltipIcon';

const UserListView = ({
  user,
  isSelect,
  collect,
  onCheckItem,
  intl,
  canCheckItem,
}) => {
  let immutabilityComponent;
  if (user.immutable) {
    immutabilityComponent = (
      <TooltipIcon
        icon="simple-icon-lock"
        item={{
          body: intl.formatMessage({ id: 'tip.immutable.user' }),
          placement: 'top',
        }}
        id={`user_${user.id}`}
      />
    );
  }
  return (
    <Colxx xxs="12" className="mb-3">
      <ContextMenuTrigger id="menu_id" data={user.id} collect={collect}>
        <Card
          onClick={(event) => onCheckItem(event, user.id)}
          className={classnames('d-flex flex-row', {
            active: isSelect,
          })}
        >
          <div className="pl-2 d-flex flex-grow-1 min-width-zero">
            <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
              <NavLink to={`/iam/ui/user/${user.id}`} className="w-10 w-sm-100">
                <p className="list-item-heading mb-1 truncate">
                  {user.username && user.username}
                  {!user.username && <span>(none)</span>}
                </p>
              </NavLink>
              <p
                style={{ userSelect: 'text' }}
                className="mb-1 text-muted text-small w-30 w-sm-100"
              >
                {user.id}
              </p>
              <p className="mb-1 text-muted text-small w-15 w-sm-100">
                Groups: {user.groupCount}
              </p>
              <div className="w-15 w-sm-100">{immutabilityComponent}</div>
            </div>
            {canCheckItem && (
              <div className="custom-control custom-checkbox pl-1 align-self-center pr-4">
                <CustomInput
                  className="item-check mb-0"
                  type="checkbox"
                  id={`check_${user.id}`}
                  checked={isSelect}
                  onChange={() => {}}
                  label=""
                />
              </div>
            )}
          </div>
        </Card>
      </ContextMenuTrigger>
    </Colxx>
  );
};

/* React.memo detail : https://reactjs.org/docs/react-api.html#reactpurecomponent  */
export default React.memo(UserListView);
