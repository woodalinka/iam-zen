import { FcCancel, FcCheckmark } from 'react-icons/fc';
import React, { useEffect, useState } from 'react';
import {
  Button,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from 'reactstrap';
import IntlMessages from 'helpers/IntlMessages';
import axios from 'axios';
import Multiselect from 'multiselect-react-dropdown';
import { injectIntl } from 'react-intl';
import ApiUtil from '../../../redux/api-util';
import GroupApi from '../../../api/group.api';
import UserApi from '../../../api/user.api';

const AddUsersToGroupsModal = ({
  modalOpen,
  toggleModal,
  intl,
  onGroupsUpdated,
  selectedUserIds,
}) => {
  const [searchGroupName, setSearchGroupName] = useState('');

  const initialState = {
    groups: [],
    groupsSelected: [],
  };
  const [state, setState] = useState(initialState);

  const resetState = () => {
    let initialStateToUse = {
      ...initialState,
    };
    if (searchGroupName || searchGroupName.trim() === '') {
      initialStateToUse = {
        ...initialStateToUse,
        groups: state.groups,
      };
    }

    setSearchGroupName('');
    setState(initialStateToUse);
  };

  useEffect(() => {
    const fetchData = async () => {};
    fetchData().then();
  }, []);

  useEffect(async () => {
    const apiGroups = await GroupApi.getGroups(searchGroupName);
    setState({
      ...state,
      groups: apiGroups.map((item) => ({
        id: item.id,
        name: item.name,
      })),
    });
  }, [searchGroupName]);

  const iconSize = 22;

  const onAddButtonClicked = async () => {
    await UserApi.addGroupsToUsers(
      selectedUserIds,
      state.groupsSelected.map((group) => group.id)
    );
    await resetState();
    onGroupsUpdated();
    toggleModal();
  };

  const setGroupsSelected = (groupsSelected) => {
    setState({
      ...state,
      groupsSelected,
    });
  };

  return (
    <Modal isOpen={modalOpen} toggle={toggleModal} backdrop="static">
      <ModalHeader toggle={toggleModal}>
        <IntlMessages id="pages.add-users-to-groups-modal-title" />
      </ModalHeader>
      <ModalBody>
        <div className="modal-checkbox">
          <div className="modal-checkbox-container">
            <div className="modal-checkbox-wrapper">
              {state.userExists === 'exists' && (
                <FcCancel size={iconSize} className="modal-checkbox-icon" />
              )}
              {state.userExists === 'not-exists' && (
                <FcCheckmark size={iconSize} className="modal-checkbox-icon" />
              )}
              {state.userExists === 'unknown' && (
                <div
                  style={{ width: iconSize }}
                  className="modal-checkbox-icon"
                >
                  &#8203;
                </div>
              )}
            </div>
          </div>
        </div>

        <Label className="mt-4">
          <IntlMessages id="dashboards.groups" />
        </Label>
        <Multiselect
          style={{ color: 'black' }}
          avoidHighlightFirstOption={true}
          options={state.groups}
          selectedValues={state.groupsSelected}
          onSearch={setSearchGroupName}
          onSelect={setGroupsSelected}
          onRemove={setGroupsSelected}
          displayValue="name"
        />
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" outline onClick={toggleModal}>
          <IntlMessages id="pages.cancel" />
        </Button>
        <Button
          color="primary"
          disabled={state.userExists === 'exists'}
          onClick={onAddButtonClicked}
        >
          <IntlMessages id="pages.add" />
        </Button>{' '}
      </ModalFooter>
    </Modal>
  );
};

export default injectIntl(AddUsersToGroupsModal);
