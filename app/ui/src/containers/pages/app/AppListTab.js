import React, { useState } from 'react';
import {
  Button,
  ButtonDropdown,
  CustomInput,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Row,
} from 'reactstrap';
import { Colxx } from 'components/common/CustomBootstrap';
import IntlMessages from '../../../helpers/IntlMessages';
import AppListView from './AppListView';
import ArrayUtil from '../../../util/array.util';
import AddAppsToRoleModal from '../role/AddAppsToRoleModal';
import AppApi from '../../../api/app.api';

const AppListTab = ({ apps, roleId, intl, onChange }) => {
  const [lastCheckedApp, setLastCheckedApp] = useState(null);
  const [dropdownSplitOpen, setDropdownSplitOpen] = useState(false);
  const [selectedItems, setSelectedItems] = useState([]);
  const [addAppsToRoleModalOpen, setAddAppsToRoleModalOpen] = useState(false);

  const onAppSelected = (event, id) => {
    if (
      event.target.tagName === 'A' ||
      (event.target.parentElement && event.target.parentElement.tagName === 'A')
    ) {
      return true;
    }
    if (lastCheckedApp === null) {
      setLastCheckedApp(id);
    }

    let selectedList = [...selectedItems];
    if (selectedList.includes(id)) {
      selectedList = selectedList.filter((x) => x !== id);
    } else {
      selectedList.push(id);
    }
    setSelectedItems(selectedList);

    if (event.shiftKey) {
      let newItems = [...apps];
      const start = ArrayUtil.getIndex(id, newItems, 'id');
      const end = ArrayUtil.getIndex(lastCheckedApp, newItems, 'id');
      newItems = newItems.slice(Math.min(start, end), Math.max(start, end) + 1);
      selectedItems.push(
        ...newItems.map((item) => {
          return item.id;
        })
      );
      selectedList = Array.from(new Set(selectedItems));
      setSelectedItems(selectedList);
    }
    document.activeElement.blur();
    return false;
  };

  const handleChangeSelectAll = (isToggle) => {
    if (selectedItems.length >= apps.length) {
      if (isToggle) {
        setSelectedItems([]);
      }
    } else {
      setSelectedItems(apps.map((x) => x.id));
    }
    document.activeElement.blur();
    return false;
  };

  const onRemoveClicked = async () => {
    if (roleId) {
      await AppApi.removeRolesFromApps(selectedItems, [roleId]);
    }

    await onChange();
  };

  return (
    <>
      {roleId && (
        <Row>
          <Colxx xxs="12" className="mb-4">
            <div className="text-zero top-right-button-container">
              <Button
                color="primary"
                size="lg"
                className="top-right-button mr-1"
                onClick={() => {
                  if (roleId) {
                    setAddAppsToRoleModalOpen(!addAppsToRoleModalOpen);
                  }
                }}
              >
                <IntlMessages id="pages.add" />
              </Button>
              <ButtonDropdown
                className="btn-role"
                isOpen={dropdownSplitOpen}
                toggle={() => setDropdownSplitOpen(!dropdownSplitOpen)}
              >
                <div className="btn btn-primary btn-lg pl-4 pr-0 check-button check-all">
                  <CustomInput
                    className="custom-checkbox mb-0 d-inline-block"
                    type="checkbox"
                    id="checkAll"
                    checked={selectedItems.length >= apps.length}
                    onChange={() => handleChangeSelectAll(true)}
                    label={
                      <span
                        className={`custom-control-label ${
                          selectedItems.length > 0 &&
                          selectedItems.length < apps.length
                            ? 'indeterminate'
                            : ''
                        }`}
                      />
                    }
                  />
                </div>
                <DropdownToggle
                  caret
                  color="primary"
                  className="dropdown-toggle-split btn-lg"
                />
                <DropdownMenu right>
                  <DropdownItem onClick={onRemoveClicked}>
                    <IntlMessages id="pages.remove" />
                  </DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
            </div>
          </Colxx>
        </Row>
      )}

      <Row>
        {apps.map((app) => {
          return (
            <AppListView
              showDetails={false}
              isSelect={selectedItems.includes(app.id)}
              onCheckItem={onAppSelected}
              intl={intl}
              key={app.id}
              app={app}
            />
          );
        })}
      </Row>
      {roleId && (
        <AddAppsToRoleModal
          roleId={roleId}
          intl={intl}
          modalOpen={addAppsToRoleModalOpen}
          toggleModal={() => setAddAppsToRoleModalOpen(!addAppsToRoleModalOpen)}
          onRoleUpdated={onChange}
          existingAppIds={apps.map((role) => role.id)}
        />
      )}
    </>
  );
};

export default AppListTab;
