import { v4 as uuidv4 } from 'uuid';
import React, { useEffect, useState } from 'react';
import {
  Button,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from 'reactstrap';
import IntlMessages from 'helpers/IntlMessages';
import axios from 'axios';
import Multiselect from 'multiselect-react-dropdown';
import { injectIntl } from 'react-intl';
import ApiUtil from '../../../redux/api-util';
import AppApi from '../../../api/app.api';
import RoleApi from '../../../api/role.api';

const AddNewAppModal = ({ modalOpen, toggleModal, intl, onCreateApp }) => {
  const [appName, setAppName] = useState('');
  const [appDescription, setAppDescription] = useState('');
  const [searchRoleName, setSearchRoleName] = useState('');

  const initialState = {
    appName: '',
    description: '',
    roles: [],
    rolesSelected: [],
  };
  const [state, setState] = useState(initialState);

  const resetState = () => {
    setAppName('');
    setAppDescription('');

    let initialStateToUse = {
      ...initialState,
    };
    if (searchRoleName || searchRoleName.trim() === '') {
      initialStateToUse = {
        ...initialStateToUse,
        roles: state.roles,
      };
    }

    setSearchRoleName('');
    setState(initialStateToUse);
  };

  useEffect(() => {
    const fetchRoles = async () => {
      const apiRoles = await RoleApi.getRoles(searchRoleName);

      setState({
        ...state,
        roles: apiRoles.map((item) => ({
          id: item.id,
          name: item.name,
        })),
      });
    };
    fetchRoles().then();
  }, [searchRoleName]);

  const createApp = async (appId) => {
    await AppApi.createApp(appId, appName, appDescription, state.rolesSelected);
  };

  const onCreateButtonClicked = async () => {
    const appId = uuidv4();
    await createApp(appId);
    await toggleModal();
    await onCreateApp();
    await resetState();
  };

  const setRolesSelected = (rolesSelected) => {
    setState({
      ...state,
      rolesSelected,
    });
  };

  return (
    <Modal
      isOpen={modalOpen}
      toggle={toggleModal}
      wrapClassName="modal-right"
      backdrop="static"
    >
      <ModalHeader toggle={toggleModal}>
        <IntlMessages id="pages.add-new-app-modal-title" />
      </ModalHeader>
      <ModalBody>
        <Label>
          <IntlMessages id="app.name" />
        </Label>
        <Input
          value={appName}
          onChange={(event) => {
            setAppName(event.target.value);
          }}
        />

        <Label className="mt-4">
          <IntlMessages id="app.description" />
        </Label>
        <Input
          type="textarea"
          value={appDescription}
          onChange={(event) => {
            setAppDescription(event.target.value);
          }}
        />

        <Label className="mt-4">
          <IntlMessages id="app.roles" />
        </Label>
        <Multiselect
          style={{ color: 'black' }}
          avoidHighlightFirstOption={true}
          options={state.roles}
          selectedValues={state.rolesSelected}
          onSearch={setSearchRoleName}
          onSelect={setRolesSelected}
          onRemove={setRolesSelected}
          displayValue="name"
        />
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" outline onClick={toggleModal}>
          <IntlMessages id="pages.cancel" />
        </Button>
        <Button color="primary" onClick={onCreateButtonClicked}>
          <IntlMessages id="pages.submit" />
        </Button>{' '}
      </ModalFooter>
    </Modal>
  );
};

export default injectIntl(AddNewAppModal);
