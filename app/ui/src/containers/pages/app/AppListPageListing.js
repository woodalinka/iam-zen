import React from 'react';
import { Row, Spinner } from 'reactstrap';
import Pagination from '../Pagination';
import AppListView from './AppListView';

function collect(props) {
  return { data: props.data };
}

const AppListPageListing = ({
  items,
  selectedItems,
  onCheckItem,
  currentPage,
  totalPage,
  onChangePage,
  isLoading,
  intl,
}) => {
  if (!isLoading) {
    return (
      <Row>
        {items.map((app) => {
          return (
            <AppListView
              key={app.id}
              app={app}
              isSelect={selectedItems.includes(app.id)}
              onCheckItem={onCheckItem}
              collect={collect}
              intl={intl}
            />
          );
        })}
        <Pagination
          currentPage={currentPage}
          totalPage={totalPage}
          onChangePage={(i) => onChangePage(i)}
        />
      </Row>
    );
  }

  return (
    <div style={{ display: 'flex', justifyContent: 'center' }}>
      <Spinner color="primary" />;
    </div>
  );
};

export default React.memo(AppListPageListing);
