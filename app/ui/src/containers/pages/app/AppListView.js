import React from 'react';
import { Card, CustomInput } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import classnames from 'classnames';
import { ContextMenuTrigger } from 'react-contextmenu';
import { Colxx } from 'components/common/CustomBootstrap';
import TooltipIcon from '../../../components/common/TooltipIcon';

const AppListView = ({ app, isSelect, collect, onCheckItem, intl }) => {
  let immutabilityComponent;
  if (app.immutable) {
    immutabilityComponent = (
      <TooltipIcon
        icon="simple-icon-lock"
        item={{
          body: intl.formatMessage({ id: 'tip.immutable.app' }),
          placement: 'top',
        }}
        id={`app_${app.id}`}
      />
    );
  }
  return (
    <Colxx xxs="12" className="mb-3">
      <ContextMenuTrigger id="menu_id" data={app.id} collect={collect}>
        <Card
          onClick={(event) => onCheckItem(event, app.id)}
          className={classnames('d-flex flex-row', {
            active: isSelect,
          })}
        >
          <div className="pl-2 d-flex flex-grow-1 min-width-zero">
            <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
              <NavLink to={`/iam/ui/app/${app.id}`} className="w-40 w-sm-100">
                <p className="list-item-heading mb-1 truncate">{app.name}</p>
              </NavLink>
              {app.policyCount !== undefined && (
                <p className="mb-1 text-muted text-small w-15 w-sm-100">
                  Policies: {app.policyCount}
                </p>
              )}
              {app.description !== undefined && (
                <p className="mb-1 text-muted text-small w-15 w-sm-100">
                  {app.description}
                </p>
              )}
              {app.immutable !== undefined && (
                <div className="w-15 w-sm-100">{immutabilityComponent}</div>
              )}
            </div>
            <div className="custom-control custom-checkbox pl-1 align-self-center pr-4">
              <CustomInput
                className="item-check mb-0"
                type="checkbox"
                id={`check_${app.id}`}
                checked={isSelect}
                onChange={() => {}}
                label=""
              />
            </div>
          </div>
        </Card>
      </ContextMenuTrigger>
    </Colxx>
  );
};

/* React.memo detail : https://reactjs.org/docs/react-api.html#reactpurecomponent  */
export default React.memo(AppListView);
