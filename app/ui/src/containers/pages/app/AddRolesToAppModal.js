import React, { useEffect, useState } from 'react';
import {
  Button,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from 'reactstrap';
import IntlMessages from 'helpers/IntlMessages';
import Multiselect from 'multiselect-react-dropdown';
import { injectIntl } from 'react-intl';
import RoleApi from '../../../api/role.api';
import AppApi from '../../../api/app.api';

const AdRolesToAppModal = ({
  modalOpen,
  toggleModal,
  intl,
  onAppUpdated,
  appId,
  existingRoleIds,
}) => {
  const [searchRoleName, setSearchRoleName] = useState('');
  const [roles, setRoles] = useState([]);
  const [rolesSelected, setRolesSelected] = useState([]);

  const resetState = () => {
    setSearchRoleName('');
    setRoles([]);
    setRolesSelected([]);
  };

  useEffect(async () => {
    if (modalOpen) {
      const apiRoles = await RoleApi.getRoles(searchRoleName);
      setRoles(
        apiRoles
          .map((item) => ({
            id: item.id,
            name: item.name,
          }))
          .filter((role) => !existingRoleIds.includes(role.id))
      );
    }
  }, [modalOpen, searchRoleName]);

  const onAddButtonClicked = async () => {
    await AppApi.addRolesToApps(
      [appId],
      rolesSelected.map((role) => role.id)
    );
    await resetState();
    onAppUpdated();
    toggleModal();
  };

  return (
    <Modal isOpen={modalOpen} toggle={toggleModal} backdrop="static">
      <ModalHeader toggle={toggleModal}>
        <IntlMessages id="pages.add-roles-to-app-modal-title" />
      </ModalHeader>
      <ModalBody>
        <Label className="mt-4">
          <IntlMessages id="app.roles" />
        </Label>
        <Multiselect
          style={{ color: 'black' }}
          avoidHighlightFirstOption={true}
          options={roles}
          selectedValues={rolesSelected}
          onSearch={setSearchRoleName}
          onSelect={setRolesSelected}
          onRemove={setRolesSelected}
          displayValue="name"
        />
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" outline onClick={toggleModal}>
          <IntlMessages id="pages.cancel" />
        </Button>
        <Button color="primary" onClick={onAddButtonClicked}>
          <IntlMessages id="pages.add" />
        </Button>{' '}
      </ModalFooter>
    </Modal>
  );
};

export default injectIntl(AdRolesToAppModal);
