import React from 'react';
import { Card, CustomInput } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import classnames from 'classnames';
import { ContextMenuTrigger } from 'react-contextmenu';
import { Colxx } from 'components/common/CustomBootstrap';
import TooltipIcon from '../../../components/common/TooltipIcon';

const RoleListView = ({ role, isSelect, collect, onCheckItem, intl }) => {
  let immutabilityComponent;
  if (role.immutable) {
    immutabilityComponent = (
      <TooltipIcon
        icon="simple-icon-lock"
        item={{
          body: intl.formatMessage({ id: 'tip.immutable.role' }),
          placement: 'top',
        }}
        id={`role_${role.id}`}
      />
    );
  }
  return (
    <Colxx xxs="12" className="mb-3">
      <ContextMenuTrigger id="menu_id" data={role.id} collect={collect}>
        <Card
          onClick={(event) => onCheckItem(event, role.id)}
          className={classnames('d-flex flex-row', {
            active: isSelect,
          })}
        >
          <div className="pl-2 d-flex flex-grow-1 min-width-zero">
            <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
              <NavLink to={`/iam/ui/role/${role.id}`} className="w-40 w-sm-100">
                <p className="list-item-heading mb-1 truncate">{role.name}</p>
              </NavLink>

              {role.policyCount !== undefined && (
                <p className="mb-1 text-muted text-small w-15 w-sm-100">
                  Policies: {role.policyCount}
                </p>
              )}
              {role.description !== undefined && (
                <p className="mb-1 text-muted text-small w-15 w-sm-100">
                  {role.description}
                </p>
              )}
              {role.immutable !== undefined && (
                <div className="w-15 w-sm-100">{immutabilityComponent}</div>
              )}
            </div>
            <div className="custom-control custom-checkbox pl-1 align-self-center pr-4">
              <CustomInput
                className="item-check mb-0"
                type="checkbox"
                id={`check_${role.id}`}
                checked={isSelect}
                onChange={() => {}}
                label=""
              />
            </div>
          </div>
        </Card>
      </ContextMenuTrigger>
    </Colxx>
  );
};

/* React.memo detail : https://reactjs.org/docs/react-api.html#reactpurecomponent  */
export default React.memo(RoleListView);
