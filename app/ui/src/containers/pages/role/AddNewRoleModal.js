import { v4 as uuidv4 } from 'uuid';
import React, { useEffect, useState } from 'react';
import {
  Button,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from 'reactstrap';
import IntlMessages from 'helpers/IntlMessages';
import axios from 'axios';
import Multiselect from 'multiselect-react-dropdown';
import { injectIntl } from 'react-intl';
import ApiUtil from '../../../redux/api-util';
import RoleApi from '../../../api/role.api';
import PolicyApi from '../../../api/policy.api';
import AppApi from '../../../api/app.api';

const AddNewRoleModal = ({ modalOpen, toggleModal, intl, onCreateRole }) => {
  const [roleName, setRoleName] = useState('');
  const [roleDescription, setRoleDescription] = useState('');
  const [searchAppName, setSearchAppName] = useState('');
  const [searchPolicyName, setSearchPolicyName] = useState('');

  const initialAppState = {
    apps: [],
    appsSelected: [],
  };
  const initialPolicyState = {
    policies: [],
    policiesSelected: [],
  };
  const [appState, setAppState] = useState(initialAppState);
  const [policyState, setPolicyState] = useState(initialPolicyState);

  const resetState = () => {
    setRoleName('');
    setRoleDescription('');

    let initialAppStateToUse = {
      ...initialAppState,
    };
    if (searchAppName || searchAppName.trim() === '') {
      initialAppStateToUse = {
        ...initialAppStateToUse,
        apps: appState.apps,
      };
    }

    let initialPolicyStateToUse = {
      ...initialPolicyState,
    };
    if (searchPolicyName || searchPolicyName.trim() === '') {
      initialPolicyStateToUse = {
        ...initialPolicyStateToUse,
        policies: policyState.policies,
      };
    }

    setSearchAppName('');
    setSearchPolicyName('');

    setAppState(initialAppStateToUse);
    setPolicyState(initialPolicyStateToUse);
  };

  useEffect(() => {
    const fetchApps = async () => {
      const apiApps = await AppApi.getApps(searchAppName);
      setAppState({
        ...appState,
        apps: apiApps.map((item) => ({
          id: item.id,
          name: item.name,
        })),
      });
    };
    fetchApps().then();
  }, [searchAppName]);

  useEffect(async () => {
    const apiPolicies = await PolicyApi.getPolicies(searchPolicyName);

    setPolicyState({
      ...policyState,
      policies: apiPolicies.map((item) => ({
        id: item.id,
        name: item.name,
      })),
    });
  }, [searchPolicyName]);

  const createRole = async (roleId) => {
    await RoleApi.saveRole(
      roleId,
      roleName,
      roleDescription,
      appState.appsSelected.map((app) => app.id),
      policyState.policiesSelected.map((policy) => policy.id)
    );
  };

  const onCreateButtonClicked = async () => {
    const roleId = uuidv4();
    await createRole(roleId);
    await toggleModal();
    await onCreateRole();
    await resetState();
  };

  const setAppsSelected = (appsSelected) => {
    setAppState({
      ...appState,
      appsSelected,
    });
  };

  const setPoliciesSelected = (policiesSelected) => {
    setPolicyState({
      ...policyState,
      policiesSelected,
    });
  };

  return (
    <Modal
      isOpen={modalOpen}
      toggle={toggleModal}
      wrapClassName="modal-right"
      backdrop="static"
    >
      <ModalHeader toggle={toggleModal}>
        <IntlMessages id="pages.add-new-role-modal-title" />
      </ModalHeader>
      <ModalBody>
        <Label>
          <IntlMessages id="role.name" />
        </Label>
        <Input
          value={roleName}
          onChange={(event) => {
            setRoleName(event.target.value);
          }}
        />
        <Label>
          <IntlMessages id="role.description" />
        </Label>
        <Input
          type="textarea"
          value={roleDescription}
          onChange={(event) => {
            setRoleDescription(event.target.value);
          }}
        />

        <Label className="mt-4">
          <IntlMessages id="policy.policies" />
        </Label>
        <Multiselect
          style={{ color: 'black' }}
          avoidHighlightFirstOption={true}
          options={policyState.policies}
          selectedValues={policyState.policiesSelected}
          onSearch={setSearchPolicyName}
          onSelect={setPoliciesSelected}
          onRemove={setPoliciesSelected}
          displayValue="name"
        />

        <Label className="mt-4">
          <IntlMessages id="app.apps" />
        </Label>
        <Multiselect
          style={{ color: 'black' }}
          avoidHighlightFirstOption={true}
          options={appState.apps}
          selectedValues={appState.appsSelected}
          onSearch={setSearchAppName}
          onSelect={setAppsSelected}
          onRemove={setAppsSelected}
          displayValue="name"
        />
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" outline onClick={toggleModal}>
          <IntlMessages id="pages.cancel" />
        </Button>
        <Button color="primary" onClick={onCreateButtonClicked}>
          <IntlMessages id="pages.submit" />
        </Button>{' '}
      </ModalFooter>
    </Modal>
  );
};

export default injectIntl(AddNewRoleModal);
