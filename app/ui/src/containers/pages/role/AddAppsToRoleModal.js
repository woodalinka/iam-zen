import React, { useEffect, useState } from 'react';
import {
  Button,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from 'reactstrap';
import IntlMessages from 'helpers/IntlMessages';
import Multiselect from 'multiselect-react-dropdown';
import { injectIntl } from 'react-intl';
import AppApi from '../../../api/app.api';

const AddAppsToRoleModal = ({
  modalOpen,
  toggleModal,
  intl,
  onRoleUpdated,
  roleId,
  existingAppIds,
}) => {
  const [searchAppName, setSearchAppName] = useState('');
  const [apps, setApps] = useState([]);
  const [appsSelected, setAppsSelected] = useState([]);

  const resetState = () => {
    setSearchAppName('');
    setApps([]);
    setAppsSelected([]);
  };

  useEffect(async () => {
    if (modalOpen) {
      const apiApps = await AppApi.getApps(searchAppName);
      setApps(
        apiApps
          .map((item) => ({
            id: item.id,
            name: item.name,
          }))
          .filter((app) => !existingAppIds.includes(app.id))
      );
    }
  }, [modalOpen, searchAppName]);

  const onAddButtonClicked = async () => {
    await AppApi.addRolesToApps(
      appsSelected.map((app) => app.id),
      [roleId]
    );
    await resetState();
    onRoleUpdated();
    toggleModal();
  };

  return (
    <Modal isOpen={modalOpen} toggle={toggleModal} backdrop="static">
      <ModalHeader toggle={toggleModal}>
        <IntlMessages id="pages.add-apps-to-role-modal-title" />
      </ModalHeader>
      <ModalBody>
        <Label className="mt-4">
          <IntlMessages id="role.apps" />
        </Label>
        <Multiselect
          style={{ color: 'black' }}
          avoidHighlightFirstOption={true}
          options={apps}
          selectedValues={appsSelected}
          onSearch={setSearchAppName}
          onSelect={setAppsSelected}
          onRemove={setAppsSelected}
          displayValue="name"
        />
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" outline onClick={toggleModal}>
          <IntlMessages id="pages.cancel" />
        </Button>
        <Button color="primary" onClick={onAddButtonClicked}>
          <IntlMessages id="pages.add" />
        </Button>{' '}
      </ModalFooter>
    </Modal>
  );
};

export default injectIntl(AddAppsToRoleModal);
