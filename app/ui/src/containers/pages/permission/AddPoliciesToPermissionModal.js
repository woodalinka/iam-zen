import React, { useEffect, useState } from 'react';
import {
  Button,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from 'reactstrap';
import IntlMessages from 'helpers/IntlMessages';
import Multiselect from 'multiselect-react-dropdown';
import { injectIntl } from 'react-intl';
import PolicyApi from '../../../api/policy.api';

const AddPoliciesToPermissionModal = ({
  modalOpen,
  toggleModal,
  intl,
  onPermissionUpdated,
  permissionId,
  existingPolicyIds,
}) => {
  const [searchPolicyName, setSearchPolicyName] = useState('');
  const [policies, setPolicies] = useState([]);
  const [policiesSelected, setPoliciesSelected] = useState([]);

  const resetState = () => {
    setSearchPolicyName('');
    setPolicies([]);
    setPoliciesSelected([]);
  };

  useEffect(async () => {
    if (modalOpen) {
      const apiPolicies = await PolicyApi.getPolicies(searchPolicyName);
      setPolicies(
        apiPolicies
          .map((item) => ({
            id: item.id,
            name: item.name,
          }))
          .filter((policy) => !existingPolicyIds.includes(policy.id))
      );
    }
  }, [modalOpen, searchPolicyName]);

  const onAddButtonClicked = async () => {
    await PolicyApi.addPermissionsToPolicies(
      policiesSelected.map((policy) => policy.id),
      [permissionId]
    );
    await resetState();
    onPermissionUpdated();
    toggleModal();
  };

  return (
    <Modal isOpen={modalOpen} toggle={toggleModal} backdrop="static">
      <ModalHeader toggle={toggleModal}>
        <IntlMessages id="pages.add-policies-to-permission-modal-title" />
      </ModalHeader>
      <ModalBody>
        <Label className="mt-4">
          <IntlMessages id="permission.policies" />
        </Label>
        <Multiselect
          style={{ color: 'black' }}
          avoidHighlightFirstOption={true}
          options={policies}
          selectedValues={policiesSelected}
          onSearch={setSearchPolicyName}
          onSelect={setPoliciesSelected}
          onRemove={setPoliciesSelected}
          displayValue="name"
        />
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" outline onClick={toggleModal}>
          <IntlMessages id="pages.cancel" />
        </Button>
        <Button color="primary" onClick={onAddButtonClicked}>
          <IntlMessages id="pages.add" />
        </Button>{' '}
      </ModalFooter>
    </Modal>
  );
};

export default injectIntl(AddPoliciesToPermissionModal);
