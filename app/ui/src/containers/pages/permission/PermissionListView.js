import React from 'react';
import { Card, CustomInput } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import classnames from 'classnames';
import { ContextMenuTrigger } from 'react-contextmenu';
import { Colxx } from 'components/common/CustomBootstrap';
import TooltipIcon from '../../../components/common/TooltipIcon';

const PermissionListView = ({
  permission,
  isSelect,
  collect,
  onCheckItem,
  canEdit = true,
  intl,
}) => {
  let immutabilityComponent;
  if (permission.immutable) {
    immutabilityComponent = (
      <TooltipIcon
        icon="simple-icon-lock"
        item={{
          body: intl.formatMessage({ id: 'tip.immutable.permission' }),
          placement: 'top',
        }}
        id={`permission_${permission.id}`}
      />
    );
  }
  return (
    <Colxx xxs="12" className="mb-3">
      <ContextMenuTrigger id="menu_id" data={permission.id} collect={collect}>
        <Card
          onClick={(event) => onCheckItem(event, permission.id)}
          className={classnames('d-flex flex-row', {
            active: isSelect && canEdit,
          })}
        >
          <div
            className={classnames('pl-2 d-flex flex-grow-1 min-width-zero', {
              row: !canEdit,
            })}
          >
            <div
              className={classnames(
                'card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center',
                {
                  'col-lg-7': !canEdit,
                  'col-md-5': !canEdit,
                  'col-sm-12': !canEdit,
                }
              )}
            >
              <NavLink
                to={`/iam/ui/permission/${permission.id}`}
                className={classnames(undefined, {
                  'w-30': canEdit,
                  'w-50': !canEdit,
                  'w-sm-100': !canEdit,
                })}
              >
                <p className="list-item-heading mb-1 w-sm-100 truncate">
                  {permission.name}
                </p>
              </NavLink>
              <p className="list-item-heading text-muted mb-1">
                {permission.value}
              </p>
              <div className="d-flex flex-grow-1 justify-content-end align-items-end">
                <div className="w-sm-100 flex-grow-0">
                  {immutabilityComponent}
                </div>
              </div>
            </div>
            {permission.fromPolicies && (
              <div className="mt-2 mr-3 col-lg-2 col-md-3 col-sm-12">
                <span>From Policies: </span>
                <ul className="mr-3">
                  {permission.fromPolicies.map((policy) => {
                    return (
                      <li key={policy.id}>
                        <NavLink
                          to={`/iam/ui/policy/${policy.id}`}
                          className="w-10 w-sm-100"
                        >
                          {policy.name}
                        </NavLink>
                      </li>
                    );
                  })}
                </ul>
              </div>
            )}
            {permission.fromRoles && (
              <div className="mt-2 mr-3 col-lg-2 col-md-3 col-sm-12">
                <span>From Roles: </span>
                <ul className="mr-3">
                  {permission.fromRoles.map((role) => {
                    return (
                      <li key={role.id}>
                        <NavLink
                          to={`/iam/ui/role/${role.id}`}
                          className="w-10 w-sm-100"
                        >
                          {role.name}
                        </NavLink>
                      </li>
                    );
                  })}
                </ul>
              </div>
            )}
            {permission.fromGroups && (
              <div className="mt-2 mr-3 col-lg-2 col-md-3 col-sm-12">
                <span>From Groups: </span>
                <ul className="mr-3">
                  {permission.fromGroups.map((group) => {
                    return (
                      <li key={group.id}>
                        <NavLink
                          to={`/iam/ui/group/${group.id}`}
                          className="w-10 w-sm-100"
                        >
                          {group.name}
                        </NavLink>
                      </li>
                    );
                  })}
                </ul>
              </div>
            )}
            {canEdit && (
              <div className="custom-control custom-checkbox pl-1 align-self-center pr-4">
                <CustomInput
                  className="item-check mb-0"
                  type="checkbox"
                  id={`check_${permission.id}`}
                  checked={isSelect}
                  onChange={() => {}}
                  label=""
                />
              </div>
            )}
          </div>
        </Card>
      </ContextMenuTrigger>
    </Colxx>
  );
};

/* React.memo detail : https://reactjs.org/docs/react-api.html#reactpurecomponent  */
export default React.memo(PermissionListView);
