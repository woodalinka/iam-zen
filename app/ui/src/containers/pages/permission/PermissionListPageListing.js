import React from 'react';
import { Row, Spinner } from 'reactstrap';
import Pagination from '../Pagination';
import ContextMenuContainer from '../ContextMenuContainer';
import PermissionListView from './PermissionListView';

function collect(props) {
  return { data: props.data };
}

const PermissionListPageListing = ({
  items,
  selectedItems,
  onCheckItem,
  currentPage,
  totalPage,
  onContextMenuClick,
  onContextMenu,
  onChangePage,
  isLoading,
  intl,
}) => {
  if (!isLoading) {
    return (
      <Row>
        {items.map((permission) => {
          return (
            <PermissionListView
              key={permission.id}
              permission={permission}
              isSelect={selectedItems.includes(permission.id)}
              onCheckItem={onCheckItem}
              collect={collect}
              intl={intl}
            />
          );
        })}
        <Pagination
          currentPage={currentPage}
          totalPage={totalPage}
          onChangePage={(i) => onChangePage(i)}
          intl={intl}
        />
        <ContextMenuContainer
          onContextMenuClick={onContextMenuClick}
          onContextMenu={onContextMenu}
        />
      </Row>
    );
  }

  return (
    <div style={{ display: 'flex', justifyContent: 'center' }}>
      <Spinner color="primary" />;
    </div>
  );
};

export default React.memo(PermissionListPageListing);
