import React from 'react';
import { Row, Spinner } from 'reactstrap';
import Pagination from '../Pagination';
import ContextMenuContainer from '../ContextMenuContainer';
import GroupListView from './GroupListView';

function collect(props) {
  return { data: props.data };
}

const GroupListPageListing = ({
  items,
  selectedItems,
  onCheckItem,
  currentPage,
  totalPage,
  onContextMenuClick,
  onContextMenu,
  onChangePage,
  isLoading,
  intl,
}) => {
  if (!isLoading) {
    return (
      <Row>
        {items.map((group) => {
          return (
            <GroupListView
              key={group.id}
              group={group}
              isSelect={selectedItems.includes(group.id)}
              onCheckItem={onCheckItem}
              collect={collect}
              intl={intl}
            />
          );
        })}
        <Pagination
          currentPage={currentPage}
          totalPage={totalPage}
          onChangePage={(i) => onChangePage(i)}
        />
        <ContextMenuContainer
          onContextMenuClick={onContextMenuClick}
          onContextMenu={onContextMenu}
        />
      </Row>
    );
  }

  return (
    <div style={{ display: 'flex', justifyContent: 'center' }}>
      <Spinner color="primary" />;
    </div>
  );
};

export default React.memo(GroupListPageListing);
