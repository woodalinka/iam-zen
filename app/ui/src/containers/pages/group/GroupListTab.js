import React, { useState } from 'react';
import {
  Button,
  ButtonDropdown,
  CustomInput,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Row,
} from 'reactstrap';
import { Colxx, Separator } from 'components/common/CustomBootstrap';
import IntlMessages from '../../../helpers/IntlMessages';
import GroupListView from './GroupListView';
import ArrayUtil from '../../../util/array.util';
import UserApi from '../../../api/user.api';
import AddGroupsToUserModal from '../user/AddGroupsToUserModal';
import AddGroupsToPolicyModal from '../policy/AddGroupsToPolicyModal';
import GroupApi from '../../../api/group.api';

const GroupListTab = ({ groups, userId, policyId, intl, onChange }) => {
  const [lastCheckedGroup, setLastCheckedGroup] = useState(null);
  const [dropdownSplitOpen, setDropdownSplitOpen] = useState(false);
  const [selectedItems, setSelectedItems] = useState([]);
  const [addGroupsToUserModalOpen, setAddGroupsToUserModalOpen] =
    useState(false);
  const [addGroupsToPolicyModalOpen, setAddGroupsToPolicyOpen] =
    useState(false);

  const onGroupSelected = (event, id) => {
    if (
      event.target.tagName === 'A' ||
      (event.target.parentElement && event.target.parentElement.tagName === 'A')
    ) {
      return true;
    }
    if (lastCheckedGroup === null) {
      setLastCheckedGroup(id);
    }

    let selectedList = [...selectedItems];
    if (selectedList.includes(id)) {
      selectedList = selectedList.filter((x) => x !== id);
    } else {
      selectedList.push(id);
    }
    setSelectedItems(selectedList);

    if (event.shiftKey) {
      let newItems = [...groups];
      const start = ArrayUtil.getIndex(id, newItems, 'id');
      const end = ArrayUtil.getIndex(lastCheckedGroup, newItems, 'id');
      newItems = newItems.slice(Math.min(start, end), Math.max(start, end) + 1);
      selectedItems.push(
        ...newItems.map((item) => {
          return item.id;
        })
      );
      selectedList = Array.from(new Set(selectedItems));
      setSelectedItems(selectedList);
    }
    document.activeElement.blur();
    return false;
  };

  const handleChangeSelectAll = (isToggle) => {
    if (selectedItems.length >= groups.length) {
      if (isToggle) {
        setSelectedItems([]);
      }
    } else {
      setSelectedItems(groups.map((x) => x.id));
    }
    document.activeElement.blur();
    return false;
  };

  const onRemoveClicked = async () => {
    if (userId) {
      await UserApi.removeGroupsFromUsers([userId], selectedItems);
    }
    if (policyId) {
      await GroupApi.removePoliciesFromGroups(selectedItems, [policyId]);
    }

    await onChange();
  };

  return (
    <>
      {(userId || policyId) && (
        <Row>
          <Colxx xxs="12" className="mb-4">
            <div className="text-zero top-right-button-container">
              <Button
                color="primary"
                size="lg"
                className="top-right-button mr-1"
                onClick={() => {
                  if (userId) {
                    setAddGroupsToUserModalOpen(!addGroupsToUserModalOpen);
                  }
                  if (policyId) {
                    setAddGroupsToPolicyOpen(!addGroupsToPolicyModalOpen);
                  }
                }}
              >
                <IntlMessages id="pages.add" />
              </Button>
              <ButtonDropdown
                className="btn-group"
                isOpen={dropdownSplitOpen}
                toggle={() => setDropdownSplitOpen(!dropdownSplitOpen)}
              >
                <div className="btn btn-primary btn-lg pl-4 pr-0 check-button check-all">
                  <CustomInput
                    className="custom-checkbox mb-0 d-inline-block"
                    type="checkbox"
                    id="checkAll"
                    checked={selectedItems.length >= groups.length}
                    onChange={() => handleChangeSelectAll(true)}
                    label={
                      <span
                        className={`custom-control-label ${
                          selectedItems.length > 0 &&
                          selectedItems.length < groups.length
                            ? 'indeterminate'
                            : ''
                        }`}
                      />
                    }
                  />
                </div>
                <DropdownToggle
                  caret
                  color="primary"
                  className="dropdown-toggle-split btn-lg"
                />
                <DropdownMenu right>
                  <DropdownItem onClick={onRemoveClicked}>
                    <IntlMessages id="pages.remove" />
                  </DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
            </div>
          </Colxx>
        </Row>
      )}

      <Row>
        {groups.map((group) => {
          return (
            <GroupListView
              showDetails={false}
              isSelect={selectedItems.includes(group.id)}
              onCheckItem={onGroupSelected}
              intl={intl}
              key={group.id}
              group={group}
            />
          );
        })}
      </Row>
      {userId && (
        <AddGroupsToUserModal
          userId={userId}
          intl={intl}
          modalOpen={addGroupsToUserModalOpen}
          toggleModal={() =>
            setAddGroupsToUserModalOpen(!addGroupsToUserModalOpen)
          }
          onUserUpdated={onChange}
          existingGroupIds={groups.map((group) => group.id)}
        />
      )}
      {policyId && (
        <AddGroupsToPolicyModal
          policyId={policyId}
          intl={intl}
          modalOpen={addGroupsToPolicyModalOpen}
          toggleModal={() =>
            setAddGroupsToPolicyOpen(!addGroupsToPolicyModalOpen)
          }
          onPolicyUpdated={onChange}
          existingGroupIds={groups.map((group) => group.id)}
        />
      )}
    </>
  );
};

export default GroupListTab;
