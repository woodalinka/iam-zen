import React, { useEffect, useState } from 'react';
import {
  Button,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from 'reactstrap';
import IntlMessages from 'helpers/IntlMessages';
import Multiselect from 'multiselect-react-dropdown';
import { injectIntl } from 'react-intl';
import GroupApi from '../../../api/group.api';
import PolicyApi from '../../../api/policy.api';

const AddPoliciesToGroupModal = ({
  modalOpen,
  toggleModal,
  intl,
  onGroupUpdated,
  groupId,
  existingPolicyIds,
}) => {
  const [searchPolicyName, setSearchPolicyName] = useState('');
  const [policies, setPolicies] = useState([]);
  const [policiesSelected, setPoliciesSelected] = useState([]);

  const resetState = () => {
    setSearchPolicyName('');
    setPolicies([]);
    setPoliciesSelected([]);
  };

  useEffect(async () => {
    if (modalOpen) {
      const apiPolicies = await PolicyApi.getPolicies(searchPolicyName);
      setPolicies(
        apiPolicies
          .map((item) => ({
            id: item.id,
            name: item.name,
          }))
          .filter((group) => !existingPolicyIds.includes(group.id))
      );
    }
  }, [modalOpen, searchPolicyName]);

  const onAddButtonClicked = async () => {
    await GroupApi.addPoliciesToGroups(
      [groupId],
      policiesSelected.map((group) => group.id)
    );
    await resetState();
    onGroupUpdated();
    toggleModal();
  };

  return (
    <Modal isOpen={modalOpen} toggle={toggleModal} backdrop="static">
      <ModalHeader toggle={toggleModal}>
        <IntlMessages id="pages.add-policies-to-group-modal-title" />
      </ModalHeader>
      <ModalBody>
        <Label className="mt-4">
          <IntlMessages id="menu.policies" />
        </Label>
        <Multiselect
          style={{ color: 'black' }}
          avoidHighlightFirstOption={true}
          options={policies}
          selectedValues={policiesSelected}
          onSearch={setSearchPolicyName}
          onSelect={setPoliciesSelected}
          onRemove={setPoliciesSelected}
          displayValue="name"
        />
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" outline onClick={toggleModal}>
          <IntlMessages id="pages.cancel" />
        </Button>
        <Button color="primary" onClick={onAddButtonClicked}>
          <IntlMessages id="pages.add" />
        </Button>{' '}
      </ModalFooter>
    </Modal>
  );
};

export default injectIntl(AddPoliciesToGroupModal);
