export default class ArrayUtil {
  static getIndex(value, arr, prop) {
    for (let i = 0; i < arr.length; i += 1) {
      if (arr[i][prop] === value) {
        return i;
      }
    }
    return -1;
  }
}
