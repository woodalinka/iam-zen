import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

const Forms = React.lazy(() =>
  import(/* webpackChunkName: "ui-forms" */ './forms')
);

const UI = ({ match }) => (
  <Suspense fallback={<div className="loading" />}>
    <Switch>
      <Redirect exact from={`${match.url}/`} to={`${match.url}/forms`} />
      <Route
        path={`${match.url}/forms`}
        render={(props) => <Forms {...props} />}
      />

      <Redirect to="/error" />
    </Switch>
  </Suspense>
);
export default UI;
