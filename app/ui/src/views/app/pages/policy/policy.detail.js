import React, { useEffect, useState } from 'react';
import { injectIntl } from 'react-intl';
import { NavLink } from 'react-router-dom';
import { Nav, NavItem, Row, TabContent, TabPane } from 'reactstrap';
import classnames from 'classnames';
import PolicyApi from '../../../../api/policy.api';
import { Colxx } from '../../../../components/common/CustomBootstrap';
import IntlMessages from '../../../../helpers/IntlMessages';
import GroupListTab from '../../../../containers/pages/group/GroupListTab';
import RoleListTab from '../../../../containers/pages/role/RoleListTab';
import PermissionListTab from '../../../../containers/pages/permission/PermissionListTab';
import PolicyDetailPageHeading from '../../../../containers/pages/policy/PolicyDetailPageHeading';
import AppListTab from '../../../../containers/pages/app/AppListTab';
import UserListTab from '../../../../containers/pages/user/UserListTab';

const PolicyDetail = ({ intl, match }) => {
  const [isLoaded, setIsLoaded] = useState(false);
  const [policy, setPolicy] = useState({ groups: [], roles: [], apps: [] });

  async function fetchData() {
    const policyFromApi = await PolicyApi.getPolicy(match.params.policyId);
    setPolicy(policyFromApi);
    setIsLoaded(true);
  }

  useEffect(() => {
    fetchData().then();
  }, [policy.value]);

  const onPolicyUpdated = async () => {
    await new Promise((resolve) => setTimeout(resolve, 1000));
    fetchData().then();
  };

  return !isLoaded ? (
    <div className="loading" />
  ) : (
    <>
      <PolicyDetailPageHeading
        immutable={policy.immutable}
        match={match}
        policyName={policy.name}
        policyDescription={policy.description}
        intl={intl}
      />
      <Row>
        <Colxx xxs="12">
          <Nav tabs className="separator-tabs ml-0 mb-4">
            <NavItem>
              <NavLink
                location={{}}
                to={match.path
                  .replace(':tab', 'groups')
                  .replace(':policyId', match.params.policyId)}
                className={classnames({
                  active: match.params.tab === 'groups',
                  'nav-link': true,
                })}
              >
                <IntlMessages id="menu.groups" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                location={{}}
                to={match.path
                  .replace(':tab', 'users')
                  .replace(':policyId', match.params.policyId)}
                className={classnames({
                  active: match.params.tab === 'users',
                  'nav-link': true,
                })}
              >
                <IntlMessages id="menu.users" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                location={{}}
                to={match.path
                  .replace(':tab', 'roles')
                  .replace(':policyId', match.params.policyId)}
                className={classnames({
                  active: match.params.tab === 'roles',
                  'nav-link': true,
                })}
              >
                <IntlMessages id="menu.roles" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                location={{}}
                to={match.path
                  .replace(':tab', 'apps')
                  .replace(':policyId', match.params.policyId)}
                className={classnames({
                  active: match.params.tab === 'apps',
                  'nav-link': true,
                })}
              >
                <IntlMessages id="menu.apps" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                location={{}}
                to={match.path
                  .replace(':tab', 'permissions')
                  .replace(':policyId', match.params.policyId)}
                className={classnames({
                  active: match.params.tab === 'permissions',
                  'nav-link': true,
                })}
              >
                <IntlMessages id="menu.permissions" />
              </NavLink>
            </NavItem>
          </Nav>

          <TabContent activeTab={match.params.tab}>
            <TabPane tabId="groups">
              <GroupListTab
                policyId={match.params.policyId}
                intl={intl}
                groups={policy.groups}
                onChange={onPolicyUpdated}
              />
            </TabPane>
            <TabPane tabId="users">
              <UserListTab intl={intl} policyId={match.params.policyId} />
            </TabPane>
            <TabPane tabId="roles">
              <RoleListTab
                policyId={match.params.policyId}
                intl={intl}
                roles={policy.roles}
                onChange={onPolicyUpdated}
              />
            </TabPane>
            <TabPane tabId="apps">
              <AppListTab intl={intl} apps={[]} />
            </TabPane>
            <TabPane tabId="permissions">
              <PermissionListTab
                policyId={match.params.policyId}
                intl={intl}
                permissions={policy.permissions}
                onChange={onPolicyUpdated}
              />
            </TabPane>
          </TabContent>
        </Colxx>
      </Row>
    </>
  );
};

export default injectIntl(PolicyDetail);
