import React, { useEffect, useState } from 'react';

import axios from 'axios';
import { injectIntl } from 'react-intl';
import UserListPageHeading from 'containers/pages/user/UserListPageHeading';
import AddNewUserModal from 'containers/pages/user/AddNewUserModal';
import AddUsersToGroupsModal from 'containers/pages/user/AddUsersToGroupsModal';
import RemoveUsersFromGroupsModal from 'containers/pages/user/RemoveUsersFromGroupsModal';
import UserListPageListing from 'containers/pages/user/UserListPageListing';
import useMousetrap from 'hooks/use-mousetrap';
import ApiUtil from '../../../../redux/api-util';
import ArrayUtil from '../../../../util/array.util';

const orderOptions = [
  { column: 'username', label: 'Username' },
  { column: 'groupCount', label: 'Group Count' },
];
const pageSizes = [4, 8, 12, 20];

const categories = [];

const User = ({ match, intl }) => {
  const [isLoaded, setIsLoaded] = useState(false);
  const [displayMode, setDisplayMode] = useState('list');
  const [currentPage, setCurrentPage] = useState(1);
  const [selectedPageSize, setSelectedPageSize] = useState(8);
  const [dataIsFetching, setDataIsFetching] = useState(true);
  const [selectedOrderOption, setSelectedOrderOption] = useState({
    column: 'username',
    label: 'Username',
  });
  const [selectedOrderDirection, setSelectedOrderDirection] = useState('asc');

  const [addNewUserModalOpen, setAddNewUserModalOpen] = useState(false);
  const [addUsersToGroupsModalOpen, setAddUsersToGroupsModalOpen] =
    useState(false);
  const [removeUsersFromGroupsModalOpen, setRemoveUsersFromGroupsModalOpen] =
    useState(false);
  const [totalItemCount, setTotalItemCount] = useState(0);
  const [totalPage, setTotalPage] = useState(1);
  const [search, setSearch] = useState('');
  const [selectedUserIds, setSelectedUserIds] = useState([]);
  const [items, setItems] = useState([]);
  const [lastChecked, setLastChecked] = useState(null);

  useEffect(() => {
    setCurrentPage(1);
  }, [selectedPageSize, selectedOrderOption]);

  async function fetchData() {
    setDataIsFetching(true);
    const headers = await ApiUtil.getAuthorizedApiHeaders();
    axios
      .get(`/user`, {
        headers,
        baseURL: ApiUtil.apiBasePath,
        params: {
          pageSize: selectedPageSize,
          page: currentPage,
          orderBy: selectedOrderOption.column,
          orderDirection: selectedOrderDirection,
          search,
        },
      })
      .then((res) => {
        return res.data;
      })
      .then((message) => {
        console.log(message);
        setTotalPage(
          Math.ceil(message.meta.totalRecords / (selectedPageSize || 1))
        );
        setItems(
          message.data.map((x) => {
            return { ...x };
          })
        );
        setSelectedUserIds([]);
        setTotalItemCount(message.meta.totalRecords);
        setIsLoaded(true);
        setDataIsFetching(false);
        if (message.data.length === 0 && currentPage > 1) {
          setCurrentPage(currentPage - 1);
        }
      });
  }

  useEffect(() => {
    fetchData().then();
  }, [
    selectedPageSize,
    currentPage,
    selectedOrderOption,
    selectedOrderDirection,
    search,
  ]);

  const onCheckItem = (event, id) => {
    if (
      event.target.tagName === 'A' ||
      (event.target.parentElement && event.target.parentElement.tagName === 'A')
    ) {
      return true;
    }
    if (lastChecked === null) {
      setLastChecked(id);
    }

    let selectedList = [...selectedUserIds];
    if (selectedList.includes(id)) {
      selectedList = selectedList.filter((x) => x !== id);
    } else {
      selectedList.push(id);
    }
    setSelectedUserIds(selectedList);

    if (event.shiftKey) {
      let newItems = [...items];
      const start = ArrayUtil.getIndex(id, newItems, 'id');
      const end = ArrayUtil.getIndex(lastChecked, newItems, 'id');
      newItems = newItems.slice(Math.min(start, end), Math.max(start, end) + 1);
      selectedUserIds.push(
        ...newItems.map((item) => {
          return item.id;
        })
      );
      selectedList = Array.from(new Set(selectedUserIds));
      setSelectedUserIds(selectedList);
    }
    document.activeElement.blur();
    return false;
  };

  const handleChangeSelectAll = (isToggle) => {
    if (selectedUserIds.length >= items.length) {
      if (isToggle) {
        setSelectedUserIds([]);
      }
    } else {
      setSelectedUserIds(items.map((x) => x.id));
    }
    document.activeElement.blur();
    return false;
  };

  const onContextMenuClick = (e, data) => {
    console.log('onContextMenuClick - selected items', selectedUserIds);
    console.log('onContextMenuClick - action : ', data.action);
  };

  const onContextMenu = (e, data) => {
    const clickedProductId = data.data;
    if (!selectedUserIds.includes(clickedProductId)) {
      setSelectedUserIds([clickedProductId]);
    }

    return true;
  };

  useMousetrap(['ctrl+a', 'command+a'], () => {
    handleChangeSelectAll(false);
  });

  useMousetrap(['ctrl+d', 'command+d'], () => {
    setSelectedUserIds([]);
    return false;
  });

  const startIndex = (currentPage - 1) * selectedPageSize;
  const endIndex = currentPage * selectedPageSize;

  const onDataChanged = async () => {
    setDataIsFetching(true);
    await new Promise((resolve) => setTimeout(resolve, 1000));
    fetchData().then();
  };

  const onDeleteClicked = async () => {
    setDataIsFetching(true);
    const headers = await ApiUtil.getAuthorizedApiHeaders();

    await axios
      .delete(`/user`, {
        headers,
        baseURL: ApiUtil.apiBasePath,
        params: {
          ids: selectedUserIds,
        },
      })
      .catch();

    await new Promise((resolve) => setTimeout(resolve, 1000));
    fetchData().then();
  };

  return !isLoaded ? (
    <div className="loading" />
  ) : (
    <>
      <div className="disable-text-selection">
        <UserListPageHeading
          heading="menu.users"
          displayMode={displayMode}
          changeDisplayMode={setDisplayMode}
          handleChangeSelectAll={handleChangeSelectAll}
          changeOrderBy={(column) => {
            setSelectedOrderOption(
              orderOptions.find((x) => x.column === column)
            );
          }}
          changeOrderDirection={(direction) => {
            setSelectedOrderDirection(direction);
          }}
          selectedOrderDirection={selectedOrderDirection}
          changePageSize={setSelectedPageSize}
          selectedPageSize={selectedPageSize}
          totalItemCount={totalItemCount}
          selectedOrderOption={selectedOrderOption}
          match={match}
          startIndex={startIndex}
          endIndex={endIndex}
          selectedItemsLength={selectedUserIds ? selectedUserIds.length : 0}
          itemsLength={items ? items.length : 0}
          onSearchKey={(e) => {
            if (e.key === 'Enter') {
              setSearch(e.target.value.toLowerCase());
            }
          }}
          orderOptions={orderOptions}
          pageSizes={pageSizes}
          onDeleteClicked={onDeleteClicked}
          onAddGroups={() =>
            setAddUsersToGroupsModalOpen(!addUsersToGroupsModalOpen)
          }
          onRemoveGroups={() =>
            setRemoveUsersFromGroupsModalOpen(!removeUsersFromGroupsModalOpen)
          }
          toggleModal={() => setAddNewUserModalOpen(!addNewUserModalOpen)}
        />
        <AddNewUserModal
          modalOpen={addNewUserModalOpen}
          toggleModal={() => setAddNewUserModalOpen(!addNewUserModalOpen)}
          categories={categories}
          onCreateUser={onDataChanged}
        />
        <AddUsersToGroupsModal
          modalOpen={addUsersToGroupsModalOpen}
          toggleModal={() =>
            setAddUsersToGroupsModalOpen(!addUsersToGroupsModalOpen)
          }
          selectedUserIds={selectedUserIds}
          onGroupsUpdated={onDataChanged}
        />
        <RemoveUsersFromGroupsModal
          modalOpen={removeUsersFromGroupsModalOpen}
          toggleModal={() =>
            setRemoveUsersFromGroupsModalOpen(!removeUsersFromGroupsModalOpen)
          }
          selectedUserIds={selectedUserIds}
          onGroupsUpdated={onDataChanged}
        />
        <UserListPageListing
          items={items}
          displayMode={displayMode}
          selectedItems={selectedUserIds}
          onCheckItem={onCheckItem}
          currentPage={currentPage}
          totalPage={totalPage}
          onContextMenuClick={onContextMenuClick}
          onContextMenu={onContextMenu}
          onChangePage={setCurrentPage}
          isLoading={dataIsFetching}
          intl={intl}
        />
      </div>
    </>
  );
};

export default injectIntl(User);
