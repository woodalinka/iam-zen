import React, { useEffect, useState } from 'react';
import { injectIntl } from 'react-intl';
import { NavLink } from 'react-router-dom';
import { Nav, NavItem, Row, TabContent, TabPane } from 'reactstrap';
import classnames from 'classnames';
import UserDetailPageHeading from '../../../../containers/pages/user/UserDetailPageHeading';
import UserApi from '../../../../api/user.api';
import { Colxx } from '../../../../components/common/CustomBootstrap';
import IntlMessages from '../../../../helpers/IntlMessages';
import GroupListTab from '../../../../containers/pages/group/GroupListTab';
import PolicyListTab from '../../../../containers/pages/policy/PolicyListTab';
import PermissionListTab from '../../../../containers/pages/permission/PermissionListTab';

const UserDetail = ({ intl, match }) => {
  const [isLoaded, setIsLoaded] = useState(false);
  const [user, setUser] = useState({ groups: [], policies: [] });

  async function fetchData() {
    const userFromApi = await UserApi.getUser(match.params.userId);
    setUser(userFromApi);
    setIsLoaded(true);
  }

  useEffect(() => {
    fetchData().then();
  }, [user.value]);

  const onUserUpdated = async () => {
    await new Promise((resolve) => setTimeout(resolve, 1000));
    fetchData().then();
  };

  return !isLoaded ? (
    <div className="loading" />
  ) : (
    <>
      <UserDetailPageHeading
        immutable={user.immutable}
        match={match}
        userName={user.username}
        intl={intl}
      />
      <Row>
        <Colxx xxs="12">
          <Nav tabs className="separator-tabs ml-0 mb-4">
            <NavItem>
              <NavLink
                location={{}}
                to={match.path
                  .replace(':tab', 'groups')
                  .replace(':userId', match.params.userId)}
                className={classnames({
                  active: match.params.tab === 'groups',
                  'nav-link': true,
                })}
              >
                <IntlMessages id="menu.groups" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                location={{}}
                to={match.path
                  .replace(':tab', 'policies')
                  .replace(':userId', match.params.userId)}
                className={classnames({
                  active: match.params.tab === 'policies',
                  'nav-link': true,
                })}
              >
                <IntlMessages id="menu.policies" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                location={{}}
                to={match.path
                  .replace(':tab', 'permissions')
                  .replace(':userId', match.params.userId)}
                className={classnames({
                  active: match.params.tab === 'permissions',
                  'nav-link': true,
                })}
              >
                <IntlMessages id="menu.permissions" />
              </NavLink>
            </NavItem>
          </Nav>

          <TabContent activeTab={match.params.tab}>
            <TabPane tabId="groups">
              <GroupListTab
                userId={match.params.userId}
                intl={intl}
                groups={user.groups}
                onChange={onUserUpdated}
              />
            </TabPane>
            <TabPane tabId="policies">
              <PolicyListTab intl={intl} policies={user.policies} />
            </TabPane>
            <TabPane tabId="permissions">
              <PermissionListTab intl={intl} permissions={user.permissions} />
            </TabPane>
          </TabContent>
        </Colxx>
      </Row>
    </>
  );
};

export default injectIntl(UserDetail);
