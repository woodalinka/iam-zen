import React, { useEffect, useState } from 'react';
import { injectIntl } from 'react-intl';
import { NavLink } from 'react-router-dom';
import { Nav, NavItem, Row, TabContent, TabPane } from 'reactstrap';
import classnames from 'classnames';
import RoleDetailPageHeading from '../../../../containers/pages/role/RoleDetailPageHeading';
import RoleApi from '../../../../api/role.api';
import { Colxx } from '../../../../components/common/CustomBootstrap';
import IntlMessages from '../../../../helpers/IntlMessages';
import PolicyListTab from '../../../../containers/pages/policy/PolicyListTab';
import PermissionListTab from '../../../../containers/pages/permission/PermissionListTab';
import AppListTab from '../../../../containers/pages/app/AppListTab';

const RoleDetail = ({ intl, match }) => {
  const [activeTab, setActiveTab] = useState(match.params.tab);
  const [isLoaded, setIsLoaded] = useState(false);
  const [role, setRole] = useState({ roles: [], policies: [] });

  async function fetchData() {
    const roleFromApi = await RoleApi.getRole(match.params.roleId);
    setRole(roleFromApi);
    setIsLoaded(true);
  }

  useEffect(() => {
    fetchData().then();
  }, [role.value]);

  const onRoleUpdated = async () => {
    await new Promise((resolve) => setTimeout(resolve, 1000));
    fetchData().then();
  };

  return !isLoaded ? (
    <div className="loading" />
  ) : (
    <>
      <RoleDetailPageHeading
        immutable={role.immutable}
        match={match}
        roleName={role.name}
        roleDescription={role.description}
        intl={intl}
      />
      <Row>
        <Colxx xxs="12">
          <Nav tabs className="separator-tabs ml-0 mb-4">
            <NavItem>
              <NavLink
                location={{}}
                to={match.path
                  .replace(':tab', 'apps')
                  .replace(':roleId', match.params.roleId)}
                className={classnames({
                  active: match.params.tab === 'apps',
                  'nav-link': true,
                })}
              >
                <IntlMessages id="menu.apps" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                location={{}}
                to={match.path
                  .replace(':tab', 'policies')
                  .replace(':roleId', match.params.roleId)}
                className={classnames({
                  active: match.params.tab === 'policies',
                  'nav-link': true,
                })}
              >
                <IntlMessages id="menu.policies" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                location={{}}
                to={match.path
                  .replace(':tab', 'permissions')
                  .replace(':roleId', match.params.roleId)}
                className={classnames({
                  active: match.params.tab === 'permissions',
                  'nav-link': true,
                })}
              >
                <IntlMessages id="menu.permissions" />
              </NavLink>
            </NavItem>
          </Nav>

          <TabContent activeTab={match.params.tab}>
            <TabPane tabId="apps">
              <AppListTab
                roleId={match.params.roleId}
                intl={intl}
                apps={role.apps}
                onChange={onRoleUpdated}
              />
            </TabPane>
            <TabPane tabId="policies">
              <PolicyListTab
                onChange={onRoleUpdated}
                intl={intl}
                policies={role.policies}
                roleId={match.params.roleId}
              />
            </TabPane>
            <TabPane tabId="permissions">
              <PermissionListTab intl={intl} permissions={role.permissions} />
            </TabPane>
          </TabContent>
        </Colxx>
      </Row>
    </>
  );
};

export default injectIntl(RoleDetail);
