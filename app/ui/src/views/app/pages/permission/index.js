import React, { useEffect, useState } from 'react';

import axios from 'axios';
import { injectIntl } from 'react-intl';
import PermissionListPageHeading from 'containers/pages/permission/PermissionListPageHeading';
import PermissionListPageListing from 'containers/pages/permission/PermissionListPageListing';
import useMousetrap from 'hooks/use-mousetrap';
import AddNewPermissionModal from '../../../../containers/pages/permission/AddNewPermissionModal';
import ApiUtil from '../../../../redux/api-util';
import ArrayUtil from '../../../../util/array.util';
import PermissionApi from '../../../../api/permission.api';

const orderOptions = [
  { column: 'name', label: 'Name' },
  { column: 'value', label: 'Value' },
];
const pageSizes = [4, 8, 12, 20];

const categories = [];

const Permission = ({ match, intl }) => {
  const [isLoaded, setIsLoaded] = useState(false);
  const [displayMode, setDisplayMode] = useState('list');
  const [currentPage, setCurrentPage] = useState(1);
  const [selectedPageSize, setSelectedPageSize] = useState(8);
  const [dataIsFetching, setDataIsFetching] = useState(true);
  const [selectedOrderOption, setSelectedOrderOption] = useState({
    column: 'name',
    label: 'Name',
  });
  const [selectedOrderDirection, setSelectedOrderDirection] = useState('asc');

  const [modalOpen, setModalOpen] = useState(false);
  const [totalItemCount, setTotalItemCount] = useState(0);
  const [totalPage, setTotalPage] = useState(1);
  const [search, setSearch] = useState('');
  const [selectedItems, setSelectedItems] = useState([]);
  const [items, setItems] = useState([]);
  const [lastChecked, setLastChecked] = useState(null);

  useEffect(() => {
    setCurrentPage(1);
  }, [selectedPageSize, selectedOrderOption]);

  async function fetchData() {
    setDataIsFetching(true);
    const headers = await ApiUtil.getAuthorizedApiHeaders();
    axios
      .get(`/permission`, {
        headers,
        baseURL: ApiUtil.apiBasePath,
        params: {
          pageSize: selectedPageSize,
          page: currentPage,
          orderBy: selectedOrderOption.column,
          orderDirection: selectedOrderDirection,
          search,
        },
      })
      .then((res) => {
        return res.data;
      })
      .then((message) => {
        console.log(message);
        setTotalPage(
          Math.ceil(message.meta.totalRecords / (selectedPageSize || 1))
        );
        setItems(
          message.data.map((x) => {
            return { ...x };
          })
        );
        setSelectedItems([]);
        setTotalItemCount(message.meta.totalRecords);
        setIsLoaded(true);
        setDataIsFetching(false);
        if (message.data.length === 0 && currentPage > 1) {
          setCurrentPage(currentPage - 1);
        }
      });
  }

  useEffect(() => {
    fetchData().then();
  }, [
    selectedPageSize,
    currentPage,
    selectedOrderOption,
    selectedOrderDirection,
    search,
  ]);

  const onCheckItem = (event, id) => {
    if (
      event.target.tagName === 'A' ||
      (event.target.parentElement && event.target.parentElement.tagName === 'A')
    ) {
      return true;
    }
    if (lastChecked === null) {
      setLastChecked(id);
    }

    let selectedList = [...selectedItems];
    if (selectedList.includes(id)) {
      selectedList = selectedList.filter((x) => x !== id);
    } else {
      selectedList.push(id);
    }
    setSelectedItems(selectedList);

    if (event.shiftKey) {
      let newItems = [...items];
      const start = ArrayUtil.getIndex(id, newItems, 'id');
      const end = ArrayUtil.getIndex(lastChecked, newItems, 'id');
      newItems = newItems.slice(Math.min(start, end), Math.max(start, end) + 1);
      selectedItems.push(
        ...newItems.map((item) => {
          return item.id;
        })
      );
      selectedList = Array.from(new Set(selectedItems));
      setSelectedItems(selectedList);
    }
    document.activeElement.blur();
    return false;
  };

  const handleChangeSelectAll = (isToggle) => {
    if (selectedItems.length >= items.length) {
      if (isToggle) {
        setSelectedItems([]);
      }
    } else {
      setSelectedItems(items.map((x) => x.id));
    }
    document.activeElement.blur();
    return false;
  };

  const onContextMenuClick = (e, data) => {
    console.log('onContextMenuClick - selected items', selectedItems);
    console.log('onContextMenuClick - action : ', data.action);
  };

  const onContextMenu = (e, data) => {
    const clickedProductId = data.data;
    if (!selectedItems.includes(clickedProductId)) {
      setSelectedItems([clickedProductId]);
    }

    return true;
  };

  useMousetrap(['ctrl+a', 'command+a'], () => {
    handleChangeSelectAll(false);
  });

  useMousetrap(['ctrl+d', 'command+d'], () => {
    setSelectedItems([]);
    return false;
  });

  const startIndex = (currentPage - 1) * selectedPageSize;
  const endIndex = currentPage * selectedPageSize;

  const onDataChanged = async () => {
    setDataIsFetching(true);
    await new Promise((resolve) => setTimeout(resolve, 1000));
    fetchData().then();
  };

  const onDeleteClicked = async () => {
    setDataIsFetching(true);

    await PermissionApi.deletePermissions(selectedItems);

    await new Promise((resolve) => setTimeout(resolve, 1000));
    fetchData().then();
  };

  return !isLoaded ? (
    <div className="loading" />
  ) : (
    <>
      <div className="disable-text-selection">
        <PermissionListPageHeading
          heading="menu.permissions"
          displayMode={displayMode}
          changeDisplayMode={setDisplayMode}
          handleChangeSelectAll={handleChangeSelectAll}
          changeOrderBy={(column) => {
            setSelectedOrderOption(
              orderOptions.find((x) => x.column === column)
            );
          }}
          changeOrderDirection={(direction) => {
            setSelectedOrderDirection(direction);
          }}
          selectedOrderDirection={selectedOrderDirection}
          changePageSize={setSelectedPageSize}
          selectedPageSize={selectedPageSize}
          totalItemCount={totalItemCount}
          selectedOrderOption={selectedOrderOption}
          match={match}
          startIndex={startIndex}
          endIndex={endIndex}
          selectedItemsLength={selectedItems ? selectedItems.length : 0}
          itemsLength={items ? items.length : 0}
          onSearchKey={(e) => {
            if (e.key === 'Enter') {
              setSearch(e.target.value.toLowerCase());
            }
          }}
          orderOptions={orderOptions}
          pageSizes={pageSizes}
          onDeleteClicked={onDeleteClicked}
          toggleModal={() => setModalOpen(!modalOpen)}
        />
        <AddNewPermissionModal
          modalOpen={modalOpen}
          toggleModal={() => setModalOpen(!modalOpen)}
          categories={categories}
          onCreatePermission={onDataChanged}
        />
        <PermissionListPageListing
          items={items}
          displayMode={displayMode}
          selectedItems={selectedItems}
          onCheckItem={onCheckItem}
          currentPage={currentPage}
          totalPage={totalPage}
          onContextMenuClick={onContextMenuClick}
          onContextMenu={onContextMenu}
          onChangePage={setCurrentPage}
          isLoading={dataIsFetching}
          intl={intl}
        />
      </div>
    </>
  );
};

export default injectIntl(Permission);
