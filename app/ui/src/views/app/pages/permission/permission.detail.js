import React, { useEffect, useState } from 'react';
import { injectIntl } from 'react-intl';
import { NavLink } from 'react-router-dom';
import { Nav, NavItem, Row, TabContent, TabPane } from 'reactstrap';
import classnames from 'classnames';
import PermissionDetailPageHeading from '../../../../containers/pages/permission/PermissionDetailPageHeading';
import PermissionApi from '../../../../api/permission.api';
import { Colxx } from '../../../../components/common/CustomBootstrap';
import IntlMessages from '../../../../helpers/IntlMessages';
import RoleListTab from '../../../../containers/pages/role/RoleListTab';
import GroupListTab from '../../../../containers/pages/group/GroupListTab';
import AppListTab from '../../../../containers/pages/app/AppListTab';
import PolicyListTab from '../../../../containers/pages/policy/PolicyListTab';
import UserListTab from '../../../../containers/pages/user/UserListTab';

const PermissionDetail = ({ intl, match }) => {
  const [isLoaded, setIsLoaded] = useState(false);
  const [permission, setPermission] = useState({
    policies: [],
    roles: [],
    groups: [],
    apps: [],
  });

  async function fetchData() {
    const permissionFromApi = await PermissionApi.getPermission(
      match.params.permissionId
    );
    setPermission(permissionFromApi);
    setIsLoaded(true);
  }

  useEffect(() => {
    fetchData().then();
  }, [permission.value]);

  const onPermissionUpdated = async () => {
    await new Promise((resolve) => setTimeout(resolve, 1000));
    fetchData().then();
  };

  return !isLoaded ? (
    <div className="loading" />
  ) : (
    <>
      <PermissionDetailPageHeading
        immutable={permission.immutable}
        match={match}
        permissionName={permission.name}
        permissionDescription={permission.description}
        permissionValue={permission.value}
        intl={intl}
      />
      <Row>
        <Colxx xxs="12">
          <Nav tabs className="separator-tabs ml-0 mb-4">
            <NavItem>
              <NavLink
                location={{}}
                to={match.path
                  .replace(':tab', 'policies')
                  .replace(':permissionId', match.params.permissionId)}
                className={classnames({
                  active: match.params.tab === 'policies',
                  'nav-link': true,
                })}
              >
                <IntlMessages id="menu.policies" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                location={{}}
                to={match.path
                  .replace(':tab', 'roles')
                  .replace(':permissionId', match.params.permissionId)}
                className={classnames({
                  active: match.params.tab === 'roles',
                  'nav-link': true,
                })}
              >
                <IntlMessages id="menu.roles" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                location={{}}
                to={match.path
                  .replace(':tab', 'apps')
                  .replace(':permissionId', match.params.permissionId)}
                className={classnames({
                  active: match.params.tab === 'apps',
                  'nav-link': true,
                })}
              >
                <IntlMessages id="menu.apps" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                location={{}}
                to={match.path
                  .replace(':tab', 'groups')
                  .replace(':permissionId', match.params.permissionId)}
                className={classnames({
                  active: match.params.tab === 'groups',
                  'nav-link': true,
                })}
              >
                <IntlMessages id="menu.groups" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                location={{}}
                to={match.path
                  .replace(':tab', 'users')
                  .replace(':permissionId', match.params.permissionId)}
                className={classnames({
                  active: match.params.tab === 'users',
                  'nav-link': true,
                })}
              >
                <IntlMessages id="menu.users" />
              </NavLink>
            </NavItem>
          </Nav>

          <TabContent activeTab={match.params.tab}>
            <TabPane tabId="policies">
              <PolicyListTab
                intl={intl}
                permissionId={match.params.permissionId}
                policies={permission.policies}
                onChange={onPermissionUpdated}
              />
            </TabPane>
            <TabPane tabId="roles">
              <RoleListTab intl={intl} roles={permission.roles} />
            </TabPane>
            <TabPane tabId="apps">
              <AppListTab intl={intl} apps={permission.apps} />
            </TabPane>
            <TabPane tabId="groups">
              <GroupListTab intl={intl} groups={permission.groups} />
            </TabPane>
            <TabPane tabId="users">
              <UserListTab
                intl={intl}
                permissionId={match.params.permissionId}
              />
            </TabPane>
          </TabContent>
        </Colxx>
      </Row>
    </>
  );
};

export default injectIntl(PermissionDetail);
