import React, { useEffect, useState } from 'react';

import axios from 'axios';
import { injectIntl } from 'react-intl';
import AppListPageHeading from 'containers/pages/app/AppListPageHeading';
import AppListPageListing from 'containers/pages/app/AppListPageListing';
import useMousetrap from 'hooks/use-mousetrap';
import ApiUtil from '../../../../redux/api-util';
import AddNewAppModal from '../../../../containers/pages/app/AddNewAppModal';
import AppApi from '../../../../api/app.api';
import ArrayUtil from '../../../../util/array.util';

const orderOptions = [{ column: 'name', label: 'Name' }];
const pageSizes = [4, 8, 12, 20];

const categories = [];

const App = ({ match, intl }) => {
  const [isLoaded, setIsLoaded] = useState(false);
  const [displayMode, setDisplayMode] = useState('list');
  const [currentPage, setCurrentPage] = useState(1);
  const [dataIsFetching, setDataIsFetching] = useState(true);
  const [selectedPageSize, setSelectedPageSize] = useState(8);
  const [selectedOrderOption, setSelectedOrderOption] = useState({
    column: 'name',
    label: 'Name',
  });
  const [selectedOrderDirection, setSelectedOrderDirection] = useState('asc');

  const [modalOpen, setModalOpen] = useState(false);
  const [totalItemCount, setTotalItemCount] = useState(0);
  const [totalPage, setTotalPage] = useState(1);
  const [search, setSearch] = useState('');
  const [selectedItems, setSelectedItems] = useState([]);
  const [items, setItems] = useState([]);
  const [lastChecked, setLastChecked] = useState(null);

  useEffect(() => {
    setCurrentPage(1);
  }, [selectedPageSize, selectedOrderOption]);

  async function fetchData() {
    setDataIsFetching(true);

    const apiPaginatedAppsMessage = await AppApi.getPaginatedApps(
      search,
      currentPage,
      selectedPageSize,
      selectedOrderOption.column,
      selectedOrderDirection
    );

    setTotalPage(
      Math.ceil(
        apiPaginatedAppsMessage.meta.totalRecords / (selectedPageSize || 1)
      )
    );
    setItems(
      apiPaginatedAppsMessage.data.map((x) => {
        return { ...x };
      })
    );
    setSelectedItems([]);
    setTotalItemCount(apiPaginatedAppsMessage.meta.totalRecords);
    setIsLoaded(true);
    setDataIsFetching(false);
    if (apiPaginatedAppsMessage.data.length === 0 && currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  }

  useEffect(() => {
    fetchData();
  }, [
    selectedPageSize,
    currentPage,
    selectedOrderOption,
    selectedOrderDirection,
    search,
  ]);

  const onCheckItem = (event, id) => {
    if (
      event.target.tagName === 'A' ||
      (event.target.parentElement && event.target.parentElement.tagName === 'A')
    ) {
      return true;
    }
    if (lastChecked === null) {
      setLastChecked(id);
    }

    let selectedIdList = [...selectedItems];
    if (selectedIdList.includes(id)) {
      selectedIdList = selectedIdList.filter((x) => x !== id);
    } else {
      selectedIdList.push(id);
    }
    setSelectedItems(selectedIdList);

    if (event.shiftKey) {
      let newItems = [...items];
      const start = ArrayUtil.getIndex(id, newItems, 'id');
      const end = ArrayUtil.getIndex(lastChecked, newItems, 'id');
      newItems = newItems.slice(Math.min(start, end), Math.max(start, end) + 1);
      selectedItems.push(
        ...newItems.map((item) => {
          return item.id;
        })
      );
      selectedIdList = Array.from(new Set(selectedItems));
      setSelectedItems(selectedIdList);
    }
    document.activeElement.blur();
    return false;
  };

  const handleChangeSelectAll = (isToggle) => {
    if (selectedItems.length >= items.length) {
      if (isToggle) {
        setSelectedItems([]);
      }
    } else {
      setSelectedItems(items.map((x) => x.id));
    }
    document.activeElement.blur();
    return false;
  };

  const onContextMenu = (e, data) => {
    const clickedProductId = data.data;
    if (!selectedItems.includes(clickedProductId)) {
      setSelectedItems([clickedProductId]);
    }

    return true;
  };

  useMousetrap(['ctrl+a', 'command+a'], () => {
    handleChangeSelectAll(false);
  });

  useMousetrap(['ctrl+d', 'command+d'], () => {
    setSelectedItems([]);
    return false;
  });

  const onDataChanged = async () => {
    setDataIsFetching(true);
    await new Promise((resolve) => setTimeout(resolve, 1000));
    fetchData().then();
  };

  const onDeleteClicked = async () => {
    setDataIsFetching(true);
    try {
      await AppApi.deleteApp(selectedItems);
      await new Promise((resolve) => setTimeout(resolve, 1000));
    } catch (e) {}
    fetchData().then();
  };

  const startIndex = (currentPage - 1) * selectedPageSize;
  const endIndex = currentPage * selectedPageSize;

  return !isLoaded ? (
    <div className="loading" />
  ) : (
    <>
      <div className="disable-text-selection">
        <AppListPageHeading
          heading="menu.apps"
          displayMode={displayMode}
          changeDisplayMode={setDisplayMode}
          handleChangeSelectAll={handleChangeSelectAll}
          changeOrderBy={(column) => {
            setSelectedOrderOption(
              orderOptions.find((x) => x.column === column)
            );
          }}
          changeOrderDirection={(direction) => {
            setSelectedOrderDirection(direction);
          }}
          selectedOrderDirection={selectedOrderDirection}
          changePageSize={setSelectedPageSize}
          selectedPageSize={selectedPageSize}
          totalItemCount={totalItemCount}
          selectedOrderOption={selectedOrderOption}
          match={match}
          startIndex={startIndex}
          endIndex={endIndex}
          selectedItemsLength={selectedItems ? selectedItems.length : 0}
          itemsLength={items ? items.length : 0}
          onSearchKey={(e) => {
            if (e.key === 'Enter') {
              setSearch(e.target.value.toLowerCase());
            }
          }}
          orderOptions={orderOptions}
          pageSizes={pageSizes}
          toggleModal={() => setModalOpen(!modalOpen)}
          onDeleteClicked={onDeleteClicked}
        />
        <AddNewAppModal
          modalOpen={modalOpen}
          toggleModal={() => setModalOpen(!modalOpen)}
          categories={categories}
          onCreateApp={onDataChanged}
        />
        <AppListPageListing
          items={items}
          displayMode={displayMode}
          selectedItems={selectedItems}
          onCheckItem={onCheckItem}
          currentPage={currentPage}
          totalPage={totalPage}
          onContextMenu={onContextMenu}
          onChangePage={setCurrentPage}
          intl={intl}
          isLoading={dataIsFetching}
        />
      </div>
    </>
  );
};

export default injectIntl(App);
