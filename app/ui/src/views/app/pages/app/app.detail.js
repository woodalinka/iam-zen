import React, { useEffect, useState } from 'react';
import { injectIntl } from 'react-intl';
import { NavLink } from 'react-router-dom';
import { Nav, NavItem, Row, TabContent, TabPane } from 'reactstrap';
import classnames from 'classnames';
import AppDetailPageHeading from '../../../../containers/pages/app/AppDetailPageHeading';
import AppApi from '../../../../api/app.api';
import { Colxx } from '../../../../components/common/CustomBootstrap';
import IntlMessages from '../../../../helpers/IntlMessages';
import RoleListTab from '../../../../containers/pages/role/RoleListTab';
import PolicyListTab from '../../../../containers/pages/policy/PolicyListTab';
import PermissionListTab from '../../../../containers/pages/permission/PermissionListTab';

const AppDetail = ({ intl, match }) => {
  const [isLoaded, setIsLoaded] = useState(false);
  const [app, setApp] = useState({ roles: [], policies: [] });

  async function fetchData() {
    const appFromApi = await AppApi.getApp(match.params.appId);
    setApp(appFromApi);
    setIsLoaded(true);
  }

  useEffect(() => {
    fetchData().then();
  }, [app.value]);

  const onAppUpdated = async () => {
    await new Promise((resolve) => setTimeout(resolve, 1000));
    fetchData().then();
  };

  return !isLoaded ? (
    <div className="loading" />
  ) : (
    <>
      <AppDetailPageHeading
        immutable={app.immutable}
        match={match}
        appName={app.name}
        appDescription={app.description}
        intl={intl}
      />
      <Row>
        <Colxx xxs="12">
          <Nav tabs className="separator-tabs ml-0 mb-4">
            <NavItem>
              <NavLink
                location={{}}
                to={match.path
                  .replace(':tab', 'roles')
                  .replace(':appId', match.params.appId)}
                className={classnames({
                  active: match.params.tab === 'roles',
                  'nav-link': true,
                })}
              >
                <IntlMessages id="menu.roles" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                location={{}}
                to={match.path
                  .replace(':tab', 'policies')
                  .replace(':appId', match.params.appId)}
                className={classnames({
                  active: match.params.tab === 'policies',
                  'nav-link': true,
                })}
              >
                <IntlMessages id="menu.policies" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                location={{}}
                to={match.path
                  .replace(':tab', 'permissions')
                  .replace(':appId', match.params.appId)}
                className={classnames({
                  active: match.params.tab === 'permissions',
                  'nav-link': true,
                })}
              >
                <IntlMessages id="menu.permissions" />
              </NavLink>
            </NavItem>
          </Nav>

          <TabContent activeTab={match.params.tab}>
            <TabPane tabId="roles">
              <RoleListTab
                appId={match.params.appId}
                intl={intl}
                roles={app.roles}
                onChange={onAppUpdated}
              />
            </TabPane>
            <TabPane tabId="policies">
              <PolicyListTab intl={intl} policies={app.policies} />
            </TabPane>
            <TabPane tabId="permissions">
              <PermissionListTab intl={intl} permissions={app.permissions} />
            </TabPane>
          </TabContent>
        </Colxx>
      </Row>
    </>
  );
};

export default injectIntl(AppDetail);
