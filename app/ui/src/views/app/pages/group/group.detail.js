import React, { useEffect, useState } from 'react';
import { injectIntl } from 'react-intl';
import { NavLink } from 'react-router-dom';
import { Nav, NavItem, Row, TabContent, TabPane } from 'reactstrap';
import classnames from 'classnames';
import GroupDetailPageHeading from '../../../../containers/pages/group/GroupDetailPageHeading';
import GroupApi from '../../../../api/group.api';
import { Colxx } from '../../../../components/common/CustomBootstrap';
import IntlMessages from '../../../../helpers/IntlMessages';
import PolicyListTab from '../../../../containers/pages/policy/PolicyListTab';
import PermissionListTab from '../../../../containers/pages/permission/PermissionListTab';
import UserListTab from '../../../../containers/pages/user/UserListTab';

const GroupDetail = ({ intl, match }) => {
  const [isLoaded, setIsLoaded] = useState(false);
  const [group, setGroup] = useState({ roles: [], policies: [] });

  async function fetchData() {
    const groupFromApi = await GroupApi.getGroup(match.params.groupId);
    setGroup(groupFromApi);
    setIsLoaded(true);
  }

  useEffect(() => {
    fetchData().then();
  }, [group.value]);

  const onGroupUpdated = async () => {
    await new Promise((resolve) => setTimeout(resolve, 1000));
    fetchData().then();
  };

  return !isLoaded ? (
    <div className="loading" />
  ) : (
    <>
      <GroupDetailPageHeading
        immutable={group.immutable}
        match={match}
        groupName={group.name}
        groupDescription={group.description}
        intl={intl}
      />
      <Row>
        <Colxx xxs="12">
          <Nav tabs className="separator-tabs ml-0 mb-4">
            <NavItem>
              <NavLink
                location={{}}
                to={match.path
                  .replace(':tab', 'users')
                  .replace(':groupId', match.params.groupId)}
                className={classnames({
                  active: match.params.tab === 'users',
                  'nav-link': true,
                })}
              >
                <IntlMessages id="menu.users" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                location={{}}
                to={match.path
                  .replace(':tab', 'policies')
                  .replace(':groupId', match.params.groupId)}
                className={classnames({
                  active: match.params.tab === 'policies',
                  'nav-link': true,
                })}
              >
                <IntlMessages id="menu.policies" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                location={{}}
                to={match.path
                  .replace(':tab', 'permissions')
                  .replace(':groupId', match.params.groupId)}
                className={classnames({
                  active: match.params.tab === 'permissions',
                  'nav-link': true,
                })}
              >
                <IntlMessages id="menu.permissions" />
              </NavLink>
            </NavItem>
          </Nav>

          <TabContent activeTab={match.params.tab}>
            <TabPane tabId="users">
              <UserListTab intl={intl} groupId={match.params.groupId} />
            </TabPane>
            <TabPane tabId="policies">
              <PolicyListTab
                onChange={onGroupUpdated}
                intl={intl}
                policies={group.policies}
                groupId={match.params.groupId}
              />
            </TabPane>
            <TabPane tabId="permissions">
              <PermissionListTab intl={intl} permissions={group.permissions} />
            </TabPane>
          </TabContent>
        </Colxx>
      </Row>
    </>
  );
};

export default injectIntl(GroupDetail);
