import { v4 as uuidv4 } from 'uuid';
import * as jwt from 'jsonwebtoken';
import axios from 'axios';
import * as packageJson from '../../package.json';
import { NotificationManager } from '../components/common/react-notifications';
import AuthApi from '../api/auth.api';

export default class ApiUtil {
  static getApiHeaders() {
    const now = new Date();
    return {
      'X-Correlation-Id': uuidv4(),
      'Accept-Language': 'en-US',
      'X-Started': now.toISOString(),
      'X-Created': now.toISOString(),
      'X-Context-Category': 'default',
      'X-Context-Id': 'none',
      'X-Client-Name': 'I Am Zen UI',
      'X-Client-Version': packageJson.version,
      'X-Client-Variant': '',
    };
  }

  static async getAuthorizedApiHeaders() {
    const token = await ApiUtil.getValidToken();

    return {
      ...this.getApiHeaders(),
      Authorization: `Bearer ${token}`,
    };
  }

  static async getAuthorizedPayloadApiHeaders(metaType) {
    const token = await ApiUtil.getValidToken();

    return {
      ...this.getPayloadApiHeaders(metaType),
      Authorization: `Bearer ${token}`,
    };
  }

  static async getValidToken() {
    let token = localStorage.getItem('accessToken');

    const decoded = jwt.decode(token);

    if (new Date().getTime() > decoded.exp * 1000) {
      token = await AuthApi.getRefreshedToken(
        ApiUtil.getApiHeaders(),
        ApiUtil.apiBasePath
      );
      if (!token) {
        AuthApi.logout();
      }
    }

    return token;
  }

  static getPayloadApiHeaders(metaType) {
    return {
      ...this.getApiHeaders(),
      'X-Schema-Version': '1.0.0',
      'X-Meta-Type': metaType,
      'Content-Type': 'application/json',
    };
  }

  static get apiBasePath() {
    return window.ENVIRONMENT.apiBaseUrl;
  }

  static showErrorToast(message) {
    NotificationManager.error(message, 'Error', 3000, null, null, 'filled');
  }

  static async handleError(e) {
    if (e.response.status === 401) {
      const { config } = e;
      if (!config) {
        return Promise.reject(e);
      }
      const userRaw = localStorage.getItem('current_user');
      if (!userRaw) {
        return Promise.reject(e);
      }
      const user = JSON.parse(userRaw);
      const rawData = config.data;
      let jsonData;
      if (rawData) {
        jsonData = JSON.parse(rawData);
      }
      const isRefreshing =
        config.url === `/user/${user.uid}/token` &&
        jsonData?.type === 'refresh' &&
        config.method === 'post';
      if (isRefreshing) {
        AuthApi.logout();
      }
      config.retry = 1;

      await AuthApi.getRefreshedToken(
        ApiUtil.getApiHeaders(),
        ApiUtil.apiBasePath
      );
      config.headers = await ApiUtil.getAuthorizedApiHeaders();
      return axios(e.config);
    }
    if (e?.response?.data?.data?.message?.developer) {
      console.error(e.response.data.data.message.developer);
    }
    if (e?.response?.data?.data?.stack) {
      console.error(e?.response?.data?.data?.stack);
    }
    if (e?.response?.data?.data?.message?.user) {
      ApiUtil.showErrorToast(e?.response?.data?.data?.message?.user);
    } else {
      ApiUtil.showErrorToast('Unknown error');
    }
    throw e;
  }
}

axios.interceptors.response.use(undefined, ApiUtil.handleError);
