{
  "env": {
    "browser": true,
    "commonjs": true,
    "es6": true,
    "jest": true,
    "node": true
  },
  "extends": [
    "airbnb",
    "plugin:react/recommended",
    "plugin:jsx-a11y/strict",
    "plugin:prettier/recommended",
    "prettier"
  ],
  "settings": {
    "import/resolver": {
      "node": {
        "paths": [
          "src"
        ]
      }
    }
  },
  "parser": "babel-eslint",
  "parserOptions": {
    "ecmaVersion": 6,
    "sourceType": "module",
    "ecmaFeatures": {
      "jsx": true,
      "modules": true,
      "experimentalObjectRestSpread": true
    }
  },
  "plugins": [
    "react",
    "react-hooks",
    "jsx-a11y",
    "prettier"
  ],
  "rules": {
    "react/jsx-filename-extension": [
      1,
      {
        "extensions": [
          ".js",
          ".jsx"
        ]
      }
    ],
    "no-restricted-syntax": [
      "error",
      {
        "selector": "LabeledStatement",
        "message":"Labels are a form of GOTO; using them makes code confusing and hard to maintain and understand.",
      },
      {
        "selector": "WithStatement",
        "message": "`with` is disallowed in strict mode because it makes code impossible to predict and optimize.",
      }
    ],
    "no-unused-vars": "off",
    "no-useless-catch": "off",
    "no-await-in-loop": "off",
    "react/jsx-boolean-value": "off",
    "no-empty": "off",
    "react-hooks/rules-of-hooks": "error",
    "react-hooks/exhaustive-deps": 1,
    "react/jsx-props-no-spreading": 0,
    "no-console": "off",
    "react/prop-types": 0,
    "prettier/prettier": [
      "warn"
    ]
  },
  "globals": {
    "window": true,
    "document": true,
    "localStorage": true,
    "FormData": true,
    "FileReader": true,
    "Blob": true,
    "navigator": true
  }
}
