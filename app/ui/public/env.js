window.ENVIRONMENT = {
  apiBaseUrl: 'http://localhost:3000/api/v1',
  userRegistrationEnabled: true,
  forgotPasswordEnabled: true,
  uiBasePath: '/iam',
};
