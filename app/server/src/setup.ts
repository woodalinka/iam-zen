import { Config } from './config';
import { baseDir, serverDir } from './root';
import {
  ContextBuilder,
  CustomLogger,
  ServiceClient,
} from '@cryptexlabs/codex-nodejs-common';
import { Logger } from '@nestjs/common';
import { MessageContext } from '@cryptexlabs/codex-data-model';
import { i18nData } from './locale/locales';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { DatabaseTypeEnum } from './data/database-type.enum';

const appConfig = new Config(baseDir, serverDir, 'localhost');
const baseLogger = new CustomLogger(
  'info',
  appConfig.logLevels,
  false,
  new Logger(),
);
const contextBuilder = new ContextBuilder(
  baseLogger,
  appConfig,
  new ServiceClient(appConfig),
  new MessageContext('default', null),
).setI18nApi(i18nData);

let elasticsearchService = null;
if (appConfig.dbType === DatabaseTypeEnum.ELASTICSEARCH) {
  elasticsearchService = new ElasticsearchService({
    node: appConfig.elasticsearch.url,
  });
}

const setupProviders = [
  {
    provide: 'CONTEXT_BUILDER',
    useValue: contextBuilder,
  },
  {
    provide: 'CONFIG',
    useValue: appConfig,
  },
  {
    provide: 'LOGGER',
    useValue: baseLogger,
  },
];

export {
  appConfig,
  baseLogger,
  contextBuilder,
  setupProviders,
  elasticsearchService,
};
