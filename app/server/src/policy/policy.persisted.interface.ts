import { PolicyInterface } from './policy.interface';

export type PolicyPersistedInterface = PolicyInterface;
