export interface PolicyUpdateInterface {
  name: string;
  description: string;
  permissions: string[];
  groups?: string[];
  roles?: string[];
}
