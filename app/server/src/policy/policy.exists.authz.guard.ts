import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { HttpAuthzGuardUtil } from '@cryptexlabs/codex-nodejs-common';
import { AuthzNamespace } from '../constants';

@Injectable()
export class PolicyExistsAuthzGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const util = new HttpAuthzGuardUtil(context);

    return (
      util.isAuthorized(AuthzNamespace, {
        action: 'get',
        object: 'policy',
        objectId: 'any',
      }) ||
      (util.isAuthorized(AuthzNamespace, {
        action: 'list',
        object: 'policy',
        objectId: '',
      }) &&
        !util.isAuthorized(AuthzNamespace, {
          action: 'any',
          object: 'policy',
          objectId: '',
        })) ||
      (util.isAuthorized(AuthzNamespace, {
        action: 'create',
        object: 'policy',
        objectId: '',
      }) &&
        !util.isAuthorized(AuthzNamespace, {
          action: 'any',
          object: 'policy',
          objectId: '',
        }))
    );
  }
}
