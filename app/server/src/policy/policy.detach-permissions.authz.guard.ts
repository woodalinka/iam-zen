import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AuthzNamespace } from '../constants';
import { HttpAuthzDetachObjectsGuardUtil } from '@cryptexlabs/codex-nodejs-common';

@Injectable()
export class PolicyDetachPermissionsAuthzGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const util = new HttpAuthzDetachObjectsGuardUtil(context);
    return util.isAuthorized(
      'policy',
      util.params.policyId,
      'permission',
      util.query.ids,
      AuthzNamespace.object,
    );
  }
}
