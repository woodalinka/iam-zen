export interface PolicyGroupInterface {
  id: string;
  name: string;
  description: string;
  immutable: boolean;
}
