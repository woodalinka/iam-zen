import { PolicyPersistedInterface } from './policy.persisted.interface';

export class PolicyInListDto {
  public readonly id: string;
  public readonly name: string;
  public readonly permissionCount: number;

  constructor(policy: PolicyPersistedInterface, public readonly immutable) {
    this.id = policy.id;
    this.name = policy.name;
    this.permissionCount = policy.permissions?.length || 0;
  }
}
