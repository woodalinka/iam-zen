import { PolicyInListDto } from './policy.in-list.dto';

export const policyUpdateExample = {
  name: 'Widget Policy 7',
  description: 'Policy 7 for widget app',
  permissions: ['ec7d1b2f-a23c-4b8a-86fc-1a9276c4ff88'],
};

export const policyPatchExample = {
  name: 'Widget Policy 7',
  description: 'Policy 7 for widget app',
};

export const examplePolicy = new PolicyInListDto(
  {
    id: '9f20bdb4-9283-4375-a9d1-43dffd09d5c1',
    ...policyUpdateExample,
  },
  true,
);
