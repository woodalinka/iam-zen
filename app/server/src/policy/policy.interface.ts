export interface PolicyInterface {
  id: string;
  name: string;
  description: string;
  permissions: string[];
}
