import { PolicyDetachPermissionsAuthzGuard } from './policy.detach-permissions.authz.guard';
import { ExecutionContext } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { AuthzNamespace } from '../constants';

describe(PolicyDetachPermissionsAuthzGuard.name, () => {
  it('Should allow super admin to detach a permission from a policy', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::any:any:any:any:any:any:any:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            policyId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new PolicyDetachPermissionsAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to detach any permission from a policy to detach a permission to the policy', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::policy:4d2114ca-24e2-43e5-bddb-d9a6688b8340::permission:any:delete`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            policyId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new PolicyDetachPermissionsAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to do anything to any permission on a policy to detach a permission to the policy', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::policy:4d2114ca-24e2-43e5-bddb-d9a6688b8340::permission:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            policyId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new PolicyDetachPermissionsAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to do anything to any sub object for a policy to detach a permission to the policy', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::policy:4d2114ca-24e2-43e5-bddb-d9a6688b8340::any:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            policyId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new PolicyDetachPermissionsAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to detach a specific permission from a policy to detach the permission to the policy', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::policy:4d2114ca-24e2-43e5-bddb-d9a6688b8340::permission:680dddec-f0b9-4a01-b8b5-be725f946935:delete`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            policyId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new PolicyDetachPermissionsAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should not allow someone with permission to detach any permission to a different policy to detach a permission to the policy', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::policy:4d2114ca-24e2-43e5-bddb-d9a6688b8340::permission:any:delete`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            policyId: '001d4f53-798b-4a0b-8ef7-330a7bf72147',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new PolicyDetachPermissionsAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should not allow someone with permission to do anything to any permission on a different policy to detach a permission to the policy', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::policy:4d2114ca-24e2-43e5-bddb-d9a6688b8340::permission:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            policyId: '001d4f53-798b-4a0b-8ef7-330a7bf72147',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new PolicyDetachPermissionsAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should not allow someone with permission to do anything to any sub object for a different policy to detach a permission to the policy', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::policy:4d2114ca-24e2-43e5-bddb-d9a6688b8340::any:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            policyId: '001d4f53-798b-4a0b-8ef7-330a7bf72147',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new PolicyDetachPermissionsAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should not allow someone with permission to detach a specific permission to a different policy to detach the permission to the policy', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::policy:4d2114ca-24e2-43e5-bddb-d9a6688b8340::permission:680dddec-f0b9-4a01-b8b5-be725f946935:delete`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            policyId: '001d4f53-798b-4a0b-8ef7-330a7bf72147',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new PolicyDetachPermissionsAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should not allow someone with permission to detach a different specific permission from a policy to detach the permission to the policy', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::policy:4d2114ca-24e2-43e5-bddb-d9a6688b8340::permission:680dddec-f0b9-4a01-b8b5-be725f946935:delete`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            policyId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          query: { ids: ['5be3176f-c066-4418-b682-18e16fd07b84'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new PolicyDetachPermissionsAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });
});
