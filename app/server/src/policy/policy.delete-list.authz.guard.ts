import {
  CanActivate,
  ExecutionContext,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { HttpAuthzGuardUtil } from '@cryptexlabs/codex-nodejs-common';
import { AuthzNamespace } from '../constants';

@Injectable()
export class PolicyDeleteListAuthzGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const util = new HttpAuthzGuardUtil(context);

    if (!util.query.ids) {
      throw new HttpException(
        `ids query parameter cannot be empty`,
        HttpStatus.BAD_REQUEST,
      );
    }

    for (const id of util.query.ids) {
      if (
        !util.isAuthorized(AuthzNamespace, {
          action: 'delete',
          object: 'policy',
          objectId: id,
        })
      ) {
        return false;
      }
    }

    return true;
  }
}
