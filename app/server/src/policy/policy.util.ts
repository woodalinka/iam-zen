export class PolicyUtil {
  public static getUniquePermissionIdsForPolicies(
    policies: { permissions?: string[] }[],
  ) {
    const uniquePermissionIds = [];
    for (const policy of policies) {
      for (const permissionId of policy.permissions || []) {
        if (!uniquePermissionIds.includes(permissionId)) {
          uniquePermissionIds.push(permissionId);
        }
      }
    }
    return uniquePermissionIds;
  }
}
