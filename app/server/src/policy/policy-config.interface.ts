export interface PolicyConfigInterface {
  id: string;
  name: string;
  description?: string;
  permissions: string[];
}
