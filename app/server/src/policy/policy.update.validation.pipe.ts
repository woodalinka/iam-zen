import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import * as Joi from 'joi';
import { PolicyUpdateInterface } from './policy.update.interface';

@Injectable()
export class PolicyUpdateValidationPipe implements PipeTransform {
  private _schema: Joi.ObjectSchema<PolicyUpdateInterface>;

  constructor() {
    this._schema = Joi.object({
      name: Joi.string().alphanum().required(),
      description: Joi.string().required(),
      permissions: Joi.array()
        .items(Joi.string().uuid({ version: 'uuidv4' }))
        .optional(),
      groups: Joi.array()
        .items(Joi.string().uuid({ version: 'uuidv4' }))
        .optional(),
      roles: Joi.array()
        .items(Joi.string().uuid({ version: 'uuidv4' }))
        .optional(),
    });
  }

  public transform(value: any, metadata: ArgumentMetadata): any {
    const { error } = this._schema.validate(value.data);
    if (error) {
      throw new BadRequestException(error.message);
    }
    return value;
  }
}
