import {
  Body,
  Controller,
  Delete,
  Get,
  Headers,
  HttpCode,
  HttpStatus,
  Inject,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Put,
  Query,
  Res,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  ApiMetaHeaders,
  ApiPagination,
  ContextBuilder,
  NotEmptyPipe,
  RestPaginatedResponse,
  RestResponse,
  SimpleHttpResponse,
} from '@cryptexlabs/codex-nodejs-common';
import { PolicyService } from './policy.service';
import { ExampleSimpleResponse } from '../example/example.simple.response';
import { LocalesEnum } from '../locale/enum';
import { PolicyUpdateInterface } from './policy.update.interface';
import { PolicyPatchInterface } from './policy.patch.interface';
import {
  examplePolicy,
  policyPatchExample,
  policyUpdateExample,
} from './example';
import { appConfig } from '../setup';
import { PolicyListAuthzGuard } from './policy.list.authz.guard';
import { PolicyGetAuthzGuard } from './policy.get.authz.guard';
import { PolicyDeleteAuthzGuard } from './policy.delete.authz.guard';
import { PolicyUpdateAuthzGuard } from './policy.update.authz.guard';
import { PolicyAttachPermissionsAuthzGuard } from './policy.attach-permissions.authz.guard';
import { PolicyDetachPermissionsAuthzGuard } from './policy.detach-permissions.authz.guard';
import { ExampleRestPaginatedResponse } from '../example/example.rest.paginated.response';
import { PolicyDeleteListAuthzGuard } from './policy.delete-list.authz.guard';
import { ExampleRestResponse } from '../example/example.rest.response';
import { PolicyExistsAuthzGuard } from './policy.exists.authz.guard';
import { Uuidv4ArrayValidationPipe } from '@cryptexlabs/codex-nodejs-common/lib/src/pipe/uuidv4.array.validation.pipe';
import { PolicyUpdateValidationPipe } from './policy.update.validation.pipe';
import { OrderDirectionEnum } from '../query.options.interface';
import { OrderDirectionValidationPipe } from '../order.direction.validation.pipe';
import { UserService } from '../user/user.service';

@Controller(`/${appConfig.appPrefix}/${appConfig.apiVersion}/policy`)
@ApiTags('policy')
@ApiMetaHeaders()
@ApiBearerAuth()
export class PolicyController {
  constructor(
    private readonly policyService: PolicyService,
    private readonly userService: UserService,
    @Inject('CONTEXT_BUILDER') private readonly contextBuilder: ContextBuilder,
  ) {}

  @Get()
  @ApiOperation({
    summary: 'Get list of policies',
  })
  @ApiPagination()
  @ApiQuery({
    name: 'orderBy',
    required: false,
    schema: {
      example: 'name',
    },
  })
  @ApiQuery({
    name: 'orderDirection',
    required: false,
    schema: {
      example: 'asc',
    },
  })
  @ApiResponse({
    schema: {
      example: new ExampleRestPaginatedResponse(
        HttpStatus.OK,
        'cryptexlabs.iam-zen.policy.list',
        [examplePolicy],
      ),
    },
  })
  @ApiQuery({
    name: 'searchName',
    required: false,
  })
  @ApiQuery({
    name: 'search',
    required: false,
  })
  @UseGuards(PolicyListAuthzGuard)
  public async getPolicies(
    @Query('page', ParseIntPipe) page: number,
    @Query('pageSize', ParseIntPipe) pageSize: number,
    @Query('searchName') searchName: string,
    @Query('search') search: string,
    @Query('orderBy') orderBy: string,
    @Query('orderDirection', OrderDirectionValidationPipe)
    orderDirection: OrderDirectionEnum,
    @Headers() headers: Record<string, string>,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const policies = await this.policyService.getPolicies(
      context,
      page,
      pageSize,
      {
        searchName,
        search,
        orderBy,
        orderDirection,
      },
    );

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestPaginatedResponse(
      responseContext,
      HttpStatus.OK,
      policies.total,
      'cryptexlabs.iam-zen.policy.list',
      policies.results,
    );
  }

  @Get(':policyId')
  @ApiOperation({
    summary: 'Get a policy by policy ID',
  })
  @ApiParam({
    name: 'policyId',
    example: 'ac5c44eb-501d-4b53-bcca-9aac58b438de',
  })
  @UseGuards(PolicyGetAuthzGuard)
  public async getPolicy(
    @Param('policyId') policyId: string,
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const policy = await this.policyService.getPolicyById(context, policyId);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestResponse(
      responseContext,
      HttpStatus.OK,
      'cryptexlabs.iam-zen.policy',
      policy,
    );
  }

  @Get('any/exists')
  @ApiOperation({
    summary: 'Get whether or not a policy exists',
  })
  @ApiQuery({
    name: 'name',
    example: 'default',
    required: true,
  })
  @UseGuards(PolicyExistsAuthzGuard)
  @ApiResponse({
    schema: {
      example: new ExampleRestResponse(HttpStatus.OK, 'boolean', false),
    },
  })
  public async getPolicyExists(
    @Query('name', NotEmptyPipe) name: string,
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const policyExists = await this.policyService.getPolicyExists(
      context,
      name,
    );

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestResponse(
      responseContext,
      HttpStatus.OK,
      'boolean',
      policyExists,
    );
  }

  @Delete(':policyId')
  @ApiOperation({
    summary: 'Delete policy by policy ID',
  })
  @ApiParam({
    name: 'policyId',
    example: '632efe54-6055-4abf-8cd3-554228b96bd2',
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(PolicyDeleteAuthzGuard)
  public async deletePolicy(
    @Param('policyId', NotEmptyPipe) userId: string,
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    await this.policyService.deletePolicy(context, userId);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Delete()
  @ApiOperation({
    summary: 'Delete policies by policy ID',
  })
  @ApiQuery({
    name: 'ids',
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(PolicyDeleteListAuthzGuard)
  public async deletePolicies(
    @Query('ids', NotEmptyPipe) policyIds: string[],
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const usePolicyIds = Array.isArray(policyIds) ? policyIds : [policyIds];

    await this.policyService.deletePolicies(context, usePolicyIds);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Put(':policyId')
  @ApiOperation({
    summary: 'Create or update policy by policy ID',
  })
  @ApiBody({
    schema: {
      example: policyUpdateExample,
    },
    required: true,
  })
  @ApiParam({
    name: 'policyId',
    example: '632efe54-6055-4abf-8cd3-554228b96bd2',
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(PolicyUpdateAuthzGuard)
  public async updatePolicy(
    @Param('policyId', NotEmptyPipe) policyId: string,
    @Headers() headers,
    @Res() res,
    @Body(NotEmptyPipe, PolicyUpdateValidationPipe)
    policyUpdate: PolicyUpdateInterface,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const status = await this.policyService.savePolicy(
      context,
      policyId,
      policyUpdate,
    );

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    res
      .status(status)
      .send(
        new SimpleHttpResponse(
          responseContext,
          HttpStatus.ACCEPTED,
          LocalesEnum.SUCCESS,
        ),
      );
  }

  @Patch(':policyId')
  @ApiOperation({
    summary: 'Partially policy by policy ID',
  })
  @ApiParam({
    name: 'policyId',
    example: '632efe54-6055-4abf-8cd3-554228b96bd2',
    required: true,
  })
  @ApiBody({
    schema: {
      example: policyPatchExample,
    },
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(PolicyUpdateAuthzGuard)
  public async patchPolicy(
    @Param('policyId', NotEmptyPipe) policyId: string,
    @Body(NotEmptyPipe) policyPatch: PolicyPatchInterface,
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    await this.policyService.patchPolicy(context, policyId, policyPatch);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Post(':policyId/permission')
  @ApiOperation({
    summary: 'Add permissions to policy',
  })
  @ApiParam({
    name: 'policyId',
    example: 'ac5c44eb-501d-4b53-bcca-9aac58b438de',
    required: true,
  })
  @ApiBody({
    schema: {
      example: ['7da13f60-8536-4795-ade3-373aea49d1f7'],
    },
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(PolicyAttachPermissionsAuthzGuard)
  public async addPermissions(
    @Param('policyId', NotEmptyPipe) policyId: string,
    @Body(NotEmptyPipe, Uuidv4ArrayValidationPipe) policies: string[],
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    await this.policyService.addPermissions(context, policyId, policies);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Delete(':policyId/permission')
  @ApiOperation({
    summary: 'Remove permissions from policy',
  })
  @ApiParam({
    name: 'policyId',
    example: 'ac5c44eb-501d-4b53-bcca-9aac58b438de',
    required: true,
  })
  @ApiQuery({
    name: 'ids',
    example: ['7da13f60-8536-4795-ade3-373aea49d1f7'],
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(PolicyDetachPermissionsAuthzGuard)
  public async removePermissions(
    @Param('policyId', NotEmptyPipe) policyId: string,
    @Query('ids', NotEmptyPipe) ids: string[],
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const policyIds = typeof ids === 'string' ? [ids] : ids;

    await this.policyService.removePermissions(context, policyId, policyIds);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Get(':policyId/user')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Get list of users for a policy',
  })
  @ApiPagination()
  @ApiParam({
    name: 'policyId',
    example: 'ac5c44eb-501d-4b53-bcca-9aac58b438de',
    required: true,
  })
  @ApiQuery({
    name: 'orderBy',
    required: false,
    schema: {
      example: 'name',
    },
  })
  @ApiQuery({
    name: 'orderDirection',
    required: false,
    schema: {
      example: 'asc',
    },
  })
  @ApiQuery({
    name: 'search',
    required: false,
  })
  public async getUsersForPolicy(
    @Param('policyId', NotEmptyPipe) policyId: string,
    @Query('page', ParseIntPipe) page: number,
    @Query('pageSize', ParseIntPipe) pageSize: number,
    @Query('search') search: string,
    @Query('orderBy') orderBy: string,
    @Query('orderDirection', OrderDirectionValidationPipe)
    orderDirection: OrderDirectionEnum,
    @Headers() headers: Record<string, string>,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const users = await this.userService.getUsersForPolicy(
      context,
      policyId,
      page,
      pageSize,
      {
        search,
        orderBy,
        orderDirection,
      },
    );

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestPaginatedResponse(
      responseContext,
      HttpStatus.OK,
      users.total,
      'cryptexlabs.iam-zen.group.list',
      users.results,
    );
  }
}
