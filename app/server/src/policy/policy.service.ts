import {
  forwardRef,
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { Config } from '../config';
import { PolicyConfigInterface } from './policy-config.interface';
import { GroupPoliciesProviderInterface } from '../group/group.policies.provider.interface';
import { GroupPolicyInterface } from '../group/group.policy.interface';
import {
  ArrayUtil,
  Context,
  PaginatedResults,
} from '@cryptexlabs/codex-nodejs-common';
import { RolePoliciesProviderInterface } from '../role/role.policies.provider.interface';
import { RolePolicyInterface } from '../role/role.policy.interface';
import { PolicyInListDto } from './policy.in-list.dto';
import { PolicyDataServiceInterface } from './policy.data.service.interface';
import { PolicyDto } from './policy.dto';
import { RolePermissionDetailInterface } from '../role/role.permission.interface';
import { PolicyPermissionsProviderInterface } from './policy.permissions.provider.interface';
import { RolePermissionsProviderInterface } from '../role/role.permissions.provider.interface';
import { PolicyUtil } from './policy.util';
import { ImmutableObjectError } from '../error/immutable-object.error';
import { LocalesEnum } from '../locale/enum';
import { PolicyUpdateInterface } from './policy.update.interface';
import { PolicyPatchInterface } from './policy.patch.interface';
import { QueryOptionsInterface } from '../query.options.interface';
import { GroupPermissionsProviderInterface } from '../group/group.permissions-provider.interface';
import { GroupInterface } from '../group/group.interface';
import { PermissionForPolicyInterface } from '../permission/permission.for-policy.interface';
import { RoleService } from '../role/role.service';
import { GroupService } from '../group/group.service';
import { PermissionPolicyInterface } from '../permission/permission.policy.interface';

@Injectable()
export class PolicyService
  implements
    GroupPoliciesProviderInterface,
    GroupPermissionsProviderInterface,
    RolePoliciesProviderInterface,
    RolePermissionsProviderInterface
{
  constructor(
    @Inject('CONFIG') private readonly config: Config,
    @Inject('POLICY_DATA_SERVICE')
    private readonly policyDataService: PolicyDataServiceInterface,
    @Inject(forwardRef(() => 'POLICY_PERMISSIONS_PROVIDER'))
    private readonly policyPermissionsProvider: PolicyPermissionsProviderInterface,
    @Inject(forwardRef(() => 'ROLE_SERVICE'))
    private readonly roleService: RoleService,
    @Inject(forwardRef(() => 'GROUP_SERVICE'))
    private readonly groupService: GroupService,
  ) {}

  public async getPoliciesByIds(
    context: Context,
    policyIds: string[],
  ): Promise<PolicyConfigInterface[]> {
    const defaultPolicies = this.config.defaultPolicies.filter((policy) =>
      policyIds.includes(policy.id),
    );
    // TODO lookup policies from DB
    return defaultPolicies;
  }

  public async getPoliciesForGroup(
    context: Context,
    groupId: string,
    policyIds: string[],
  ): Promise<GroupPolicyInterface[]> {
    const policies = await this.getPoliciesByIds(context, policyIds);
    return policies.map((policy) => {
      const configGroup = this.config.defaultGroups.find(
        (g) => g.id === groupId,
      );
      let immutable = false;
      if (configGroup) {
        immutable = configGroup.policies.includes(policy.id);
      }

      return {
        id: policy.id,
        name: policy.name,
        description: policy.description,
        immutable,
      };
    });
  }

  public async getPoliciesForRole(
    context: Context,
    roleId: string,
    policyIds: string[],
  ): Promise<RolePolicyInterface[]> {
    const policies = await this.getPoliciesByIds(context, policyIds);

    return policies.map((policy) => {
      const configRole = this.config.defaultRoles.find((r) => r.id === roleId);
      let immutable = false;
      if (configRole) {
        immutable = configRole.policies.includes(policy.id);
      }

      return {
        id: policy.id,
        name: policy.name,
        description: policy.description,
        immutable,
      };
    });
  }

  public async getPolicies(
    context: Context,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<PolicyInListDto>> {
    const policies = await this.policyDataService.getPolicies(
      context,
      page,
      pageSize,
      options,
    );
    return {
      total: policies.total,
      results: policies.results.map((policy) => {
        const configPolicy = this.config.defaultPolicies.find(
          (findPolicy) => findPolicy.id === policy.id,
        );
        return new PolicyInListDto(policy, !!configPolicy);
      }),
    };
  }

  public async getPoliciesForPermission(
    context: Context,
    permissionId: string,
  ): Promise<PermissionPolicyInterface[]> {
    const policies = await this.policyDataService.getPoliciesForPermission(
      context,
      permissionId,
    );

    return policies.map((policy) => {
      let immutable = false;
      const configPolicy = this.config.defaultPolicies.find(
        (p) => p.id === policy.id,
      );
      if (configPolicy) {
        immutable = policy.permissions.includes(permissionId);
      }

      return {
        id: policy.id,
        name: policy.name,
        description: policy.description,
        immutable,
      };
    });
  }

  public async getPolicyById(
    context: Context,
    policyId: string,
  ): Promise<PolicyDto> {
    const policy = await this.policyDataService.getPolicyById(
      context,
      policyId,
    );
    if (!policy) {
      throw new NotFoundException();
    }
    const permissions =
      await this.policyPermissionsProvider.getPermissionsForPolicy(
        context,
        policy.id,
        policy.permissions || [],
      );
    const configPolicy = this.config.defaultPolicies.find(
      (findPolicies) => findPolicies.id === policy.id,
    );

    const groups = await this.groupService.getGroupsForPolicyId(
      context,
      policyId,
    );
    const roles = await this.roleService.getRolesForPolicyId(context, policyId);

    return new PolicyDto(policy, permissions, roles, groups, !!configPolicy);
  }

  public async getPolicyExists(
    context: Context,
    name: string,
  ): Promise<boolean> {
    const policy = await this.policyDataService.getPolicyWithName(
      context,
      name,
    );
    return !!policy;
  }

  public async getPermissionsForRoleByPolicyIds(
    context: Context,
    policyIds: string[],
  ): Promise<RolePermissionDetailInterface[]> {
    const policies = await this.getPoliciesByIds(context, policyIds);
    const permissionIds =
      PolicyUtil.getUniquePermissionIdsForPolicies(policies);
    const policyPermissions =
      await this.policyPermissionsProvider.getPermissionsForPoliciesByPermissionIds(
        context,
        permissionIds,
      );
    return policyPermissions.map((policyPermission) => {
      const fromPolicies = [];
      for (const policy of policies) {
        if (policy.permissions.includes(policyPermission.id)) {
          fromPolicies.push({
            id: policy.id,
            name: policy.name,
          });
        }
      }

      return {
        name: policyPermission.name,
        id: policyPermission.id,
        value: policyPermission.value,
        description: policyPermission.description,
        fromPolicies,
      };
    });
  }

  public async deletePolicy(context: Context, policyId: string): Promise<void> {
    this._immutabilityCheck(context, policyId);
    const didModify = await this.policyDataService.deletePolicy(
      context,
      policyId,
    );
    if (!didModify) {
      throw new NotFoundException();
    }
  }

  public async deletePolicies(
    context: Context,
    policyIds: string[],
  ): Promise<void> {
    for (const policyId of policyIds) {
      this._immutabilityCheck(context, policyId);
    }

    const didModify = await this.policyDataService.deletePolicies(
      context,
      policyIds,
    );
    if (!didModify) {
      throw new NotFoundException();
    }
  }

  public async savePolicy(
    context: Context,
    policyId: string,
    policyUpdate: PolicyUpdateInterface,
  ): Promise<HttpStatus> {
    this._immutabilityCheck(context, policyId);
    const didModify = await this.policyDataService.savePolicy(
      context,
      policyId,
      policyUpdate,
    );
    return didModify ? HttpStatus.ACCEPTED : HttpStatus.CREATED;
  }

  public async patchPolicy(
    context: Context,
    policyId: string,
    policyPatch: PolicyPatchInterface,
  ): Promise<void> {
    this._immutabilityCheck(context, policyId);
    const didModify = await this.policyDataService.patchPolicy(
      context,
      policyId,
      policyPatch,
    );
    if (!didModify) {
      throw new NotFoundException();
    }
  }

  public async addPermissions(
    context: Context,
    policyId: string,
    permissionIds: string[],
  ): Promise<void> {
    console.log(`permissionIds`, permissionIds);
    console.log(`policyId`, policyId);
    const foundPermissions =
      await this.policyPermissionsProvider.getPermissionsForPolicy(
        context,
        policyId,
        permissionIds,
      );

    if (foundPermissions.length !== permissionIds.length) {
      throw new HttpException(`Invalid permissions`, HttpStatus.BAD_REQUEST);
    }

    const didModify = await this.policyDataService.addPermissions(
      context,
      policyId,
      permissionIds,
    );
    if (!didModify) {
      throw new NotFoundException();
    }
  }

  public async removePermissions(
    context: Context,
    policyId: string,
    permissionIds: string[],
  ): Promise<void> {
    const immutablePolicy = this.config.defaultPolicies.find(
      (findPolicy) => findPolicy.id === policyId,
    );

    if (immutablePolicy) {
      if (
        ArrayUtil.intersection(permissionIds, immutablePolicy.permissions)
          .length > 0
      ) {
        const locale =
          permissionIds.length === 1
            ? LocalesEnum.ERROR_IMMUTABLE_POLICY_PERMISSION_DELETE_SINGLE
            : LocalesEnum.ERROR_IMMUTABLE_POLICY_PERMISSION_DELETE_MULTIPLE;
        throw new ImmutableObjectError(context, locale);
      }
    }

    const didModify = await this.policyDataService.removePermissions(
      context,
      policyId,
      permissionIds,
    );

    if (!didModify) {
      throw new NotFoundException();
    }
  }

  private _immutabilityCheck(context: Context, policyId: string) {
    const immutablePolicy = this.config.defaultPolicies.find(
      (findPolicy) => findPolicy.id === policyId,
    );
    if (immutablePolicy) {
      throw new ImmutableObjectError(
        context,
        LocalesEnum.ERROR_IMMUTABLE_POLICY,
      );
    }
  }

  public async getPermissionsForGroup(
    context: Context,
    group: GroupInterface,
  ): Promise<PermissionForPolicyInterface[]> {
    const policies = await this.getPoliciesByIds(context, group.policies);
    const permissionIds =
      PolicyUtil.getUniquePermissionIdsForPolicies(policies);

    const permissions =
      await this.policyPermissionsProvider.getPermissionsForPoliciesByPermissionIds(
        context,
        permissionIds,
      );

    return permissions.map((permission) => {
      const fromPolicies = [];
      for (const policy of policies) {
        if (policy.permissions.includes(permission.id)) {
          fromPolicies.push({
            id: policy.id,
            name: policy.name,
          });
        }
      }
      return {
        ...permission,
        fromPolicies,
      };
    });
  }
}
