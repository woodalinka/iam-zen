import { PolicyPersistedInterface } from './policy.persisted.interface';
import { PolicyPermissionDetailedInterface } from './policy.permission.interface';
import { PolicyRoleInterface } from './policy.role.interface';
import { PolicyGroupInterface } from './policy.group.interface';

export class PolicyDto {
  public readonly id: string;
  public readonly name: string;
  public readonly description: string;

  constructor(
    policy: PolicyPersistedInterface,
    public readonly permissions: PolicyPermissionDetailedInterface[],
    public readonly roles: PolicyRoleInterface[],
    public readonly groups: PolicyGroupInterface[],
    public readonly immutable,
  ) {
    this.id = policy.id;
    this.name = policy.name;
    this.description = policy.description;
  }
}
