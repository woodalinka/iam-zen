import { PolicyService } from './policy.service';
import { appConfig, elasticsearchService } from '../setup';
import { DatabaseTypeEnum } from '../data/database-type.enum';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { PolicyElasticsearchDataService } from './policy.elasticsearch.data.service';
import { PolicyDefaultLoaderService } from './policy.default-loader.service';
import { PolicyDataServiceInterface } from './policy.data.service.interface';
import { Config } from '../config';
import { ContextBuilder } from '@cryptexlabs/codex-nodejs-common';

const providers = [];
if (appConfig.dbType === DatabaseTypeEnum.ELASTICSEARCH) {
  providers.push(
    {
      provide: ElasticsearchService,
      useValue: elasticsearchService,
    },
    PolicyElasticsearchDataService,
    {
      provide: 'POLICY_DATA_SERVICE',
      useFactory: async (service: PolicyElasticsearchDataService) => {
        await service.initializeIndex();
        return service;
      },
      inject: [PolicyElasticsearchDataService],
    },
  );
}

export const policyProviders = [
  ...providers,
  PolicyService,
  {
    provide: 'GROUP_POLICIES_PROVIDER',
    useClass: PolicyService,
  },
  {
    provide: 'GROUP_PERMISSIONS_PROVIDER',
    useClass: PolicyService,
  },
  {
    provide: 'ROLE_POLICIES_PROVIDER',
    useClass: PolicyService,
  },
  {
    provide: 'ROLE_PERMISSIONS_PROVIDER',
    useClass: PolicyService,
  },
  {
    provide: 'POLICY_SERVICE',
    useClass: PolicyService,
  },
  {
    provide: PolicyDefaultLoaderService,
    useFactory: async (
      policyDataService: PolicyDataServiceInterface,
      config: Config,
      contextBuilder: ContextBuilder,
    ) => {
      const service = new PolicyDefaultLoaderService(
        policyDataService,
        config,
        contextBuilder,
      );
      if (appConfig.autoLoadConfig) {
        await service.load();
      }
      return service;
    },
    inject: ['POLICY_DATA_SERVICE', 'CONFIG', 'CONTEXT_BUILDER'],
  },
];
