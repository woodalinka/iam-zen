import { PolicyUpdateAuthzGuard } from './policy.update.authz.guard';
import * as jwt from 'jsonwebtoken';
import { AuthzNamespace } from '../constants';
import { ExecutionContext } from '@nestjs/common';

describe(PolicyUpdateAuthzGuard.name, () => {
  it('Should allow a super admin to update a policy', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::any:any:any`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            policyId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new PolicyUpdateAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to update any object to update the policy', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::any:any:update`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            policyId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new PolicyUpdateAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to update a specific policy to update the specific policy', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::policy:4d2114ca-24e2-43e5-bddb-d9a6688b8340:update`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            policyId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new PolicyUpdateAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to do anything to a specific policy to update the policy', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::policy:4d2114ca-24e2-43e5-bddb-d9a6688b8340:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            policyId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new PolicyUpdateAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to update any policy to update the policy', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::policy:any:update`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            policyId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new PolicyUpdateAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should not allow someone with permission to update a different specific policy to update the specific policy', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::policy:4d2114ca-24e2-43e5-bddb-d9a6688b8340:update`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            policyId: '3630a7ed-fef5-4345-9947-c54e8d15f954',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new PolicyUpdateAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should not allow someone with permission to do anything to a different specific policy to update the policy', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::policy:4d2114ca-24e2-43e5-bddb-d9a6688b8340:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            policyId: '3630a7ed-fef5-4345-9947-c54e8d15f954',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new PolicyUpdateAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });
});
