import { PolicyGetAuthzGuard } from './policy.get.authz.guard';
import * as jwt from 'jsonwebtoken';
import { AuthzNamespace } from '../constants';
import { ExecutionContext } from '@nestjs/common';

describe(PolicyGetAuthzGuard.name, () => {
  it('Should allow a super admin to get a policy', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::any:any:any`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            policyId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          body: ['680dddec-f0b9-4a01-b8b5-be725f946935'],
        }),
      }),
    } as ExecutionContext;

    const guard = new PolicyGetAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to get any object to get the policy', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::any:any:get`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            policyId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new PolicyGetAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to get a specific policy to get the specific policy', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::policy:4d2114ca-24e2-43e5-bddb-d9a6688b8340:get`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            policyId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new PolicyGetAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to do anything to a specific policy to get the policy', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::policy:4d2114ca-24e2-43e5-bddb-d9a6688b8340:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            policyId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new PolicyGetAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to get any policy to get the policy', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::policy:any:get`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            policyId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new PolicyGetAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should not allow someone with permission to get a different specific policy to get the specific policy', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::policy:4d2114ca-24e2-43e5-bddb-d9a6688b8340:get`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            policyId: '3630a7ed-fef5-4345-9947-c54e8d15f954',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new PolicyGetAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should not allow someone with permission to do anything to a different specific policy to get the policy', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::policy:4d2114ca-24e2-43e5-bddb-d9a6688b8340:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            policyId: '3630a7ed-fef5-4345-9947-c54e8d15f954',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new PolicyGetAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });
});
