import { Inject, Injectable } from '@nestjs/common';
import { PolicyDataServiceInterface } from './policy.data.service.interface';
import { PolicyPersistedInterface } from './policy.persisted.interface';
import {
  Context,
  CustomLogger,
  ElasticsearchInitializer,
  PaginatedResults,
} from '@cryptexlabs/codex-nodejs-common';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { PolicyPatchInterface } from './policy.patch.interface';
import { PolicyUpdateInterface } from './policy.update.interface';
import { GroupElasticsearchDataService } from '../group/group.elasticsearch.data.service';
import { RoleElasticsearchDataService } from '../role/role.elasticsearch.data.service';
import { QueryOptionsInterface } from '../query.options.interface';

@Injectable()
export class PolicyElasticsearchDataService
  implements PolicyDataServiceInterface
{
  private static get DEFAULT_INDEX() {
    return 'policy';
  }

  private readonly elasticsearchInitializer: ElasticsearchInitializer;

  constructor(
    @Inject('LOGGER') private readonly logger: CustomLogger,
    private readonly elasticsearchService: ElasticsearchService,
    private readonly groupElasticsearchService: GroupElasticsearchDataService,
    private readonly roleElasticsearchService: RoleElasticsearchDataService,
  ) {
    this.elasticsearchInitializer = new ElasticsearchInitializer(
      logger,
      PolicyElasticsearchDataService.DEFAULT_INDEX,
      elasticsearchService,
      {
        id: {
          type: 'keyword',
        },
        permissions: {
          type: 'keyword',
        },
        name: {
          type: 'text',
          fields: {
            keyword: {
              type: 'keyword',
              normalizer: 'lowercase_normalizer',
            },
          },
        },
        description: {
          type: 'text',
        },
      },
    );
  }

  public async initializeIndex() {
    const exists = await this.elasticsearchService.indices.exists({
      index: PolicyElasticsearchDataService.DEFAULT_INDEX,
    });

    if (!exists) {
      await this.elasticsearchService.indices.create({
        index: PolicyElasticsearchDataService.DEFAULT_INDEX,
        body: {
          settings: {
            analysis: {
              normalizer: {
                lowercase_normalizer: {
                  type: 'custom',
                  char_filter: [],
                  filter: ['lowercase'],
                },
              },
            },
          },
        },
      });
    }

    await this.elasticsearchInitializer.initializeIndex();
  }

  public async getPoliciesByIds(
    context: Context,
    policyIds: string[],
  ): Promise<PolicyPersistedInterface[]> {
    const response = await this.elasticsearchService.search({
      index: PolicyElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                terms: {
                  id: policyIds,
                },
              },
            ],
          },
        },
      },
    });

    return response.hits.hits.map((message: { _source?: any }) => {
      return message._source;
    });
  }

  public async savePoliciesFromConfig(
    context: Context,
    policies: PolicyPersistedInterface[],
  ): Promise<PolicyPersistedInterface[]> {
    if (policies.length === 0) {
      return [];
    }
    const response = await this.elasticsearchService.search({
      index: PolicyElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                terms: {
                  id: policies.map((policy) => policy.id),
                },
              },
            ],
          },
        },
      },
    });

    const existingPolicies = response.hits.hits;

    const existingPolicyIds = existingPolicies.map(
      (existingPolicy) => (existingPolicy._source as any).id,
    );

    const newPolicies = policies.filter(
      (policy) => !existingPolicyIds.includes(policy.id),
    );
    const newPoliciesBulk = [];
    for (const newPolicy of newPolicies) {
      newPoliciesBulk.push({
        index: {
          _index: PolicyElasticsearchDataService.DEFAULT_INDEX,
          _id: newPolicy.id,
        },
      });
      newPoliciesBulk.push({
        ...newPolicy,
      });
    }

    const existingPoliciesBulk = [];
    for (const existingPolicy of existingPolicies) {
      const updatedPolicy = policies.find(
        (policy) => policy.id === (existingPolicy._source as any).id,
      );
      existingPoliciesBulk.push({
        index: {
          _index: PolicyElasticsearchDataService.DEFAULT_INDEX,
          _id: existingPolicy._id,
        },
      });
      existingPoliciesBulk.push({
        ...(existingPolicy._source as any),
        name: updatedPolicy.name,
        description: updatedPolicy.description
          ? updatedPolicy.description
          : (existingPolicy._source as any).description,
        permissions: updatedPolicy.permissions,
      });
    }

    const bulk = [...newPoliciesBulk, ...existingPoliciesBulk];

    await this.elasticsearchService.bulk({ body: bulk });

    return newPolicies;
  }

  public async getPolicies(
    context: Context,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<PolicyPersistedInterface>> {
    let query: { query?: any; sort?: any } = {};

    if (!options.search && options.searchName) {
      query = {
        query: {
          wildcard: {
            name: {
              value: `*${options.searchName}*`,
              boost: 1.0,
              rewrite: 'constant_score',
            },
          },
        },
      };
    }
    if (options.search) {
      query = {
        query: {
          query_string: {
            query: `*${options.search}*`,
            fields: ['name', 'description'],
          },
        },
      };
    }

    const sortMap = {
      name: [{ 'name.keyword': options.orderDirection || 'asc' }, '_score'],
      permissionCount: {
        _script: {
          type: 'number',
          script: {
            lang: 'painless',
            source: 'doc.permissions.size()',
          },
          order: options.orderDirection || 'asc',
        },
      },
    };

    if (options.orderBy) {
      const sort = sortMap[options.orderBy];
      query = {
        ...query,
        sort,
      };
    }

    const response = await this.elasticsearchService.search({
      index: PolicyElasticsearchDataService.DEFAULT_INDEX,
      track_total_hits: true,
      body: {
        from: (page - 1) * pageSize,
        size: pageSize,
        ...query,
      },
    });

    return {
      total: (response.hits.total as any).value as number,
      results: response.hits.hits.map((message: { _source?: any }) => {
        return message._source;
      }),
    };
  }

  public async getPolicyById(
    context: Context,
    permissionId: string,
  ): Promise<PolicyPersistedInterface> {
    const response = await this.elasticsearchService.search({
      index: PolicyElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                term: {
                  id: permissionId,
                },
              },
            ],
          },
        },
      },
    });

    if (response.hits.total === 0) {
      return null;
    }

    return response.hits.hits[0]._source as any;
  }

  public async deletePolicy(
    context: Context,
    policyId: string,
  ): Promise<boolean> {
    const response = await this.elasticsearchService.deleteByQuery({
      index: PolicyElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          match: {
            id: policyId,
          },
        },
      },
    });
    return response.total === 1;
  }

  public async deletePolicies(
    context: Context,
    policyIds: string[],
  ): Promise<boolean> {
    const response = await this.elasticsearchService.deleteByQuery({
      index: PolicyElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                terms: {
                  id: policyIds,
                },
              },
            ],
          },
        },
      },
    });
    return response.total === policyIds.length;
  }

  public async patchPolicy(
    context: Context,
    policyId: string,
    policyPatch: PolicyPatchInterface,
  ): Promise<boolean> {
    const response = await this.elasticsearchService.updateByQuery({
      index: PolicyElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
             if (params.name != null) {
               ctx._source.name = params.name
             }
             if (params.description != null) {
               ctx._source.description = params.description
             }
            `,
          params: {
            name: policyPatch.name,
            description: policyPatch.description,
          },
        },
        query: {
          match: {
            id: policyId,
          },
        },
      },
    });
    return response.total === 1;
  }

  public async savePolicy(
    context: Context,
    policyId: string,
    policyUpdate: PolicyUpdateInterface,
  ): Promise<boolean> {
    let updated = false;
    const response = await this.elasticsearchService.updateByQuery({
      index: PolicyElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
             ctx._source.name = params.name;
             ctx._source.description = params.description;
             ctx._source.permissions = params.permissions;
            `,
          params: {
            name: policyUpdate.name,
            description: policyUpdate.description,
            permissions: policyUpdate.permissions,
          },
        },
        query: {
          match: {
            id: policyId,
          },
        },
      },
    });
    if (response.total === 1) {
      updated = true;
    } else {
      await this.elasticsearchService.index({
        index: PolicyElasticsearchDataService.DEFAULT_INDEX,
        id: policyId,
        body: {
          id: policyId,
          ...policyUpdate,
        },
      });
      updated = false;
    }

    for (const groupId of policyUpdate.groups) {
      await this.groupElasticsearchService.addPolicies(context, groupId, [
        policyId,
      ]);
    }
    for (const roleId of policyUpdate.roles) {
      await this.roleElasticsearchService.addPolicies(context, roleId, [
        policyId,
      ]);
    }
    return updated;
  }

  public async addPermissions(
    context: Context,
    policyId: string,
    permissionIds: string[],
  ): Promise<boolean> {
    const response = await this.elasticsearchService.updateByQuery({
      index: PolicyElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
           if (!(ctx._source.permissions instanceof List)) {
             ctx._source.permissions = new ArrayList();
           }
           ctx._source.permissions.addAll(params.permissions);
           ctx._source.permissions = ctx._source.permissions.stream().distinct().sorted().collect(Collectors.toList());
          `,
          params: {
            permissions: permissionIds,
          },
        },
        query: {
          match: {
            id: policyId,
          },
        },
      },
    });
    return response.total === 1;
  }

  public async removePermissions(
    context: Context,
    policyId: string,
    permissionIds: string[],
  ): Promise<boolean> {
    const response = await this.elasticsearchService.updateByQuery({
      index: PolicyElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
            if (!(ctx._source.permissions instanceof List)) {
              ctx._source.permissions = new ArrayList();
            }
            ctx._source.permissions.removeAll(params.permissions);
          `,
          params: {
            permissions: permissionIds,
          },
        },
        query: {
          match: {
            id: policyId,
          },
        },
      },
    });

    return response.total === 1;
  }

  public async getPolicyWithName(
    context: Context,
    name: string,
  ): Promise<PolicyPersistedInterface | null> {
    const response = await this.elasticsearchService.search({
      index: PolicyElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                term: {
                  'name.keyword': name,
                },
              },
            ],
          },
        },
      },
    });

    if (response.hits.total === 0) {
      return null;
    }

    return response.hits.hits[0]._source as any;
  }

  async getPoliciesForPermission(
    context: Context,
    permissionId: string,
  ): Promise<PolicyPersistedInterface[]> {
    const response = await this.elasticsearchService.search({
      index: PolicyElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                term: {
                  permissions: permissionId,
                },
              },
            ],
          },
        },
      },
    });

    return response.hits.hits.map((message: { _source?: any }) => {
      return message._source;
    });
  }
}
