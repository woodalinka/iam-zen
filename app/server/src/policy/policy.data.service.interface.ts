import { Context, PaginatedResults } from '@cryptexlabs/codex-nodejs-common';
import { PolicyPersistedInterface } from './policy.persisted.interface';
import { PolicyUpdateInterface } from './policy.update.interface';
import { PolicyPatchInterface } from './policy.patch.interface';
import { QueryOptionsInterface } from '../query.options.interface';
import { PermissionPolicyInterface } from '../permission/permission.policy.interface';

export interface PolicyDataServiceInterface {
  getPoliciesByIds(
    context: Context,
    policyIds: string[],
  ): Promise<PolicyPersistedInterface[]>;
  savePoliciesFromConfig(
    context: Context,
    policies: PolicyPersistedInterface[],
  ): Promise<PolicyPersistedInterface[]>;
  getPolicyById(
    context: Context,
    permissionId: string,
  ): Promise<PolicyPersistedInterface>;
  getPolicyWithName(
    context: Context,
    name: string,
  ): Promise<PolicyPersistedInterface | null>;
  getPolicies(
    context: Context,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<PolicyPersistedInterface>>;

  getPoliciesForPermission(
    context: Context,
    permissionId: string,
  ): Promise<PolicyPersistedInterface[]>;

  deletePolicy(context: Context, policyId: string): Promise<boolean>;

  deletePolicies(context: Context, policyIds: string[]): Promise<boolean>;

  savePolicy(
    context: Context,
    policyId: string,
    policyUpdate: PolicyUpdateInterface,
  ): Promise<boolean>;

  patchPolicy(
    context: Context,
    policyId: string,
    policyPatch: PolicyPatchInterface,
  ): Promise<boolean>;

  addPermissions(
    context: Context,
    policyId: string,
    permissionIds: string[],
  ): Promise<boolean>;

  removePermissions(
    context: Context,
    policyId: string,
    permissionIds: string[],
  ): Promise<boolean>;
}
