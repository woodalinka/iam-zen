import { I18n } from 'i18n';
import * as packageJSON from '../../package.json';

const i18nData = {} as i18nAPI;

const i18nInstance = new I18n();

i18nInstance.configure({
  locales: packageJSON.i18n.languages,
  directory: __dirname,
  register: i18nData,
});

export { i18nData };
