import { TokenPairExpirationPolicyInterface } from '@cryptexlabs/authf-data-model';
import { StringUtil } from '@cryptexlabs/codex-nodejs-common';

export class TokenUtil {
  static getTokenExpirationPolicyHash(
    expirationPolicy: TokenPairExpirationPolicyInterface,
  ) {
    return StringUtil.insecureMd5(JSON.stringify(expirationPolicy));
  }
}
