import { Controller, Get, HttpStatus, Inject } from '@nestjs/common';
import { HealthzResponse } from '@cryptexlabs/codex-nodejs-common';
import { Locale } from '@cryptexlabs/codex-data-model';
import { Config } from './config';
import { UserDataServiceInterface } from './user/persistence/user.data.service.interface';

@Controller('healthz')
export class HealthzController {
  constructor(
    private readonly fallbackLocale: Locale,
    @Inject('CONFIG') private readonly config: Config,
    @Inject('USER_DATA_SERVICE')
    private readonly userDataServiceInterface: UserDataServiceInterface,
  ) {}

  @Get()
  public async root() {
    await this.userDataServiceInterface.healthz();
    return new HealthzResponse(HttpStatus.OK, this.fallbackLocale, this.config);
  }
}
