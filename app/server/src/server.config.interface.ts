import { TokenPairExpirationPolicyInterface } from '@cryptexlabs/authf-data-model';
import { GroupConfigInterface } from './group/group-config.interface';
import { PolicyConfigInterface } from './policy/policy-config.interface';
import { IdentityLinkInterface } from './user/identity-link/identity-link.interface';
import { UserConfigInterface } from './user/user.config.interface';
import { PermissionConfigInterface } from './permission/permission.config.interface';
import { RoleConfigInterface } from './role/role-config.interface';
import { AppConfigInterface } from './app/app.config.interface';

export interface NewUserConfigInterface {
  groups?: string[];
  identityLinks?: IdentityLinkInterface[];
}

export interface NewGroupConfigInterface {
  organization?: string;
  policies?: string[];
}

export interface CognitoIdentityLinkConfigInterface {
  poolId: string;
  developerProviderName: string;
}

export interface IdentityLinkConfigInterface {
  cognito?: CognitoIdentityLinkConfigInterface;
}

export interface ServerConfigInterface {
  token: {
    expirationPolicy: TokenPairExpirationPolicyInterface;
  };
  policies?: PolicyConfigInterface[];
  groups?: GroupConfigInterface[];
  roles?: RoleConfigInterface[];
  apps?: AppConfigInterface[];
  users?: UserConfigInterface[];
  permissions?: PermissionConfigInterface[];
  newUser?: NewUserConfigInterface;
  newGroup?: NewGroupConfigInterface;
  identityLink?: IdentityLinkConfigInterface;
}
