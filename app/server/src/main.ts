import { NestFactory } from '@nestjs/core';
import { MainModule } from './main.module';
import * as packageJSON from '../package.json';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { appConfig } from './setup';
import { LoadConfigService } from './load-config.service';

const asciiText = `
    ____   ___       __  ___     _____            
   /  _/  /   |     /  |/  /    /__  /  ___  ____ 
   / /   / /| |    / /|_/ /       / /  / _ \\/ __ \\
 _/ /   / ___ |   / /  / /       / /__/  __/ / / /
/___/  /_/  |_|  /_/  /_/       /____/\\___/_/ /_/ 
                                                 
****************************************************
*                                                  *
*     IDENTITY AND ACCESS MANAGEMENT SERVER        *
*                                                  *
****************************************************             

`;
async function rest() {
  const config = appConfig;
  const app = await NestFactory.create(MainModule);

  const docOptions = new DocumentBuilder()
    .setTitle(packageJSON.name)
    .setDescription(packageJSON.description)
    .setVersion(packageJSON.version)
    .addBearerAuth({ in: 'header', type: 'http' })
    .build();

  const document = SwaggerModule.createDocument(app, docOptions);
  SwaggerModule.setup(
    `${config.docsPrefix}/${config.apiVersion}${config.docsPath}`,
    app,
    document,
    {
      swaggerOptions: {
        docExpansion: false,
      },
    },
  );
  app.enableCors({
    origin: true,
    methods: '*',
    allowedHeaders: '*',
    credentials: true,
  });

  await app.listen(3000).then(() => {
    console.log(asciiText);
    if (['docker', 'localhost'].includes(appConfig.environmentName)) {
      const port =
        appConfig.environmentName === 'localhost' ? appConfig.httpPort : 3000;
      console.log(`Swagger docs available on http://localhost:${port}/${config.docsPrefix}/${config.apiVersion}${config.docsPath}
      `);
    }
  });
}

const loadConfig = async () => {
  const app = await NestFactory.createApplicationContext(MainModule);

  const loadConfigService = app.get(LoadConfigService);

  console.log(asciiText);

  await loadConfigService.loadConfig();

  process.exit(0);
};

const mode = process.env.MODE || 'rest';

const modeMap = {
  rest,
  loadConfig,
};

modeMap[mode]();
