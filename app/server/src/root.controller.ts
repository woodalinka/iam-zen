import { Controller, Get, Res } from '@nestjs/common';

@Controller()
export class RootController {
  @Get()
  public async root(@Res() res) {
    if (process.env.UI_PREFIX) {
      res.redirect(process.env.UI_PREFIX);
    }
  }
}
