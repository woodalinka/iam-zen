import { ApiKeyInterface } from './api-key.interface';
import { ApiAuthAuthenticateDataInterface } from '@cryptexlabs/authf-data-model';

export interface NewApiKeyInterface {
  apiKey: ApiKeyInterface;
  authenticationData: ApiAuthAuthenticateDataInterface;
}
