import * as crypto from 'crypto';
import { ApiAuthAuthenticateDataInterface } from '@cryptexlabs/authf-data-model';

export class ApiKeyUtil {
  public static randomIdString(size: number) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < size; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  public static randomSecretString(size: number) {
    return crypto.randomBytes(size).toString('hex');
  }

  public static generateApiAuth(): ApiAuthAuthenticateDataInterface {
    return {
      apiKey: this.randomIdString(20),
      secret: this.randomSecretString(20),
    };
  }
}
