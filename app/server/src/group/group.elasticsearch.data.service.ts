import { Inject, Injectable } from '@nestjs/common';
import { GroupDataServiceInterface } from './group.data.service.interface';
import { GroupPersistedInterface } from './group.persisted.interface';
import {
  Context,
  CustomLogger,
  ElasticsearchInitializer,
  PaginatedResults,
} from '@cryptexlabs/codex-nodejs-common';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { GroupUpdateInterface } from './group.update.interface';
import { GroupPatchInterface } from './group.patch.interface';
import { QueryOptionsInterface } from '../query.options.interface';
import { GroupInterface } from './group.interface';
import { RoleInterface } from '../role/role.interface';

@Injectable()
export class GroupElasticsearchDataService
  implements GroupDataServiceInterface
{
  private static get DEFAULT_INDEX() {
    return 'group';
  }

  private readonly elasticsearchInitializer: ElasticsearchInitializer;

  constructor(
    @Inject('LOGGER') private readonly logger: CustomLogger,
    private readonly elasticsearchService: ElasticsearchService,
  ) {
    this.elasticsearchInitializer = new ElasticsearchInitializer(
      logger,
      GroupElasticsearchDataService.DEFAULT_INDEX,
      elasticsearchService,
      {
        id: {
          type: 'keyword',
        },
        policies: {
          type: 'keyword',
        },
        name: {
          type: 'text',
          fields: {
            keyword: {
              type: 'keyword',
              normalizer: 'lowercase_normalizer',
            },
          },
        },
        description: {
          type: 'text',
        },
      },
    );
  }

  public async initializeIndex() {
    const exists = await this.elasticsearchService.indices.exists({
      index: GroupElasticsearchDataService.DEFAULT_INDEX,
    });

    if (!exists) {
      await this.elasticsearchService.indices.create({
        index: GroupElasticsearchDataService.DEFAULT_INDEX,
        body: {
          settings: {
            analysis: {
              normalizer: {
                lowercase_normalizer: {
                  type: 'custom',
                  char_filter: [],
                  filter: ['lowercase'],
                },
              },
            },
          },
        },
      });
    }

    await this.elasticsearchInitializer.initializeIndex();
  }

  public async getGroupWithId(
    context: Context,
    groupId: string,
  ): Promise<GroupPersistedInterface> {
    const response = await this.elasticsearchService.search({
      index: GroupElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                term: {
                  id: groupId,
                },
              },
            ],
          },
        },
      },
    });

    if (response.hits.total === 0) {
      return null;
    }

    return response.hits.hits[0]._source as any;
  }

  public async getGroupsByIds(
    context: Context,
    groupIds: string[],
  ): Promise<GroupPersistedInterface[]> {
    const response = await this.elasticsearchService.search({
      index: GroupElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                terms: {
                  id: groupIds,
                },
              },
            ],
          },
        },
      },
    });

    return response.hits.hits.map((message: { _source?: any }) => {
      return message._source;
    });
  }

  public async saveGroupsFromConfig(
    context: Context,
    groups: GroupPersistedInterface[],
  ): Promise<GroupPersistedInterface[]> {
    if (groups.length === 0) {
      return [];
    }
    const response = await this.elasticsearchService.search({
      index: GroupElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                terms: {
                  id: groups.map((group) => group.id),
                },
              },
            ],
          },
        },
      },
    });

    const existingGroups = response.hits.hits;

    const existingGroupIds = existingGroups.map(
      (existingGroup) => (existingGroup._source as any).id,
    );

    const newGroups = groups.filter(
      (group) => !existingGroupIds.includes(group.id),
    );
    const newGroupsBulk = [];
    for (const newGroup of newGroups) {
      newGroupsBulk.push({
        index: {
          _index: GroupElasticsearchDataService.DEFAULT_INDEX,
          _id: newGroup.id,
        },
      });
      newGroupsBulk.push({
        ...newGroup,
      });
    }

    const existingGroupsBulk = [];
    for (const existingGroup of existingGroups) {
      const updatedGroup = groups.find(
        (group) => group.id === (existingGroup._source as any).id,
      );
      existingGroupsBulk.push({
        index: {
          _index: GroupElasticsearchDataService.DEFAULT_INDEX,
          _id: existingGroup._id,
        },
      });
      existingGroupsBulk.push({
        ...(existingGroup._source as any),
        name: updatedGroup.name,
        description: updatedGroup.description
          ? updatedGroup.description
          : (existingGroup._source as any).description,
        policies: updatedGroup.policies,
      });
    }

    const bulk = [...newGroupsBulk, ...existingGroupsBulk];

    await this.elasticsearchService.bulk({ body: bulk });

    return newGroups;
  }

  public async getGroups(
    context: Context,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<GroupPersistedInterface>> {
    let query: { query?: any; sort?: any } = {};
    if (!options.search && options.searchName) {
      query = {
        query: {
          wildcard: {
            name: {
              value: `*${options.searchName}*`,
              boost: 1.0,
              rewrite: 'constant_score',
            },
          },
        },
      };
    }
    if (options.search) {
      query = {
        query: {
          query_string: {
            query: `*${options.search}*`,
            fields: ['name', 'description'],
          },
        },
      };
    }
    const sortMap = {
      name: [{ 'name.keyword': options.orderDirection || 'asc' }, '_score'],
      policyCount: {
        _script: {
          type: 'number',
          script: {
            lang: 'painless',
            source: 'doc.policies.size()',
          },
          order: options.orderDirection || 'asc',
        },
      },
    };

    if (options.orderBy) {
      const sort = sortMap[options.orderBy];
      query = {
        ...query,
        sort,
      };
    }
    const response = await this.elasticsearchService.search({
      index: GroupElasticsearchDataService.DEFAULT_INDEX,
      track_total_hits: true,
      body: {
        from: (page - 1) * pageSize,
        size: pageSize,
        ...query,
      },
    });

    return {
      total: (response.hits.total as any).value as number,
      results: response.hits.hits.map((message: { _source?: any }) => {
        return message._source;
      }),
    };
  }

  public async deleteGroup(
    context: Context,
    groupId: string,
  ): Promise<boolean> {
    const response = await this.elasticsearchService.deleteByQuery({
      index: GroupElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          match: {
            id: groupId,
          },
        },
      },
    });
    return response.total === 1;
  }

  public async deleteGroups(
    context: Context,
    groupIds: string[],
  ): Promise<boolean> {
    const response = await this.elasticsearchService.deleteByQuery({
      index: GroupElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                terms: {
                  id: groupIds,
                },
              },
            ],
          },
        },
      },
    });
    return response.total === groupIds.length;
  }

  public async patchGroup(
    context: Context,
    groupId: string,
    groupPatch: GroupPatchInterface,
  ): Promise<boolean> {
    const response = await this.elasticsearchService.updateByQuery({
      index: GroupElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
             if (params.name != null) {
               ctx._source.name = params.name
             }
             if (params.description != null) {
               ctx._source.description = params.description
             }
            `,
          params: {
            name: groupPatch.name,
            description: groupPatch.description,
          },
        },
        query: {
          match: {
            id: groupId,
          },
        },
      },
    });
    return response.total === 1;
  }

  public async saveGroup(
    context: Context,
    groupId: string,
    groupUpdate: GroupUpdateInterface,
  ): Promise<boolean> {
    const response = await this.elasticsearchService.updateByQuery({
      index: GroupElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
             ctx._source.name = params.name;
             ctx._source.description = params.description;
             ctx._source.policies = params.policies;
            `,
          params: {
            name: groupUpdate.name,
            description: groupUpdate.description,
            policies: groupUpdate.policies,
          },
        },
        query: {
          match: {
            id: groupId,
          },
        },
      },
    });
    if (response.total === 1) {
      return true;
    } else {
      await this.elasticsearchService.index({
        index: GroupElasticsearchDataService.DEFAULT_INDEX,
        id: groupId,
        body: {
          id: groupId,
          ...groupUpdate,
        },
      });
      return false;
    }
  }

  public async addPolicies(
    context: Context,
    groupId: string,
    policyIds: string[],
  ): Promise<boolean> {
    const response = await this.elasticsearchService.updateByQuery({
      index: GroupElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
           if (!(ctx._source.policies instanceof List)) {
             ctx._source.policies = new ArrayList();
           }
           ctx._source.policies.addAll(params.policies);
           ctx._source.policies = ctx._source.policies.stream().distinct().sorted().collect(Collectors.toList());
          `,
          params: {
            policies: policyIds,
          },
        },
        query: {
          match: {
            id: groupId,
          },
        },
      },
    });
    return response.total === 1;
  }

  public async removePolicies(
    context: Context,
    groupId: string,
    policyIds: string[],
  ): Promise<boolean> {
    const response = await this.elasticsearchService.updateByQuery({
      index: GroupElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
            if (!(ctx._source.policies instanceof List)) {
              ctx._source.policies = new ArrayList();
            }
            ctx._source.policies.removeAll(params.policies);
          `,
          params: {
            policies: policyIds,
          },
        },
        query: {
          match: {
            id: groupId,
          },
        },
      },
    });

    return response.total === 1;
  }

  public async getGroupWithName(
    context: Context,
    name: string,
  ): Promise<GroupPersistedInterface | null> {
    const response = await this.elasticsearchService.search({
      index: GroupElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                term: {
                  'name.keyword': name,
                },
              },
            ],
          },
        },
      },
    });

    if (response.hits.total === 0) {
      return null;
    }

    return response.hits.hits[0]._source as any;
  }

  async getGroupsForPolicy(
    context: Context,
    policyId: string,
  ): Promise<GroupInterface[]> {
    const response = await this.elasticsearchService.search({
      index: GroupElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                term: {
                  policies: policyId,
                },
              },
            ],
          },
        },
      },
    });

    return response.hits.hits.map((message: { _source?: any }) => {
      return message._source;
    });
  }

  async getGroupsForPolicies(
    context: Context,
    policyIds: string[],
  ): Promise<RoleInterface[]> {
    const response = await this.elasticsearchService.search({
      index: GroupElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                terms: {
                  policies: policyIds,
                },
              },
            ],
          },
        },
      },
    });

    return response.hits.hits.map((message: { _source?: any }) => {
      return message._source;
    });
  }
}
