import {
  Body,
  Controller,
  Delete,
  Get,
  Headers,
  HttpCode,
  HttpStatus,
  Inject,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Put,
  Query,
  Res,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  ApiMetaHeaders,
  ApiPagination,
  ContextBuilder,
  NotEmptyPipe,
  RestPaginatedResponse,
  RestResponse,
  SimpleHttpResponse,
} from '@cryptexlabs/codex-nodejs-common';
import { GroupService } from './group.service';
import { ExampleSimpleResponse } from '../example/example.simple.response';
import { LocalesEnum } from '../locale/enum';
import { GroupPatchInterface } from './group.patch.interface';
import { GroupUpdateInterface } from './group.update.interface';
import { exampleGroup, groupPatchExample, groupUpdateExample } from './example';
import { appConfig } from '../setup';
import { GroupListAuthzGuard } from './group.list.authz.guard';
import { GroupGetAuthzGuard } from './group.get.authz.guard';
import { GroupDeleteAuthzGuard } from './group.delete.authz.guard';
import { GroupUpdateAuthzGuard } from './group.update.authz.guard';
import { GroupCreateAuthzGuard } from './group.create.authz.guard';
import { GroupAttachPoliciesAuthzGuard } from './group.attach-policies.authz.guard';
import { GroupDetachPoliciesAuthzGuard } from './group.detach-policies.authz.guard';
import { ExampleRestPaginatedResponse } from '../example/example.rest.paginated.response';
import { GroupDeleteListAuthzGuard } from './group.delete-list.authz.guard';
import { ExampleRestResponse } from '../example/example.rest.response';
import { GroupExistsAuthzGuard } from './group.exists.authz.guard';
import { GroupPatchValidationPipe } from './group.patch.validation.pipe';
import { GroupUpdateValidationPipe } from './group.update.validation.pipe';
import { Uuidv4ArrayValidationPipe } from '@cryptexlabs/codex-nodejs-common/lib/src/pipe/uuidv4.array.validation.pipe';
import { OrderDirectionEnum } from '../query.options.interface';
import { OrderDirectionValidationPipe } from '../order.direction.validation.pipe';
import { UserService } from '../user/user.service';

@Controller(`/${appConfig.appPrefix}/${appConfig.apiVersion}/group`)
@ApiTags('group')
@ApiMetaHeaders()
@ApiBearerAuth()
export class GroupController {
  constructor(
    private readonly groupService: GroupService,
    private readonly userService: UserService,
    @Inject('CONTEXT_BUILDER') private readonly contextBuilder: ContextBuilder,
  ) {}

  @Get()
  @ApiOperation({
    summary: 'Get list of groups',
  })
  @ApiPagination()
  @ApiQuery({
    name: 'orderBy',
    required: false,
    schema: {
      example: 'name',
    },
  })
  @ApiQuery({
    name: 'orderDirection',
    required: false,
    schema: {
      example: 'asc',
    },
  })
  @ApiQuery({
    name: 'searchName',
    required: false,
  })
  @ApiQuery({
    name: 'search',
    required: false,
  })
  @UseGuards(GroupListAuthzGuard)
  @ApiResponse({
    schema: {
      example: new ExampleRestPaginatedResponse(
        HttpStatus.OK,
        'cryptexlabs.iam-zen.group.list',
        [exampleGroup],
      ),
    },
    status: HttpStatus.OK,
  })
  public async getGroups(
    @Query('page', ParseIntPipe) page: number,
    @Query('pageSize', ParseIntPipe) pageSize: number,
    @Query('searchName') searchName: string,
    @Query('search') search: string,
    @Query('orderBy') orderBy: string,
    @Query('orderDirection', OrderDirectionValidationPipe)
    orderDirection: OrderDirectionEnum,
    @Headers() headers: Record<string, string>,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const groups = await this.groupService.getGroups(context, page, pageSize, {
      searchName,
      search,
      orderBy,
      orderDirection,
    });

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestPaginatedResponse(
      responseContext,
      HttpStatus.OK,
      groups.total,
      'cryptexlabs.iam-zen.group.list',
      groups.results,
    );
  }

  @Get(':groupId')
  @ApiOperation({
    summary: 'Get a group by group ID',
  })
  @ApiParam({
    name: 'groupId',
    example: 'cf24881f-0983-40e2-a6d1-2e560863e89e',
  })
  @UseGuards(GroupGetAuthzGuard)
  public async getGroup(@Param('groupId') groupId: string, @Headers() headers) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const group = await this.groupService.getGroupById(context, groupId);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestResponse(
      responseContext,
      HttpStatus.OK,
      'cryptexlabs.iam-zen.group',
      group,
    );
  }

  @Get('any/exists')
  @ApiOperation({
    summary: 'Get whether or not a group exists',
  })
  @ApiQuery({
    name: 'name',
    example: 'admin',
    required: true,
  })
  @UseGuards(GroupExistsAuthzGuard)
  @ApiResponse({
    schema: {
      example: new ExampleRestResponse(HttpStatus.OK, 'boolean', false),
    },
  })
  public async getGroupExists(
    @Query('name', NotEmptyPipe) name: string,
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const groupExists = await this.groupService.getGroupExists(context, name);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestResponse(
      responseContext,
      HttpStatus.OK,
      'boolean',
      groupExists,
    );
  }

  @Delete(':groupId')
  @ApiOperation({
    summary: 'Delete group by group ID',
  })
  @ApiParam({
    name: 'groupId',
    example: '287683c2-d76f-4e2d-9fdd-d83baef01cbb',
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(GroupDeleteAuthzGuard)
  public async deleteGroup(
    @Param('groupId', NotEmptyPipe) groupId: string,
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    await this.groupService.deleteGroup(context, groupId);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Delete()
  @ApiOperation({
    summary: 'Delete groups by group ID',
  })
  @ApiQuery({
    name: 'ids',
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(GroupDeleteListAuthzGuard)
  public async deleteGroups(
    @Query('ids', NotEmptyPipe) groupIds: string[],
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const useGroupIds = Array.isArray(groupIds) ? groupIds : [groupIds];

    await this.groupService.deleteGroups(context, useGroupIds);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Put(':groupId')
  @ApiOperation({
    summary: 'Create or update group by group ID',
  })
  @ApiParam({
    name: 'groupId',
    example: '287683c2-d76f-4e2d-9fdd-d83baef01cbb',
    required: true,
  })
  @ApiBody({
    schema: {
      example: groupUpdateExample,
    },
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(GroupCreateAuthzGuard)
  public async saveGroup(
    @Param('groupId', NotEmptyPipe) groupId: string,
    @Headers() headers,
    @Res() res,
    @Body(NotEmptyPipe, GroupUpdateValidationPipe)
    groupUpdate: GroupUpdateInterface,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const status = await this.groupService.updateGroup(
      context,
      groupId,
      groupUpdate,
    );

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    res
      .status(status)
      .send(
        new SimpleHttpResponse(
          responseContext,
          HttpStatus.ACCEPTED,
          LocalesEnum.SUCCESS,
        ),
      );
  }

  @Patch(':groupId')
  @ApiOperation({
    summary: 'Partially update group by group ID',
  })
  @ApiParam({
    name: 'groupId',
    example: '287683c2-d76f-4e2d-9fdd-d83baef01cbb',
    required: true,
  })
  @ApiBody({
    schema: {
      example: groupPatchExample,
    },
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(GroupUpdateAuthzGuard)
  public async patchGroup(
    @Param('groupId', NotEmptyPipe) groupId: string,
    @Body(NotEmptyPipe, GroupPatchValidationPipe)
    groupPatch: GroupPatchInterface,
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    await this.groupService.patchGroup(context, groupId, groupPatch);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Post(':groupId/policy')
  @ApiOperation({
    summary: 'Add policies to group',
  })
  @ApiParam({
    name: 'groupId',
    example: '613b6c12-d9c1-47cf-b401-0b5b03ef33d0',
    required: true,
  })
  @ApiBody({
    schema: {
      example: ['ac5c44eb-501d-4b53-bcca-9aac58b438de'],
    },
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(GroupAttachPoliciesAuthzGuard)
  public async addPolicies(
    @Param('groupId', NotEmptyPipe) groupId: string,
    @Body(NotEmptyPipe, Uuidv4ArrayValidationPipe) policies: string[],
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    await this.groupService.addPolicies(context, groupId, policies);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Delete(':groupId/policy')
  @ApiOperation({
    summary: 'Remove policies from group',
  })
  @ApiParam({
    name: 'groupId',
    example: '613b6c12-d9c1-47cf-b401-0b5b03ef33d0',
    required: true,
  })
  @ApiQuery({
    name: 'ids',
    example: [
      'ac5c44eb-501d-4b53-bcca-9aac58b438de',
      'ec570e6e-caaf-43f6-ac6b-cea8b7949124',
    ],
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(GroupDetachPoliciesAuthzGuard)
  public async removePolicies(
    @Param('groupId', NotEmptyPipe) groupId: string,
    @Query('ids', NotEmptyPipe) ids: string[],
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const policyIds = typeof ids === 'string' ? [ids] : ids;

    await this.groupService.removePolicies(context, groupId, policyIds);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Get('any/exists')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Checks if group exists by name',
  })
  @ApiQuery({
    name: 'name',
    example: 'A group name',
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleRestResponse(
        HttpStatus.OK,
        'cryptexlabs.iam-zen.group.exists',
        true,
      ),
    },
  })
  @UseGuards(GroupCreateAuthzGuard)
  public async getGroupNameExists(
    @Headers() headers,
    @Query('name') name: string,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const exists = await this.groupService.existsByName(context, name);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestResponse(
      responseContext,
      HttpStatus.OK,
      'cryptexlabs.iam-zen.user.exists',
      exists,
    );
  }

  @Get(':groupId/user')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Get list of users for a group',
  })
  @ApiPagination()
  @ApiParam({
    name: 'groupId',
  })
  @ApiQuery({
    name: 'orderBy',
    required: false,
    schema: {
      example: 'name',
    },
  })
  @ApiQuery({
    name: 'orderDirection',
    required: false,
    schema: {
      example: 'asc',
    },
  })
  @ApiQuery({
    name: 'search',
    required: false,
  })
  public async getUsersForGroup(
    @Param('groupId', NotEmptyPipe) groupId: string,
    @Query('page', ParseIntPipe) page: number,
    @Query('pageSize', ParseIntPipe) pageSize: number,
    @Query('search') search: string,
    @Query('orderBy') orderBy: string,
    @Query('orderDirection', OrderDirectionValidationPipe)
    orderDirection: OrderDirectionEnum,
    @Headers() headers: Record<string, string>,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const users = await this.userService.getUsersForGroup(
      context,
      groupId,
      page,
      pageSize,
      {
        search,
        orderBy,
        orderDirection,
      },
    );

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestPaginatedResponse(
      responseContext,
      HttpStatus.OK,
      users.total,
      'cryptexlabs.iam-zen.group.list',
      users.results,
    );
  }
}
