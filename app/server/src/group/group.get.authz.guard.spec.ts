import { GroupGetAuthzGuard } from './group.get.authz.guard';
import * as jwt from 'jsonwebtoken';
import { AuthzNamespace } from '../constants';
import { ExecutionContext } from '@nestjs/common';

describe(GroupGetAuthzGuard.name, () => {
  it('Should allow a super admin to get a group', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::any:any:any`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            groupId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          body: ['680dddec-f0b9-4a01-b8b5-be725f946935'],
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupGetAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to get any object to get the group', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::any:any:get`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            groupId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupGetAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to get a specific group to get the specific group', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::group:4d2114ca-24e2-43e5-bddb-d9a6688b8340:get`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            groupId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupGetAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to do anything to a specific group to get the group', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::group:4d2114ca-24e2-43e5-bddb-d9a6688b8340:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            groupId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupGetAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to get any group to get the group', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::group:any:get`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            groupId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupGetAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should not allow someone with permission to get a different specific group to get the specific group', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::group:4d2114ca-24e2-43e5-bddb-d9a6688b8340:get`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            groupId: '3630a7ed-fef5-4345-9947-c54e8d15f954',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupGetAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should not allow someone with permission to do anything to a different specific group to get the group', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::group:4d2114ca-24e2-43e5-bddb-d9a6688b8340:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            groupId: '3630a7ed-fef5-4345-9947-c54e8d15f954',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupGetAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });
});
