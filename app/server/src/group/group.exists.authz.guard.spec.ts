import { GroupExistsAuthzGuard } from './group.exists.authz.guard';
import * as jwt from 'jsonwebtoken';
import { AuthzNamespace } from '../constants';
import { ExecutionContext } from '@nestjs/common';

describe(GroupExistsAuthzGuard.name, () => {
  it('Should allow a super admin to get a group existence', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::any:any:any`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            name: 'example',
          },
          body: ['680dddec-f0b9-4a01-b8b5-be725f946935'],
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupExistsAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to get any object to get the group existence', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::any:any:get`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            name: 'example',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupExistsAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to list any object to get the group existence', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::any::list`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            name: 'example',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupExistsAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to list groups to get the group existence', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::group::list`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            name: 'example',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupExistsAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should not allow someone with permission to get a specific group to get the specific group existence', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::group:4d2114ca-24e2-43e5-bddb-d9a6688b8340:get`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            name: 'example',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupExistsAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should allow someone with permission to create groups to get the group existence', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::group:any:create`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            name: 'example',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupExistsAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to create a specific group to get the specific group existence', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::group:4d2114ca-24e2-43e5-bddb-d9a6688b8340:create`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            name: 'example',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupExistsAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to create any object to get the specific group existence', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::any:any:create`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            name: 'example',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupExistsAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Could not allow someone with permission to do anything to a specific group to get the group existence', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::group:4d2114ca-24e2-43e5-bddb-d9a6688b8340:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            name: 'example',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupExistsAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should allow someone with permission to get any group to get the group existence', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::group:any:get`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            name: 'example',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupExistsAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Could not allow someone with permission to get a specific group to get the specific group existence', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::group:4d2114ca-24e2-43e5-bddb-d9a6688b8340:get`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            name: 'example',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupExistsAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });
});
