import { GroupDetachPoliciesAuthzGuard } from './group.detach-policies.authz.guard';
import { ExecutionContext } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { AuthzNamespace } from '../constants';

describe(GroupDetachPoliciesAuthzGuard.name, () => {
  it('Should allow super admin to detach a policy from a group', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::any:any:any:any:any:any:any:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            groupId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupDetachPoliciesAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to detach any policy from a group to detach a policy to the group', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::group:4d2114ca-24e2-43e5-bddb-d9a6688b8340::policy:any:delete`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            groupId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupDetachPoliciesAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to do anything to any permission on a group to detach a policy to the group', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::group:4d2114ca-24e2-43e5-bddb-d9a6688b8340::policy:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            groupId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupDetachPoliciesAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to do anything to any sub object for a group to detach a policy to the group', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::group:4d2114ca-24e2-43e5-bddb-d9a6688b8340::any:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            groupId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupDetachPoliciesAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to detach a specific policy from a group to detach the policy to the group', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::group:4d2114ca-24e2-43e5-bddb-d9a6688b8340::policy:680dddec-f0b9-4a01-b8b5-be725f946935:delete`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            groupId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupDetachPoliciesAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should not allow someone with permission to detach any policy to a different group to detach a policy to the group', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::group:4d2114ca-24e2-43e5-bddb-d9a6688b8340::policy:any:delete`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            groupId: '001d4f53-798b-4a0b-8ef7-330a7bf72147',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupDetachPoliciesAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should not allow someone with permission to do anything to any permission on a different group to detach a policy to the group', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::group:4d2114ca-24e2-43e5-bddb-d9a6688b8340::policy:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            groupId: '001d4f53-798b-4a0b-8ef7-330a7bf72147',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupDetachPoliciesAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should not allow someone with permission to do anything to any sub object for a different group to detach a policy to the group', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::group:4d2114ca-24e2-43e5-bddb-d9a6688b8340::any:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            groupId: '001d4f53-798b-4a0b-8ef7-330a7bf72147',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupDetachPoliciesAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should not allow someone with permission to detach a specific policy to a different group to detach the policy to the group', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::group:4d2114ca-24e2-43e5-bddb-d9a6688b8340::policy:680dddec-f0b9-4a01-b8b5-be725f946935:delete`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            groupId: '001d4f53-798b-4a0b-8ef7-330a7bf72147',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupDetachPoliciesAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should not allow someone with permission to detach a different specific permission from a group to detach the policy to the group', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::group:4d2114ca-24e2-43e5-bddb-d9a6688b8340::policy:680dddec-f0b9-4a01-b8b5-be725f946935:delete`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            groupId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          query: { ids: ['5be3176f-c066-4418-b682-18e16fd07b84'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new GroupDetachPoliciesAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });
});
