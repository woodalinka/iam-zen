import { GroupPersistedInterface } from './group.persisted.interface';
import { Context, PaginatedResults } from '@cryptexlabs/codex-nodejs-common';
import { GroupUpdateInterface } from './group.update.interface';
import { GroupPatchInterface } from './group.patch.interface';
import { QueryOptionsInterface } from '../query.options.interface';
import { GroupInterface } from './group.interface';
import { RoleInterface } from '../role/role.interface';

export interface GroupDataServiceInterface {
  getGroupsByIds(
    context: Context,
    groupIds: string[],
  ): Promise<GroupPersistedInterface[]>;
  saveGroupsFromConfig(
    context: Context,
    groups: GroupPersistedInterface[],
  ): Promise<GroupPersistedInterface[]>;
  getGroupWithId(
    context: Context,
    groupId: string,
  ): Promise<GroupPersistedInterface | null>;

  getGroupWithName(
    context: Context,
    name: string,
  ): Promise<GroupPersistedInterface | null>;

  getGroups(
    context: Context,
    page: number,
    pageSize: number,
    options: QueryOptionsInterface,
  ): Promise<PaginatedResults<GroupPersistedInterface>>;

  deleteGroup(context: Context, groupId: string): Promise<boolean>;

  deleteGroups(context: Context, groupIds: string[]): Promise<boolean>;

  saveGroup(
    context: Context,
    groupId: string,
    groupUpdate: GroupUpdateInterface,
  ): Promise<boolean>;

  patchGroup(
    context: Context,
    groupId: string,
    groupPatch: GroupPatchInterface,
  ): Promise<boolean>;

  addPolicies(
    context: Context,
    groupId: string,
    policyIds: string[],
  ): Promise<boolean>;

  removePolicies(
    context: Context,
    groupId: string,
    policyIds: string[],
  ): Promise<boolean>;

  getGroupsForPolicy(
    context: Context,
    policyId: string,
  ): Promise<GroupInterface[]>;

  getGroupsForPolicies(
    context: Context,
    policyIds: string[],
  ): Promise<RoleInterface[]>;
}
