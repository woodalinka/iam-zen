export interface GroupPatchInterface {
  name?: string;
  description?: string;
}
