import { GroupService } from './group.service';
import { Config } from '../config';
import { instance, mock, verify, when } from 'ts-mockito';
import { GroupDataServiceInterface } from './group.data.service.interface';
import { GroupPoliciesProviderInterface } from './group.policies.provider.interface';
import { PolicyService } from '../policy/policy.service';
import { Context } from '@cryptexlabs/codex-nodejs-common';
import { ImmutableObjectError } from '../error/immutable-object.error';

describe(GroupService.name, () => {
  let context;
  beforeEach(() => {
    const ContextMock = mock(Context);
    context = instance(ContextMock);
    when(ContextMock.locale).thenReturn({
      country: 'US',
      language: 'en',
      i18n: 'en-US',
    });
  });

  describe('deleteGroups', () => {
    let groupService: GroupService;
    let GroupDataServiceMock: GroupDataServiceInterface;
    let ConfigMock: Config;

    beforeEach(() => {
      ConfigMock = mock(Config);
      const config = instance(ConfigMock);
      GroupDataServiceMock = mock<GroupDataServiceInterface>();
      const groupDataService = instance(GroupDataServiceMock);
      const GroupPoliciesProviderMock = mock<GroupPoliciesProviderInterface>();
      const groupPoliciesProvider = instance(GroupPoliciesProviderMock);
      const PolicyServiceMock = mock(PolicyService);
      const policyService = instance(PolicyServiceMock);

      groupService = new GroupService(
        config,
        groupDataService,
        groupPoliciesProvider,
        policyService,
        policyService,
      );
    });

    it('Should delete a list of mutable groups', async () => {
      const groupIds = ['e37d9360-66a5-4880-a46e-214abbfd259c'];

      when(ConfigMock.defaultGroups).thenReturn([]);
      when(GroupDataServiceMock.deleteGroups(context, groupIds)).thenResolve(
        true,
      );

      await groupService.deleteGroups(context, groupIds);

      verify(GroupDataServiceMock.deleteGroups(context, groupIds)).called();
    });

    it('Should not delete a list of groups if at least 1 group is immutable', async () => {
      const groupIds = [
        '74599229-f1b2-4975-83dc-f6bca13c24a2',
        'e37d9360-66a5-4880-a46e-214abbfd259c',
      ];

      when(ConfigMock.defaultGroups).thenReturn([
        { id: 'e37d9360-66a5-4880-a46e-214abbfd259c', name: 'hello' },
      ]);
      when(GroupDataServiceMock.deleteGroups(context, groupIds)).thenResolve(
        true,
      );

      await expect(
        groupService.deleteGroups(context, groupIds),
      ).rejects.toBeInstanceOf(ImmutableObjectError);

      verify(GroupDataServiceMock.deleteGroups(context, groupIds)).never();
    });
  });
});
