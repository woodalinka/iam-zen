import { GroupPersistedInterface } from './group.persisted.interface';
import { GroupPolicyInterface } from './group.policy.interface';
import { GroupPermissionInterface } from './group.permission.interface';

export class GroupDto {
  public readonly id: string;
  public readonly name: string;
  public readonly description: string;

  constructor(
    group: GroupPersistedInterface,
    public readonly policies: GroupPolicyInterface[],
    public readonly permissions: GroupPermissionInterface[],
    public readonly immutable,
  ) {
    this.id = group.id;
    this.name = group.name;
    this.description = group.description;
  }
}
