import { GroupInListDto } from './group.in-list.dto';

export const groupUpdateExample = {
  name: 'Widget Group 5',
  description: 'Group 5 for widget app',
  policies: ['ec570e6e-caaf-43f6-ac6b-cea8b7949124'],
};

export const groupPatchExample = {
  name: 'Widget Group 5',
  description: 'Group 5 for widget app',
};

export const exampleGroup = new GroupInListDto(
  {
    id: '0289fc05-a4f8-480e-8495-4640526fe1dc',
    ...groupUpdateExample,
  },
  true,
);
