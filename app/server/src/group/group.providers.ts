import { GroupService } from './group.service';
import { appConfig, elasticsearchService } from '../setup';
import { DatabaseTypeEnum } from '../data/database-type.enum';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { GroupDefaultLoaderService } from './group.default-loader.service';
import { GroupDataServiceInterface } from './group.data.service.interface';
import { ContextBuilder } from '@cryptexlabs/codex-nodejs-common';
import { Config } from '../config';
import { GroupElasticsearchDataService } from './group.elasticsearch.data.service';

const providers = [];

if (appConfig.dbType === DatabaseTypeEnum.ELASTICSEARCH) {
  providers.push(
    {
      provide: ElasticsearchService,
      useValue: elasticsearchService,
    },
    GroupElasticsearchDataService,
    {
      provide: 'GROUP_DATA_SERVICE',
      useFactory: async (service: GroupElasticsearchDataService) => {
        await service.initializeIndex();
        return service;
      },
      inject: [GroupElasticsearchDataService],
    },
  );
}

export const groupProviders = [
  ...providers,
  GroupService,
  {
    provide: 'USER_GROUPS_PROVIDER',
    useClass: GroupService,
  },
  {
    provide: 'GROUP_SERVICE',
    useClass: GroupService,
  },
  {
    provide: GroupDefaultLoaderService,
    useFactory: async (
      groupDataService: GroupDataServiceInterface,
      config: Config,
      contextBuilder: ContextBuilder,
    ) => {
      const service = new GroupDefaultLoaderService(
        groupDataService,
        config,
        contextBuilder,
      );
      if (appConfig.autoLoadConfig) {
        await service.load();
      }
      return service;
    },
    inject: ['GROUP_DATA_SERVICE', 'CONFIG', 'CONTEXT_BUILDER'],
  },
];
