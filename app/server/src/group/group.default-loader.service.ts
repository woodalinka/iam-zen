import { Inject, Injectable } from '@nestjs/common';
import { Config } from '../config';
import { ContextBuilder } from '@cryptexlabs/codex-nodejs-common';
import { GroupDataServiceInterface } from './group.data.service.interface';
import { GroupPersistedInterface } from './group.persisted.interface';

@Injectable()
export class GroupDefaultLoaderService {
  constructor(
    @Inject('GROUP_DATA_SERVICE')
    private readonly groupDataService: GroupDataServiceInterface,
    @Inject('CONFIG') private readonly config: Config,
    @Inject('CONTEXT_BUILDER') private readonly contextBuilder: ContextBuilder,
  ) {}

  public async load() {
    const persistedGroups: GroupPersistedInterface[] =
      this.config.defaultGroups.map((configGroup) => {
        return {
          name: configGroup.name,
          id: configGroup.id,
          description: configGroup.description || '',
          policies: configGroup.policies || [],
        };
      });

    const context = this.contextBuilder.build().getResult();

    await this.groupDataService.saveGroupsFromConfig(context, persistedGroups);
  }
}
