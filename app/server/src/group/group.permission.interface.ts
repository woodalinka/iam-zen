export interface GroupPermissionInterface {
  id: string;
  name: string;
  value: string;
}
