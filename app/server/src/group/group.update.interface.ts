export interface GroupUpdateInterface {
  name: string;
  description: string;
  policies: string[];
}
