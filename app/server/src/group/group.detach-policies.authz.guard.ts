import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { HttpAuthzDetachObjectsGuardUtil } from '@cryptexlabs/codex-nodejs-common';
import { AuthzNamespace } from '../constants';

@Injectable()
export class GroupDetachPoliciesAuthzGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const util = new HttpAuthzDetachObjectsGuardUtil(context);
    return util.isAuthorized(
      'group',
      util.params.groupId,
      'policy',
      util.query.ids,
      AuthzNamespace.object,
    );
  }
}
