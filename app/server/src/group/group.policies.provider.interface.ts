import { Context } from '@cryptexlabs/codex-nodejs-common';
import { GroupPolicyInterface } from './group.policy.interface';

export interface GroupPoliciesProviderInterface {
  getPoliciesForGroup(
    context: Context,
    groupId: string,
    policyIds: string[],
  ): Promise<GroupPolicyInterface[]>;
}
