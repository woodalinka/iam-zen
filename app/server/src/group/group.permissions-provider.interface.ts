import { GroupInterface } from './group.interface';
import { Context } from '@cryptexlabs/codex-nodejs-common';
import { PermissionForPolicyInterface } from '../permission/permission.for-policy.interface';

export interface GroupPermissionsProviderInterface {
  getPermissionsForGroup(
    context: Context,
    group: GroupInterface,
  ): Promise<PermissionForPolicyInterface[]>;
}
