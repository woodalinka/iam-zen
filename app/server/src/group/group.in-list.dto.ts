import { GroupPersistedInterface } from './group.persisted.interface';

export class GroupInListDto {
  public readonly id: string;
  public readonly name: string;
  public readonly policyCount: number;

  constructor(group: GroupPersistedInterface, public readonly immutable) {
    this.id = group.id;
    this.name = group.name;
    this.policyCount = group.policies.length;
  }
}
