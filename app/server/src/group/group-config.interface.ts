export class GroupConfigInterface {
  name: string;
  id: string;
  description?: string;
  policies?: string[];
}
