import {
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { Config } from '../config';
import { UserGroupsProviderInterface } from '../user/group/user.groups.provider.interface';
import { UserGroupDetailedInterface } from '../user/group/user.group.interface';
import { GroupDataServiceInterface } from './group.data.service.interface';
import {
  ArrayUtil,
  Context,
  PaginatedResults,
} from '@cryptexlabs/codex-nodejs-common';
import { GroupInterface } from './group.interface';
import { GroupDto } from './group.dto';
import { GroupPoliciesProviderInterface } from './group.policies.provider.interface';
import { GroupInListDto } from './group.in-list.dto';
import { ImmutableObjectError } from '../error/immutable-object.error';
import { LocalesEnum } from '../locale/enum';
import { GroupUpdateInterface } from './group.update.interface';
import { GroupPatchInterface } from './group.patch.interface';
import { PolicyService } from '../policy/policy.service';
import { QueryOptionsInterface } from '../query.options.interface';
import { GroupPermissionsProviderInterface } from './group.permissions-provider.interface';
import { PolicyGroupInterface } from '../policy/policy.group.interface';
import { PermissionRoleInterface } from '../permission/permission.role.interface';
import { PermissionGroupInterface } from '../permission/permission.group.interface';

@Injectable()
export class GroupService implements UserGroupsProviderInterface {
  constructor(
    @Inject('CONFIG') private readonly config: Config,
    @Inject('GROUP_DATA_SERVICE')
    private readonly groupDataService: GroupDataServiceInterface,
    @Inject('GROUP_POLICIES_PROVIDER')
    private readonly groupPoliciesProvider: GroupPoliciesProviderInterface,
    @Inject('POLICY_SERVICE')
    private readonly policyService: PolicyService,
    @Inject('GROUP_PERMISSIONS_PROVIDER')
    private readonly permissionsProvider: GroupPermissionsProviderInterface,
  ) {}
  public async getGroupsByIds(
    context: Context,
    groupIds: string[],
  ): Promise<GroupInterface[]> {
    const persistedGroups = await this.groupDataService.getGroupsByIds(
      context,
      groupIds,
    );
    const persistedGroupIds = persistedGroups.map(
      (persistedGroup) => persistedGroup.id,
    );
    const configGroupsNotPersisted = this.config.defaultGroups.filter(
      (configGroup) =>
        !persistedGroupIds.includes(configGroup.id) &&
        groupIds.includes(configGroup.id),
    );

    const groups = persistedGroups.map((persistedGroup) => {
      return {
        name: persistedGroup.name,
        id: persistedGroup.id,
        description: persistedGroup.description,
        policies: persistedGroup.policies,
      };
    });
    for (const configGroup of configGroupsNotPersisted) {
      groups.push({
        name: configGroup.name,
        id: configGroup.id,
        description: configGroup.description || '',
        policies: configGroup.policies || [],
      });
    }
    return groups;
  }

  public async getGroupsByPoliciesForPermissions(
    context: Context,
    policyIds: string[],
  ): Promise<PermissionGroupInterface[]> {
    const groups = await this.groupDataService.getGroupsForPolicies(
      context,
      policyIds,
    );

    return groups.map((group) => {
      return {
        id: group.id,
        name: group.name,
        description: group.description,
      };
    });
  }

  public async getGroupsForPolicyId(
    context: Context,
    policyId: string,
  ): Promise<PolicyGroupInterface[]> {
    const groups = await this.groupDataService.getGroupsForPolicy(
      context,
      policyId,
    );

    return groups.map((group) => {
      let immutable = false;
      const configGroup = this.config.defaultGroups.find(
        (g) => g.id === group.id,
      );
      if (configGroup) {
        immutable = group.policies.includes(policyId);
      }

      return {
        id: group.id,
        name: group.name,
        description: group.description,
        immutable,
      };
    });
  }

  public async getGroupsForUser(
    context: Context,
    groupIds: string[],
  ): Promise<UserGroupDetailedInterface[]> {
    const groups = await this.getGroupsByIds(context, groupIds);
    return groups.map((group) => ({
      id: group.id,
      name: group.name,
      description: group.description,
      policies: group.policies,
    }));
  }

  public async getGroupById(
    context: Context,
    groupId: string,
  ): Promise<GroupDto> {
    const group = await this.groupDataService.getGroupWithId(context, groupId);
    if (!group) {
      throw new NotFoundException();
    }
    const immutable = !!this.config.defaultGroups.find(
      (configGroup) => configGroup.id === group.id,
    );

    const policies = await this.groupPoliciesProvider.getPoliciesForGroup(
      context,
      groupId,
      group.policies,
    );

    const permissions = await this.permissionsProvider.getPermissionsForGroup(
      context,
      group,
    );

    return new GroupDto(group, policies, permissions, immutable);
  }

  public async getGroupExists(
    context: Context,
    name: string,
  ): Promise<boolean> {
    const group = await this.groupDataService.getGroupWithName(context, name);
    return !!group;
  }

  public async getGroups(
    context: Context,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<GroupInListDto>> {
    const persistedGroups = await this.groupDataService.getGroups(
      context,
      page,
      pageSize,
      options,
    );
    return {
      total: persistedGroups.total,
      results: persistedGroups.results.map((persistedUser) => {
        const immutable = !!this.config.defaultGroups.find(
          (configGroup) => configGroup.id === persistedUser.id,
        );
        return new GroupInListDto(persistedUser, immutable);
      }),
    };
  }

  public async deleteGroup(context: Context, groupId: string): Promise<void> {
    this._immutabilityCheck(context, groupId);
    const didModify = await this.groupDataService.deleteGroup(context, groupId);
    if (!didModify) {
      throw new NotFoundException();
    }
  }

  public async deleteGroups(
    context: Context,
    groupIds: string[],
  ): Promise<void> {
    for (const groupId of groupIds) {
      this._immutabilityCheck(context, groupId);
    }
    const didModify = await this.groupDataService.deleteGroups(
      context,
      groupIds,
    );
    if (!didModify) {
      throw new NotFoundException();
    }
  }

  public async updateGroup(
    context: Context,
    groupId: string,
    permissionUpdate: GroupUpdateInterface,
  ): Promise<HttpStatus> {
    this._immutabilityCheck(context, groupId);
    const didModify = await this.groupDataService.saveGroup(
      context,
      groupId,
      permissionUpdate,
    );
    return didModify ? HttpStatus.ACCEPTED : HttpStatus.CREATED;
  }

  public async patchGroup(
    context: Context,
    groupId: string,
    groupPatch: GroupPatchInterface,
  ): Promise<void> {
    this._immutabilityCheck(context, groupId);
    const didModify = await this.groupDataService.patchGroup(
      context,
      groupId,
      groupPatch,
    );
    if (!didModify) {
      throw new NotFoundException();
    }
  }

  private _immutabilityCheck(context: Context, groupId: string) {
    const immutableGroup = this.config.defaultGroups.find(
      (findGroup) => findGroup.id === groupId,
    );
    if (immutableGroup) {
      throw new ImmutableObjectError(
        context,
        LocalesEnum.ERROR_IMMUTABLE_GROUP,
      );
    }
  }

  public async addPolicies(
    context: Context,
    groupId: string,
    policyIds: string[],
  ): Promise<void> {
    const foundPolicies = await this.policyService.getPoliciesByIds(
      context,
      policyIds,
    );

    if (foundPolicies.length !== policyIds.length) {
      throw new HttpException(`Invalid policies`, HttpStatus.BAD_REQUEST);
    }

    const didModify = await this.groupDataService.addPolicies(
      context,
      groupId,
      policyIds,
    );
    if (!didModify) {
      throw new NotFoundException();
    }
  }

  public async removePolicies(
    context: Context,
    groupId: string,
    policyIds: string[],
  ): Promise<void> {
    const immutableRole = this.config.defaultGroups.find(
      (findGroup) => findGroup.id === groupId,
    );

    if (immutableRole) {
      if (
        ArrayUtil.intersection(policyIds, immutableRole.policies).length > 0
      ) {
        const locale =
          policyIds.length === 1
            ? LocalesEnum.ERROR_IMMUTABLE_GROUP_POLICY_DELETE_SINGLE
            : LocalesEnum.ERROR_IMMUTABLE_GROUP_POLICY_DELETE_MULTIPLE;
        throw new ImmutableObjectError(context, locale);
      }
    }

    const didModify = await this.groupDataService.removePolicies(
      context,
      groupId,
      policyIds,
    );

    if (!didModify) {
      throw new NotFoundException();
    }
  }

  public async existsByName(context: Context, name: string): Promise<boolean> {
    const group = await this.groupDataService.getGroupWithName(context, name);
    return !!group;
  }
}
