import { Controller, Get, Headers, Inject, Query, Res } from '@nestjs/common';
import { ApiQuery, ApiTags } from '@nestjs/swagger';
import { ReAuthAuthorizeDataInterface } from '@cryptexlabs/authf-data-model';
import { ReauthService } from './reauth.service';
import { ContextBuilder } from '@cryptexlabs/codex-nodejs-common';

@Controller('reauth/v1')
@ApiTags('reauth')
export class ReAuthController {
  constructor(
    private readonly reauthService: ReauthService,
    @Inject('CONTEXT_BUILDER') private readonly contextBuilder: ContextBuilder,
  ) {}

  @Get('authorize')
  @ApiQuery({
    name: 'data',
  })
  public async authorize(
    @Query('data') data: string,
    @Res() res,
    @Headers() headers,
  ) {
    const authorizeData = JSON.parse(
      Buffer.from(data, 'base64').toString('utf-8'),
    ) as ReAuthAuthorizeDataInterface;

    const context = this.contextBuilder.build().getResult();

    await this.reauthService.authorize(context, res, authorizeData);
  }
}
