import { Inject, Injectable } from '@nestjs/common';
import {
  CreateTokenResponseDataInterface,
  ReAuthAuthorizeDataInterface,
  ReAuthCompleteDataInterface,
  ReAuthCreateTokenDataInterface,
} from '@cryptexlabs/authf-data-model';
import { IdentityLinkService } from '../user/identity-link/identity-link.service';
import { IdentityLinkUsageEnum } from '../user/identity-link/identity-link-usage.enum';
import { IdentityLinkTypeEnum } from '../user/identity-link/identity-link-type.enum';
import { UserService } from '../user/user.service';
import { v4 as uuidv4 } from 'uuid';
import { Context } from '@cryptexlabs/codex-nodejs-common';
import { AuthenticationServiceInterface } from '../authentication/authentication.service.interface';
import { AuthfIdentityLinkInterface } from '../authentication/authf.identity-link.interface';
import { UserLoginExtraService } from '../user/identity-link/user.login.extra.service';

@Injectable()
export class ReauthService {
  constructor(
    private readonly identityLinkService: IdentityLinkService,
    private readonly userService: UserService,
    private readonly loginExtraService: UserLoginExtraService,
    @Inject('AUTHENTICATION_SERVICE')
    private readonly authenticationService: AuthenticationServiceInterface,
  ) {}

  /**
   * AuthF is currently only ReAuth supported Authentication service
   *
   * @param {Context} context
   * @param {any} res
   * @param {ReAuthAuthorizeDataInterface} data
   */
  public async authorize(
    context: Context,
    res: any,
    data: ReAuthAuthorizeDataInterface,
  ) {
    const identityLink: AuthfIdentityLinkInterface = {
      data: { authenticationId: data.authenticationId },
      type: IdentityLinkTypeEnum.AUTHF,
      usage: IdentityLinkUsageEnum.LOGIN,
    };
    await this.authenticationService.validateReAuthAuthorizeToken(
      context,
      data.accessToken,
      identityLink,
    );

    let userId = await this.identityLinkService.getUserIdForIdentityLink(
      IdentityLinkUsageEnum.LOGIN,
      IdentityLinkTypeEnum.AUTHF,
      data.authenticationId,
    );

    let tokenData;

    if (!userId) {
      userId = uuidv4();
      await this.userService.createUser(
        context,
        userId,
        /**
         * No authentication is necessary because with reauth, authentication has already taken place
         * No username is possible during initial user creation through re-auth because that is not a field available in oAuth
         * and anyways the user can set their username after they initially register if they want a username
         * we could add an option and feature to generate a random username here for applications that need a username where user
         * have signed up through oAuth
         *
         * So authentication is undefined and username is also undefined on purpose
         */
        {
          username: data.user?.username || undefined,
        },
        data.authenticationId,
      );
    } else {
      tokenData = await this.userService.refreshUserPermissions(
        context,
        userId,
        identityLink,
        true,
      );
    }

    if (!tokenData) {
      const createTokenReAuthData: ReAuthCreateTokenDataInterface = {
        accessToken: data.accessToken,
        finalRedirectUrl: data.finalRedirectUrl,
        user: data.user,
        userId,
      };

      const queryData = Buffer.from(
        JSON.stringify(createTokenReAuthData),
      ).toString('base64');

      res.redirect(`${data.redirectUrl}?data=${queryData}`);
    } else {
      const useTokenData = tokenData as CreateTokenResponseDataInterface;

      const reAuthCompleteData: ReAuthCompleteDataInterface = {
        extra: useTokenData.extra,
        token: useTokenData.token,
        user: useTokenData.user || data.user,
        userId,
      };

      const queryData = Buffer.from(
        JSON.stringify(reAuthCompleteData),
      ).toString('base64');

      res.redirect(`${data.finalRedirectUrl}?data=${queryData}`);
    }
  }
}
