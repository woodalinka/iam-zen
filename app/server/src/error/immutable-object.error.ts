import {
  Context,
  FriendlyHttpException,
} from '@cryptexlabs/codex-nodejs-common';
import { LocalesEnum } from '../locale/enum';
import { HttpStatus } from '@nestjs/common';
import { i18nData } from '../locale/locales';

export class ImmutableObjectError extends FriendlyHttpException {
  constructor(context: Context, phraseKey: LocalesEnum) {
    super(
      'Object is immutable',
      context,
      i18nData.__({
        locale: context.locale.i18n,
        phrase: phraseKey,
      }),
      HttpStatus.METHOD_NOT_ALLOWED,
    );
  }
}
