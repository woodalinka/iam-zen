import { QueryOptionsInterface } from '../query.options.interface';

export interface RoleQueryOptionsInterface extends QueryOptionsInterface {
  roleIds?: string[];
}
