import { RoleExistsAuthzGuard } from './role.exists.authz.guard';
import * as jwt from 'jsonwebtoken';
import { AuthzNamespace } from '../constants';
import { ExecutionContext } from '@nestjs/common';

describe(RoleExistsAuthzGuard.name, () => {
  it('Should allow a super admin to get a role existence', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::any:any:any`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            name: 'example',
          },
          body: ['680dddec-f0b9-4a01-b8b5-be725f946935'],
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleExistsAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to get any object to get the role existence', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::any:any:get`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            name: 'example',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleExistsAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to list any object to get the role existence', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::any::list`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            name: 'example',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleExistsAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to list roles to get the role existence', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::role::list`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            name: 'example',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleExistsAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should not allow someone with permission to get a specific role to get the specific role existence', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::role:4d2114ca-24e2-43e5-bddb-d9a6688b8340:get`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            name: 'example',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleExistsAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should allow someone with permission to create roles to get the role existence', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::role:any:create`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            name: 'example',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleExistsAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to create a specific role to get the specific role existence', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::role:4d2114ca-24e2-43e5-bddb-d9a6688b8340:create`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            name: 'example',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleExistsAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to create any object to get the specific role existence', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::any:any:create`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            name: 'example',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleExistsAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Could not allow someone with permission to do anything to a specific role to get the role existence', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::role:4d2114ca-24e2-43e5-bddb-d9a6688b8340:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            name: 'example',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleExistsAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should allow someone with permission to get any role to get the role existence', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::role:any:get`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            name: 'example',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleExistsAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Could not allow someone with permission to get a specific role to get the specific role existence', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::role:4d2114ca-24e2-43e5-bddb-d9a6688b8340:get`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            name: 'example',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleExistsAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });
});
