export class RoleUtil {
  public static getUniquePolicyIdsForRoles(roles: { policies?: string[] }[]) {
    const uniquePolicyIds = [];
    for (const role of roles) {
      for (const policyId of role.policies || []) {
        if (!uniquePolicyIds.includes(policyId)) {
          uniquePolicyIds.push(policyId);
        }
      }
    }
    return uniquePolicyIds;
  }
}
