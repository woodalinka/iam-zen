import { Context } from '@cryptexlabs/codex-nodejs-common';
import { RolePolicyInterface } from './role.policy.interface';

export interface RolePoliciesProviderInterface {
  getPoliciesForRole(
    context: Context,
    roleId: string,
    policyIds: string[],
  ): Promise<RolePolicyInterface[]>;
}
