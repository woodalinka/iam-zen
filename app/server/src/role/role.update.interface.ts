export interface RoleUpdateInterface {
  name: string;
  description: string;
  policies: string[];
  apps: string[];
}
