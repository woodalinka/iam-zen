import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import * as Joi from 'joi';
import { RolePatchInterface } from './role.patch.interface';

@Injectable()
export class RolePatchValidationPipe implements PipeTransform {
  private _schema: Joi.ObjectSchema<RolePatchInterface>;
  constructor() {
    this._schema = Joi.object({
      name: Joi.string().alphanum().optional(),
      description: Joi.string().optional(),
    });
  }

  public transform(value: any, metadata: ArgumentMetadata): any {
    const { error } = this._schema.validate(value.data);
    if (error) {
      throw new BadRequestException(error.message);
    }
    return value;
  }
}
