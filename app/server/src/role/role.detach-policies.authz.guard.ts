import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AuthzNamespace } from '../constants';
import { HttpAuthzDetachObjectsGuardUtil } from '@cryptexlabs/codex-nodejs-common';

@Injectable()
export class RoleDetachPoliciesAuthzGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const util = new HttpAuthzDetachObjectsGuardUtil(context);
    return util.isAuthorized(
      'role',
      util.params.roleId,
      'policy',
      util.query.ids,
      AuthzNamespace.object,
    );
  }
}
