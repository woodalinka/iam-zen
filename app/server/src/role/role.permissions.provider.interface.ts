import { Context } from '@cryptexlabs/codex-nodejs-common';
import { RolePermissionInterface } from './role.permission.interface';

export interface RolePermissionsProviderInterface {
  getPermissionsForRoleByPolicyIds(
    context: Context,
    policyIds: string[],
  ): Promise<RolePermissionInterface[]>;
}
