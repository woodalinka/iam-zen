import { Inject, Injectable } from '@nestjs/common';
import { RoleDataServiceInterface } from './role.data.service.interface';
import { RolePersistedInterface } from './role.persisted.interface';
import {
  Context,
  CustomLogger,
  ElasticsearchInitializer,
  PaginatedResults,
} from '@cryptexlabs/codex-nodejs-common';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { RoleUpdateInterface } from './role.update.interface';
import { RolePatchInterface } from './role.patch.interface';
import { AppElasticsearchDataService } from '../app/app.elasticsearch.data.service';
import { RoleQueryOptionsInterface } from './role.query.options.interface';
import { RoleInterface } from './role.interface';

@Injectable()
export class RoleElasticsearchDataService implements RoleDataServiceInterface {
  private static get DEFAULT_INDEX() {
    return 'role';
  }

  private readonly elasticsearchInitializer: ElasticsearchInitializer;

  constructor(
    @Inject('LOGGER') private readonly logger: CustomLogger,
    private readonly elasticsearchService: ElasticsearchService,
    private readonly appElasticsearchDataService: AppElasticsearchDataService,
  ) {
    this.elasticsearchInitializer = new ElasticsearchInitializer(
      logger,
      RoleElasticsearchDataService.DEFAULT_INDEX,
      elasticsearchService,
      {
        id: {
          type: 'keyword',
        },
        policies: {
          type: 'keyword',
        },
        name: {
          type: 'text',
          fields: {
            keyword: {
              type: 'keyword',
              normalizer: 'lowercase_normalizer',
            },
          },
        },
        description: {
          type: 'text',
        },
      },
    );
  }

  public async initializeIndex() {
    const exists = await this.elasticsearchService.indices.exists({
      index: RoleElasticsearchDataService.DEFAULT_INDEX,
    });

    if (!exists) {
      await this.elasticsearchService.indices.create({
        index: RoleElasticsearchDataService.DEFAULT_INDEX,
        body: {
          settings: {
            analysis: {
              normalizer: {
                lowercase_normalizer: {
                  type: 'custom',
                  char_filter: [],
                  filter: ['lowercase'],
                },
              },
            },
          },
        },
      });
    }

    await this.elasticsearchInitializer.initializeIndex();
  }

  public async getRoleWithId(
    context: Context,
    roleId: string,
  ): Promise<RolePersistedInterface> {
    const response = await this.elasticsearchService.search({
      index: RoleElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                term: {
                  id: roleId,
                },
              },
            ],
          },
        },
      },
    });

    if (response.hits.total === 0) {
      return null;
    }

    return response.hits.hits[0]._source as any;
  }

  public async getRolesByIds(
    context: Context,
    roleIds: string[],
  ): Promise<RolePersistedInterface[]> {
    const response = await this.elasticsearchService.search({
      index: RoleElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                terms: {
                  id: roleIds,
                },
              },
            ],
          },
        },
      },
    });

    return response.hits.hits.map((message: { _source?: any }) => {
      return message._source;
    });
  }

  public async saveRolesFromConfig(
    context: Context,
    roles: RolePersistedInterface[],
  ): Promise<RolePersistedInterface[]> {
    if (roles.length === 0) {
      return [];
    }

    const response = await this.elasticsearchService.search({
      index: RoleElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                terms: {
                  id: roles.map((role) => role.id),
                },
              },
            ],
          },
        },
      },
    });

    const existingRoles = response.hits.hits;

    const existingRoleIds = existingRoles.map(
      (existingRole) => (existingRole._source as any).id,
    );

    const newRoles = roles.filter((role) => !existingRoleIds.includes(role.id));
    const newRolesBulk = [];
    for (const newRole of newRoles) {
      newRolesBulk.push({
        index: {
          _index: RoleElasticsearchDataService.DEFAULT_INDEX,
          _id: newRole.id,
        },
      });
      newRolesBulk.push({
        ...newRole,
      });
    }

    const existingRolesBulk = [];
    for (const existingRole of existingRoles) {
      const updatedRole = roles.find(
        (role) => role.id === (existingRole._source as any).id,
      );
      existingRolesBulk.push({
        index: {
          _index: RoleElasticsearchDataService.DEFAULT_INDEX,
          _id: existingRole._id,
        },
      });
      existingRolesBulk.push({
        ...(existingRole._source as any),
        name: updatedRole.name,
        description: updatedRole.description
          ? updatedRole.description
          : (existingRole._source as any).description,
        policies: updatedRole.policies,
      });
    }

    const bulk = [...newRolesBulk, ...existingRolesBulk];

    await this.elasticsearchService.bulk({ body: bulk });

    return newRoles;
  }

  public async getRoles(
    context: Context,
    page: number,
    pageSize: number,
    options?: RoleQueryOptionsInterface,
  ): Promise<PaginatedResults<RolePersistedInterface>> {
    let query: { query?: any; sort?: any } = {};

    if (options) {
      if (!options.search && options.searchName) {
        query = {
          query: {
            wildcard: {
              name: {
                value: `*${options.searchName}*`,
                boost: 1.0,
                rewrite: 'constant_score',
              },
            },
          },
        };
      }
      if (options.search) {
        const searchString = options.search
          .replace(/-/g, ' AND ')
          .replace(/"/g, '\\"')
          .replace(/:/g, `":"`);

        query = {
          query: {
            ...query.query,
            query_string: {
              query: `*${searchString}*`,
              fields: ['name', 'description', 'value'],
            },
          },
        };
      }

      if (options.roleIds) {
        query = {
          query: {
            ...query.query,
            bool: {
              must: [
                {
                  terms: {
                    id: options.roleIds,
                  },
                },
              ],
            },
          },
        };
      }

      const sortMap = {
        name: [{ 'name.keyword': options.orderDirection || 'asc' }, '_score'],
        policyCount: {
          _script: {
            type: 'number',
            script: {
              lang: 'painless',
              source: 'doc.policies.size()',
            },
            order: options.orderDirection || 'asc',
          },
        },
      };

      if (options.orderBy) {
        const sort = sortMap[options.orderBy];
        query = {
          ...query,
          sort,
        };
      }
    }

    const response = await this.elasticsearchService.search({
      index: RoleElasticsearchDataService.DEFAULT_INDEX,
      track_total_hits: true,
      body: {
        from: (page - 1) * pageSize,
        size: pageSize,
        ...query,
      },
    });

    return {
      total: (response.hits.total as any).value as number,
      results: response.hits.hits.map((message: { _source?: any }) => {
        return message._source;
      }),
    };
  }

  public async deleteRole(context: Context, roleId: string): Promise<boolean> {
    const response = await this.elasticsearchService.deleteByQuery({
      index: RoleElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          match: {
            id: roleId,
          },
        },
      },
    });
    return response.total === 1;
  }

  public async deleteRoles(
    context: Context,
    roleIds: string[],
  ): Promise<boolean> {
    const response = await this.elasticsearchService.deleteByQuery({
      index: RoleElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                terms: {
                  id: roleIds,
                },
              },
            ],
          },
        },
      },
    });
    return response.total === roleIds.length;
  }

  public async patchRole(
    context: Context,
    roleId: string,
    rolePatch: RolePatchInterface,
  ): Promise<boolean> {
    const response = await this.elasticsearchService.updateByQuery({
      index: RoleElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
             if (params.name != null) {
               ctx._source.name = params.name
             }
             if (params.description != null) {
               ctx._source.description = params.description
             }
            `,
          params: {
            name: rolePatch.name,
            description: rolePatch.description,
          },
        },
        query: {
          match: {
            id: roleId,
          },
        },
      },
    });
    return response.total === 1;
  }

  public async saveRole(
    context: Context,
    roleId: string,
    roleUpdate: RoleUpdateInterface,
  ): Promise<boolean> {
    let updated = false;
    const response = await this.elasticsearchService.updateByQuery({
      index: RoleElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
             ctx._source.name = params.name;
             ctx._source.description = params.description;
             ctx._source.policies = params.policies;
            `,
          params: {
            name: roleUpdate.name,
            description: roleUpdate.description,
            policies: roleUpdate.policies,
          },
        },
        query: {
          match: {
            id: roleId,
          },
        },
      },
    });
    if (response.total === 1) {
      updated = true;
    } else {
      await this.elasticsearchService.index({
        index: RoleElasticsearchDataService.DEFAULT_INDEX,
        id: roleId,
        body: {
          id: roleId,
          ...roleUpdate,
        },
      });
    }

    const addRolePromises = [];
    for (const app of roleUpdate.apps) {
      addRolePromises.push(
        this.appElasticsearchDataService.addRoles(context, app, [roleId]),
      );
    }

    await Promise.all(addRolePromises);

    return updated;
  }

  public async addPolicies(
    context: Context,
    roleId: string,
    policyIds: string[],
  ): Promise<boolean> {
    const response = await this.elasticsearchService.updateByQuery({
      index: RoleElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
           if (!(ctx._source.policies instanceof List)) {
             ctx._source.policies = new ArrayList();
           }
           ctx._source.policies.addAll(params.policies);
           ctx._source.policies = ctx._source.policies.stream().distinct().sorted().collect(Collectors.toList());
          `,
          params: {
            policies: policyIds,
          },
        },
        query: {
          match: {
            id: roleId,
          },
        },
      },
    });
    return response.total === 1;
  }

  public async removePolicies(
    context: Context,
    roleId: string,
    policyIds: string[],
  ): Promise<boolean> {
    const response = await this.elasticsearchService.updateByQuery({
      index: RoleElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
            if (!(ctx._source.policies instanceof List)) {
              ctx._source.policies = new ArrayList();
            }
            ctx._source.policies.removeAll(params.policies);
          `,
          params: {
            policies: policyIds,
          },
        },
        query: {
          match: {
            id: roleId,
          },
        },
      },
    });

    return response.total === 1;
  }

  public async getRoleWithName(
    context: Context,
    name: string,
  ): Promise<RolePersistedInterface | null> {
    const response = await this.elasticsearchService.search({
      index: RoleElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                term: {
                  'name.keyword': name,
                },
              },
            ],
          },
        },
      },
    });

    if (response.hits.total === 0) {
      return null;
    }

    return response.hits.hits[0]._source as any;
  }

  async getRolesForPolicy(
    context: Context,
    policyId: string,
  ): Promise<RoleInterface[]> {
    const response = await this.elasticsearchService.search({
      index: RoleElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                term: {
                  policies: policyId,
                },
              },
            ],
          },
        },
      },
    });

    return response.hits.hits.map((message: { _source?: any }) => {
      return message._source;
    });
  }

  async getRolesForPolicies(
    context: Context,
    policyIds: string[],
  ): Promise<RoleInterface[]> {
    const response = await this.elasticsearchService.search({
      index: RoleElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                terms: {
                  policies: policyIds,
                },
              },
            ],
          },
        },
      },
    });

    return response.hits.hits.map((message: { _source?: any }) => {
      return message._source;
    });
  }
}
