import { RolePersistedInterface } from './role.persisted.interface';

export class RoleInListDto {
  public readonly id: string;
  public readonly name: string;
  public readonly policyCount: number;

  constructor(
    role: RolePersistedInterface,
    public readonly appCount: number,
    public readonly immutable,
  ) {
    this.id = role.id;
    this.name = role.name;
    this.policyCount = role.policies ? role.policies.length : 0;
  }
}
