import { RolePersistedInterface } from './role.persisted.interface';
import { RolePolicyInterface } from './role.policy.interface';
import { RoleAppDetailInterface } from './role.app.interface';
import { RolePermissionInterface } from './role.permission.interface';

export class RoleDto {
  public readonly id: string;
  public readonly name: string;
  public readonly description: string;

  constructor(
    role: RolePersistedInterface,
    public readonly policies: RolePolicyInterface[],
    public readonly permissions: RolePermissionInterface[],
    public readonly apps: RoleAppDetailInterface[],
    public readonly immutable,
  ) {
    this.id = role.id;
    this.name = role.name;
    this.description = role.description;
  }
}
