export interface RolePolicyInterface {
  id: string;
  name: string;
  description: string;
  immutable: boolean;
}
