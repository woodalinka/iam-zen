import { RoleService } from './role.service';
import { appConfig, elasticsearchService } from '../setup';
import { DatabaseTypeEnum } from '../data/database-type.enum';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { RoleDefaultLoaderService } from './role.default-loader.service';
import { RoleDataServiceInterface } from './role.data.service.interface';
import { ContextBuilder } from '@cryptexlabs/codex-nodejs-common';
import { Config } from '../config';
import { RoleElasticsearchDataService } from './role.elasticsearch.data.service';

const providers = [];

if (appConfig.dbType === DatabaseTypeEnum.ELASTICSEARCH) {
  providers.push(
    {
      provide: ElasticsearchService,
      useValue: elasticsearchService,
    },
    RoleElasticsearchDataService,
    {
      provide: 'ROLE_DATA_SERVICE',
      useFactory: async (service: RoleElasticsearchDataService) => {
        await service.initializeIndex();
        return service;
      },
      inject: [RoleElasticsearchDataService],
    },
  );
}

export const roleProviders = [
  ...providers,
  RoleService,
  {
    provide: 'ROLE_SERVICE',
    useClass: RoleService,
  },
  {
    provide: 'APP_ROLES_PROVIDER',
    useClass: RoleService,
  },
  {
    provide: 'APP_POLICIES_PROVIDER',
    useClass: RoleService,
  },
  {
    provide: RoleDefaultLoaderService,
    useFactory: async (
      roleDataService: RoleDataServiceInterface,
      config: Config,
      contextBuilder: ContextBuilder,
    ) => {
      const service = new RoleDefaultLoaderService(
        roleDataService,
        config,
        contextBuilder,
      );
      if (appConfig.autoLoadConfig) {
        await service.load();
      }
      return service;
    },
    inject: ['ROLE_DATA_SERVICE', 'CONFIG', 'CONTEXT_BUILDER'],
  },
];
