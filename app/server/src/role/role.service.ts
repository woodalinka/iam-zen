import {
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { Config } from '../config';
import { AppRolesProviderInterface } from '../app/app.roles.provider.interface';
import { AppRoleWithPolicies } from '../app/app.role.interface';
import { RoleDataServiceInterface } from './role.data.service.interface';
import {
  ArrayUtil,
  Context,
  PaginatedResults,
} from '@cryptexlabs/codex-nodejs-common';
import { RoleInterface } from './role.interface';
import { RoleDto } from './role.dto';
import { RolePoliciesProviderInterface } from './role.policies.provider.interface';
import { RoleInListDto } from './role.in-list.dto';
import { RoleAppsProviderInterface } from './role.apps.provider.interface';
import { RolePermissionsProviderInterface } from './role.permissions.provider.interface';
import { RoleUtil } from './role.util';
import { AppPoliciesProviderInterface } from '../app/app.policies-provider.interface';
import { AppInterface } from '../app/app.interface';
import { AppPolicyWithPermissionsInterface } from '../app/app.policy.interface';
import { ImmutableObjectError } from '../error/immutable-object.error';
import { LocalesEnum } from '../locale/enum';
import { RoleUpdateInterface } from './role.update.interface';
import { RolePatchInterface } from './role.patch.interface';
import { PolicyService } from '../policy/policy.service';
import { RoleQueryOptionsInterface } from './role.query.options.interface';
import { AppPermissionInterface } from '../app/app.permission.interface';
import { PolicyRoleInterface } from '../policy/policy.role.interface';
import { PermissionRoleInterface } from '../permission/permission.role.interface';

@Injectable()
export class RoleService
  implements AppRolesProviderInterface, AppPoliciesProviderInterface
{
  constructor(
    @Inject('CONFIG') private readonly config: Config,
    @Inject('ROLE_DATA_SERVICE')
    private readonly roleDataService: RoleDataServiceInterface,
    @Inject('ROLE_POLICIES_PROVIDER')
    private readonly rolePoliciesProvider: RolePoliciesProviderInterface,
    @Inject('ROLE_APPS_PROVIDER')
    private readonly roleAppsProvider: RoleAppsProviderInterface,
    @Inject('ROLE_PERMISSIONS_PROVIDER')
    private readonly rolePermissionsProvider: RolePermissionsProviderInterface,
    @Inject('POLICY_SERVICE')
    private readonly policyService: PolicyService,
  ) {}
  public async getRolesByIds(
    context: Context,
    roleIds: string[],
  ): Promise<RoleInterface[]> {
    const persistedRoles = await this.roleDataService.getRolesByIds(
      context,
      roleIds,
    );
    const persistedRoleIds = persistedRoles.map(
      (persistedRole) => persistedRole.id,
    );
    const configRolesNotPersisted = this.config.defaultRoles.filter(
      (configRole) =>
        !persistedRoleIds.includes(configRole.id) &&
        roleIds.includes(configRole.id),
    );

    const roles = persistedRoles.map((persistedRole) => {
      return {
        name: persistedRole.name,
        id: persistedRole.id,
        description: persistedRole.description,
        policies: persistedRole.policies,
      };
    });
    for (const configRole of configRolesNotPersisted) {
      roles.push({
        name: configRole.name,
        id: configRole.id,
        description: configRole.description || '',
        policies: configRole.policies || [],
      });
    }
    return roles;
  }

  public async getRolesForApp(
    context: Context,
    appId: string,
    roleIds: string[],
  ): Promise<AppRoleWithPolicies[]> {
    const roles = await this.getRolesByIds(context, roleIds);
    return roles.map((role) => {
      const configApp = this.config.defaultApps.find((a) => a.id === appId);
      let immutable = false;
      if (configApp) {
        immutable = configApp.roles.includes(role.id);
      }
      return {
        id: role.id,
        name: role.name,
        description: role.description,
        policies: role.policies,
        immutable,
      };
    });
  }

  public async getRoleById(context: Context, roleId: string): Promise<RoleDto> {
    const role = await this.roleDataService.getRoleWithId(context, roleId);
    if (!role) {
      throw new HttpException('Role does not exist', HttpStatus.NOT_FOUND);
    }
    const immutable = !!this.config.defaultRoles.find(
      (configRole) => configRole.id === role.id,
    );

    const policies = await this.rolePoliciesProvider.getPoliciesForRole(
      context,
      roleId,
      role.policies,
    );

    const apps = await this.roleAppsProvider.getAppsForRole(context, role.id);

    const permissions =
      await this.rolePermissionsProvider.getPermissionsForRoleByPolicyIds(
        context,
        role.policies,
      );

    return new RoleDto(role, policies, permissions, apps, immutable);
  }

  public async getRoleExists(context: Context, name: string): Promise<boolean> {
    const role = await this.roleDataService.getRoleWithName(context, name);
    return !!role;
  }

  public async getRolesForPolicyId(
    context: Context,
    policyId: string,
  ): Promise<PolicyRoleInterface[]> {
    const roles = await this.roleDataService.getRolesForPolicy(
      context,
      policyId,
    );

    return roles.map((role) => {
      let immutable = false;
      const configRole = this.config.defaultRoles.find((r) => r.id === role.id);
      if (configRole) {
        immutable = role.policies.includes(policyId);
      }

      return {
        id: role.id,
        name: role.name,
        description: role.description,
        immutable,
      };
    });
  }

  public async getRolesByPoliciesForPermissions(
    context: Context,
    policyIds: string[],
  ): Promise<PermissionRoleInterface[]> {
    const roles = await this.roleDataService.getRolesForPolicies(
      context,
      policyIds,
    );

    return roles.map((role) => {
      return {
        id: role.id,
        name: role.name,
        description: role.description,
      };
    });
  }

  public async getRoles(
    context: Context,
    page: number,
    pageSize: number,
    options?: RoleQueryOptionsInterface,
  ): Promise<PaginatedResults<RoleInListDto>> {
    const persistedRoles = await this.roleDataService.getRoles(
      context,
      page,
      pageSize,
      options,
    );
    const apps = await this.roleAppsProvider.getAppsForRoles(
      context,
      persistedRoles.results.map((persistedRole) => persistedRole.id),
    );

    return {
      total: persistedRoles.total,
      results: persistedRoles.results.map((persistedRole) => {
        const immutable = !!this.config.defaultRoles.find(
          (configRole) => configRole.id === persistedRole.id,
        );
        const roleApps = apps.filter((app) =>
          app.roles.includes(persistedRole.id),
        );
        return new RoleInListDto(persistedRole, roleApps.length, immutable);
      }),
    };
  }

  public async getPoliciesForApp(
    context: Context,
    app: AppInterface,
  ): Promise<AppPolicyWithPermissionsInterface[]> {
    const roles = await this.getRolesByIds(context, app.roles);
    const policyIds = RoleUtil.getUniquePolicyIdsForRoles(roles);

    const policies = await this.policyService.getPoliciesByIds(
      context,
      policyIds,
    );

    return policies.map((policy) => {
      const fromRoles = [];
      for (const role of roles) {
        if (role.policies.includes(policy.id)) {
          fromRoles.push({
            id: role.id,
            name: role.name,
          });
        }
      }

      return {
        id: policy.id,
        name: policy.name,
        description: policy.description,
        permissions: policy.permissions,
        fromRoles,
      };
    });
  }

  public async getPermissionsForApp(
    context: Context,
    app: AppInterface,
  ): Promise<AppPermissionInterface[]> {
    const roles = await this.getRolesByIds(context, app.roles);
    const policyIds = RoleUtil.getUniquePolicyIdsForRoles(roles);
    const permissions =
      await this.rolePermissionsProvider.getPermissionsForRoleByPolicyIds(
        context,
        policyIds,
      );

    const policies = await this.policyService.getPoliciesByIds(
      context,
      policyIds,
    );

    return permissions.map((permission) => {
      const fromPolicies = [];
      for (const policy of policies) {
        if (policy.permissions.includes(permission.id)) {
          fromPolicies.push({
            id: policy.id,
            name: policy.name,
          });
        }
      }

      const fromRoles = [];
      const policyIds = fromPolicies.map((policy) => policy.id);
      for (const role of roles) {
        const intersect = role.policies.filter((policyId) =>
          policyIds.includes(policyId),
        );
        if (intersect.length > 0) {
          fromRoles.push(role);
        }
      }

      return {
        id: permission.id,
        name: permission.name,
        value: permission.value,
        fromPolicies,
        fromRoles,
      };
    });
  }

  public async deleteRole(context: Context, roleId: string): Promise<void> {
    this._immutabilityCheck(context, roleId);
    const didModify = await this.roleDataService.deleteRole(context, roleId);
    if (!didModify) {
      throw new NotFoundException();
    }
  }

  public async deleteRoles(context: Context, roleIds: string[]): Promise<void> {
    for (const roleId of roleIds) {
      this._immutabilityCheck(context, roleId);
    }
    const didModify = await this.roleDataService.deleteRoles(context, roleIds);
    if (!didModify) {
      throw new NotFoundException();
    }
  }

  public async updateRole(
    context: Context,
    roleId: string,
    roleUpdate: RoleUpdateInterface,
  ): Promise<HttpStatus> {
    this._immutabilityCheck(context, roleId);
    const didModify = await this.roleDataService.saveRole(
      context,
      roleId,
      roleUpdate,
    );
    return didModify ? HttpStatus.ACCEPTED : HttpStatus.CREATED;
  }

  public async patchRole(
    context: Context,
    roleId: string,
    rolePatch: RolePatchInterface,
  ): Promise<void> {
    this._immutabilityCheck(context, roleId);
    const didModify = await this.roleDataService.patchRole(
      context,
      roleId,
      rolePatch,
    );
    if (!didModify) {
      throw new NotFoundException();
    }
  }

  public async addPolicies(
    context: Context,
    roleId: string,
    policyIds: string[],
  ): Promise<void> {
    const foundPolicies = await this.policyService.getPoliciesByIds(
      context,
      policyIds,
    );

    if (foundPolicies.length !== policyIds.length) {
      throw new HttpException(`Invalid policies`, HttpStatus.BAD_REQUEST);
    }

    const didModify = await this.roleDataService.addPolicies(
      context,
      roleId,
      policyIds,
    );
    if (!didModify) {
      throw new NotFoundException();
    }
  }

  public async removePolicies(
    context: Context,
    roleId: string,
    policyIds: string[],
  ): Promise<void> {
    const immutableRole = this.config.defaultRoles.find(
      (findRole) => findRole.id === roleId,
    );

    if (immutableRole) {
      if (
        ArrayUtil.intersection(policyIds, immutableRole.policies).length > 0
      ) {
        const locale =
          policyIds.length === 1
            ? LocalesEnum.ERROR_IMMUTABLE_ROLE_POLICY_DELETE_SINGLE
            : LocalesEnum.ERROR_IMMUTABLE_ROLE_POLICY_DELETE_MULTIPLE;
        throw new ImmutableObjectError(context, locale);
      }
    }

    const didModify = await this.roleDataService.removePolicies(
      context,
      roleId,
      policyIds,
    );

    if (!didModify) {
      throw new NotFoundException();
    }
  }

  private _immutabilityCheck(context: Context, roleId: string) {
    const immutableRole = this.config.defaultRoles.find(
      (findRole) => findRole.id === roleId,
    );
    if (immutableRole) {
      throw new ImmutableObjectError(context, LocalesEnum.ERROR_IMMUTABLE_ROLE);
    }
  }
}
