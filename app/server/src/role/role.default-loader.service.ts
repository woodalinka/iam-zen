import { Inject, Injectable } from '@nestjs/common';
import { Config } from '../config';
import { ContextBuilder } from '@cryptexlabs/codex-nodejs-common';
import { RoleDataServiceInterface } from './role.data.service.interface';
import { RolePersistedInterface } from './role.persisted.interface';

@Injectable()
export class RoleDefaultLoaderService {
  constructor(
    @Inject('ROLE_DATA_SERVICE')
    private readonly roleDataService: RoleDataServiceInterface,
    @Inject('CONFIG') private readonly config: Config,
    @Inject('CONTEXT_BUILDER') private readonly contextBuilder: ContextBuilder,
  ) {}

  public async load() {
    const persistedRoles: RolePersistedInterface[] =
      this.config.defaultRoles.map((configRole) => {
        return {
          name: configRole.name,
          id: configRole.id,
          description: configRole.description || '',
          policies: configRole.policies || [],
        };
      });

    const context = this.contextBuilder.build().getResult();

    await this.roleDataService.saveRolesFromConfig(context, persistedRoles);
  }
}
