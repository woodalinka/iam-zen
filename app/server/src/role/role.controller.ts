import {
  Body,
  Controller,
  Delete,
  Get,
  Headers,
  HttpCode,
  HttpStatus,
  Inject,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Put,
  Query,
  Res,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  ApiMetaHeaders,
  ApiPagination,
  ContextBuilder,
  NotEmptyPipe,
  RestPaginatedResponse,
  RestResponse,
  SimpleHttpResponse,
} from '@cryptexlabs/codex-nodejs-common';
import { RoleService } from './role.service';
import { ExampleSimpleResponse } from '../example/example.simple.response';
import { LocalesEnum } from '../locale/enum';
import { RolePatchInterface } from './role.patch.interface';
import { RoleUpdateInterface } from './role.update.interface';
import { rolePatchExample, roleUpdateExample } from './example';
import { appConfig } from '../setup';
import { RoleListAuthzGuard } from './role.list.authz.guard';
import { RoleGetAuthzGuard } from './role.get.authz.guard';
import { RoleDeleteAuthzGuard } from './role.delete.authz.guard';
import { RoleUpdateAuthzGuard } from './role.update.authz.guard';
import { RoleAttachPoliciesAuthzGuard } from './role.attach-policies.authz.guard';
import { RoleDetachPoliciesAuthzGuard } from './role.detach-policies.authz.guard';
import { RoleDeleteListAuthzGuard } from './role.delete-list.authz.guard';
import { RoleExistsAuthzGuard } from './role.exists.authz.guard';
import { ExampleRestResponse } from '../example/example.rest.response';
import { Uuidv4ArrayValidationPipe } from '@cryptexlabs/codex-nodejs-common/lib/src/pipe/uuidv4.array.validation.pipe';
import { RolePatchValidationPipe } from './role.patch.validation.pipe';
import { RoleUpdateValidationPipe } from './role.update.validation.pipe';
import { OrderDirectionEnum } from '../query.options.interface';
import { OrderDirectionValidationPipe } from '../order.direction.validation.pipe';

@Controller(`${appConfig.appPrefix}/${appConfig.apiVersion}/role`)
@ApiTags('role')
@ApiMetaHeaders()
@ApiBearerAuth()
export class RoleController {
  constructor(
    private readonly roleService: RoleService,
    @Inject('CONTEXT_BUILDER') private readonly contextBuilder: ContextBuilder,
  ) {}
  @Get()
  @ApiOperation({
    summary: 'Get list of roles',
  })
  @ApiPagination()
  @ApiQuery({
    name: 'orderBy',
    required: false,
    schema: {
      example: 'name',
    },
  })
  @ApiQuery({
    name: 'orderDirection',
    required: false,
    schema: {
      example: 'asc',
    },
  })
  @ApiQuery({
    name: 'searchName',
    required: false,
  })
  @ApiQuery({
    name: 'search',
    required: false,
  })
  @UseGuards(RoleListAuthzGuard)
  public async getRoles(
    @Query('page', ParseIntPipe) page: number,
    @Query('pageSize', ParseIntPipe) pageSize: number,
    @Query('searchName') searchName: string,
    @Query('search') search: string,
    @Query('orderBy') orderBy: string,
    @Query('orderDirection', OrderDirectionValidationPipe)
    orderDirection: OrderDirectionEnum,
    @Headers() headers: Record<string, string>,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const roles = await this.roleService.getRoles(context, page, pageSize, {
      searchName,
      search,
      orderBy,
      orderDirection,
    });

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestPaginatedResponse(
      responseContext,
      HttpStatus.OK,
      roles.total,
      'cryptexlabs.iam-zen.role.list',
      roles.results,
    );
  }

  @Get(':roleId')
  @ApiOperation({
    summary: 'Get a role by role ID',
  })
  @ApiParam({
    name: 'roleId',
    example: 'db1c7afb-0e59-496f-90e4-07c96757dd8c',
  })
  @UseGuards(RoleGetAuthzGuard)
  public async getRole(@Param('roleId') roleId: string, @Headers() headers) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const role = await this.roleService.getRoleById(context, roleId);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestResponse(
      responseContext,
      HttpStatus.OK,
      'cryptexlabs.iam-zen.role',
      role,
    );
  }

  @Get('any/exists')
  @ApiOperation({
    summary: 'Get whether or not a role exists',
  })
  @ApiQuery({
    name: 'name',
    example: 'Widget',
    required: true,
  })
  @UseGuards(RoleExistsAuthzGuard)
  @ApiResponse({
    schema: {
      example: new ExampleRestResponse(HttpStatus.OK, 'boolean', false),
    },
  })
  public async getRoleExists(
    @Query('name', NotEmptyPipe) name: string,
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const roleExists = await this.roleService.getRoleExists(context, name);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestResponse(
      responseContext,
      HttpStatus.OK,
      'boolean',
      roleExists,
    );
  }

  @Delete(':roleId')
  @ApiOperation({
    summary: 'Delete role by role ID',
  })
  @ApiParam({
    name: 'roleId',
    example: '02181388-edef-41fc-95d7-3cd98bb981e2',
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(RoleDeleteAuthzGuard)
  public async deleteRole(
    @Param('roleId', NotEmptyPipe) roleId: string,
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    await this.roleService.deleteRole(context, roleId);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Delete()
  @ApiOperation({
    summary: 'Delete roles by role ID',
  })
  @ApiQuery({
    name: 'ids',
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(RoleDeleteListAuthzGuard)
  public async deleteRoles(
    @Query('ids', NotEmptyPipe) roleIds: string[],
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const useRoleIds = Array.isArray(roleIds) ? roleIds : [roleIds];

    await this.roleService.deleteRoles(context, useRoleIds);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Put(':roleId')
  @ApiOperation({
    summary: 'Create or update role by role ID',
  })
  @ApiParam({
    name: 'roleId',
    example: '02181388-edef-41fc-95d7-3cd98bb981e2',
    required: true,
  })
  @ApiBody({
    schema: {
      example: roleUpdateExample,
    },
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(RoleUpdateAuthzGuard)
  public async updateRole(
    @Param('roleId', NotEmptyPipe) roleId: string,
    @Headers() headers,
    @Res() res,
    @Body(NotEmptyPipe, RoleUpdateValidationPipe)
    roleUpdate: RoleUpdateInterface,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const status = await this.roleService.updateRole(
      context,
      roleId,
      roleUpdate,
    );

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    res
      .status(status)
      .send(
        new SimpleHttpResponse(
          responseContext,
          HttpStatus.ACCEPTED,
          LocalesEnum.SUCCESS,
        ),
      );
  }

  @Patch(':roleId')
  @ApiOperation({
    summary: 'Partially update role by role ID',
  })
  @ApiParam({
    name: 'roleId',
    example: '02181388-edef-41fc-95d7-3cd98bb981e2',
    required: true,
  })
  @ApiBody({
    schema: {
      example: rolePatchExample,
    },
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(RoleUpdateAuthzGuard)
  public async patchRole(
    @Param('roleId', NotEmptyPipe) roleId: string,
    @Body(NotEmptyPipe, RolePatchValidationPipe) rolePatch: RolePatchInterface,
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    await this.roleService.patchRole(context, roleId, rolePatch);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Post(':roleId/policy')
  @ApiOperation({
    summary: 'Add policies to role',
  })
  @ApiParam({
    name: 'roleId',
    example: '16b327c5-af12-4f02-a772-005c6b92ba6d',
    required: true,
  })
  @ApiBody({
    schema: {
      example: ['ac5c44eb-501d-4b53-bcca-9aac58b438de'],
    },
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(RoleAttachPoliciesAuthzGuard)
  public async addPolicies(
    @Param('roleId', NotEmptyPipe) roleId: string,
    @Body(NotEmptyPipe, Uuidv4ArrayValidationPipe) policies: string[],
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    await this.roleService.addPolicies(context, roleId, policies);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Delete(':roleId/policy')
  @ApiOperation({
    summary: 'Remove policies from role',
  })
  @ApiParam({
    name: 'roleId',
    example: '02181388-edef-41fc-95d7-3cd98bb981e2',
    required: true,
  })
  @ApiQuery({
    name: 'ids',
    example: [
      'ac5c44eb-501d-4b53-bcca-9aac58b438de',
      'ec570e6e-caaf-43f6-ac6b-cea8b7949124',
    ],
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(RoleDetachPoliciesAuthzGuard)
  public async removePolicies(
    @Param('roleId', NotEmptyPipe) roleId: string,
    @Query('ids', NotEmptyPipe) ids: string[],
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const policyIds = typeof ids === 'string' ? [ids] : ids;

    await this.roleService.removePolicies(context, roleId, policyIds);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }
}
