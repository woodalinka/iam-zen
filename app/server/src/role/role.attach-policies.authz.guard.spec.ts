import { RoleAttachPoliciesAuthzGuard } from './role.attach-policies.authz.guard';
import { ExecutionContext } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { AuthzNamespace } from '../constants';

describe(RoleAttachPoliciesAuthzGuard.name, () => {
  it('Should allow super admin to attach a permission to a role', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::any:any:any:any:any:any:any:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            roleId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          body: ['680dddec-f0b9-4a01-b8b5-be725f946935'],
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleAttachPoliciesAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to attach any permission to a role to attach a permission to the role', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::role:4d2114ca-24e2-43e5-bddb-d9a6688b8340::policy:any:create`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            roleId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          body: ['680dddec-f0b9-4a01-b8b5-be725f946935'],
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleAttachPoliciesAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to do anything to any permission on a role to attach a permission to the role', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::role:4d2114ca-24e2-43e5-bddb-d9a6688b8340::policy:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            roleId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          body: ['680dddec-f0b9-4a01-b8b5-be725f946935'],
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleAttachPoliciesAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to do anything to any sub object for a role to attach a permission to the role', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::role:4d2114ca-24e2-43e5-bddb-d9a6688b8340::any:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            roleId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          body: ['680dddec-f0b9-4a01-b8b5-be725f946935'],
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleAttachPoliciesAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to attach a specific permission to a role to attach the permission to the role', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::role:4d2114ca-24e2-43e5-bddb-d9a6688b8340::policy:680dddec-f0b9-4a01-b8b5-be725f946935:create`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            roleId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          body: ['680dddec-f0b9-4a01-b8b5-be725f946935'],
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleAttachPoliciesAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should not allow someone with permission to attach any permission to a different role to attach a permission to the role', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::role:4d2114ca-24e2-43e5-bddb-d9a6688b8340::policy:any:create`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            roleId: '001d4f53-798b-4a0b-8ef7-330a7bf72147',
          },
          body: ['680dddec-f0b9-4a01-b8b5-be725f946935'],
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleAttachPoliciesAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should not allow someone with permission to do anything to any permission on a different role to attach a permission to the role', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::role:4d2114ca-24e2-43e5-bddb-d9a6688b8340::policy:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            roleId: '001d4f53-798b-4a0b-8ef7-330a7bf72147',
          },
          body: ['680dddec-f0b9-4a01-b8b5-be725f946935'],
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleAttachPoliciesAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should not allow someone with permission to do anything to any sub object for a different role to attach a permission to the role', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::role:4d2114ca-24e2-43e5-bddb-d9a6688b8340::any:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            roleId: '001d4f53-798b-4a0b-8ef7-330a7bf72147',
          },
          body: ['680dddec-f0b9-4a01-b8b5-be725f946935'],
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleAttachPoliciesAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should not allow someone with permission to attach a specific permission to a different role to attach the permission to the role', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::role:4d2114ca-24e2-43e5-bddb-d9a6688b8340::policy:680dddec-f0b9-4a01-b8b5-be725f946935:create`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            roleId: '001d4f53-798b-4a0b-8ef7-330a7bf72147',
          },
          body: ['680dddec-f0b9-4a01-b8b5-be725f946935'],
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleAttachPoliciesAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should not allow someone with permission to attach a different specific permission to a role to attach the permission to the role', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::role:4d2114ca-24e2-43e5-bddb-d9a6688b8340::policy:680dddec-f0b9-4a01-b8b5-be725f946935:create`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            roleId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          body: ['5be3176f-c066-4418-b682-18e16fd07b84'],
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleAttachPoliciesAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });
});
