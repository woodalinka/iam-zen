import { AppService } from './app.service';
import { appConfig, elasticsearchService } from '../setup';
import { DatabaseTypeEnum } from '../data/database-type.enum';
import { AppElasticsearchDataService } from './app.elasticsearch.data.service';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { AppDefaultLoaderService } from './app.default-loader.service';
import { AppDataServiceInterface } from './app.data.service.interface';
import { Config } from '../config';
import { AppPermissionsProviderInterface } from './app.permissions-provider.interface';
import { AuthenticationServiceInterface } from '../authentication/authentication.service.interface';
import { ContextBuilder } from '@cryptexlabs/codex-nodejs-common';
import { AuthfService } from '../authentication/authf.service';
import { AuthenticationServiceFactory } from '../authentication/authentication-service-factory';

const providers = [];

if (appConfig.dbType === DatabaseTypeEnum.ELASTICSEARCH) {
  providers.push(
    {
      provide: ElasticsearchService,
      useValue: elasticsearchService,
    },
    AppElasticsearchDataService,
    {
      provide: 'APP_DATA_SERVICE',
      useFactory: async (service: AppElasticsearchDataService) => {
        await service.initializeIndex();
        return service;
      },
      inject: [AppElasticsearchDataService],
    },
  );
}

export const appProviders = [
  ...providers,
  AppService,
  {
    provide: 'ROLE_APPS_PROVIDER',
    useClass: AppService,
  },
  {
    provide: AppDefaultLoaderService,
    useFactory: async (
      appDataService: AppDataServiceInterface,
      config: Config,
      appPermissionsProvider: AppPermissionsProviderInterface,
      authenticationService: AuthenticationServiceInterface,
      contextBuilder: ContextBuilder,
    ) => {
      const appDefaultService = new AppDefaultLoaderService(
        appDataService,
        config,
        appPermissionsProvider,
        authenticationService,
        contextBuilder,
      );
      if (appConfig.autoLoadConfig) {
        await appDefaultService.load();
      }
      return appDefaultService;
    },
    inject: [
      'APP_DATA_SERVICE',
      'CONFIG',
      'APP_PERMISSIONS_PROVIDER',
      'AUTHENTICATION_SERVICE',
      'CONTEXT_BUILDER',
    ],
  },
  AuthenticationServiceFactory,
  AuthfService,
];
