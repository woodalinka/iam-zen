import { AppInterface } from './app.interface';
import { Context, PaginatedResults } from '@cryptexlabs/codex-nodejs-common';
import { AppPersistedInterface } from './app.persisted.interface';
import { ApiKeyInterface } from '../api-key/api-key.interface';
import { AppUpdateInterface } from './app.update.interface';
import { AppPatchInterface } from './app.patch.interface';
import { QueryOptionsInterface } from '../query.options.interface';

export interface AppDataServiceInterface {
  getAppWithId(appId: string): Promise<AppPersistedInterface | null>;

  getAppWithName(
    context: Context,
    name: string,
  ): Promise<AppPersistedInterface | null>;

  getAppsWithIds(
    context: Context,
    appIds: string[],
  ): Promise<AppPersistedInterface[]>;

  getAppsWithRoleIds(
    context: Context,
    roleIds: string[],
  ): Promise<AppPersistedInterface[]>;

  createApp(context: Context, app: AppInterface): Promise<void>;

  updateTokenHashes(
    context: Context,
    appId: string,
    permissionsHash: string,
    expirationPolicyHash: string,
  ): Promise<void>;

  getApps(
    context: Context,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<AppPersistedInterface>>;

  /**
   * Returns list of newly created apps
   *
   * @param {AppPersistedInterface[]} apps
   */
  saveAppsFromConfig(
    apps: AppPersistedInterface[],
  ): Promise<AppPersistedInterface[]>;

  saveApiKeys(
    context: Context,
    appId: string,
    apiKey: ApiKeyInterface[],
  ): Promise<void>;

  deleteAppById(context: Context, appId: string): Promise<boolean>;

  deleteAppsByIds(context: Context, appIds: string[]): Promise<boolean>;

  updateAppById(
    context: Context,
    appId: string,
    updateApp: AppUpdateInterface,
  ): Promise<boolean>;

  addRoles(context: Context, appId: string, roles: string[]): Promise<boolean>;

  removeRoles(
    context: Context,
    appId: string,
    roles: string[],
  ): Promise<boolean>;

  getRoleIds(context: Context, appId: string): Promise<string[]>;

  getAllApps(context: Context): Promise<AppPersistedInterface[]>;

  addApiKey(
    context: Context,
    appId: string,
    apiKey: ApiKeyInterface,
  ): Promise<void>;

  getApiKeys(
    context: Context,
    appId: string,
    page: number,
    pageSize: number,
  ): Promise<ApiKeyInterface[]>;

  removeApiKeys(
    context: Context,
    appId: string,
    apiKeys: string[],
  ): Promise<boolean>;

  getApiKeysByIds(
    context: Context,
    appId: string,
    apiKeyIds: string[],
  ): Promise<ApiKeyInterface[] | boolean>;

  patchAppWithId(
    context: Context,
    appId: string,
    app: AppPatchInterface,
  ): Promise<boolean>;
}
