import { ApiAuthAuthenticateInterface } from '@cryptexlabs/authf-data-model';

export interface CreateAppInterface {
  name: string;
  description?: string;
  authentications?: ApiAuthAuthenticateInterface[];
}
