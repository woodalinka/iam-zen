import { Inject, Injectable } from '@nestjs/common';
import { AppDataServiceInterface } from './app.data.service.interface';
import { Config } from '../config';
import { AppPersistedInterface } from './app.persisted.interface';
import { AppPermissionsProviderInterface } from './app.permissions-provider.interface';
import { ContextBuilder } from '@cryptexlabs/codex-nodejs-common';
import { AuthenticationServiceInterface } from '../authentication/authentication.service.interface';
import { AppInterface } from './app.interface';
import { PermissionUtil } from '../permission/permission.util';
import { TokenUtil } from '../token/token.util';
import { AppUtil } from './app.util';

@Injectable()
export class AppDefaultLoaderService {
  constructor(
    @Inject('APP_DATA_SERVICE')
    private readonly appDataService: AppDataServiceInterface,
    @Inject('CONFIG') private readonly config: Config,
    @Inject('APP_PERMISSIONS_PROVIDER')
    private readonly appPermissionsProvider: AppPermissionsProviderInterface,
    @Inject('AUTHENTICATION_SERVICE')
    private readonly authenticationService: AuthenticationServiceInterface,
    @Inject('CONTEXT_BUILDER') private readonly contextBuilder: ContextBuilder,
  ) {}

  public async load() {
    const context = this.contextBuilder.build().getResult();

    const permissionsMap = {};
    const persistedApps: AppPersistedInterface[] = [];

    for (const configApp of this.config.defaultApps) {
      const appForPermissions: AppInterface = {
        apiKeys: [],
        description: configApp.description,
        id: configApp.id,
        name: configApp.name,
        roles: configApp.roles || [],
      };

      const permissions =
        await this.appPermissionsProvider.getPermissionsForApp(
          context,
          appForPermissions,
        );
      permissionsMap[configApp.id] = permissions;

      persistedApps.push({
        id: configApp.id,
        name: configApp.name,
        description: configApp.description || null,
        roles: configApp.roles,
        apiKeys: this.authenticationService.getApiKeysFromApiAuthentication(
          configApp.authentications || [],
        ),
        hash: {
          permissions: PermissionUtil.getPermissionsHash(permissions),
          tokenExpirationPolicy: TokenUtil.getTokenExpirationPolicyHash(
            this.config.tokenExpirationPolicy,
          ),
          preConfiguredApiKeys: AppUtil.getAuthenticationsHash(
            configApp.authentications,
          ),
        },
      });
    }

    const configAppIds = persistedApps.map((persistedApp) => persistedApp.id);
    const existingApps = await this.appDataService.getAppsWithIds(
      context,
      configAppIds,
    );

    const newApps = await this.appDataService.saveAppsFromConfig(persistedApps);

    const newAppIds = [];
    for (const newApp of newApps) {
      newAppIds.push(newApp.id);
      const configApp = this.config.defaultApps.find(
        (configApp) => configApp.id === newApp.id,
      );

      await this.authenticationService.createApp(
        context,
        newApp.apiKeys,
        configApp.id,
        {
          name: configApp.name,
          description: configApp.description || null,
          authentications: configApp.authentications || [],
        },
        permissionsMap[configApp.id],
        this.config.tokenExpirationPolicy,
      );
    }

    for (const existingApp of existingApps) {
      const persistedApp = persistedApps.find(
        (findApp) => findApp.id === existingApp.id,
      );

      if (
        existingApp.hash.preConfiguredApiKeys !==
        persistedApp.hash.preConfiguredApiKeys
      ) {
        const configApp = this.config.defaultApps.find(
          (configApp) => configApp.id === existingApp.id,
        );

        await this.authenticationService.updateApiKeyAuthentications(
          context,
          existingApp.apiKeys,
          permissionsMap[configApp.id],
          this.config.tokenExpirationPolicy,
          existingApp.id,
          configApp.authentications,
        );
      }
    }
  }
}
