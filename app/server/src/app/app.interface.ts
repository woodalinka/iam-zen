import { ApiKeyInterface } from '../api-key/api-key.interface';

export interface AppInterface {
  id: string;
  name: string | null;
  description: string | null;
  apiKeys: ApiKeyInterface[];
  roles: string[];
}
