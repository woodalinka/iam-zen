import { AppPersistedInterface } from './app.persisted.interface';

export class AppInListDto {
  public readonly id: string;
  public readonly roleCount: number;
  public readonly apiKeyCount: number;
  public readonly name: string;

  constructor(app: AppPersistedInterface, public readonly immutable) {
    this.id = app.id;
    this.roleCount = app.roles.length;
    this.apiKeyCount = app.apiKeys.length;
    this.name = app.name;
  }
}
