import { ApiAuthAuthenticationProviderInterface } from '@cryptexlabs/authf-data-model';

export interface AppConfigInterface {
  name: string;
  description?: string;
  id: string;
  roles?: string[];
  authentications?: ApiAuthAuthenticationProviderInterface[];
}
