import { AppPersistedInterface } from './app.persisted.interface';
import { AppRoleInterface } from './app.role.interface';
import { AppPermissionInterface } from './app.permission.interface';
import { AppPolicyInterface } from './app.policy.interface';

export class AppDto {
  public readonly id: string;
  public readonly name: string;
  public readonly description: string;

  constructor(
    app: AppPersistedInterface,
    public readonly roles: AppRoleInterface[],
    public readonly policies: AppPolicyInterface[],
    public readonly permissions: AppPermissionInterface[],
    public readonly apiKeys: string[],
    public readonly immutable,
  ) {
    this.id = app.id;
    this.name = app.name;
    this.description = app.description;
  }
}
