import { CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';
import { HttpAuthzGuardUtil } from '@cryptexlabs/codex-nodejs-common';

export class AppAuthzModifyGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const guardUtil = new HttpAuthzGuardUtil(context);
    return guardUtil.isAuthorized({
      object: '',
      objectId: '',
      action: '',
    });
  }
}
