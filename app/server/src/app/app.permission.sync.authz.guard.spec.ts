import { AppPermissionSyncAuthzGuard } from './app.permission.sync.authz.guard';
import * as jwt from 'jsonwebtoken';
import { AuthzNamespace } from '../constants';
import { ExecutionContext } from '@nestjs/common';

describe(AppPermissionSyncAuthzGuard.name, () => {
  it('It should allow a super admin to sync permissions for an app', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::any:any:any:any:any:any:any:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            appId: '13b75180-0e05-472c-bbc9-8bf7c80f9c54',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppPermissionSyncAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('It should allow someone with permission to sync app permissions for an app to sync  app permissions', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::app:13b75180-0e05-472c-bbc9-8bf7c80f9c54::permission:all::updated::create`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            appId: '13b75180-0e05-472c-bbc9-8bf7c80f9c54',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppPermissionSyncAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('It should not allow someone with permission to sync a specific app permissions to sync  app permissions', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::app:13b75180-0e05-472c-bbc9-8bf7c80f9c54::permission:fd724e04-ced4-40b5-9a9d-cc861116f0c8::updated::create`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            appId: '13b75180-0e05-472c-bbc9-8bf7c80f9c54',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppPermissionSyncAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('It should allow someone with permission to do anything for a specific app to sync permissions for an app', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::app:13b75180-0e05-472c-bbc9-8bf7c80f9c54:any:any:any:any:any:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            appId: '13b75180-0e05-472c-bbc9-8bf7c80f9c54',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppPermissionSyncAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('It should allow someone with permission to do anything for a specific apps permissions to sync permissions for an app', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::app:13b75180-0e05-472c-bbc9-8bf7c80f9c54::permission:any::any:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            appId: '13b75180-0e05-472c-bbc9-8bf7c80f9c54',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppPermissionSyncAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('It should not allow someone without any permissions to sync permissions for an app', () => {
    const token = jwt.sign(
      {
        scopes: [],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            appId: '13b75180-0e05-472c-bbc9-8bf7c80f9c54',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppPermissionSyncAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });
});
