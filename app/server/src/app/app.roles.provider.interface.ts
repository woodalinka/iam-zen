import { AppRoleWithPolicies } from './app.role.interface';
import { Context } from '@cryptexlabs/codex-nodejs-common';

export interface AppRolesProviderInterface {
  getRolesForApp(
    context: Context,
    appId: string,
    roleIds: string[],
  ): Promise<AppRoleWithPolicies[]>;
}
