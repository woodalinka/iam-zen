import {
  ApiAuthAuthenticationProviderDataInterface,
  ApiAuthAuthenticationProviderInterface,
  AuthenticationProviderTypeEnum,
  AuthfMetaTypeEnum,
  CreateTokenResponseDataInterface,
  RefreshAuthAuthenticationInterface,
  TokenInterface,
  TokenPairInterface,
} from '@cryptexlabs/authf-data-model';
import { CreateAppInterface } from './create-app.interface';
import {
  ContextBuilder,
  RestResponse,
  ServiceClient,
} from '@cryptexlabs/codex-nodejs-common';
import { HttpStatus } from '@nestjs/common';
import { appConfig, baseLogger } from '../setup';
import { MessageContext } from '@cryptexlabs/codex-data-model';
import { i18nData } from '../locale/locales';
import { AppUpdateInterface } from './app.update.interface';
import { AppPatchInterface } from './app.patch.interface';

export const exampleAppId = '1a2ec631-208d-4157-a741-3be6334cae6b';
export const exampleApiKey = 'daf7cb4d-a463-43a2-b5ee-1ecf65bb26e6';
export const exampleAppName = 'MyWidget';

export class ExampleApiAuth implements ApiAuthAuthenticationProviderInterface {
  data: ApiAuthAuthenticationProviderDataInterface = {
    apiKey: exampleApiKey,
    secret: '12d2d44b1e904fd583c43a5b5f9df3357a8b7992',
  };
  type: AuthenticationProviderTypeEnum.API = AuthenticationProviderTypeEnum.API;
}

export class ExampleRefreshAuth implements RefreshAuthAuthenticationInterface {
  data: {
    token: string;
  } = {
    token:
      'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI0ZWEyN2M2ZS01N2IxLTRjZjctODIyYi1mM2RiODE3MWE4YmQiLCJ0eXBlIjoiYWNjZXNzIiwic2NvcGVzIjpbInVzZXI6c2VsZjphbGwiXSwiaWF0IjoxNjMxMDQzMDAzfQ.0hlmsRyS0SMs6LY8_9mfmyCKYr2yYSsWeC6ULct1GmH-IPsdDJM_p5Gjesa24R59v2_Ut6lJG6bPJKA1vGN5pg9HnaA2aBF4zGQ6zQayKZmElZxD2tv2pEmrjvWn1TXyKEga9Td9it2q9gxIwzDATeCPtzj7amvcgi47KI3jw5A_67jwdRkvimz9nagn4Y_xdahpcuxQD2tSQD-iRo7bnRS4cfAPNU_yXr6CP0HEbS7dLndvE6LfTPg5-WG3zwB6xNVMDCTYz_a7WAbj_knXzr2q0SnGsjVl_c1mxMSP3pmOye4KAVRMMXdIONd9_JGpnryAiE00Dp1bdPr-V4n-crkdq6rWVy-bG0ycnfJD6rCZNJkafsCNx0QTWs8iFNeciZ3NkU0_EMymK9_kAH4oO5sHu4eDvDMXBgABAmK_KHg_cZcKCnELNEy13H-ozEM9PDGDjBfvNwul9WyETSmLRtYBNAXlo5QOtFLzKh9lsoDjN0l1zQEgtaoDXqwSs7Ow1WP_1u_SB-WeI7uldwRGAuQHRLJHBwHyb42_8amqpcJJJpNa_6onFqaFgNyefUwaA596NokbMdno8kIwhk4l7NkjyFhGw3L0nI6YIY92Ym0C3ZVm8HxDyoznAgzvq_PI-lgpdoV8KFYOAZWutLBndylep3iqZlcL0MmpJeyvAnI',
  };
  type: AuthenticationProviderTypeEnum.REFRESH =
    AuthenticationProviderTypeEnum.REFRESH;
}

export const exampleNewApiAuthApp: CreateAppInterface = {
  name: exampleAppName,
  description: 'A great app',
};

export const exampleUpdateApp: AppUpdateInterface = {
  name: exampleAppName,
  description: 'A better description for an app was written here',
  roles: ['ec570e6e-caaf-43f6-ac6b-cea8b7949124'],
};

export const exampleApiKeys = ['N8FCHG1RWGNWIUQKXGN5', 'EB9DW3OM5OGZYGNM08IO'];

export const exampleAppRoles = [
  {
    appCount: 1,
    immutable: true,
    id: 'db1c7afb-0e59-496f-90e4-07c96757dd8c',
    name: 'Widget Role',
    policyCount: 1,
  },
];

export const exampleApps = [
  {
    immutable: false,
    id: '232f4ceb-eb91-4fb1-a42f-a01632d4bde5',
    roleCount: 0,
    apiKeyCount: 0,
    name: 'Other Cool widget',
  },
  {
    immutable: false,
    id: '3a693d90-6470-4b88-a6da-595cffc3a538',
    roleCount: 0,
    apiKeyCount: 0,
    name: 'OtherWidget',
  },
  {
    immutable: false,
    id: '40090cb5-96a4-4be6-bd1d-a50bf1f92fde',
    roleCount: 1,
    apiKeyCount: 0,
    name: 'MyWidget',
  },
  {
    immutable: true,
    id: '1a2ec631-208d-4157-a741-3be6334cae6b',
    roleCount: 1,
    apiKeyCount: 1,
    name: 'Widget',
  },
];

export const exampleApp = {
  roles: [
    {
      id: 'db1c7afb-0e59-496f-90e4-07c96757dd8c',
      name: 'Widget Role',
    },
  ],
  policies: [
    {
      id: '7da13f60-8536-4795-ade3-373aea49d1f7',
      name: 'IAM Zen Admin user',
    },
  ],
  permissions: [
    {
      id: '7da13f60-8536-4795-ade3-373aea49d1f7',
      name: 'IAM Zen Admin user',
      value: 'iam-zen:any:any:any',
    },
  ],
  apiKeys: ['daf7cb4d-a463-43a2-b5ee-1ecf65bb26e6'],
  immutable: true,
  id: '1a2ec631-208d-4157-a741-3be6334cae6b',
  name: 'Widget',
  description: 'Automatic widgetinator',
};

export const examplePatchApp: AppPatchInterface = {
  name: exampleAppName,
  description: 'A better description for an app was written here',
};

export class ExampleTokenPair implements TokenPairInterface {
  access: TokenInterface = {
    token:
      'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI0ZWEyN2M2ZS01N2IxLTRjZjctODIyYi1mM2RiODE3MWE4YmQiLCJ0eXBlIjoiYWNjZXNzIiwic2NvcGVzIjpbInVzZXI6c2VsZjphbGwiXSwiaWF0IjoxNjMxMDQzMDAzfQ.0hlmsRyS0SMs6LY8_9mfmyCKYr2yYSsWeC6ULct1GmH-IPsdDJM_p5Gjesa24R59v2_Ut6lJG6bPJKA1vGN5pg9HnaA2aBF4zGQ6zQayKZmElZxD2tv2pEmrjvWn1TXyKEga9Td9it2q9gxIwzDATeCPtzj7amvcgi47KI3jw5A_67jwdRkvimz9nagn4Y_xdahpcuxQD2tSQD-iRo7bnRS4cfAPNU_yXr6CP0HEbS7dLndvE6LfTPg5-WG3zwB6xNVMDCTYz_a7WAbj_knXzr2q0SnGsjVl_c1mxMSP3pmOye4KAVRMMXdIONd9_JGpnryAiE00Dp1bdPr-V4n-crkdq6rWVy-bG0ycnfJD6rCZNJkafsCNx0QTWs8iFNeciZ3NkU0_EMymK9_kAH4oO5sHu4eDvDMXBgABAmK_KHg_cZcKCnELNEy13H-ozEM9PDGDjBfvNwul9WyETSmLRtYBNAXlo5QOtFLzKh9lsoDjN0l1zQEgtaoDXqwSs7Ow1WP_1u_SB-WeI7uldwRGAuQHRLJHBwHyb42_8amqpcJJJpNa_6onFqaFgNyefUwaA596NokbMdno8kIwhk4l7NkjyFhGw3L0nI6YIY92Ym0C3ZVm8HxDyoznAgzvq_PI-lgpdoV8KFYOAZWutLBndylep3iqZlcL0MmpJeyvAnI',
    expiration: new Date(new Date().getTime() + 15 * 60 * 1000),
  };
  refresh: TokenInterface = {
    token:
      'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI0ZWEyN2M2ZS01N2IxLTRjZjctODIyYi1mM2RiODE3MWE4YmQiLCJ0eXBlIjoicmVmcmVzaCIsImlhdCI6MTYzMTA0MzAwM30.R9nIpPBZdcc6pW89TdLhpIyEdQUOBAQXw2qB8PQyjLx6P9iVPO_o9yLdjC0bdGxy7Vl8ETIYCJPcn0IBVgtqaMvzPx9mtFOrPh9orMKNbwXOF9F4Kl5R4mBHvP-sstpXiu64BIsGCTDXv7IQv1L0TlqzwrogOy4Wg5-GV3MaCZF0wJ9jehqD1hpqLrmBu3QtytdyNrJqkzrWU3T-Hvewxy-UtjeTcyljBY-R-q82toGEO2vHeO5E2SqVCdDVWozS6tok66zlelQdhMm4CPIkF43MRinUWey9OLENvKqUZ_ANzIO5ABBcuxouBTEigxV7Zz7oGjC2sGxc8oFKsW7lGMqqZdiiyD_Q0qRbQIe9lQbl2xgV83C-UGGJ0uyPVFKruTEoJ_8rFozAsKT-LFB3M0Zdx64ekjFOD7K1qZEOrLa9s5Qo22_gh9YA3zsk84_IXRBN1nk6mLeSC8EE6hJXFOwmFCc8r3jRT1BS1VNDFXAu74smyj5gK2eQ9tJv-aQ50AA8d7lm8Duo12OsNXH0HdS9ZKRefsC7r97N3wt8ggigK_qL-F6BVwtctK1kJXF9XjH64iW9qqVbgh7XvhmzMFkihwdLP6_QTUiK7MsDaxZNtYCaV9BTFNNAnUpIRWQ4tB7BjnDkpSd6jT4aJ31uvEBGrzqsPH2pNWfyfTv9nHo',
    expiration: new Date(new Date().getTime() + 30 * 24 * 60 * 60 * 1000),
  };
}

const responseData: CreateTokenResponseDataInterface = {
  token: new ExampleTokenPair(),
};

const exampleContext = new ContextBuilder(
  baseLogger,
  appConfig,
  new ServiceClient(appConfig),
  new MessageContext('default', null),
)
  .setI18nApi(i18nData)
  .build()
  .getResult();

export class ExampleAuthTokenPairResponse extends RestResponse {
  constructor() {
    super(
      exampleContext,
      HttpStatus.CREATED,
      AuthfMetaTypeEnum.AUTHENTICATION_TOKEN_PAIR,
      responseData,
    );
  }
}
