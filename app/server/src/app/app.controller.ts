import {
  Body,
  Controller,
  Delete,
  Get,
  Headers,
  HttpStatus,
  Inject,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Put,
  Query,
  Res,
  Response,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  ExampleApiAuth,
  exampleApiKeys,
  exampleApp,
  exampleAppId,
  exampleAppRoles,
  exampleApps,
  ExampleAuthTokenPairResponse,
  examplePatchApp,
  ExampleRefreshAuth,
  exampleUpdateApp,
} from './example';
import { AppService } from './app.service';
import {
  ApiMetaHeaders,
  ApiPagination,
  ContextBuilder,
  NotEmptyPipe,
  RestPaginatedResponse,
  RestResponse,
  SimpleHttpResponse,
} from '@cryptexlabs/codex-nodejs-common';
import { LocalesEnum } from '../locale/enum';
import { AppUpdateInterface } from './app.update.interface';
import { AppUpdateValidationPipe } from './app.update.validation.pipe';
import { AppPatchValidationPipe } from './app.patch.validation.pipe';
import { AppPatchInterface } from './app.patch.interface';
import { ExampleSimpleResponse } from '../example/example.simple.response';
import { ExampleRestResponse } from '../example/example.rest.response';
import { appConfig } from '../setup';
import { AppGetAuthzGuard } from './app.get.authz.guard';
import { AppDeleteAuthzGuard } from './app.delete.authz.guard';
import { AppUpdateAuthzGuard } from './app.update.authz.guard';
import { AppAttachRolesAuthzGuard } from './app.attach-roles.authz.guard';
import { AppDetachRolesAuthzGuard } from './app.detach-roles.authz.guard';
import { AppRoleListAuthzGuard } from './app.role.list.authz.guard';
import { AppPermissionSyncAuthzGuard } from './app.permission.sync.authz.guard';
import { AppApiKeyCreateAuthzGuard } from './app.api-key.create.authz.guard';
import { AppApiKeyDeleteAuthzGuard } from './app.api-key.delete.authz.guard';
import { AppApiKeyListAuthzGuard } from './app.api-key.list.authz.guard';
import { AppListAuthzGuard } from './app.list.authz.guard';
import { AppPermissionSyncAllAuthzGuard } from './app.permission.sync-all.authz.guard';
import { ExampleRestPaginatedResponse } from '../example/example.rest.paginated.response';
import { AppExistsAuthzGuard } from './app.exists.authz.guard';
import { Uuidv4ArrayValidationPipe } from '@cryptexlabs/codex-nodejs-common/lib/src/pipe/uuidv4.array.validation.pipe';
import {
  ApiAuthAuthenticateInterface,
  RefreshAuthAuthenticationInterface,
} from '@cryptexlabs/authf-data-model';
import { OrderDirectionEnum } from '../query.options.interface';
import { OrderDirectionValidationPipe } from '../order.direction.validation.pipe';

@Controller(`/${appConfig.appPrefix}/${appConfig.apiVersion}/app`)
@ApiTags('app')
@ApiMetaHeaders()
export class AppController {
  private static forceRefreshPermissionsDescription = `May be necessary when tokens are issued from an external authentication provider like AuthF.
    If applications are refreshing tokens and creating new tokens in with external authentication provider directly
    this API will use permissions defined in IAM Zen to update that external authentication provider so that
    the next time a token is issued it will be issued with the updated permissions.
    If an external authentication provider is not used then this endpoint is not necessary
    `;

  constructor(
    private readonly appService: AppService,
    @Inject('CONTEXT_BUILDER') private readonly contextBuilder: ContextBuilder,
  ) {}

  @Post(':appId/token')
  @ApiOperation({
    summary: 'Create a new token for an app (Login)',
  })
  @ApiBody({
    examples: {
      'Api Auth': {
        value: new ExampleApiAuth(),
      },
      Refresh: {
        value: new ExampleRefreshAuth(),
      },
    },
    schema: {},
    required: true,
  })
  @ApiParam({
    name: 'appId',
    example: exampleAppId,
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleAuthTokenPairResponse(),
    },
    status: HttpStatus.OK,
  })
  public async login(
    @Response() res,
    @Headers() headers,
    @Body(NotEmptyPipe)
    body: ApiAuthAuthenticateInterface | RefreshAuthAuthenticationInterface,
    @Param('appId', NotEmptyPipe) appId: string,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();
    return this.appService.createTokenAppWithId(context, res, appId, body);
  }

  @Get()
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Get list of apps',
  })
  @ApiPagination()
  @ApiQuery({
    name: 'orderBy',
    required: false,
    schema: {
      example: 'name',
    },
  })
  @ApiQuery({
    name: 'orderDirection',
    required: false,
    schema: {
      example: 'asc',
    },
  })
  @ApiQuery({
    name: 'searchName',
    required: false,
  })
  @ApiQuery({
    name: 'search',
    required: false,
  })
  @ApiResponse({
    schema: {
      example: new ExampleRestPaginatedResponse(
        HttpStatus.OK,
        'cryptexlabs.iam-zen.app.list',
        exampleApps,
      ),
    },
    status: HttpStatus.OK,
  })
  @UseGuards(AppListAuthzGuard)
  public async getApps(
    @Query('page', NotEmptyPipe, ParseIntPipe) page: number,
    @Query('pageSize', NotEmptyPipe, ParseIntPipe) pageSize: number,
    @Query('searchName') searchName: string,
    @Query('search') search: string,
    @Query('orderBy') orderBy: string,
    @Query('orderDirection', OrderDirectionValidationPipe)
    orderDirection: OrderDirectionEnum,
    @Headers() headers: Record<string, string>,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const apps = await this.appService.getApps(context, page, pageSize, {
      searchName,
      search,
      orderBy,
      orderDirection,
    });

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestPaginatedResponse(
      responseContext,
      HttpStatus.OK,
      apps.total,
      'cryptexlabs.iam-zen.app.list',
      apps.results,
    );
  }

  @Get(':appId')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Get an app by app ID',
  })
  @ApiParam({
    name: 'appId',
    example: '1a2ec631-208d-4157-a741-3be6334cae6b',
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleRestResponse(
        HttpStatus.OK,
        'cryptexlabs.iam-zen.app',
        exampleApp,
      ),
    },
    status: HttpStatus.OK,
  })
  @UseGuards(AppGetAuthzGuard)
  public async getApp(
    @Param('appId', NotEmptyPipe) appId: string,
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const app = await this.appService.getAppById(context, appId);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestResponse(
      responseContext,
      HttpStatus.OK,
      'cryptexlabs.iam-zen.app',
      app,
    );
  }

  @Get('any/exists')
  @ApiOperation({
    summary: 'Get whether or not an app exists',
  })
  @ApiQuery({
    name: 'name',
    example: 'Widget',
    required: true,
  })
  @UseGuards(AppExistsAuthzGuard)
  @ApiResponse({
    schema: {
      example: new ExampleRestResponse(HttpStatus.OK, 'boolean', false),
    },
  })
  @ApiBearerAuth()
  public async getAppExists(
    @Query('name', NotEmptyPipe) name: string,
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const appExists = await this.appService.getAppExists(context, name);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestResponse(
      responseContext,
      HttpStatus.OK,
      'boolean',
      appExists,
    );
  }

  @Delete(':appId')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Delete an app by app ID',
  })
  @ApiParam({
    name: 'appId',
    example: '40090cb5-96a4-4be6-bd1d-a50bf1f92fde',
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @UseGuards(AppDeleteAuthzGuard)
  public async deleteApp(
    @Param('appId', NotEmptyPipe) appId: string,
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    await this.appService.deleteApp(context, appId);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.NO_CONTENT,
      LocalesEnum.SUCCESS,
    );
  }

  @Delete()
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Delete an app by app ID',
  })
  @ApiQuery({
    name: 'ids',
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @UseGuards(AppDeleteAuthzGuard)
  public async deleteApps(
    @Query('ids', NotEmptyPipe) appIds: string[],
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const useAppIds = Array.isArray(appIds) ? appIds : [appIds];

    await this.appService.deleteApps(context, useAppIds);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.NO_CONTENT,
      LocalesEnum.SUCCESS,
    );
  }

  @Put(':appId')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Create or update an app by app ID',
  })
  @ApiParam({
    name: 'appId',
    example: '40090cb5-96a4-4be6-bd1d-a50bf1f92fde',
    required: true,
  })
  @ApiBody({
    schema: {
      example: exampleUpdateApp,
    },
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @UseGuards(AppUpdateAuthzGuard)
  public async updateApp(
    @Param('appId', NotEmptyPipe) appId: string,
    @Res() res: any,
    @Headers() headers,
    @Body(NotEmptyPipe, AppUpdateValidationPipe) body: AppUpdateInterface,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const httpStatus = await this.appService.updateApp(context, appId, body);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    res
      .status(httpStatus)
      .send(
        new SimpleHttpResponse(
          responseContext,
          HttpStatus.ACCEPTED,
          LocalesEnum.SUCCESS,
        ),
      );
  }

  @Patch(':appId')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Partially update an app by app ID',
  })
  @ApiParam({
    name: 'appId',
    example: '40090cb5-96a4-4be6-bd1d-a50bf1f92fde',
    required: true,
  })
  @ApiBody({
    schema: {
      example: examplePatchApp,
    },
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @UseGuards(AppUpdateAuthzGuard)
  public async patchApp(
    @Param('appId', NotEmptyPipe) appId: string,
    @Headers() headers,
    @Body(NotEmptyPipe, AppPatchValidationPipe) body: AppPatchInterface,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    await this.appService.patchApp(context, appId, body);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Post(':appId/role')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Add roles to app',
  })
  @ApiParam({
    name: 'appId',
    example: '40090cb5-96a4-4be6-bd1d-a50bf1f92fde',
    required: true,
  })
  @ApiBody({
    examples: {
      'Add multiple roles': {
        value: [
          'db1c7afb-0e59-496f-90e4-07c96757dd8c',
          'f331b48c-e26c-4f13-b588-7e9f0cbfeaa8',
        ],
        description: 'Add multiple roles at once',
      },
    },
    schema: {},
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @UseGuards(AppAttachRolesAuthzGuard)
  public async addRoles(
    @Param('appId', NotEmptyPipe) appId: string,
    @Body(NotEmptyPipe, Uuidv4ArrayValidationPipe) roles: string[],
    @Headers() headers,
  ): Promise<void> {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    await this.appService.addRoles(context, appId, roles);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Delete(':appId/role')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Remove roles from app',
  })
  @ApiQuery({
    name: 'ids',
    example: [
      'db1c7afb-0e59-496f-90e4-07c96757dd8c',
      'd86b135e-1b5b-412d-af6d-7ecfe7a473f5',
    ],
    required: true,
  })
  @ApiParam({
    name: 'appId',
    example: '40090cb5-96a4-4be6-bd1d-a50bf1f92fde',
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @UseGuards(AppDetachRolesAuthzGuard)
  public async removeRoles(
    @Param('appId', NotEmptyPipe) appId: string,
    @Query('ids', NotEmptyPipe) ids: string[],
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const roleIds = typeof ids === 'string' ? [ids] : ids;
    await this.appService.removeRoles(context, appId, roleIds);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Get(':appId/role')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Get roles for app',
  })
  @ApiParam({
    name: 'appId',
    example: '40090cb5-96a4-4be6-bd1d-a50bf1f92fde',
  })
  @ApiPagination()
  @ApiResponse({
    schema: {
      example: new ExampleRestPaginatedResponse(
        HttpStatus.OK,
        'cryptexlabs.iam-zen.role.list.simple',
        exampleAppRoles,
      ),
    },
    status: HttpStatus.OK,
  })
  @UseGuards(AppRoleListAuthzGuard)
  public async getAppRoles(
    @Param('appId') appId: string,
    @Query('page', NotEmptyPipe, ParseIntPipe) page: number,
    @Query('pageSize', NotEmptyPipe, ParseIntPipe) pageSize: number,
    @Headers() headers: Record<string, string>,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const roles = await this.appService.getRoles(
      context,
      appId,
      page,
      pageSize,
    );

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestPaginatedResponse(
      responseContext,
      HttpStatus.OK,
      roles.total,
      'cryptexlabs.iam-zen.role.list.simple',
      roles.results,
    );
  }

  @Post(':appId/permission/all/updated')
  @ApiBearerAuth()
  @ApiTags('permission')
  @ApiOperation({
    summary: 'Force permissions to be updated for an app',
    description: AppController.forceRefreshPermissionsDescription,
  })
  @ApiParam({
    name: 'appId',
    example: '40090cb5-96a4-4be6-bd1d-a50bf1f92fde',
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @UseGuards(AppPermissionSyncAuthzGuard)
  public async updateAppPermissions(
    @Res() res,
    @Headers() headers,
    @Param('appId', NotEmptyPipe) appId: string,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    if (appId === 'all') {
      return this.updateAppPermissionsForAllApps(res, headers);
    }
    const httpStatus = await this.appService.updatePermissions(context, appId);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    res
      .status(httpStatus)
      .send(
        new SimpleHttpResponse(
          responseContext,
          httpStatus,
          LocalesEnum.SUCCESS,
        ),
      );
  }

  @Post('all/permission/all/updated')
  @ApiBearerAuth()
  @ApiTags('permission')
  @ApiOperation({
    summary: 'Force permissions to be updated for all apps',
    description: `Runs in background after response is sent. ${AppController.forceRefreshPermissionsDescription}`,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @UseGuards(AppPermissionSyncAllAuthzGuard)
  public async updateAppPermissionsForAllApps(@Res() res, @Headers() headers) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    this.appService.updatePermissionsForAllApps(context).then(() => {
      setTimeout(() => {
        context.logger.log(`All apps permissions update complete`);
      }, 5000);
    });

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    res
      .status(HttpStatus.ACCEPTED)
      .send(
        new SimpleHttpResponse(
          responseContext,
          HttpStatus.ACCEPTED,
          LocalesEnum.SUCCESS,
        ),
      );
  }

  @Post(':appId/api-key')
  @ApiBearerAuth()
  @ApiTags('api-key')
  @ApiOperation({
    summary: 'Creates a new API key and secret for an app',
  })
  @ApiParam({
    name: 'appId',
    example: '40090cb5-96a4-4be6-bd1d-a50bf1f92fde',
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleRestResponse(
        HttpStatus.OK,
        'cryptexlabs.iam-zen.api-key',
        {
          apiKey: 'N8FCHG1RWGNWIUQKXGN5',
          secret:
            'a635d5de20bf39b0641b7f06cdce2a794c9799c856966a9a23a5a3edaa7b91401619dc8368d78842',
        },
      ),
    },
    status: HttpStatus.OK,
  })
  @UseGuards(AppApiKeyCreateAuthzGuard)
  public async createApiKey(
    @Headers() headers,
    @Param('appId', NotEmptyPipe) appId: string,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const apiKey = await this.appService.createApiKey(context, appId);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestResponse(
      responseContext,
      HttpStatus.CREATED,
      'cryptexlabs.iam-zen.api-key',
      apiKey,
    );
  }

  @Delete(':appId/api-key/:apiKey')
  @ApiBearerAuth()
  @ApiTags('api-key')
  @ApiOperation({
    summary: 'Deletes an API key for an app',
  })
  @ApiParam({
    name: 'appId',
    example: '40090cb5-96a4-4be6-bd1d-a50bf1f92fde',
    required: true,
  })
  @ApiParam({
    name: 'apiKey',
    example: 'N8FCHG1RWGNWIUQKXGN5',
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @UseGuards(AppApiKeyDeleteAuthzGuard)
  public async deleteApiKey(
    @Headers() headers,
    @Param('appId', NotEmptyPipe) appId: string,
    @Param('apiKey', NotEmptyPipe) apiKey: string,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    await this.appService.removeApiKeys(context, appId, [apiKey]);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Get(':appId/api-key')
  @ApiBearerAuth()
  @ApiTags('api-key')
  @ApiOperation({
    summary: 'Gets the API keys for an app',
  })
  @ApiParam({
    name: 'appId',
    example: '40090cb5-96a4-4be6-bd1d-a50bf1f92fde',
    required: true,
  })
  @ApiPagination()
  @ApiResponse({
    schema: {
      example: new ExampleRestPaginatedResponse(
        HttpStatus.OK,
        'cryptexlabs.iam-zen.api-key.list',
        exampleApiKeys,
      ),
    },
    status: HttpStatus.OK,
  })
  @UseGuards(AppApiKeyListAuthzGuard)
  public async getApiKeys(
    @Headers() headers,
    @Param('appId', NotEmptyPipe) appId: string,
    @Query('page', NotEmptyPipe, ParseIntPipe) page: number,
    @Query('pageSize', NotEmptyPipe, ParseIntPipe) pageSize: number,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const apiKeys = await this.appService.getApiKeyIds(
      context,
      appId,
      page,
      pageSize,
    );

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestResponse(
      responseContext,
      HttpStatus.CREATED,
      'cryptexlabs.iam-zen.api-key.list',
      apiKeys,
    );
  }
}
