import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { HttpAuthzGuardUtil } from '@cryptexlabs/codex-nodejs-common';
import { AuthzNamespace } from '../constants';

@Injectable()
export class AppExistsAuthzGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const util = new HttpAuthzGuardUtil(context);

    return (
      util.isAuthorized(AuthzNamespace, {
        action: 'get',
        object: 'app',
        objectId: 'any',
      }) ||
      (util.isAuthorized(AuthzNamespace, {
        action: 'list',
        object: 'app',
        objectId: '',
      }) &&
        !util.isAuthorized(AuthzNamespace, {
          action: 'any',
          object: 'app',
          objectId: '',
        })) ||
      (util.isAuthorized(AuthzNamespace, {
        action: 'create',
        object: 'app',
        objectId: '',
      }) &&
        !util.isAuthorized(AuthzNamespace, {
          action: 'any',
          object: 'app',
          objectId: '',
        }))
    );
  }
}
