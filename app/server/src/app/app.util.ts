import { ApiAuthAuthenticationProviderInterface } from '@cryptexlabs/authf-data-model';
import { StringUtil } from '@cryptexlabs/codex-nodejs-common';

export class AppUtil {
  static getAuthenticationsHash(
    authentications: ApiAuthAuthenticationProviderInterface[],
  ): string {
    return StringUtil.sha512(JSON.stringify(authentications));
  }
}
