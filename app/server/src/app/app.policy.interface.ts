export interface AppPolicyInterface {
  id: string;
  name: string;
  description: string;
  fromRoles: {
    id: string;
    name: string;
  }[];
}

export interface AppPolicyWithPermissionsInterface extends AppPolicyInterface {
  permissions: string[];
}
