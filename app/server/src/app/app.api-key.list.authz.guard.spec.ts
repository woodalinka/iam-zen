import { AppApiKeyListAuthzGuard } from './app.api-key.list.authz.guard';
import { ExecutionContext } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { AuthzNamespace } from '../constants';

describe(AppApiKeyListAuthzGuard.name, () => {
  it('Should allow super admin to list api keys for an app', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::any:any:any:any:any:any:any:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            appId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppApiKeyListAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to list api keys for an app to attach a role to the app', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::app:4d2114ca-24e2-43e5-bddb-d9a6688b8340::api-key:any:list`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            appId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppApiKeyListAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to do anything on an app to attach a role to the app', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::app:4d2114ca-24e2-43e5-bddb-d9a6688b8340::api-key:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            appId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppApiKeyListAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to do anything to any sub object for an app to attach a role to the app', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::app:4d2114ca-24e2-43e5-bddb-d9a6688b8340::any:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            appId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppApiKeyListAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should not allow someone with permission to attach any role to a different app to attach a role to the app', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::app:4d2114ca-24e2-43e5-bddb-d9a6688b8340::api-key:any:list`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            appId: '001d4f53-798b-4a0b-8ef7-330a7bf72147',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppApiKeyListAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should not allow someone with permission to do anything to any permission on a different app to attach a role to the app', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::app:4d2114ca-24e2-43e5-bddb-d9a6688b8340::api-key:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            appId: '001d4f53-798b-4a0b-8ef7-330a7bf72147',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppApiKeyListAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should not allow someone with permission to do anything to any sub object for a different app to attach a role to the app', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::app:4d2114ca-24e2-43e5-bddb-d9a6688b8340::any:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            appId: '001d4f53-798b-4a0b-8ef7-330a7bf72147',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppApiKeyListAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });
});
