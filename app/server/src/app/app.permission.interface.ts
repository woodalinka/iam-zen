export interface AppPermissionInterface {
  id: string;
  name: string;
  value: string;
  fromPolicies: {
    id: string;
    name: string;
  }[];
  fromRoles: {
    id: string;
    name: string;
  }[];
}
