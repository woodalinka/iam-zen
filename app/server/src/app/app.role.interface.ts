export interface AppRoleInterface {
  id: string;
  name: string;
  description: string;
  immutable: boolean;
}

export interface AppRoleWithPolicies extends AppRoleInterface {
  policies: string[];
}
