import {
  forwardRef,
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { AppDataServiceInterface } from './app.data.service.interface';
import { AuthenticationServiceInterface } from '../authentication/authentication.service.interface';
import { Config } from '../config';
import { AuthenticationServiceTypeEnum } from '../authentication/authentication-service.type.enum';
import { v4 as uuidv4 } from 'uuid';
import { AuthenticationServiceFactory } from '../authentication/authentication-service-factory';
import { CreateAppInterface } from './create-app.interface';
import {
  ArrayUtil,
  Context,
  PaginatedResults,
} from '@cryptexlabs/codex-nodejs-common';
import { AppPermissionsProviderInterface } from './app.permissions-provider.interface';
import { AppPersistedInterface } from './app.persisted.interface';
import { AppDto } from './app.dto';
import { AppInListDto } from './app.in-list.dto';
import { AppRolesProviderInterface } from './app.roles.provider.interface';
import { ApiKeyTypeEnum } from '../api-key/api-key.type.enum';
import { AuthfApiKeyInterface } from '../authentication/authf.api-key.interface';
import { ApiKeyInterface } from '../api-key/api-key.interface';
import {
  ApiAuthAuthenticateDataInterface,
  ApiAuthAuthenticateInterface,
  AuthenticationProviderTypeEnum,
  RefreshAuthAuthenticationInterface,
} from '@cryptexlabs/authf-data-model';
import * as jwt from 'jsonwebtoken';
import { AppUpdateInterface } from './app.update.interface';
import { ImmutableObjectError } from '../error/immutable-object.error';
import { LocalesEnum } from '../locale/enum';
import { RoleService } from '../role/role.service';
import { PermissionUtil } from '../permission/permission.util';
import { TokenUtil } from '../token/token.util';
import { RoleAppsProviderInterface } from '../role/role.apps.provider.interface';
import {
  RoleAppDetailInterface,
  RoleAppInterface,
} from '../role/role.app.interface';
import { AppPoliciesProviderInterface } from './app.policies-provider.interface';
import { AppPatchInterface } from './app.patch.interface';
import { RoleInListDto } from '../role/role.in-list.dto';
import { QueryOptionsInterface } from '../query.options.interface';

@Injectable()
export class AppService implements RoleAppsProviderInterface {
  constructor(
    @Inject('CONFIG') private readonly config: Config,
    @Inject('APP_DATA_SERVICE')
    private readonly appDataService: AppDataServiceInterface,
    @Inject('APP_PERMISSIONS_PROVIDER')
    private readonly permissionsProvider: AppPermissionsProviderInterface,
    @Inject('APP_POLICIES_PROVIDER')
    private readonly policiesProvider: AppPoliciesProviderInterface,
    @Inject('APP_ROLES_PROVIDER')
    private readonly appRolesProvider: AppRolesProviderInterface,
    @Inject('AUTHENTICATION_SERVICE')
    private readonly authenticationService: AuthenticationServiceInterface,
    private readonly authenticationServiceFactory: AuthenticationServiceFactory,
    @Inject(forwardRef(() => 'ROLE_SERVICE'))
    private readonly roleService: RoleService,
  ) {}

  public async createTokenAppWithId(
    context: Context,
    res: any,
    appId: string,
    appLogin: ApiAuthAuthenticateInterface | RefreshAuthAuthenticationInterface,
  ) {
    const app = await this.appDataService.getAppWithId(appId);

    if (!app) {
      throw new HttpException(
        `App with id ${appId} not found`,
        HttpStatus.NOT_FOUND,
      );
    }

    return this._createToken(context, res, app, appLogin);
  }

  private async _createToken(
    context: Context,
    res: any,
    app: AppPersistedInterface,
    appLogin: ApiAuthAuthenticateInterface | RefreshAuthAuthenticationInterface,
  ) {
    let loginApiKey: ApiKeyInterface;
    if (appLogin.type === AuthenticationProviderTypeEnum.REFRESH) {
      const decodedAccessToken = jwt.decode(
        appLogin.data.token,
      ) as unknown as any;
      loginApiKey = this.authenticationService.getApiKeyForRefreshTokenSub(
        app.apiKeys,
        decodedAccessToken.sub,
      );
    } else {
      loginApiKey = app.apiKeys.find(
        (apiKey) => apiKey.id === appLogin.data.apiKey,
      );
    }

    if (!loginApiKey) {
      throw new HttpException('Not authorized', HttpStatus.UNAUTHORIZED);
    }

    await this._refreshAppPermissionsWithPersistedApp(context, app, [
      loginApiKey,
    ]);

    return await this.authenticationService.loginApp(
      context,
      res,
      app,
      loginApiKey,
      appLogin,
    );
  }

  public async createApp(
    context: Context,
    appId: string | null,
    newApp: CreateAppInterface,
    authenticationId?: string,
  ) {
    if (!appId && !newApp.name) {
      throw new HttpException(
        `Must provide either an appId or a name`,
        HttpStatus.BAD_REQUEST,
      );
    }

    const app = await this.appDataService.getAppWithId(appId);
    if (app) {
      throw new HttpException(
        `App with id: ${appId} already exists`,
        HttpStatus.CONFLICT,
      );
    }

    let loginApiKey: ApiKeyInterface;
    const useAppId = appId || uuidv4();

    if (
      this.config.authenticationProviderType ===
      AuthenticationServiceTypeEnum.AUTHF
    ) {
      loginApiKey = {
        type: ApiKeyTypeEnum.AUTHF,
        data: {
          authenticationId: authenticationId || uuidv4(),
        },
      } as AuthfApiKeyInterface;
    }

    if (!loginApiKey) {
      throw new HttpException(
        `Unsupported authentication provider type: ${this.config.authenticationProviderType}`,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }

    const tokenExpirationPolicy = this.config.tokenExpirationPolicy;

    const persistApp: AppPersistedInterface = {
      id: useAppId,
      apiKeys: [],
      name: newApp.name,
      roles: [],
      description: newApp.description || null,
      hash: {
        permissions: PermissionUtil.getPermissionsHash([]),
        tokenExpirationPolicy: TokenUtil.getTokenExpirationPolicyHash(
          tokenExpirationPolicy,
        ),
      },
    };

    await this.appDataService.createApp(context, persistApp);
  }

  public async refreshAppPermissions(
    context: Context,
    appId: string,
    apiKey: ApiKeyInterface,
    waitForAppToExist = false,
  ) {
    let persistedApp;

    while (
      !(persistedApp = await this.appDataService.getAppWithId(appId)) &&
      waitForAppToExist
    ) {
      await new Promise((resolve) => setTimeout(resolve, 20));
    }
    await this._refreshAppPermissionsWithPersistedApp(context, persistedApp, [
      apiKey,
    ]);
  }

  public async getApps(
    context: Context,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<AppInListDto>> {
    const results = await this.appDataService.getApps(
      context,
      page,
      pageSize,
      options,
    );
    return {
      total: results.total,
      results: results.results.map((persistedApp) => {
        const immutable = !!this.config.defaultApps.find(
          (configApp) => configApp.id === persistedApp.id,
        );
        return new AppInListDto(persistedApp, immutable);
      }),
    };
  }

  private async _refreshAppPermissionsWithPersistedApp(
    context: Context,
    persistedApp: AppPersistedInterface,
    apiKeys: ApiKeyInterface[],
  ): Promise<HttpStatus> {
    const currentPermissions =
      await this.permissionsProvider.getPermissionsForApp(
        context,
        persistedApp,
      );
    const currentPermissionsHash =
      PermissionUtil.getPermissionsHash(currentPermissions);
    const tokenExpirationPolicy = this.config.tokenExpirationPolicy;
    const currentTokenExpirationPolicyHash =
      TokenUtil.getTokenExpirationPolicyHash(tokenExpirationPolicy);

    if (
      persistedApp.hash.permissions !== currentPermissionsHash ||
      persistedApp.hash.tokenExpirationPolicy !==
        currentTokenExpirationPolicyHash
    ) {
      await this.authenticationService.updateTokenPayload(
        context,
        null,
        persistedApp.id,
        currentPermissions,
        tokenExpirationPolicy,
        apiKeys,
        false,
      );
      await this.appDataService.updateTokenHashes(
        context,
        persistedApp.id,
        currentPermissionsHash,
        currentTokenExpirationPolicyHash,
      );
      return HttpStatus.OK;
    } else {
      return HttpStatus.NO_CONTENT;
    }
  }

  public async getAppById(context: Context, appId: string): Promise<AppDto> {
    const app = await this.appDataService.getAppWithId(appId);
    if (!app) {
      throw new HttpException('App does not exist', HttpStatus.NOT_FOUND);
    }
    const immutable = !!this.config.defaultApps.find(
      (configApp) => configApp.id === app.id,
    );

    const roles = await this.appRolesProvider.getRolesForApp(
      context,
      appId,
      app.roles,
    );

    const policies = await this.policiesProvider.getPoliciesForApp(
      context,
      app,
    );

    const permissions = await this.permissionsProvider.getPermissionsForApp(
      context,
      app,
    );

    const appPermissions = permissions.map((permission) => {
      const fromPolicies = [];
      for (const policy of policies) {
        if (policy.permissions.includes(permission.id)) {
          fromPolicies.push({
            id: policy.id,
            name: policy.name,
          });
        }
      }

      const fromRoles = [];
      const policyIds = fromPolicies.map((policy) => policy.id);
      for (const role of roles) {
        const intersect = role.policies.filter((policyId) =>
          policyIds.includes(policyId),
        );
        if (intersect.length > 0) {
          fromRoles.push({
            id: role.id,
            name: role.name,
          });
        }
      }

      return {
        id: permission.id,
        name: permission.name,
        value: permission.value,
        fromPolicies,
        fromRoles,
      };
    });

    const rolesForResponse = roles.map((role) => ({
      id: role.id,
      name: role.name,
      description: role.description,
      immutable: role.immutable,
    }));

    const policiesForResponse = policies.map((policy) => {
      return {
        id: policy.id,
        name: policy.name,
        description: policy.description,
        fromRoles: policy.fromRoles,
      };
    });

    return new AppDto(
      app,
      rolesForResponse,
      policiesForResponse,
      appPermissions,
      app.apiKeys.map((apiKey) => apiKey.id),
      immutable,
    );
  }

  public async getAppExists(context: Context, name: string): Promise<boolean> {
    const app = await this.appDataService.getAppWithName(context, name);
    return !!app;
  }

  public async deleteApp(context: Context, appId: string): Promise<void> {
    this._immutabilityCheck(context, appId);
    const changed = await this.appDataService.deleteAppById(context, appId);
    if (!changed) {
      throw new NotFoundException();
    }
  }

  public async deleteApps(context: Context, appIds: string[]): Promise<void> {
    for (const appId of appIds) {
      this._immutabilityCheck(context, appId);
    }
    const changed = await this.appDataService.deleteAppsByIds(context, appIds);

    if (!changed) {
      throw new NotFoundException();
    }
  }

  public async updateApp(
    context: Context,
    appId: string,
    appUpdate: AppUpdateInterface,
  ): Promise<HttpStatus> {
    this._immutabilityCheck(context, appId);
    const changed = await this.appDataService.updateAppById(
      context,
      appId,
      appUpdate,
    );
    if (!changed) {
      await this.createApp(context, appId, appUpdate);
      return HttpStatus.CREATED;
    } else {
      return HttpStatus.ACCEPTED;
    }
  }

  public async patchApp(
    context: Context,
    appId: string,
    appPatch: AppPatchInterface,
  ): Promise<void> {
    this._immutabilityCheck(context, appId);
    const changed = await this.appDataService.patchAppWithId(
      context,
      appId,
      appPatch,
    );
    if (!changed) {
      throw new NotFoundException();
    }
  }

  public async addRoles(context: Context, appId: string, roles: string[]) {
    // check if roles exist
    const foundRoles = await this.roleService.getRolesByIds(context, roles);

    if (foundRoles.length !== roles.length) {
      throw new HttpException(`Invalid roles`, HttpStatus.BAD_REQUEST);
    }
    await this.appDataService.addRoles(context, appId, roles);
  }

  public async removeRoles(context: Context, appId: string, roles: string[]) {
    const immutableApp = this.config.defaultApps.find(
      (findApp) => findApp.id === appId,
    );
    if (immutableApp) {
      if (
        immutableApp.roles.filter((role) => roles.includes(role)).length > 0
      ) {
        throw new ImmutableObjectError(
          context,
          roles.length === 1
            ? LocalesEnum.ERROR_IMMUTABLE_APP_ROLE_DELETE_SINGLE
            : LocalesEnum.ERROR_IMMUTABLE_APP_ROLE_DELETE_MULTIPLE,
        );
      }
    }

    await this.appDataService.removeRoles(context, appId, roles);
  }

  public async getRoles(
    context: Context,
    appId: string,
    page: number,
    pageSize: number,
  ): Promise<PaginatedResults<RoleInListDto>> {
    const roleIds = await this.appDataService.getRoleIds(context, appId);
    return this.roleService.getRoles(context, page, pageSize, { roleIds });
  }

  public async updatePermissions(
    context: Context,
    appId: string,
  ): Promise<HttpStatus> {
    const app = await this.appDataService.getAppWithId(appId);
    if (!app) {
      throw new NotFoundException();
    }
    return await this._refreshAppPermissionsWithPersistedApp(
      context,
      app,
      app.apiKeys,
    );
  }

  public async updatePermissionsForAllApps(context: Context): Promise<void> {
    const apps = await this.appDataService.getAllApps(context);

    for (const app of apps) {
      await this._refreshAppPermissionsWithPersistedApp(
        context,
        app,
        app.apiKeys,
      );
    }
  }

  private _immutabilityCheck(context: Context, appId: string) {
    const immutableApp = this.config.defaultApps.find(
      (findApp) => findApp.id === appId,
    );
    if (immutableApp) {
      throw new ImmutableObjectError(context, LocalesEnum.ERROR_IMMUTABLE_APP);
    }
  }

  public async createApiKey(
    context: Context,
    appId: string,
  ): Promise<ApiAuthAuthenticateDataInterface> {
    const app = await this.appDataService.getAppWithId(appId);

    const permissions = await this.permissionsProvider.getPermissionsForApp(
      context,
      app,
    );

    const newApiKey = await this.authenticationService.createApiKeyForApp(
      context,
      app,
      permissions,
      this.config.tokenExpirationPolicy,
    );

    await this.appDataService.addApiKey(context, appId, newApiKey.apiKey);

    return newApiKey.authenticationData;
  }

  public async removeApiKeys(
    context: Context,
    appId: string,
    apiKeyIds: string[],
  ): Promise<void> {
    const immutableApp = this.config.defaultApps.find(
      (findApp) => findApp.id === appId,
    );
    if (immutableApp) {
      const immutableApiKeys = immutableApp.authentications.map(
        (authentication) => authentication.data.apiKey,
      );
      if (ArrayUtil.intersection(apiKeyIds, immutableApiKeys).length > 0) {
        throw new ImmutableObjectError(
          context,
          apiKeyIds.length === 1
            ? LocalesEnum.ERROR_IMMUTABLE_APP_API_KEY_DELETE_SINGLE
            : LocalesEnum.ERROR_IMMUTABLE_APP_API_KEY_DELETE_MULTIPLE,
        );
      }
    }

    const apiKeys = await this.appDataService.getApiKeysByIds(
      context,
      appId,
      apiKeyIds,
    );
    if (apiKeys === false) {
      throw new NotFoundException();
    }

    await this.authenticationService.deleteApiKeys(
      context,
      apiKeys as ApiKeyInterface[],
    );

    await this.appDataService.removeApiKeys(context, appId, apiKeyIds);
  }

  public async getApiKeyIds(
    context: Context,
    appId: string,
    page: number,
    pageSize: number,
  ): Promise<string[]> {
    const apiKeys = await this.appDataService.getApiKeys(
      context,
      appId,
      page,
      pageSize,
    );

    return apiKeys.map((apiKey) => apiKey.id);
  }

  public async getAppsForRoles(
    context: Context,
    roleIds: string[],
  ): Promise<RoleAppInterface[]> {
    const apps = await this.appDataService.getAppsWithRoleIds(context, roleIds);
    return apps.map((app) => ({
      id: app.id,
      name: app.name,
      roles: app.roles,
    }));
  }

  public async getAppsForRole(
    context: Context,
    roleId: string,
  ): Promise<RoleAppDetailInterface[]> {
    const apps = await this.appDataService.getAppsWithRoleIds(context, [
      roleId,
    ]);
    return apps.map((app) => {
      const configRole = this.config.defaultRoles.find((r) => r.id === roleId);
      let immutable = false;
      if (configRole) {
        immutable = configRole.policies.includes(app.id);
      }

      return {
        id: app.id,
        name: app.name,
        description: app.description,
        immutable,
      };
    });
  }
}
