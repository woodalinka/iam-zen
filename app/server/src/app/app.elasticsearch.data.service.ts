import { AppDataServiceInterface } from './app.data.service.interface';
import { Inject, Injectable } from '@nestjs/common';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import {
  ArrayUtil,
  Context,
  CustomLogger,
  ElasticsearchInitializer,
  PaginatedResults,
} from '@cryptexlabs/codex-nodejs-common';
import { AppPersistedInterface } from './app.persisted.interface';
import { ApiKeyInterface } from '../api-key/api-key.interface';
import { AppUpdateInterface } from './app.update.interface';
import { AppPatchInterface } from './app.patch.interface';
import { QueryOptionsInterface } from '../query.options.interface';

@Injectable()
export class AppElasticsearchDataService implements AppDataServiceInterface {
  private static get DEFAULT_INDEX() {
    return 'app';
  }

  private readonly elasticsearchInitializer: ElasticsearchInitializer;

  constructor(
    @Inject('LOGGER') private readonly logger: CustomLogger,
    private readonly elasticsearchService: ElasticsearchService,
  ) {
    this.elasticsearchInitializer = new ElasticsearchInitializer(
      logger,
      AppElasticsearchDataService.DEFAULT_INDEX,
      elasticsearchService,
      {
        id: {
          type: 'keyword',
        },
        roles: {
          type: 'keyword',
        },
        name: {
          type: 'text',
          fields: {
            keyword: {
              type: 'keyword',
              normalizer: 'lowercase_normalizer',
            },
          },
        },
        description: {
          type: 'text',
        },
      },
    );
  }

  async initializeIndex() {
    const exists = await this.elasticsearchService.indices.exists({
      index: AppElasticsearchDataService.DEFAULT_INDEX,
    });

    if (!exists) {
      await this.elasticsearchService.indices.create({
        index: AppElasticsearchDataService.DEFAULT_INDEX,
        body: {
          settings: {
            analysis: {
              normalizer: {
                lowercase_normalizer: {
                  type: 'custom',
                  char_filter: [],
                  filter: ['lowercase'],
                },
              },
            },
          },
        },
      });
    }

    await this.elasticsearchInitializer.initializeIndex();
  }
  public async getAppWithId(appId: string): Promise<AppPersistedInterface> {
    const response = await this.elasticsearchService.search({
      index: AppElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                term: {
                  id: appId,
                },
              },
            ],
          },
        },
      },
    });

    if (response.hits.total === 0) {
      return null;
    }

    return AppElasticsearchDataService._getAppWithDefaults(
      response.hits.hits[0]._source,
    );
  }

  public async createApp(
    context: Context,
    app: AppPersistedInterface,
  ): Promise<void> {
    await this.elasticsearchService.index({
      index: AppElasticsearchDataService.DEFAULT_INDEX,
      id: app.id,
      body: {
        ...app,
      },
    });
  }

  public async updateTokenHashes(
    context: Context,
    appId: string,
    permissionsHash: string,
    expirationPolicyHash: string,
  ): Promise<void> {
    await this.elasticsearchService.updateByQuery({
      index: AppElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: 'ctx._source.hash = params.hash',
          params: {
            hash: {
              permissions: permissionsHash,
              tokenExpirationPolicy: expirationPolicyHash,
            },
          },
        },
        query: {
          match: {
            id: appId,
          },
        },
      },
    });
  }

  public async getApps(
    context: Context,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<AppPersistedInterface>> {
    let query: { query?: any; sort?: any } = {};

    if (!options.search && options.searchName) {
      query = {
        query: {
          wildcard: {
            name: {
              value: `*${options.searchName}*`,
              boost: 1.0,
              rewrite: 'constant_score',
            },
          },
        },
      };
    }
    if (options.search) {
      query = {
        query: {
          query_string: {
            query: `*${options.search}*`,
            fields: ['name', 'description'],
          },
        },
      };
    }

    const sortMap = {
      name: [{ 'name.keyword': options.orderDirection || 'asc' }, '_score'],
    };

    if (options.orderBy) {
      const sort = sortMap[options.orderBy];
      query = {
        ...query,
        sort,
      };
    }

    const response = await this.elasticsearchService.search({
      index: AppElasticsearchDataService.DEFAULT_INDEX,
      track_total_hits: true,
      body: {
        from: (page - 1) * pageSize,
        size: pageSize,
        ...query,
      },
    });

    return {
      total: (response.hits.total as any).value as number,
      results: response.hits.hits.map((message: { _source?: any }) => {
        return AppElasticsearchDataService._getAppWithDefaults(message._source);
      }),
    };
  }

  public async saveAppsFromConfig(
    apps: AppPersistedInterface[],
  ): Promise<AppPersistedInterface[]> {
    if (apps.length === 0) {
      return [];
    }
    const response = await this.elasticsearchService.search({
      index: AppElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                terms: {
                  id: apps.map((app) => app.id),
                },
              },
            ],
          },
        },
      },
    });

    const existingApps = response.hits.hits;

    const existingAppIDs = existingApps.map(
      (existingApp) => (existingApp._source as any).id,
    );

    const newApps = apps.filter((app) => !existingAppIDs.includes(app.id));

    const newAppsBulk = [];
    for (const newApp of newApps) {
      newAppsBulk.push({
        index: {
          _index: AppElasticsearchDataService.DEFAULT_INDEX,
          _id: newApp.id,
        },
      });
      newAppsBulk.push({
        ...newApp,
      });
    }

    const existingAppsBulk = [];
    for (const existingApp of existingApps) {
      const updatedApp = apps.find(
        (app) => app.id === (existingApp._source as any).id,
      );
      existingAppsBulk.push({
        index: {
          _index: AppElasticsearchDataService.DEFAULT_INDEX,
          _id: existingApp._id,
        },
      });
      existingAppsBulk.push({
        ...(existingApp._source as any),
        name: updatedApp.name,
        roles: updatedApp.roles
          ? updatedApp.roles
          : (existingApp._source as any).roles,
        description: updatedApp
          ? updatedApp.description
          : (existingApp._source as any).description,
        hash: {
          ...(existingApp._source as any).hash,
          preConfiguredApiKeys: updatedApp.hash.preConfiguredApiKeys,
        },
      });
    }

    const bulk = [...newAppsBulk, ...existingAppsBulk];

    await this.elasticsearchService.bulk({ body: bulk });

    return newApps;
  }

  public async saveApiKeys(
    context: Context,
    appId: string,
    apiKeys: ApiKeyInterface[],
  ): Promise<void> {
    const finalApiKeys = [];
    let didChange = false;

    let app;
    while (!(app = await this.getAppWithId(appId))) {
      await new Promise((resolve) => setTimeout(resolve, 50));
    }

    for (const apiKey of app.apiKeys) {
      const newApiKey = apiKeys.find((findApiKey) => {
        return findApiKey.type === apiKey.type && findApiKey.id === apiKey.id;
      });
      if (newApiKey) {
        if (JSON.stringify(newApiKey.data) !== apiKey.data) {
          didChange = true;
        }
        finalApiKeys.push(newApiKey);
      } else {
        finalApiKeys.push(apiKey);
      }
    }

    for (const apiKey of apiKeys) {
      const existingApiKey = finalApiKeys.find((findApiKey) => {
        return findApiKey.type === apiKey.type && findApiKey.id === apiKey.id;
      });
      if (!existingApiKey) {
        didChange = true;
        finalApiKeys.push(apiKey);
      }
    }

    if (!didChange) {
      // No change to identity links.
      return;
    }

    await this.elasticsearchService.updateByQuery({
      index: AppElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: 'ctx._source.apiKeys = params.apiKeys',
          params: {
            apiKeys: finalApiKeys,
          },
        },
        query: {
          match: {
            id: appId,
          },
        },
      },
    });
  }

  public async getAppsWithIds(
    context: Context,
    appIds: string[],
  ): Promise<AppPersistedInterface[]> {
    const response = await this.elasticsearchService.search({
      index: AppElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                terms: {
                  id: appIds,
                },
              },
            ],
          },
        },
      },
    });

    return response.hits.hits.map((message: { _source?: any }) => {
      return AppElasticsearchDataService._getAppWithDefaults(message._source);
    });
  }

  public async deleteAppById(
    context: Context,
    appId: string,
  ): Promise<boolean> {
    const response = await this.elasticsearchService.deleteByQuery({
      index: AppElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          match: {
            id: appId,
          },
        },
      },
    });
    return response.total === 1;
  }

  public async deleteAppsByIds(
    context: Context,
    appIds: string[],
  ): Promise<boolean> {
    const response = await this.elasticsearchService.deleteByQuery({
      index: AppElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                terms: {
                  id: appIds,
                },
              },
            ],
          },
        },
      },
    });
    return response.total === appIds.length;
  }

  public async updateAppById(
    context: Context,
    appId: string,
    updateApp: AppUpdateInterface,
  ): Promise<boolean> {
    const response = await this.elasticsearchService.updateByQuery({
      index: AppElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `ctx._source.name = params.name;
             ctx._source.description = params.description;
             ctx._source.roles = params.roles;
            `,
          params: {
            name: updateApp.name,
            description: updateApp.description,
            roles: updateApp.roles,
          },
        },
        query: {
          match: {
            id: appId,
          },
        },
      },
    });
    return response.total === 1;
  }

  public async addRoles(
    context: Context,
    appId: string,
    roles: string[],
  ): Promise<boolean> {
    const response = await this.elasticsearchService.updateByQuery({
      index: AppElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
           if (!(ctx._source.roles instanceof List)) {
             ctx._source.roles = new ArrayList();
           }
           ctx._source.roles.addAll(params.roles);
           ctx._source.roles = ctx._source.roles.stream().distinct().sorted().collect(Collectors.toList());
          `,
          params: {
            roles,
          },
        },
        query: {
          match: {
            id: appId,
          },
        },
      },
    });
    return response.total === 1;
  }

  public async removeRoles(
    context: Context,
    appId: string,
    roles: [],
  ): Promise<boolean> {
    const response = await this.elasticsearchService.updateByQuery({
      index: AppElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
            if (!(ctx._source.roles instanceof List)) {
              ctx._source.roles = new ArrayList();
            }
            ctx._source.roles.removeAll(params.roles);
          `,
          params: {
            roles,
          },
        },
        query: {
          match: {
            id: appId,
          },
        },
      },
    });

    return response.total === 1;
  }

  public async getRoleIds(context: Context, appId: string): Promise<string[]> {
    const app = await this.getAppWithId(appId);
    return app.roles || [];
  }

  public async getAllApps(context: Context): Promise<AppPersistedInterface[]> {
    const response = await this.elasticsearchService.search({
      index: AppElasticsearchDataService.DEFAULT_INDEX,
    });

    return response.hits.hits.map((message: { _source?: any }) => {
      return AppElasticsearchDataService._getAppWithDefaults(message._source);
    });
  }

  private static _getAppWithDefaults(app: any) {
    return {
      ...app,
      apiKeys: app.apiKeys || [],
      roles: app.roles || [],
      description: app.description || '',
    };
  }

  public async addApiKey(
    context: Context,
    appId: string,
    apiKey: ApiKeyInterface,
  ): Promise<void> {
    await this.elasticsearchService.updateByQuery({
      index: AppElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
            if (!(ctx._source.apiKeys instanceof List)) {
              ctx._source.apiKeys = new ArrayList();
            }
            ctx._source.apiKeys.add(params.apiKey);
          `,
          params: {
            apiKey,
          },
        },
        query: {
          match: {
            id: appId,
          },
        },
      },
    });
  }

  public async getApiKeys(
    context: Context,
    appId: string,
    page: number,
    pageSize: number,
  ): Promise<ApiKeyInterface[]> {
    const app = await this.getAppWithId(appId);
    return ArrayUtil.paginate(app.apiKeys, page, pageSize);
  }

  public async removeApiKeys(
    context: Context,
    appId: string,
    apiKeyIds: string[],
  ): Promise<boolean> {
    const response = await this.elasticsearchService.updateByQuery({
      index: AppElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
            if (!(ctx._source.apiKeys instanceof List)) {
              ctx._source.apiKeys = new ArrayList();
            }
            ctx._source.apiKeys = ctx._source.apiKeys
               .stream()
               .filter(apiKey -> !params.apiKeyIds.contains(apiKey.id))
               .collect(Collectors.toList());
          `,
          params: {
            apiKeyIds,
          },
        },
        query: {
          match: {
            id: appId,
          },
        },
      },
    });

    return response.total === 1;
  }

  public async getApiKeysByIds(
    context: Context,
    appId: string,
    apiKeyIds: string[],
  ): Promise<ApiKeyInterface[] | boolean> {
    const app = await this.getAppWithId(appId);
    if (!app) {
      return false;
    }

    return (app.apiKeys || []).filter((apiKey) =>
      apiKeyIds.includes(apiKey.id),
    );
  }

  public async getAppsWithRoleIds(
    context: Context,
    roleIds: string[],
  ): Promise<AppPersistedInterface[]> {
    const response = await this.elasticsearchService.search({
      index: AppElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                terms: {
                  roles: roleIds,
                },
              },
            ],
          },
        },
      },
    });

    return response.hits.hits.map((message: { _source?: any }) => {
      return AppElasticsearchDataService._getAppWithDefaults(message._source);
    });
  }

  public async patchAppWithId(
    context: Context,
    appId: string,
    patchApp: AppPatchInterface,
  ): Promise<boolean> {
    const response = await this.elasticsearchService.updateByQuery({
      index: AppElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
             if (params.name != null) {
               ctx._source.name = params.name
             }
             if (params.description != null) {
               ctx._source.description = params.description
             }
            `,
          params: {
            name: patchApp.name,
            description: patchApp.description,
          },
        },
        query: {
          match: {
            id: appId,
          },
        },
      },
    });
    return response.total === 1;
  }

  public async getAppWithName(
    context: Context,
    name: string,
  ): Promise<AppPersistedInterface | null> {
    const response = await this.elasticsearchService.search({
      index: AppElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                term: {
                  'name.keyword': name,
                },
              },
            ],
          },
        },
      },
    });

    if (response.hits.total === 0) {
      return null;
    }

    return response.hits.hits[0]._source as any;
  }
}
