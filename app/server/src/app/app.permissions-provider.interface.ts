import { PermissionInterface } from '../permission/permission.interface';
import { AppInterface } from './app.interface';
import { Context } from '@cryptexlabs/codex-nodejs-common';

export interface AppPermissionsProviderInterface {
  getPermissionsForApp(
    context: Context,
    app: AppInterface,
  ): Promise<PermissionInterface[]>;
}
