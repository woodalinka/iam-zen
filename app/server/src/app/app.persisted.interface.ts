import { AppInterface } from './app.interface';

export interface AppPersistedInterface extends AppInterface {
  hash: {
    permissions: string;
    tokenExpirationPolicy: string;
    preConfiguredApiKeys?: string;
  };
}
