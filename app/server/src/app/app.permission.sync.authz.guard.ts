import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AuthzNamespace } from '../constants';
import { HttpAuthzGuardUtil } from '@cryptexlabs/codex-nodejs-common';

@Injectable()
export class AppPermissionSyncAuthzGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const util = new HttpAuthzGuardUtil(context);

    return util.isAuthorized(
      AuthzNamespace,
      { action: '', object: 'app', objectId: util.params.appId },
      { action: '', object: 'permission', objectId: 'all' },
      { action: 'create', object: 'updated', objectId: '' },
    );
  }
}
