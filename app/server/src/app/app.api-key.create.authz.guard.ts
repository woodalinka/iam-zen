import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { HttpAuthzGuardUtil } from '@cryptexlabs/codex-nodejs-common';
import { AuthzNamespace } from '../constants';

@Injectable()
export class AppApiKeyCreateAuthzGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const util = new HttpAuthzGuardUtil(context);
    return util.isAuthorized(
      AuthzNamespace,
      { action: '', object: 'app', objectId: util.params.appId },
      { action: 'create', object: 'api-key', objectId: '' },
    );
  }
}
