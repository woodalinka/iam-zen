import { ServerConfigInterface } from './server.config.interface';
import * as _ from 'lodash';
import { ArrayUtil } from '@cryptexlabs/codex-nodejs-common';

export class ConfigMerger {
  public static mergeConfigs(
    config1: ServerConfigInterface,
    config2: ServerConfigInterface,
  ): ServerConfigInterface {
    let config = config1;

    config = {
      ...config,
      token: _.merge(config1.token, config2.token),
    };

    // Policies
    if (!config1.policies && config2.policies) {
      config = {
        ...config,
        policies: config2.policies,
      };
    } else if (config1.policies && config2.policies) {
      const newPolicies = [];
      for (const policy of config1.policies || []) {
        const config2Policy = config2.policies.find(
          (find) => find.id === policy.id,
        );
        let newPolicy = policy;
        if (config2Policy) {
          const policyPermissions = [...policy.permissions];
          newPolicy = _.merge(policy, config2Policy);
          if (!newPolicy.permissions) {
            newPolicy.permissions = [];
          } else {
            newPolicy.permissions = ArrayUtil.unique(
              (policyPermissions || []).concat(config2Policy.permissions || []),
            );
          }
        }
        newPolicies.push(newPolicy);
      }

      for (const policy of config2.policies || []) {
        const existingPolicy = newPolicies.find(
          (find) => find.id === policy.id,
        );
        if (!existingPolicy) {
          newPolicies.push(policy);
        }
      }
      if (newPolicies.length > 0) {
        config = {
          ...config,
          policies: newPolicies,
        };
      }
    }

    // Groups
    if (!config1.groups && config2.groups) {
      config = {
        ...config,
        groups: config2.groups,
      };
    } else if (config1.groups && config2.groups) {
      const newGroups = [];
      for (const group of config1.groups || []) {
        const config2Group = config2.groups.find(
          (find) => find.id === group.id,
        );
        let newGroup = group;
        if (config2Group) {
          const groupPolicies = [...group.policies];
          newGroup = _.merge(group, config2Group);
          if (!newGroup.policies) {
            newGroup.policies = [];
          } else {
            newGroup.policies = ArrayUtil.unique(
              (groupPolicies || []).concat(config2Group.policies || []),
            );
          }
        }
        newGroups.push(newGroup);
      }

      for (const group of config2.groups || []) {
        const existingGroup = newGroups.find((find) => find.id === group.id);
        if (!existingGroup) {
          newGroups.push(group);
        }
      }

      if (newGroups.length > 0) {
        config = {
          ...config,
          groups: newGroups,
        };
      }
    }

    // Roles
    if (!config1.roles && config2.roles) {
      config = {
        ...config,
        roles: config2.roles,
      };
    } else if (config1.roles && config2.roles) {
      const newRoles = [];
      for (const role of config1.roles || []) {
        const config2Role = config2.roles.find((find) => find.id === role.id);
        let newRole = role;
        if (config2Role) {
          const rolePolicies = [...role.policies];
          newRole = _.merge(role, config2Role);
          if (!newRole.policies) {
            newRole.policies = [];
          } else {
            newRole.policies = ArrayUtil.unique(
              (rolePolicies || []).concat(config2Role.policies || []),
            );
          }
        }
        newRoles.push(newRole);
      }

      for (const role of config2.roles || []) {
        const existingRole = newRoles.find((find) => find.id === role.id);
        if (!existingRole) {
          newRoles.push(role);
        }
      }

      if (newRoles.length > 0) {
        config = {
          ...config,
          roles: newRoles,
        };
      }
    }

    // Apps
    if (!config1.apps && config2.apps) {
      config = {
        ...config,
        apps: config2.apps,
      };
    } else if (config1.apps && config2.apps) {
      const newApps = [];
      for (const app of config1.apps || []) {
        const config2App = config2.apps.find((find) => find.id === app.id);
        let newApp = app;
        if (config2App) {
          const appRoles = [...app.roles];
          newApp = _.merge(app, config2App);
          if (!newApp.roles) {
            newApp.roles = [];
          } else {
            newApp.roles = ArrayUtil.unique(
              (appRoles || []).concat(config2App.roles || []),
            );
          }
        }
        newApps.push(newApp);
      }

      for (const app of config2.apps || []) {
        const existingApp = newApps.find((find) => find.id === app.id);
        if (!existingApp) {
          newApps.push(app);
        }
      }

      if (newApps.length > 0) {
        config = {
          ...config,
          apps: newApps,
        };
      }
    }

    // Users
    if (!config1.users && config2.users) {
      config = {
        ...config,
        users: config2.users,
      };
    } else if (config1.users && config2.users) {
      const newUsers = [];
      for (const user of config1.users || []) {
        const config2User = config2.users.find((find) => find.id === user.id);
        let newUser = user;
        if (config2User) {
          const userGroups = [...user.groups];
          newUser = _.merge(user, config2User);
          if (!newUser.groups) {
            newUser.groups = [];
          } else {
            newUser.groups = ArrayUtil.unique(
              (userGroups || []).concat(config2User.groups || []),
            );
          }
        }
        newUsers.push(newUser);
      }

      for (const user of config2.users || []) {
        const existingUser = newUsers.find((find) => find.id === user.id);
        if (!existingUser) {
          newUsers.push(user);
        }
      }

      if (newUsers.length > 0) {
        config = {
          ...config,
          users: newUsers,
        };
      }
    }

    // Permissions
    if (!config1.permissions && config2.permissions) {
      config = {
        ...config,
        permissions: config2.permissions,
      };
    } else if (config1.permissions && config2.permissions) {
      const newPermissions = [];
      for (const permission of config1.permissions || []) {
        const config2Permission = config2.permissions.find(
          (find) => find.id === permission.id,
        );
        let newPermission = permission;
        if (config2Permission) {
          newPermission = _.merge(permission, config2Permission);
        }
        newPermissions.push(newPermission);
      }

      for (const permission of config2.permissions || []) {
        const existingPermission = newPermissions.find(
          (find) => find.id === permission.id,
        );
        if (!existingPermission) {
          newPermissions.push(permission);
        }
      }

      if (newPermissions.length > 0) {
        config = {
          ...config,
          permissions: newPermissions,
        };
      }
    }

    if (config2.newUser) {
      if (config1.newUser) {
        const config1Groups = [...config1.newUser.groups];
        const newUser = _.merge(config1.newUser, config2.newUser);
        config = {
          ...config,
          newUser: {
            ...newUser,
            groups: ArrayUtil.unique(
              config1Groups.concat(config2.newUser.groups),
            ),
          },
        };
      } else {
        config = {
          ...config,
          newUser: config2.newUser,
        };
      }
    }

    if (config2.newGroup) {
      if (config1.newGroup) {
        const config1Policies = [...(config1.newGroup.policies || [])];
        const newGroup = _.merge(config1.newGroup, config2.newGroup);
        config = {
          ...config,
          newGroup: {
            ...newGroup,
            policies: ArrayUtil.unique(
              config1Policies.concat(config2.newGroup.policies || []),
            ),
          },
        };
      } else {
        config = {
          ...config,
          newGroup: config2.newGroup,
        };
      }
    }

    if (config2.identityLink) {
      if (config1.identityLink) {
        config = {
          ...config,
          identityLink: _.merge(config1.identityLink, config2.identityLink),
        };
      } else {
        config = {
          ...config,
          identityLink: config2.identityLink,
        };
      }
    }

    return config;
  }
}
