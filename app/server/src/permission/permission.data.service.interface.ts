import { Context, PaginatedResults } from '@cryptexlabs/codex-nodejs-common';
import { PermissionPersistedInterface } from './permission.persisted.interface';
import { PermissionUpdateInterface } from './permission.update.interface';
import { PermissionPatchInterface } from './permission.patch.interface';
import { QueryOptionsInterface } from '../query.options.interface';

export interface PermissionDataServiceInterface {
  getPermissionsByIds(
    context: Context,
    permissionIds: string[],
  ): Promise<PermissionPersistedInterface[]>;
  savePermissionsFromConfig(
    context: Context,
    policies: PermissionPersistedInterface[],
  ): Promise<PermissionPersistedInterface[]>;
  getPermissionById(
    context: Context,
    permissionId: string,
  ): Promise<PermissionPersistedInterface | null>;

  getPermissionWithName(
    context: Context,
    name: string,
  ): Promise<PermissionPersistedInterface | null>;

  getPermissions(
    context: Context,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<PermissionPersistedInterface>>;

  deletePermission(context: Context, permissionId: string): Promise<boolean>;

  deletePermissions(
    context: Context,
    permissionIds: string[],
  ): Promise<boolean>;

  savePermission(
    context: Context,
    permissionId: string,
    permissionUpdate: PermissionUpdateInterface,
  ): Promise<boolean>;

  patchPermission(
    context: Context,
    permissionId: string,
    permissionPatch: PermissionPatchInterface,
  ): Promise<boolean>;
}
