import { PermissionInterface } from './permission.interface';

export type PermissionPersistedInterface = PermissionInterface;
