import { Inject, Injectable } from '@nestjs/common';
import { PermissionDataServiceInterface } from './permission.data.service.interface';
import { PermissionPersistedInterface } from './permission.persisted.interface';
import {
  Context,
  CustomLogger,
  ElasticsearchInitializer,
  PaginatedResults,
} from '@cryptexlabs/codex-nodejs-common';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { PermissionPatchInterface } from './permission.patch.interface';
import { PermissionUpdateInterface } from './permission.update.interface';
import { QueryOptionsInterface } from '../query.options.interface';

@Injectable()
export class PermissionElasticsearchDataService
  implements PermissionDataServiceInterface
{
  private static get DEFAULT_INDEX() {
    return 'permission';
  }

  private readonly elasticsearchInitializer: ElasticsearchInitializer;

  constructor(
    @Inject('LOGGER') private readonly logger: CustomLogger,
    private readonly elasticsearchService: ElasticsearchService,
  ) {
    this.elasticsearchInitializer = new ElasticsearchInitializer(
      logger,
      PermissionElasticsearchDataService.DEFAULT_INDEX,
      elasticsearchService,
      {
        id: {
          type: 'keyword',
        },
        name: {
          type: 'text',
          fields: {
            keyword: {
              type: 'keyword',
              normalizer: 'lowercase_normalizer',
            },
          },
        },
        description: {
          type: 'text',
        },
        value: {
          type: 'text',
        },
      },
    );
  }

  public async initializeIndex() {
    const exists = await this.elasticsearchService.indices.exists({
      index: PermissionElasticsearchDataService.DEFAULT_INDEX,
    });

    if (!exists) {
      await this.elasticsearchService.indices.create({
        index: PermissionElasticsearchDataService.DEFAULT_INDEX,
        body: {
          settings: {
            analysis: {
              normalizer: {
                lowercase_normalizer: {
                  type: 'custom',
                  char_filter: [],
                  filter: ['lowercase'],
                },
              },
            },
          },
        },
      });
    }

    await this.elasticsearchInitializer.initializeIndex();
  }

  public async getPermissionsByIds(
    context: Context,
    permissionIds: string[],
  ): Promise<PermissionPersistedInterface[]> {
    const response = await this.elasticsearchService.search({
      index: PermissionElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                terms: {
                  id: permissionIds,
                },
              },
            ],
          },
        },
      },
    });

    return response.hits.hits.map((message: { _source?: any }) => {
      return message._source;
    });
  }

  public async savePermissionsFromConfig(
    context: Context,
    permissions: PermissionPersistedInterface[],
  ): Promise<PermissionPersistedInterface[]> {
    if (permissions.length === 0) {
      return [];
    }
    const response = await this.elasticsearchService.search({
      index: PermissionElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                terms: {
                  id: permissions.map((permission) => permission.id),
                },
              },
            ],
          },
        },
      },
    });

    const existingPermissions = response.hits.hits;

    const existingPermissionIds = existingPermissions.map(
      (existingPermission) => (existingPermission._source as any).id,
    );

    const newPermissions = permissions.filter(
      (permission) => !existingPermissionIds.includes(permission.id),
    );
    const newPermissionsBulk = [];
    for (const newPermission of newPermissions) {
      newPermissionsBulk.push({
        index: {
          _index: PermissionElasticsearchDataService.DEFAULT_INDEX,
          _id: newPermission.id,
        },
      });
      newPermissionsBulk.push({
        ...newPermission,
      });
    }

    const existingPermissionsBulk = [];
    for (const existingPermission of existingPermissions) {
      const updatedPermission = permissions.find(
        (permission) =>
          permission.id === (existingPermission._source as any).id,
      );
      existingPermissionsBulk.push({
        index: {
          _index: PermissionElasticsearchDataService.DEFAULT_INDEX,
          _id: existingPermission._id,
        },
      });
      existingPermissionsBulk.push({
        ...(existingPermission._source as any),
        name: updatedPermission.name,
        value: updatedPermission.value,
        description: updatedPermission.description
          ? updatedPermission.description
          : (existingPermission._source as any).description,
      });
    }

    const bulk = [...newPermissionsBulk, ...existingPermissionsBulk];

    await this.elasticsearchService.bulk({ body: bulk });

    return newPermissions;
  }

  public async getPermissionById(
    context: Context,
    permissionId: string,
  ): Promise<PermissionPersistedInterface | null> {
    const response = await this.elasticsearchService.search({
      index: PermissionElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                term: {
                  id: permissionId,
                },
              },
            ],
          },
        },
      },
    });

    if (response.hits.total === 0) {
      return null;
    }

    return response.hits.hits[0]._source as any;
  }

  public async getPermissions(
    context: Context,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<PermissionPersistedInterface>> {
    let query: { query?: any; sort?: any } = {};

    if (!options.search && options.searchName) {
      query = {
        query: {
          wildcard: {
            name: {
              value: `*${options.searchName}*`,
              boost: 1.0,
              rewrite: 'constant_score',
            },
          },
        },
      };
    }

    if (options.search) {
      const searchString = options.search
        .replace(/-/g, ' AND ')
        .replace(/"/g, '\\"')
        .replace(/:/g, `":"`);

      query = {
        query: {
          query_string: {
            query: `*${searchString}*`,
            fields: ['name', 'description', 'value'],
          },
        },
      };
    }

    const sortMap = {
      name: [{ 'name.keyword': options.orderDirection || 'asc' }, '_score'],
      value: [{ 'name.keyword': options.orderDirection || 'asc' }, '_score'],
    };

    if (options.orderBy) {
      const sort = sortMap[options.orderBy];
      query = {
        ...query,
        sort,
      };
    }

    const response = await this.elasticsearchService.search({
      index: PermissionElasticsearchDataService.DEFAULT_INDEX,
      track_total_hits: true,
      body: {
        from: (page - 1) * pageSize,
        size: pageSize,
        ...query,
      },
    });

    return {
      total: (response.hits.total as any).value as number,
      results: response.hits.hits.map((message: { _source?: any }) => {
        return message._source;
      }),
    };
  }

  public async deletePermission(
    context: Context,
    permissionId: string,
  ): Promise<boolean> {
    const response = await this.elasticsearchService.deleteByQuery({
      index: PermissionElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          match: {
            id: permissionId,
          },
        },
      },
    });
    return response.total === 1;
  }

  public async deletePermissions(
    context: Context,
    permissionIds: string[],
  ): Promise<boolean> {
    const response = await this.elasticsearchService.deleteByQuery({
      index: PermissionElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                terms: {
                  id: permissionIds,
                },
              },
            ],
          },
        },
      },
    });
    return response.total === permissionIds.length;
  }

  public async patchPermission(
    context: Context,
    permissionId: string,
    permissionPatch: PermissionPatchInterface,
  ): Promise<boolean> {
    const response = await this.elasticsearchService.updateByQuery({
      index: PermissionElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
             if (params.name != null) {
               ctx._source.name = params.name
             }
             if (params.description != null) {
               ctx._source.description = params.description
             }
             if (params.value != null) {
               ctx._source.value = params.value
             }
            `,
          params: {
            name: permissionPatch.name,
            description: permissionPatch.description,
            value: permissionPatch.value,
          },
        },
        query: {
          match: {
            id: permissionId,
          },
        },
      },
    });
    return response.total === 1;
  }

  public async savePermission(
    context: Context,
    permissionId: string,
    permissionUpdate: PermissionUpdateInterface,
  ): Promise<boolean> {
    const response = await this.elasticsearchService.updateByQuery({
      index: PermissionElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
             ctx._source.name = params.name;
             ctx._source.description = params.description;
             ctx._source.value = params.value;
            `,
          params: {
            name: permissionUpdate.name,
            description: permissionUpdate.description,
            value: permissionUpdate.value,
          },
        },
        query: {
          match: {
            id: permissionId,
          },
        },
      },
    });
    if (response.total === 1) {
      return true;
    } else {
      await this.elasticsearchService.index({
        index: PermissionElasticsearchDataService.DEFAULT_INDEX,
        id: permissionId,
        body: {
          id: permissionId,
          ...permissionUpdate,
        },
      });
      return false;
    }
  }

  public async getPermissionWithName(
    context: Context,
    name: string,
  ): Promise<PermissionPersistedInterface | null> {
    const response = await this.elasticsearchService.search({
      index: PermissionElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                term: {
                  'name.keyword': name,
                },
              },
            ],
          },
        },
      },
    });

    if (response.hits.total === 0) {
      return null;
    }

    return response.hits.hits[0]._source as any;
  }
}
