import { PermissionInterface } from './permission.interface';
import { StringUtil } from '@cryptexlabs/codex-nodejs-common';

export class PermissionUtil {
  static getPermissionsHash(permissions: PermissionInterface[]): string {
    return StringUtil.insecureMd5(
      JSON.stringify(permissions.map((permission) => permission.value)),
    );
  }
  static mapSelfToUserId(permissions: PermissionInterface[], userId: string) {
    return permissions.map((item) => ({
      ...item,
      value: item.value.replace(/:self:/g, `:${userId}:`),
    }));
  }
}
