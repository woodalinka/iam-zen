import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { HttpAuthzGuardUtil } from '@cryptexlabs/codex-nodejs-common';
import { AuthzNamespace } from '../constants';

@Injectable()
export class PermissionExistsAuthzGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const util = new HttpAuthzGuardUtil(context);

    return (
      util.isAuthorized(AuthzNamespace, {
        action: 'get',
        object: 'permission',
        objectId: 'any',
      }) ||
      (util.isAuthorized(AuthzNamespace, {
        action: 'list',
        object: 'permission',
        objectId: '',
      }) &&
        !util.isAuthorized(AuthzNamespace, {
          action: 'any',
          object: 'permission',
          objectId: '',
        })) ||
      (util.isAuthorized(AuthzNamespace, {
        action: 'create',
        object: 'permission',
        objectId: '',
      }) &&
        !util.isAuthorized(AuthzNamespace, {
          action: 'any',
          object: 'permission',
          objectId: '',
        }))
    );
  }
}
