import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import * as Joi from 'joi';
import { PermissionPatchInterface } from './permission.patch.interface';

@Injectable()
export class PermissionPatchValidationPipe implements PipeTransform {
  private _schema: Joi.ObjectSchema<PermissionPatchInterface>;

  constructor() {
    this._schema = Joi.object({
      name: Joi.string().alphanum().optional(),
      description: Joi.string().optional(),
      value: Joi.string().optional(),
    });
  }

  public transform(value: any, metadata: ArgumentMetadata): any {
    const { error } = this._schema.validate(value.data);
    if (error) {
      throw new BadRequestException(error.message);
    }
    return value;
  }
}
