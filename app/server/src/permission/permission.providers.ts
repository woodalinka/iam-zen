import { PermissionService } from './permission.service';
import { appConfig, elasticsearchService } from '../setup';
import { DatabaseTypeEnum } from '../data/database-type.enum';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { Config } from '../config';
import { ContextBuilder } from '@cryptexlabs/codex-nodejs-common';
import { PermissionDefaultLoaderService } from './permission.default-loader.service';
import { PermissionDataServiceInterface } from './permission.data.service.interface';
import { PermissionElasticsearchDataService } from './permission.elasticsearch.data.service';

const providers = [];
if (appConfig.dbType === DatabaseTypeEnum.ELASTICSEARCH) {
  providers.push(
    {
      provide: ElasticsearchService,
      useValue: elasticsearchService,
    },
    PermissionElasticsearchDataService,
    {
      provide: 'PERMISSION_DATA_SERVICE',
      useFactory: async (service: PermissionElasticsearchDataService) => {
        await service.initializeIndex();
        return service;
      },
      inject: [PermissionElasticsearchDataService],
    },
  );
}

export const permissionProviders = [
  ...providers,
  PermissionService,
  {
    provide: 'PERMISSIONS_SERVICE',
    useClass: PermissionService,
  },
  {
    provide: 'USER_PERMISSIONS_PROVIDER',
    useClass: PermissionService,
  },
  {
    provide: 'APP_PERMISSIONS_PROVIDER',
    useClass: PermissionService,
  },
  {
    provide: 'POLICY_PERMISSIONS_PROVIDER',
    useClass: PermissionService,
  },
  {
    provide: PermissionDefaultLoaderService,
    useFactory: async (
      permissionDataService: PermissionDataServiceInterface,
      config: Config,
      contextBuilder: ContextBuilder,
    ) => {
      const service = new PermissionDefaultLoaderService(
        permissionDataService,
        config,
        contextBuilder,
      );
      if (appConfig.autoLoadConfig) {
        await service.load();
      }
      return service;
    },
    inject: ['PERMISSION_DATA_SERVICE', 'CONFIG', 'CONTEXT_BUILDER'],
  },
];
