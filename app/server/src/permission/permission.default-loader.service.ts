import { Inject, Injectable } from '@nestjs/common';
import { Config } from '../config';
import { ContextBuilder } from '@cryptexlabs/codex-nodejs-common';
import { PermissionDataServiceInterface } from './permission.data.service.interface';
import { PermissionPersistedInterface } from './permission.persisted.interface';

@Injectable()
export class PermissionDefaultLoaderService {
  constructor(
    @Inject('PERMISSION_DATA_SERVICE')
    private readonly permissionDataService: PermissionDataServiceInterface,
    @Inject('CONFIG') private readonly config: Config,
    @Inject('CONTEXT_BUILDER') private readonly contextBuilder: ContextBuilder,
  ) {}

  public async load() {
    const persistedPermissions: PermissionPersistedInterface[] =
      this.config.defaultPermissions.map((configPermission) => {
        return {
          name: configPermission.name,
          id: configPermission.id,
          description: configPermission.description || '',
          value: configPermission.value,
        };
      });

    const context = this.contextBuilder.build().getResult();

    await this.permissionDataService.savePermissionsFromConfig(
      context,
      persistedPermissions,
    );
  }
}
