import { PermissionPersistedInterface } from './permission.persisted.interface';

export class PermissionInListDto {
  public readonly id: string;
  public readonly name: string;
  public readonly value: string;

  constructor(
    permission: PermissionPersistedInterface,
    public readonly immutable,
  ) {
    this.id = permission.id;
    this.name = permission.name;
    this.value = permission.value;
  }
}
