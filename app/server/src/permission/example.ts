import { PermissionInListDto } from './permission.in-list.dto';

export const permissionUpdateExample = {
  name: 'Widget Permission 7',
  description: 'Role 7 for widget app',
  value: 'iam-zen:::app:self:update',
};

export const permissionPatchExample = {
  name: 'Widget Permission 7',
  description: 'Permission 7 for widget app',
};

export const examplePermission = new PermissionInListDto(
  {
    id: '923bc197-21d5-4eab-b068-916b5a757bdd',
    ...permissionUpdateExample,
  },
  false,
);
