import { PermissionInterface } from './permission.interface';

export interface PermissionForPolicyInterface extends PermissionInterface {
  fromPolicies: {
    name: string;
    id: string;
  }[];
}
