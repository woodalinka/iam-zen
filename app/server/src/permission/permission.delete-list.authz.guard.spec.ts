import { PermissionDeleteListAuthzGuard } from './permission.delete-list.authz.guard';
import * as jwt from 'jsonwebtoken';
import { AuthzNamespace } from '../constants';
import { ExecutionContext } from '@nestjs/common';

describe(PermissionDeleteListAuthzGuard.name, () => {
  it('Should allow a super admin to delete a permission', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::any:any:any`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            ids: ['4d2114ca-24e2-43e5-bddb-d9a6688b8340'],
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new PermissionDeleteListAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to delete any object to delete the permission', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::any:any:delete`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            ids: ['4d2114ca-24e2-43e5-bddb-d9a6688b8340'],
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new PermissionDeleteListAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to delete a specific permission to delete the specific permission', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::permission:4d2114ca-24e2-43e5-bddb-d9a6688b8340:delete`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            ids: ['4d2114ca-24e2-43e5-bddb-d9a6688b8340'],
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new PermissionDeleteListAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to do anything to a specific permission to delete the permission', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::permission:4d2114ca-24e2-43e5-bddb-d9a6688b8340:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            ids: ['4d2114ca-24e2-43e5-bddb-d9a6688b8340'],
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new PermissionDeleteListAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to delete any permission to delete the permission', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::permission:any:delete`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            ids: ['4d2114ca-24e2-43e5-bddb-d9a6688b8340'],
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new PermissionDeleteListAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should not allow someone with permission to delete a different specific permission to delete the specific permission', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::permission:4d2114ca-24e2-43e5-bddb-d9a6688b8340:delete`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            ids: ['3630a7ed-fef5-4345-9947-c54e8d15f954'],
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new PermissionDeleteListAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should not allow someone with permission to do anything to a different specific permission to delete the permission', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::permission:4d2114ca-24e2-43e5-bddb-d9a6688b8340:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            ids: ['3630a7ed-fef5-4345-9947-c54e8d15f954'],
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new PermissionDeleteListAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });
});
