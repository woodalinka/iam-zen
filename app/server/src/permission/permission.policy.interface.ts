export interface PermissionPolicyInterface {
  id: string;
  name: string;
  description: string;
  immutable: boolean;
}
