export interface PermissionRoleInterface {
  id: string;
  name: string;
  description: string;
}
