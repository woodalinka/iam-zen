import {
  CanActivate,
  ExecutionContext,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { HttpAuthzGuardUtil } from '@cryptexlabs/codex-nodejs-common';
import { AuthzNamespace } from '../constants';

@Injectable()
export class PermissionDeleteListAuthzGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const util = new HttpAuthzGuardUtil(context);

    if (!util.query.ids) {
      throw new HttpException(
        'Query parameter ids cannot be empty',
        HttpStatus.BAD_REQUEST,
      );
    }

    for (const id of util.query.ids) {
      if (
        !util.isAuthorized(AuthzNamespace, {
          action: 'delete',
          object: 'permission',
          objectId: id,
        })
      ) {
        return false;
      }
    }
    return true;
  }
}
