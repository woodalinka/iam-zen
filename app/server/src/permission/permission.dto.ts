import { PermissionPersistedInterface } from './permission.persisted.interface';

export class PermissionDto {
  public readonly id: string;
  public readonly name: string;
  public readonly description: string;
  public readonly value: string;

  constructor(
    permission: PermissionPersistedInterface,
    public readonly immutable,
    public readonly policies,
    public readonly roles,
    public readonly apps,
    public readonly groups,
  ) {
    this.id = permission.id;
    this.name = permission.name;
    this.description = permission.description;
    this.value = permission.value;
  }
}
