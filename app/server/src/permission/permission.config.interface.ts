export interface PermissionConfigInterface {
  id: string;
  value: string;
  description?: string;
  name: string;
}
