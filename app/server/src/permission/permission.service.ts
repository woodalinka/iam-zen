import {
  HttpStatus,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { UserPermissionsProviderInterface } from '../user/permission/user.permissions-provider.interface';
import { Config } from '../config';
import { GroupUtil } from '../group/group.util';
import { PolicyUtil } from '../policy/policy.util';
import { GroupService } from '../group/group.service';
import { PolicyService } from '../policy/policy.service';
import { PermissionInterface } from './permission.interface';
import { UserInterface } from '../user/user.interface';
import { Context, PaginatedResults } from '@cryptexlabs/codex-nodejs-common';
import { PermissionDto } from './permission.dto';
import { PermissionInListDto } from './permission.in-list.dto';
import { PermissionDataServiceInterface } from './permission.data.service.interface';
import { AppPermissionsProviderInterface } from '../app/app.permissions-provider.interface';
import { AppInterface } from '../app/app.interface';
import { RoleService } from '../role/role.service';
import { RoleUtil } from '../role/role.util';
import { PolicyPermissionsProviderInterface } from '../policy/policy.permissions.provider.interface';
import {
  PolicyPermissionDetailedInterface,
  PolicyPermissionInterface,
} from '../policy/policy.permission.interface';
import { RolePermissionInterface } from '../role/role.permission.interface';
import { ImmutableObjectError } from '../error/immutable-object.error';
import { LocalesEnum } from '../locale/enum';
import { PermissionUpdateInterface } from './permission.update.interface';
import { PermissionPatchInterface } from './permission.patch.interface';
import { QueryOptionsInterface } from '../query.options.interface';
import { AppService } from '../app/app.service';

@Injectable()
export class PermissionService
  implements
    UserPermissionsProviderInterface,
    AppPermissionsProviderInterface,
    PolicyPermissionsProviderInterface
{
  constructor(
    @Inject('CONFIG') private readonly config: Config,
    @Inject('PERMISSION_DATA_SERVICE')
    private readonly permissionDataService: PermissionDataServiceInterface,
    private readonly groupService: GroupService,
    private readonly roleService: RoleService,
    private readonly policyService: PolicyService,
    private readonly appService: AppService,
  ) {}

  async getPermissionsForNewUser(
    context: Context,
  ): Promise<PermissionInterface[]> {
    return this._getPermissionsForUserGroups(
      context,
      this.config.newUserDefaultAttributes.groups || [],
    );
  }

  public getPermissionsForUser(
    context: Context,
    user: UserInterface,
  ): Promise<PermissionInterface[]> {
    return this._getPermissionsForUserGroups(context, user.groups || []);
  }

  private async _getPermissionsForUserGroups(
    context: Context,
    groupIds: string[],
  ) {
    const newUserGroups = await this.groupService.getGroupsByIds(
      context,
      groupIds,
    );

    const newUserGroupPolicyIds =
      GroupUtil.getUniquePolicyIdsForGroups(newUserGroups);

    const newUserPolicies = await this.policyService.getPoliciesByIds(
      context,
      newUserGroupPolicyIds,
    );

    const permissionIds =
      PolicyUtil.getUniquePermissionIdsForPolicies(newUserPolicies);

    return this.getPermissionsByIds(context, permissionIds);
  }

  private async _getPermissionsForAppRoles(
    context: Context,
    roleIds: string[],
  ) {
    const roles = await this.roleService.getRolesByIds(context, roleIds);

    const policyIds = RoleUtil.getUniquePolicyIdsForRoles(roles);

    const rolesPolicies = await this.policyService.getPoliciesByIds(
      context,
      policyIds,
    );

    const permissionIds =
      PolicyUtil.getUniquePermissionIdsForPolicies(rolesPolicies);

    return this.getPermissionsByIds(context, permissionIds);
  }

  public async getPermissionsByIds(
    context: Context,
    permissionIds: string[],
  ): Promise<PermissionInterface[]> {
    const persistedPermissions =
      await this.permissionDataService.getPermissionsByIds(
        context,
        permissionIds,
      );
    const persistedPermissionIds = persistedPermissions.map(
      (persistedPermission) => persistedPermission.id,
    );
    const configPermissionsNotPersisted = this.config.defaultPermissions.filter(
      (configPermission) =>
        !persistedPermissionIds.includes(configPermission.id) &&
        permissionIds.includes(configPermission.id),
    );

    const permissions = persistedPermissions.map((persistedPermission) => {
      return {
        name: persistedPermission.name,
        value: persistedPermission.value,
        id: persistedPermission.id,
        description: persistedPermission.description,
      };
    });
    for (const configPermission of configPermissionsNotPersisted) {
      permissions.push({
        name: configPermission.name,
        value: configPermission.value,
        id: configPermission.id,
        description: configPermission.description || '',
      });
    }
    return permissions;
  }

  public async getPermissionById(
    context: Context,
    permissionId: string,
  ): Promise<PermissionDto> {
    const permission = await this.permissionDataService.getPermissionById(
      context,
      permissionId,
    );
    if (!permission) {
      throw new NotFoundException();
    }
    const configPermission = this.config.defaultPermissions.find(
      (findPermission) => findPermission.id === permission.id,
    );

    const policies = await this.policyService.getPoliciesForPermission(
      context,
      permissionId,
    );

    const roles = await this.roleService.getRolesByPoliciesForPermissions(
      context,
      policies.map((policy) => policy.id),
    );
    const groups = await this.groupService.getGroupsByPoliciesForPermissions(
      context,
      policies.map((policy) => policy.id),
    );
    const apps = await this.appService.getAppsForRoles(
      context,
      roles.map((role) => role.id),
    );

    return new PermissionDto(
      permission,
      !!configPermission,
      policies,
      roles,
      apps,
      groups,
    );
  }

  public async getPermissionExists(
    context: Context,
    name: string,
  ): Promise<boolean> {
    const permission = await this.permissionDataService.getPermissionWithName(
      context,
      name,
    );
    return !!permission;
  }

  public async getPermissions(
    context: Context,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<PermissionInListDto>> {
    const permissions = await this.permissionDataService.getPermissions(
      context,
      page,
      pageSize,
      options,
    );
    return {
      total: permissions.total,
      results: permissions.results.map((permission) => {
        const configPermission = this.config.defaultPermissions.find(
          (findPermission) => findPermission.id === permission.id,
        );
        return new PermissionInListDto(permission, !!configPermission);
      }),
    };
  }

  public getPermissionsForApp(
    context: Context,
    app: AppInterface,
  ): Promise<PermissionInterface[]> {
    return this._getPermissionsForAppRoles(context, app.roles || []);
  }

  public async getPermissionsForPoliciesByPermissionIds(
    context: Context,
    permissionIds: string[],
  ): Promise<PolicyPermissionInterface[]> {
    const permissions = await this.getPermissionsByIds(context, permissionIds);
    return permissions.map((permission) => ({
      id: permission.id,
      name: permission.name,
      value: permission.value,
      description: permission.description,
    }));
  }

  public async getPermissionsForPolicy(
    context: Context,
    policyId: string,
    permissionIds: string[],
  ): Promise<PolicyPermissionDetailedInterface[]> {
    const permissions = await this.getPermissionsByIds(context, permissionIds);
    return permissions.map((permission) => {
      const configPolicy = this.config.defaultPolicies.find(
        (p) => p.id === policyId,
      );
      let immutable = false;
      if (configPolicy) {
        immutable = configPolicy.permissions.includes(permission.id);
      }

      return {
        id: permission.id,
        name: permission.name,
        value: permission.value,
        description: permission.description,
        immutable,
      };
    });
  }

  public async getPermissionsForRoles(
    context: Context,
    permissionIds: string[],
  ): Promise<RolePermissionInterface[]> {
    const permissions = await this.getPermissionsByIds(context, permissionIds);
    return permissions.map((permission) => ({
      id: permission.id,
      name: permission.name,
      value: permission.value,
      description: permission.description,
    }));
  }

  public async deletePermission(
    context: Context,
    permissionId: string,
  ): Promise<void> {
    this._immutabilityCheck(context, permissionId);
    const didModify = await this.permissionDataService.deletePermission(
      context,
      permissionId,
    );
    if (!didModify) {
      throw new NotFoundException();
    }
  }

  public async deletePermissions(
    context: Context,
    permissionIds: string[],
  ): Promise<void> {
    for (const permissionId of permissionIds) {
      this._immutabilityCheck(context, permissionId);
    }
    const didModify = await this.permissionDataService.deletePermissions(
      context,
      permissionIds,
    );
    if (!didModify) {
      throw new NotFoundException();
    }
  }

  public async savePermission(
    context: Context,
    permissionId: string,
    permissionUpdate: PermissionUpdateInterface,
  ): Promise<HttpStatus> {
    this._immutabilityCheck(context, permissionId);
    const didModify = await this.permissionDataService.savePermission(
      context,
      permissionId,
      permissionUpdate,
    );
    return didModify ? HttpStatus.ACCEPTED : HttpStatus.CREATED;
  }

  public async patchPermission(
    context: Context,
    permissionId: string,
    permissionPatch: PermissionPatchInterface,
  ): Promise<void> {
    this._immutabilityCheck(context, permissionId);
    const didModify = await this.permissionDataService.patchPermission(
      context,
      permissionId,
      permissionPatch,
    );
    if (!didModify) {
      throw new NotFoundException();
    }
  }

  private _immutabilityCheck(context: Context, permissionId: string) {
    const immutablePermission = this.config.defaultPermissions.find(
      (findPermission) => findPermission.id === permissionId,
    );
    if (immutablePermission) {
      throw new ImmutableObjectError(
        context,
        LocalesEnum.ERROR_IMMUTABLE_PERMISSION,
      );
    }
  }
}
