import {
  Body,
  Controller,
  Delete,
  Get,
  Headers,
  HttpCode,
  HttpStatus,
  Inject,
  Param,
  ParseIntPipe,
  Patch,
  Put,
  Query,
  Res,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  ApiMetaHeaders,
  ApiPagination,
  ContextBuilder,
  NotEmptyPipe,
  RestPaginatedResponse,
  RestResponse,
  SimpleHttpResponse,
} from '@cryptexlabs/codex-nodejs-common';
import { PermissionService } from './permission.service';
import { ExampleSimpleResponse } from '../example/example.simple.response';
import { LocalesEnum } from '../locale/enum';
import { PermissionPatchInterface } from './permission.patch.interface';
import { PermissionUpdateInterface } from './permission.update.interface';
import {
  examplePermission,
  permissionPatchExample,
  permissionUpdateExample,
} from './example';
import { appConfig } from '../setup';
import { PermissionListAuthzGuard } from './permission.list.authz.guard';
import { PermissionGetAuthzGuard } from './permission.get.authz.guard';
import { PermissionDeleteAuthzGuard } from './permission.delete.authz.guard';
import { PermissionCreateAuthzGuard } from './permission.create.authz.guard';
import { PermissionUpdateAuthzGuard } from './permission.update.authz.guard';
import { ExampleRestPaginatedResponse } from '../example/example.rest.paginated.response';
import { ExampleRestResponse } from '../example/example.rest.response';
import { PermissionExistsAuthzGuard } from './permission.exists.authz.guard';
import { PermissionUpdateValidationPipe } from './permission.update.validation.pipe';
import { OrderDirectionEnum } from '../query.options.interface';
import { OrderDirectionValidationPipe } from '../order.direction.validation.pipe';
import { UserService } from '../user/user.service';

@Controller(`/${appConfig.appPrefix}/${appConfig.apiVersion}/permission`)
@ApiTags('permission')
@ApiMetaHeaders()
@ApiBearerAuth()
export class PermissionController {
  constructor(
    private readonly permissionService: PermissionService,
    private readonly userService: UserService,
    @Inject('CONTEXT_BUILDER') private readonly contextBuilder: ContextBuilder,
  ) {}

  @Get()
  @ApiOperation({
    summary: 'Get list of permissions',
  })
  @ApiPagination()
  @ApiQuery({
    name: 'orderBy',
    required: false,
    schema: {
      example: 'name',
    },
  })
  @ApiQuery({
    name: 'orderDirection',
    required: false,
    schema: {
      example: 'asc',
    },
  })
  @ApiQuery({
    name: 'searchName',
    required: false,
  })
  @ApiQuery({
    name: 'search',
    required: false,
  })
  @UseGuards(PermissionListAuthzGuard)
  @ApiResponse({
    schema: {
      example: new ExampleRestPaginatedResponse(
        HttpStatus.OK,
        'cryptexlabs.iam-zen.permission.list',
        [examplePermission],
      ),
    },
  })
  public async getPermissions(
    @Query('page', ParseIntPipe) page: number,
    @Query('pageSize', ParseIntPipe) pageSize: number,
    @Query('searchName') searchName: string,
    @Query('search') search: string,
    @Query('orderBy') orderBy: string,
    @Query('orderDirection', OrderDirectionValidationPipe)
    orderDirection: OrderDirectionEnum,
    @Headers() headers: Record<string, string>,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const permissions = await this.permissionService.getPermissions(
      context,
      page,
      pageSize,
      {
        searchName,
        search,
        orderBy,
        orderDirection,
      },
    );

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestPaginatedResponse(
      responseContext,
      HttpStatus.OK,
      permissions.total,
      'cryptexlabs.iam-zen.permission.list',
      permissions.results,
    );
  }

  @Get(':permissionId')
  @ApiOperation({
    summary: 'Get a permission by permission ID',
  })
  @ApiParam({
    name: 'permissionId',
    example: 'ec7d1b2f-a23c-4b8a-86fc-1a9276c4ff88',
  })
  @UseGuards(PermissionGetAuthzGuard)
  public async getPermission(
    @Param('permissionId') permissionId: string,
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const permission = await this.permissionService.getPermissionById(
      context,
      permissionId,
    );

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestResponse(
      responseContext,
      HttpStatus.OK,
      'cryptexlabs.iam-zen.permission',
      permission,
    );
  }

  @Get('any/exists')
  @ApiOperation({
    summary: 'Get whether or not a permission exists',
  })
  @ApiQuery({
    name: 'name',
    example: 'Self managed user',
    required: true,
  })
  @UseGuards(PermissionExistsAuthzGuard)
  @ApiResponse({
    schema: {
      example: new ExampleRestResponse(HttpStatus.OK, 'boolean', false),
    },
  })
  public async getPermissionExists(
    @Query('name', NotEmptyPipe) name: string,
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const permissionExists = await this.permissionService.getPermissionExists(
      context,
      name,
    );

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestResponse(
      responseContext,
      HttpStatus.OK,
      'boolean',
      permissionExists,
    );
  }

  @Delete(':permissionId')
  @ApiOperation({
    summary: 'Delete permission by permission ID',
  })
  @ApiParam({
    name: 'permissionId',
    example: '3d66a20d-aa32-4acc-999a-e0cf459b13f5',
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(PermissionDeleteAuthzGuard)
  public async deletePermission(
    @Param('permissionId', NotEmptyPipe) permissionId: string,
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    await this.permissionService.deletePermission(context, permissionId);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Delete()
  @ApiOperation({
    summary: 'Delete permissions by permission ID',
  })
  @ApiQuery({
    name: 'ids',
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(PermissionListAuthzGuard)
  public async deletePermissions(
    @Query('ids', NotEmptyPipe) permissionIds: string[],
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const usePermissionIds = Array.isArray(permissionIds)
      ? permissionIds
      : [permissionIds];

    await this.permissionService.deletePermissions(context, usePermissionIds);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Put(':permissionId')
  @ApiOperation({
    summary: 'Create or update permission by permission ID',
  })
  @ApiParam({
    name: 'permissionId',
    example: '3d66a20d-aa32-4acc-999a-e0cf459b13f5',
    required: true,
  })
  @ApiBody({
    schema: {
      example: permissionUpdateExample,
    },
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(PermissionCreateAuthzGuard)
  public async savePermission(
    @Param('permissionId', NotEmptyPipe) permissionId: string,
    @Headers() headers,
    @Res() res,
    @Body(NotEmptyPipe, PermissionUpdateValidationPipe)
    permissionUpdate: PermissionUpdateInterface,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const status = await this.permissionService.savePermission(
      context,
      permissionId,
      permissionUpdate,
    );

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    res
      .status(status)
      .send(
        new SimpleHttpResponse(
          responseContext,
          HttpStatus.ACCEPTED,
          LocalesEnum.SUCCESS,
        ),
      );
  }

  @Patch(':permissionId')
  @ApiOperation({
    summary: 'Partially update permission by permission ID',
  })
  @ApiParam({
    name: 'permissionId',
    example: '3d66a20d-aa32-4acc-999a-e0cf459b13f5',
    required: true,
  })
  @ApiBody({
    schema: {
      example: permissionPatchExample,
    },
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(PermissionUpdateAuthzGuard)
  public async patchPermission(
    @Param('permissionId', NotEmptyPipe) permissionId: string,
    @Body(NotEmptyPipe) permissionPatch: PermissionPatchInterface,
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    await this.permissionService.patchPermission(
      context,
      permissionId,
      permissionPatch,
    );

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Get(':permissionId/user')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Get list of users for a permission',
  })
  @ApiPagination()
  @ApiParam({
    name: 'permissionId',
    example: '3d66a20d-aa32-4acc-999a-e0cf459b13f5',
    required: true,
  })
  @ApiQuery({
    name: 'orderBy',
    required: false,
    schema: {
      example: 'name',
    },
  })
  @ApiQuery({
    name: 'orderDirection',
    required: false,
    schema: {
      example: 'asc',
    },
  })
  @ApiQuery({
    name: 'search',
    required: false,
  })
  public async getUsersForPermission(
    @Param('permissionId', NotEmptyPipe) permissionId: string,
    @Query('page', ParseIntPipe) page: number,
    @Query('pageSize', ParseIntPipe) pageSize: number,
    @Query('search') search: string,
    @Query('orderBy') orderBy: string,
    @Query('orderDirection', OrderDirectionValidationPipe)
    orderDirection: OrderDirectionEnum,
    @Headers() headers: Record<string, string>,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const users = await this.userService.getUsersForPermission(
      context,
      permissionId,
      page,
      pageSize,
      {
        search,
        orderBy,
        orderDirection,
      },
    );

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestPaginatedResponse(
      responseContext,
      HttpStatus.OK,
      users.total,
      'cryptexlabs.iam-zen.group.list',
      users.results,
    );
  }
}
