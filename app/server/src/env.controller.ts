import { Controller, Get, Inject, Req, Res } from '@nestjs/common';
import { Config } from './config';

@Controller('iam')
export class EnvController {
  constructor(@Inject('CONFIG') private readonly config: Config) {}

  // This endpoint overrides the default env.js file statically served
  @Get('env.js')
  public getEnv(@Res() res, @Req() req) {
    const thisHost = req.protocol + '://' + req.headers.host;
    const httpHost = ['docker', 'localhost'].includes(
      this.config.environmentName,
    )
      ? thisHost
      : '';
    res.set('Content-Type', 'text/javascript').send(
      `
window.ENVIRONMENT = {
  apiBaseUrl: '${httpHost}/${this.config.appPrefix}/${this.config.apiVersion}',
  userRegistrationEnabled: ${this.config.userRegistrationEnabled},
  forgotPasswordEnabled: ${this.config.forgotPasswordEnabled},
  uiBasePath: '${this.config.uiPrefix}'
};
    `,
    );
  }
}
