import {
  DefaultConfig,
  SqlConfigInterface,
} from '@cryptexlabs/codex-nodejs-common';
import { DatabaseTypeEnum } from './data/database-type.enum';
import { AuthenticationServiceTypeEnum } from './authentication/authentication-service.type.enum';
import { UrlInterface } from '@cryptexlabs/codex-nodejs-common/src/config/url.interface';
import { TokenPairExpirationPolicyInterface } from '@cryptexlabs/authf-data-model';
import * as yaml from 'js-yaml';
import * as fs from 'fs';
import { serverDir } from './root';
import {
  NewUserConfigInterface,
  ServerConfigInterface,
} from './server.config.interface';
import { AuthenticatedConfigInterface } from './authenticated-config.interface';
import { PolicyConfigInterface } from './policy/policy-config.interface';
import { GroupConfigInterface } from './group/group-config.interface';
import { UserConfigInterface } from './user/user.config.interface';
import { PermissionConfigInterface } from './permission/permission.config.interface';
import { AppConfigInterface } from './app/app.config.interface';
import { RoleConfigInterface } from './role/role-config.interface';
import * as process from 'process';
import * as glob from 'glob';
import * as watch from 'watch';
import { ConfigMerger } from './config-merger';
import * as fastq from 'fastq';
import { dirname } from 'path';

export interface RedirectUrlConfigInterface {
  redirectUrl: string;
}

export class Config extends DefaultConfig {
  private _serverConfig;
  private _configUpdateQueue;
  private _monitors: string[];
  private _pathsWatching: string[];
  private _configChangeLoaderEventHandler: () => Promise<void>;

  constructor(
    basePath: string,
    appPath: string,
    clientId: string,
    fallbackEnvironment?: string,
  ) {
    super(basePath, appPath, clientId, fallbackEnvironment);
    // Prevents concurrent config updates when watching multiple files
    this._configUpdateQueue = fastq((path, cb) => {
      this._onConfigUpdateQueueMessage(path).then(() => {
        cb(null);
      });
    }, 1);
    this._monitors = [];
    this._pathsWatching = [];
  }

  setConfigChangeLoaderEventHandler(eventHandler: () => Promise<void>): void {
    this._configChangeLoaderEventHandler = eventHandler;
  }

  private async _onConfigUpdateQueueMessage(path) {
    this._serverConfig = this._loadServerConfig(false);
    if (this._configChangeLoaderEventHandler) {
      await this._configChangeLoaderEventHandler();
    }
  }

  public get dbType(): DatabaseTypeEnum {
    return process.env.DATABASE_TYPE as DatabaseTypeEnum;
  }

  public get authf(): UrlInterface &
    AuthenticatedConfigInterface &
    RedirectUrlConfigInterface {
    return {
      url: process.env.AUTHF_URL,
      redirectUrl: process.env.AUTHF_REDIRECT_URL,
      username: process.env.AUTHF_ADMIN_USERNAME,
      password: process.env.AUTHF_ADMIN_PASSWORD,
    };
  }

  public get authenticationProviderType(): AuthenticationServiceTypeEnum {
    return (
      (process.env
        .AUTHENTICATION_SERVICE_TYPE as AuthenticationServiceTypeEnum) ||
      AuthenticationServiceTypeEnum.AUTHF
    );
  }

  public get authfAdminUsername(): string {
    return process.env.ADMIN_USERNAME || 'admin';
  }

  public get authfAdminPassword(): string {
    return process.env.ADMIN_PASSWORD || 'password';
  }
  private _getServerConfig(): ServerConfigInterface {
    if (!this._serverConfig) {
      this._serverConfig = this._loadServerConfig(true);
    }
    return this._serverConfig;
  }

  private _loadServerConfig(startWatchers: boolean): ServerConfigInterface {
    const serverConfigPath =
      process.env.IAM_ZEN_SERVER_CONFIG_PATH ||
      `${serverDir}/config/iam-zen.yaml`;

    let serverConfig = {} as ServerConfigInterface;

    if (fs.existsSync(serverConfigPath)) {
      serverConfig = this._loadConfigPath(serverConfigPath);
      if (startWatchers) {
        this._startConfigPatchWatch(serverConfigPath);
      }
    }

    if (process.env.IAM_ZEN_SERVER_CONFIG_PATTERN) {
      serverConfig = this._loadConfigPatterns(serverConfig, startWatchers);
    }

    console.log(`Server Config`, JSON.stringify(serverConfig, undefined, 2));

    return serverConfig;
  }

  private _startConfigPatchWatch(path: string) {
    if (this._hotReload) {
      console.log(`Starting watch for path ${path}`);
      const parent = dirname(path);
      if (!this._monitors.includes(parent)) {
        this._monitors.push(parent);
        console.log(`Creating monitor for directory ${parent}`);
        watch.createMonitor(dirname(path), {}, (monitor) => {
          monitor.on('changed', (f, curr, prev) => {
            if (typeof f === 'string') {
              if (this._pathsWatching.includes(f)) {
                console.log(`Config changed for path ${f}. Queueing update`);
                this._configUpdateQueue.push(f);
              }
            }
          });
        });
      }
      if (!this._pathsWatching.includes(path)) {
        this._pathsWatching.push(path);
      }
    }
  }

  private _loadConfigPath(serverConfigPath: string): ServerConfigInterface {
    const updatedServerConfig = yaml.load(
      fs.readFileSync(serverConfigPath).toString(),
    );
    if (typeof updatedServerConfig !== 'object') {
      throw new Error(
        `Invalid config in file ${serverConfigPath} with env variable IAM_ZEN_SERVER_CONFIG_PATH or default path: ${serverDir}/config/iam-zen.yaml`,
      );
    }
    return updatedServerConfig as ServerConfigInterface;
  }

  private _loadConfigPatterns(
    serverConfig: ServerConfigInterface,
    startWatchers: boolean,
  ) {
    const paths = glob.sync(process.env.IAM_ZEN_SERVER_CONFIG_PATTERN);
    console.log(`paths`, paths);
    let serverConfigUpdated = serverConfig;
    for (const path of paths) {
      if (!path.includes('.yaml')) {
        throw new Error(
          `Invalid path ${path} found for pattern ${process.env.IAM_ZEN_SERVER_CONFIG_PATTERN} with env variable IAM_ZEN_SERVER_CONFIG_PATTERN`,
        );
      }

      const config = yaml.load(fs.readFileSync(path).toString()) as any;
      if (typeof config !== 'object') {
        throw new Error(`Invalid config in file ${path}`);
      }
      serverConfigUpdated = ConfigMerger.mergeConfigs(
        serverConfigUpdated,
        config,
      );
      if (startWatchers) {
        this._startConfigPatchWatch(path);
      }
    }
    return serverConfigUpdated;
  }

  private get _hotReload() {
    return process.env.IAM_ZEN_SERVER_CONFIG_HOT_RELOAD !== 'false';
  }

  public get uiPrefix(): string {
    return process.env.UI_PREFIX || '';
  }

  public get newUserDefaultAttributes(): NewUserConfigInterface {
    const serverConfig = this._getServerConfig();
    if (serverConfig && serverConfig.newUser) {
      return serverConfig.newUser;
    } else {
      return {};
    }
  }

  public get defaultUsers(): UserConfigInterface[] {
    const serverConfig = this._getServerConfig();
    if (serverConfig && serverConfig.users) {
      return serverConfig.users;
    } else {
      return [];
    }
  }

  public get defaultGroups(): GroupConfigInterface[] {
    const serverConfig = this._getServerConfig();
    if (serverConfig && serverConfig.groups) {
      return serverConfig.groups;
    } else {
      return [];
    }
  }

  public get defaultRoles(): RoleConfigInterface[] {
    const serverConfig = this._getServerConfig();
    if (serverConfig && serverConfig.roles) {
      return serverConfig.roles;
    } else {
      return [];
    }
  }

  public get defaultApps(): AppConfigInterface[] {
    const serverConfig = this._getServerConfig();
    if (serverConfig && serverConfig.apps) {
      return serverConfig.apps;
    } else {
      return [];
    }
  }

  public get defaultPermissions(): PermissionConfigInterface[] {
    const serverConfig = this._getServerConfig();
    if (serverConfig && serverConfig.permissions) {
      return serverConfig.permissions;
    } else {
      return [];
    }
  }

  public get defaultPolicies(): PolicyConfigInterface[] {
    const serverConfig = this._getServerConfig();
    if (serverConfig && serverConfig.policies) {
      return serverConfig.policies;
    } else {
      return [];
    }
  }

  public get tokenExpirationPolicy(): TokenPairExpirationPolicyInterface {
    return this._getServerConfig().token.expirationPolicy;
  }

  public get cognitoPoolId(): string | null {
    const serverConfig = this._getServerConfig();
    if (
      serverConfig &&
      serverConfig.identityLink &&
      serverConfig.identityLink.cognito
    ) {
      if (!serverConfig.identityLink.cognito.poolId) {
        throw new Error("Invalid cognito config. Missing 'poolId'");
      }
      return serverConfig.identityLink.cognito.poolId;
    } else {
      return null;
    }
  }

  public get cognitoDeveloperProviderName(): string | null {
    const serverConfig = this._getServerConfig();
    if (
      serverConfig &&
      serverConfig.identityLink &&
      serverConfig.identityLink.cognito
    ) {
      if (!serverConfig.identityLink.cognito.developerProviderName) {
        throw new Error("Invalid cognito config. Missing 'poolId'");
      }
      return serverConfig.identityLink.cognito.developerProviderName;
    } else {
      return null;
    }
  }

  public get forgotPasswordEnabled() {
    return process.env.FORGOT_PASSWORD_DISABLED !== 'true';
  }

  public get cognitoPoolRegion(): string | null {
    const id = this.cognitoPoolId;
    if (!id) {
      return null;
    }

    const parts = id.split(':');
    if (parts.length !== 2) {
      throw new Error(`Invalid cognito pool id: ${id}`);
    }

    return parts[0];
  }

  public get userRegistrationEnabled(): boolean {
    return process.env.USER_REGISTRATION_DISABLED !== 'true';
  }

  public get autoLoadConfig(): boolean {
    return process.env.AUTOLOAD_IAM_ZEN_SERVER_CONFIG !== 'false';
  }

  public get sql(): SqlConfigInterface {
    return {
      database: process.env.SQL_DATABASE,
      client: process.env.SQL_CLIENT,
      userName: process.env.SQL_USERNAME,
      password: process.env.SQL_PASSWORD,
      primary: {
        hostName: process.env.SQL_HOSTNAME,
        port: parseInt(process.env.SQL_PORT, 10),
        pool: {
          min: parseInt(process.env.SQL_POOL_MIN, 10),
          max: parseInt(process.env.SQL_POOL_MAX, 10),
        },
      },
      reader: {
        hostName: process.env.SQL_HOSTNAME_RO,
        port: parseInt(process.env.SQL_PORT_RO, 10),
        pool: {
          min: parseInt(process.env.SQL_POOL_RO_MIN, 10),
          max: parseInt(process.env.SQL_POOL_RO_MAX, 10),
        },
      },
    };
  }
}
