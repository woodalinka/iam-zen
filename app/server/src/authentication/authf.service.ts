import {
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { UserCreateInterface } from '../user/create/user.create.interface';
import axios from 'axios';
import { AuthenticationServiceInterface } from './authentication.service.interface';
import { Config } from '../config';
import { AuthfIdentityLinkInterface } from './authf.identity-link.interface';
import {
  ApiAuthAuthenticateInterface,
  ApiAuthAuthenticationProviderInterface,
  AuthenticateTypes,
  AuthenticationInterface,
  AuthenticationProviderTypeEnum,
  AuthenticationTokenDataInterface,
  AuthenticationTypes,
  CognitoExtraInputInterface,
  CreateTokenResponseDataInterface,
  CreateTokenWithAuthorizedAuthenticationInterface,
  ExtraTypeEnum,
  TokenPairExpirationPolicyInterface,
} from '@cryptexlabs/authf-data-model';
import { UserInterface } from '../user/user.interface';
import { Context, CustomLogger } from '@cryptexlabs/codex-nodejs-common';
import * as jwt from 'jsonwebtoken';
import { PermissionInterface } from '../permission/permission.interface';
import { AppInterface } from '../app/app.interface';
import { AuthfApiKeyInterface } from './authf.api-key.interface';
import { ApiKeyTypeEnum } from '../api-key/api-key.type.enum';
import { IdentityLinkTypeEnum } from '../user/identity-link/identity-link-type.enum';
import { CreateAppInterface } from '../app/create-app.interface';
import { v4 as uuidv4 } from 'uuid';
import { ApiKeyInterface } from '../api-key/api-key.interface';
import { IdentityLinkInterface } from '../user/identity-link/identity-link.interface';
import { ApiKeyUtil } from '../api-key/api-key.util';
import { NewApiKeyInterface } from '../api-key/new-api-key.interface';
import { IdentityLinkUsageEnum } from '../user/identity-link/identity-link-usage.enum';

@Injectable()
export class AuthfService implements AuthenticationServiceInterface {
  constructor(
    @Inject('LOGGER') private readonly logger: CustomLogger,
    @Inject('CONFIG') private readonly config: Config,
  ) {}

  public async createUser(
    context: Context,
    userId: string,
    identityLink: AuthfIdentityLinkInterface,
    user: UserCreateInterface,
    permissions: PermissionInterface[],
    tokenExpirationPolicy: TokenPairExpirationPolicyInterface,
  ) {
    const providers = [];
    if (user.authentication) {
      if (user.authentication.type === AuthenticationProviderTypeEnum.BASIC) {
        providers.push({
          ...user.authentication,
          data: {
            ...user.authentication.data,
            username: user.authentication.data.username.toLowerCase(),
          },
        });
      } else {
        providers.push(user.authentication);
      }
    }

    const payload: AuthenticationInterface = {
      providers,
      token: {
        expirationPolicy: tokenExpirationPolicy,
        subject: userId,
        body: {
          scopes: permissions.map((permission) => permission.value),
        },
      },
    };

    const finalPayload = this._getExtra(payload);

    await this._patchAuthentication(
      context,
      finalPayload,
      identityLink.data.authenticationId,
    );
  }

  private _getExtra(payload) {
    const extra = {};

    if (this.config.cognitoPoolId) {
      // noinspection UnnecessaryLocalVariableJS
      const cognitoExtra: CognitoExtraInputInterface = {
        developerProviderName: this.config.cognitoDeveloperProviderName,
        poolId: this.config.cognitoPoolId,
        region: this.config.cognitoPoolRegion,
        label: '',
      };
      extra[ExtraTypeEnum.COGNITO] = cognitoExtra;
    }

    const finalExtra = Object.keys(extra).length > 0 ? extra : undefined;

    return {
      ...payload,
      extra: finalExtra,
    };
  }

  public async updateUserAuthentication(
    context: Context,
    userLogin: AuthenticateTypes | null,
    identityLink: AuthfIdentityLinkInterface,
    permissions: PermissionInterface[],
    tokenExpirationPolicy: TokenPairExpirationPolicyInterface,
    userId: string,
    authentication?: AuthenticationTypes,
  ): Promise<void | CreateTokenResponseDataInterface> {
    const providers = [];
    if (authentication) {
      if (authentication.type === AuthenticationProviderTypeEnum.BASIC) {
        providers.push({
          ...authentication,
          data: {
            ...authentication.data,
            username: authentication.data.username.toLowerCase(),
          },
        });
      } else {
        providers.push(authentication);
      }
    }

    const payload: AuthenticationInterface = {
      providers,
      token: {
        expirationPolicy: tokenExpirationPolicy,
        subject: userId,
        body: {
          scopes: permissions.map((permission) => permission.value),
        },
      },
    };

    const finalPayload = this._getExtra(payload);

    await this._patchAuthentication(
      context,
      finalPayload,
      identityLink.data.authenticationId,
    );

    return this._getToken(
      context,
      payload,
      userLogin,
      identityLink.data.authenticationId,
    );
  }

  private async _getToken(
    context: Context,
    payload: AuthenticationInterface,
    userLogin: AuthenticateTypes | null,
    authenticationId: string,
  ): Promise<CreateTokenResponseDataInterface> {
    const finalAuthentication = this._getExtra(payload);
    const createTokenPayload: CreateTokenWithAuthorizedAuthenticationInterface =
      {
        authentication: finalAuthentication,
        authenticate: userLogin,
      };

    const contentType = userLogin
      ? 'cryptexlabs.authf.token.create.authorized-authentication-with-unverified'
      : 'cryptexlabs.authf.token.create.authorized-authentication-with-verified';

    try {
      const response = await axios.post(
        `/api/v1/authentication/${authenticationId}/token`,
        createTokenPayload,
        {
          baseURL: this.config.authf.url,
          headers: {
            'Content-Type': 'application/json',
            'X-Correlation-Id': context.correlationId,
            'Accept-Language': context.locale.i18n,
            'X-Meta-Type': contentType,
          },
          auth: {
            username: this.config.authf.username,
            password: this.config.authf.password,
          },
        },
      );

      return response.data.data;
    } catch (e) {
      if (!e.response) {
        throw e;
      }
      context.logger.error(e.response.data.data);
    }
  }

  public async createApp(
    context: Context,
    apiKeys: AuthfApiKeyInterface[],
    id: string,
    app: CreateAppInterface,
    permissions: PermissionInterface[],
    tokenExpirationPolicy: TokenPairExpirationPolicyInterface,
  ) {
    if (app.authentications.length > 0) {
      const payload: AuthenticationInterface = {
        providers: app.authentications,
        token: {
          expirationPolicy: tokenExpirationPolicy,
          subject: id,
          body: {
            scopes: permissions.map((permission) => permission.value),
          },
        },
      };

      await this._patchAuthentication(
        context,
        payload,
        apiKeys[0].data.authenticationId,
      );
    }
  }

  public async waitForHealthy() {
    while (1 === 1) {
      try {
        await axios.get(`/healthz`, {
          baseURL: this.config.authf.url,
          headers: {
            'Content-Type': 'application/json',
          },
        });
        return;
      } catch (e) {
        this.logger.info('Waiting for authf service');
        await new Promise((resolve) => setTimeout(resolve, 1000));
      }
    }
  }

  public async loginUser(
    context: Context,
    res: any,
    user: UserInterface,
    loginIdentityLink: AuthfIdentityLinkInterface,
    userLogin: AuthenticateTypes,
  ): Promise<void> {
    await res.redirect(
      HttpStatus.TEMPORARY_REDIRECT,
      `${this.config.authf.redirectUrl}/api/v1/authentication/${loginIdentityLink.data.authenticationId}/token`,
    );
  }

  public async loginApp(
    context: Context,
    res: any,
    app: AppInterface,
    loginApiKey: AuthfApiKeyInterface,
    appLogin: ApiAuthAuthenticateInterface,
  ): Promise<void> {
    await res.redirect(
      HttpStatus.TEMPORARY_REDIRECT,
      `${this.config.authf.redirectUrl}/api/v1/authentication/${loginApiKey.data.authenticationId}/token`,
    );
  }

  public async updateTokenPayload(
    context: Context,
    userLogin: AuthenticateTypes | null,
    userId: string,
    permissions: PermissionInterface[],
    expirationPolicy: TokenPairExpirationPolicyInterface,
    identityLinks: AuthfIdentityLinkInterface[],
    getLogin,
  ): Promise<void | CreateTokenResponseDataInterface> {
    if (identityLinks.length === 0) {
      throw new HttpException(
        'Missing identity links',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
    const payload: { token: AuthenticationTokenDataInterface } = {
      token: {
        expirationPolicy: expirationPolicy,
        subject: userId,
        body: {
          scopes: permissions.map((permission) => permission.value),
        },
      },
    };

    const authenticationIds = identityLinks.map(
      (identityLink) => identityLink.data.authenticationId,
    );

    const uniqueAuthenticationIds = authenticationIds.filter(
      (v, i, a) => a.indexOf(v) === i,
    );

    for (const authenticationId of uniqueAuthenticationIds) {
      await this._patchAuthentication(context, payload, authenticationId);
    }

    if (getLogin) {
      const loginIdentityLink = identityLinks.find(
        (item) => item.usage === IdentityLinkUsageEnum.LOGIN,
      );
      return this._getToken(
        context,
        { ...payload, providers: [] },
        userLogin,
        loginIdentityLink.data.authenticationId,
      );
    }
  }

  private async _patchAuthentication(
    context: Context,
    payload: any,
    authenticationId: string,
  ) {
    context.logger.debug(
      `Patching authentication with id: ${authenticationId}`,
    );

    try {
      await axios.patch(`/api/v1/authentication/${authenticationId}`, payload, {
        baseURL: this.config.authf.url,
        headers: {
          'Content-Type': 'application/json',
          'X-Correlation-Id': context.correlationId,
          'Accept-Language': context.locale.i18n,
        },
        auth: {
          username: this.config.authf.username,
          password: this.config.authf.password,
        },
      });
    } catch (e) {
      if (!e.response) {
        throw e;
      }
      context.logger.error(e.response.data.data);
    }
  }

  public async validateReAuthAuthorizeToken(
    context: Context,
    token: string,
    identityLink: AuthfIdentityLinkInterface,
  ): Promise<void> {
    context.logger.debug(
      `Validating ReAuth Authorize token ${identityLink.data.authenticationId}`,
    );
    try {
      await axios.get(`/api/v1/authenticated`, {
        baseURL: this.config.authf.url,
        headers: {
          'Content-Type': 'application/json',
          'X-Correlation-Id': context.correlationId,
          'Accept-Language': context.locale.i18n,
          Authorization: `Bearer ${token}`,
        },
      });
    } catch (e) {
      context.logger.error(e.response.data.data);
    }

    const decodedAccessToken = jwt.decode(token) as unknown as any;
    const scopes = decodedAccessToken.scopes;
    if (
      !scopes.includes(
        `authentication:${identityLink.data.authenticationId}:any`,
      )
    ) {
      throw new HttpException('Not allowed', HttpStatus.FORBIDDEN);
    }
  }

  public getApiKeyType(): ApiKeyTypeEnum {
    return ApiKeyTypeEnum.AUTHF;
  }

  public getIdentityLinkType(): IdentityLinkTypeEnum {
    return IdentityLinkTypeEnum.AUTHF;
  }

  public getIdentityLinkId(identityLink: AuthfIdentityLinkInterface): string {
    return identityLink.data.authenticationId;
  }

  public getApiKeysFromApiAuthentication(
    authenticationProviders: ApiAuthAuthenticationProviderInterface[],
  ): AuthfApiKeyInterface[] {
    const authenticationId = uuidv4();
    return authenticationProviders.map((provider) => ({
      type: ApiKeyTypeEnum.AUTHF,
      id: provider.data.apiKey,
      data: {
        authenticationId,
      },
    }));
  }

  public async updateApiKeyAuthentications(
    context: Context,
    apiKeys: AuthfApiKeyInterface[],
    permissions: PermissionInterface[],
    tokenExpirationPolicy: TokenPairExpirationPolicyInterface,
    subject: string,
    authentications: ApiAuthAuthenticateInterface[],
  ): Promise<void> {
    if (authentications.length > 0) {
      const payload = {
        providers: authentications,
        token: {
          expirationPolicy: tokenExpirationPolicy,
          subject,
          body: {
            scopes: permissions.map((permission) => permission.value),
          },
        },
      };

      const authfApiKeys = apiKeys.filter(
        (apiKey) => apiKey.type === ApiKeyTypeEnum.AUTHF,
      );

      for (const apiKey of authfApiKeys) {
        await this._patchAuthentication(
          context,
          payload,
          apiKey.data.authenticationId,
        );
      }
    }
  }

  public getApiKeyForRefreshTokenSub(
    apiKeys: AuthfApiKeyInterface[],
    sub: string,
  ): ApiKeyInterface {
    return apiKeys.find(
      (findApiKey) => findApiKey.data.authenticationId === sub,
    );
  }

  public getIdentityLinkForRefreshTokenSub(
    identityLinks: AuthfIdentityLinkInterface[],
    sub: string,
  ): IdentityLinkInterface {
    return identityLinks.find(
      (findIdentityLink) => findIdentityLink.data.authenticationId === sub,
    );
  }

  public createApiKeyForApp(
    context: Context,
    app: AppInterface,
    permissions: PermissionInterface[],
    tokenExpirationPolicy: TokenPairExpirationPolicyInterface,
  ): Promise<NewApiKeyInterface> {
    return this.createApiKeys(
      context,
      app.apiKeys,
      app.id,
      permissions,
      tokenExpirationPolicy,
    );
  }
  public async createApiKeys(
    context: Context,
    apiKeys: ApiKeyInterface[],
    subject: string,
    permissions: PermissionInterface[],
    tokenExpirationPolicy: TokenPairExpirationPolicyInterface,
  ): Promise<NewApiKeyInterface> {
    const apiAuth = ApiKeyUtil.generateApiAuth();

    const authfApiKeys = apiKeys.filter(
      (apiKey) => apiKey.type === ApiKeyTypeEnum.AUTHF,
    );

    let useApiKey: AuthfApiKeyInterface;
    if (authfApiKeys.length === 0) {
      useApiKey = {
        data: {
          authenticationId: uuidv4(),
        },
        id: apiAuth.apiKey,
        type: ApiKeyTypeEnum.AUTHF,
      };
    } else {
      useApiKey = {
        data: {
          authenticationId: (authfApiKeys[0] as AuthfApiKeyInterface).data
            .authenticationId,
        },
        id: apiAuth.apiKey,
        type: ApiKeyTypeEnum.AUTHF,
      };
    }

    const authentication: ApiAuthAuthenticateInterface = {
      data: apiAuth,
      type: AuthenticationProviderTypeEnum.API,
    };

    await this.updateApiKeyAuthentications(
      context,
      [useApiKey],
      permissions,
      tokenExpirationPolicy,
      subject,
      [authentication],
    );

    return {
      apiKey: useApiKey,
      authenticationData: apiAuth,
    };
  }

  public async deleteApiKeys(
    context: Context,
    apiKeys: AuthfApiKeyInterface[],
  ): Promise<void> {
    for (const apiKey of apiKeys) {
      context.logger.debug(
        `Deleting API key for authentication with id: ${apiKey.data.authenticationId}`,
      );
      try {
        await axios.delete(
          `/api/v1/authentication/${apiKey.data.authenticationId}/provider`,
          {
            baseURL: this.config.authf.url,
            headers: {
              'Content-Type': 'application/json',
              'X-Correlation-Id': context.correlationId,
              'Accept-Language': context.locale.i18n,
            },
            auth: {
              username: this.config.authf.username,
              password: this.config.authf.password,
            },
            params: {
              type: 'api',
              identifier: apiKey.id,
              deleteAuthenticationIfProvidersEmpty: 'true',
            },
          },
        );
      } catch (e) {
        if (!e.response) {
          throw e;
        }
        context.logger.error(e.response.data.data);
        throw new InternalServerErrorException();
      }
    }
  }
}
