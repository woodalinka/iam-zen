import { UserCreateInterface } from '../user/create/user.create.interface';
import { IdentityLinkInterface } from '../user/identity-link/identity-link.interface';
import { UserInterface } from '../user/user.interface';
import {
  ApiAuthAuthenticateInterface,
  ApiAuthAuthenticationProviderInterface,
  AuthenticateTypes,
  AuthenticationTypes,
  CreateTokenResponseDataInterface,
  RefreshAuthAuthenticationInterface,
  TokenPairExpirationPolicyInterface,
} from '@cryptexlabs/authf-data-model';
import { Context } from '@cryptexlabs/codex-nodejs-common';
import { PermissionInterface } from '../permission/permission.interface';
import { AppInterface } from '../app/app.interface';
import { ApiKeyInterface } from '../api-key/api-key.interface';
import { CreateAppInterface } from '../app/create-app.interface';
import { ApiKeyTypeEnum } from '../api-key/api-key.type.enum';
import { IdentityLinkTypeEnum } from '../user/identity-link/identity-link-type.enum';
import { NewApiKeyInterface } from '../api-key/new-api-key.interface';

export interface AuthenticationServiceInterface {
  createUser(
    context: Context,
    userId: string,
    identityLink: IdentityLinkInterface,
    user: UserCreateInterface,
    permissions: PermissionInterface[],
    tokenExpirationPolicy: TokenPairExpirationPolicyInterface,
  ): Promise<void>;

  updateUserAuthentication(
    context: Context,
    userLogin: AuthenticateTypes | null,
    identityLink: IdentityLinkInterface,
    permissions: PermissionInterface[],
    tokenExpirationPolicy: TokenPairExpirationPolicyInterface,
    userId: string,
    authentication?: AuthenticationTypes,
  ): Promise<void | CreateTokenResponseDataInterface>;

  createApp(
    context: Context,
    apiKeys: ApiKeyInterface[],
    id: string,
    app: CreateAppInterface,
    permissions: PermissionInterface[],
    tokenExpirationPolicy: TokenPairExpirationPolicyInterface,
  ): Promise<void>;

  loginUser(
    context: Context,
    res: any,
    user: UserInterface,
    loginIdentityLink: IdentityLinkInterface,
    userLogin: AuthenticateTypes,
  ): Promise<any>;

  loginApp(
    context: Context,
    res: any,
    app: AppInterface,
    loginApiKey: ApiKeyInterface,
    appLogin: ApiAuthAuthenticateInterface | RefreshAuthAuthenticationInterface,
  ): Promise<any>;

  /**
   * May be unimplemented depending on whether or not the authentication service supports ReAuth
   *
   * @param {Context} context
   * @param {string} token
   * @param {IdentityLinkInterface} identityLink
   */
  validateReAuthAuthorizeToken(
    context: Context,
    token: string,
    identityLink: IdentityLinkInterface,
  ): Promise<void>;

  updateTokenPayload(
    context: Context,
    userLogin: AuthenticateTypes | null,
    userId: string,
    permissions: PermissionInterface[],
    tokenExpirationPolicy: TokenPairExpirationPolicyInterface,
    authenticationLinks: any[],
    getLogin: boolean,
  ): Promise<void | CreateTokenResponseDataInterface>;

  getApiKeyType(): ApiKeyTypeEnum;

  getIdentityLinkType(): IdentityLinkTypeEnum;

  getIdentityLinkId(identityLink: IdentityLinkInterface): string;

  getApiKeysFromApiAuthentication(
    authenticationProviders: ApiAuthAuthenticationProviderInterface[],
  ): ApiKeyInterface[];

  updateApiKeyAuthentications(
    context: Context,
    existingApiKeys: ApiKeyInterface[],
    permissions: PermissionInterface[],
    tokenExpirationPolicy: TokenPairExpirationPolicyInterface,
    appId: string,
    authentications: ApiAuthAuthenticateInterface[],
  ): Promise<void>;

  getApiKeyForRefreshTokenSub(
    apiKeys: ApiKeyInterface[],
    sub: string,
  ): ApiKeyInterface;

  getIdentityLinkForRefreshTokenSub(
    identityLinks: IdentityLinkInterface[],
    sub: string,
  ): IdentityLinkInterface;

  createApiKeyForApp(
    context: Context,
    app: AppInterface,
    permissions: PermissionInterface[],
    tokenExpirationPolicy: TokenPairExpirationPolicyInterface,
  ): Promise<NewApiKeyInterface>;

  createApiKeys(
    context: Context,
    apiKeys: ApiKeyInterface[],
    subject: string,
    permissions: PermissionInterface[],
    tokenExpirationPolicy: TokenPairExpirationPolicyInterface,
  ): Promise<NewApiKeyInterface>;

  deleteApiKeys(context: Context, apiKeys: ApiKeyInterface[]): Promise<void>;
}
