import { appConfig } from '../setup';
import { AuthenticationServiceTypeEnum } from './authentication-service.type.enum';
import { AuthfService } from './authf.service';
import { AuthenticationServiceFactory } from './authentication-service-factory';

const providers = [];

if (
  appConfig.authenticationProviderType === AuthenticationServiceTypeEnum.AUTHF
) {
  providers.push(AuthfService);
  providers.push({
    provide: 'AUTHENTICATION_SERVICE',
    useFactory: async (authenticationService: AuthfService) => {
      await authenticationService.waitForHealthy();
      return authenticationService;
    },
    inject: [AuthfService],
  });
}

export const authenticationProviders = [
  ...providers,
  AuthenticationServiceFactory,
];
