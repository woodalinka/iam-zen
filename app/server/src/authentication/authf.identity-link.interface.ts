import { IdentityLinkInterface } from '../user/identity-link/identity-link.interface';
import { IdentityLinkTypeEnum } from '../user/identity-link/identity-link-type.enum';

export interface AuthfIdentityLinkInterface extends IdentityLinkInterface {
  type: IdentityLinkTypeEnum.AUTHF;
  data: {
    authenticationId: string;
  };
}
