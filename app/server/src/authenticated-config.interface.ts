export interface AuthenticatedConfigInterface {
  username: string;
  password: string;
}
