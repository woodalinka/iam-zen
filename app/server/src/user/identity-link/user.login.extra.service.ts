import { Inject, Injectable } from '@nestjs/common';
import { Config } from '../../config';

@Injectable()
export class UserLoginExtraService {
  constructor(
    @Inject('CONFIG')
    private readonly config: Config,
  ) {}

  public async getLoginExtra(userId: string): Promise<any> {
    let extra = {};
    if (this.config.cognitoPoolId) {
      extra = {
        ...extra,
        cognito: {
          poolId: this.config.cognitoPoolId,
          region: this.config.cognitoPoolRegion,
          developerProviderName: this.config.cognitoDeveloperProviderName,
        },
      };
    }

    return extra;
  }
}
