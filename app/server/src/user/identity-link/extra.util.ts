import { StringUtil } from '@cryptexlabs/codex-nodejs-common';

export class ExtraUtil {
  static getExtraHash(extra: any): string {
    return StringUtil.insecureMd5(JSON.stringify(extra));
  }
}
