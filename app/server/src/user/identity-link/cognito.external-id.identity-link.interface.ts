import { IdentityLinkInterface } from './identity-link.interface';
import { IdentityLinkTypeEnum } from './identity-link-type.enum';
import { IdentityLinkUsageEnum } from './identity-link-usage.enum';

export interface CognitoExternalIdIdentityLinkInterface
  extends IdentityLinkInterface {
  type: IdentityLinkTypeEnum.AWS_COGNITO;
  usage: IdentityLinkUsageEnum.EXTERNAL_ID;
  data: {
    identityId: string;
  };
}
