export enum IdentityLinkUsageEnum {
  LOGIN = 'login',
  EXTERNAL_ID = 'external-id',
  API_KEY = 'api-key',
}
