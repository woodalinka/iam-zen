export enum IdentityLinkTypeEnum {
  AUTHF = 'authf',
  AWS_COGNITO = 'aws-cognito',
  LOCAL_AUTH = 'local-auth',
}
