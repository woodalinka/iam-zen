import { IdentityLinkUsageEnum } from './identity-link-usage.enum';
import { IdentityLinkTypeEnum } from './identity-link-type.enum';

export interface IdentityLinkDataServiceInterface {
  getUserIdForIdentityLink(
    usage: IdentityLinkUsageEnum,
    linkType: IdentityLinkTypeEnum,
    linkId: string,
  ): Promise<string | null>;
  saveIdentityLink(
    usage: IdentityLinkUsageEnum,
    linkType: IdentityLinkTypeEnum,
    linkId: string,
    userId: string,
  ): Promise<void>;
}
