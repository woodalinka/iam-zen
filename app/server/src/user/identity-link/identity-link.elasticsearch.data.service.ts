import { Inject, Injectable } from '@nestjs/common';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import {
  CustomLogger,
  ElasticsearchInitializer,
} from '@cryptexlabs/codex-nodejs-common';
import { IdentityLinkDataServiceInterface } from './identity-link.data.service.interface';
import { IdentityLinkUsageEnum } from './identity-link-usage.enum';
import { IdentityLinkTypeEnum } from './identity-link-type.enum';

@Injectable()
export class IdentityLinkElasticsearchDataService
  implements IdentityLinkDataServiceInterface
{
  private static get DEFAULT_INDEX() {
    return 'identity_link';
  }

  private readonly elasticsearchInitializer: ElasticsearchInitializer;

  constructor(
    @Inject('LOGGER') private readonly logger: CustomLogger,
    private readonly elasticsearchService: ElasticsearchService,
  ) {
    this.elasticsearchInitializer = new ElasticsearchInitializer(
      logger,
      IdentityLinkElasticsearchDataService.DEFAULT_INDEX,
      elasticsearchService,
      {
        usage: {
          type: 'keyword',
        },
        linkType: {
          type: 'keyword',
        },
        linkId: {
          type: 'keyword',
        },
      },
    );
  }

  async initializeIndex() {
    await this.elasticsearchInitializer.initializeIndex();
  }

  public async getUserIdForIdentityLink(
    usage: IdentityLinkUsageEnum,
    linkType: IdentityLinkTypeEnum,
    linkId: string,
  ): Promise<string | null> {
    const response = await this.elasticsearchService.search({
      index: IdentityLinkElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                term: {
                  usage,
                },
              },
              {
                term: {
                  linkType,
                },
              },
              {
                term: {
                  linkId,
                },
              },
            ],
          },
        },
      },
    });

    if (response.hits.total === 0) {
      return null;
    }

    return (response.hits.hits[0] as any)._source.userId;
  }

  public async saveIdentityLink(
    usage: IdentityLinkUsageEnum,
    linkType: IdentityLinkTypeEnum,
    linkId: string,
    userId: string,
  ): Promise<void> {
    const getResponse = await this.elasticsearchService.search({
      index: IdentityLinkElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                term: {
                  usage,
                },
              },
              {
                term: {
                  linkType,
                },
              },
              {
                term: {
                  linkId,
                },
              },
            ],
          },
        },
      },
    });

    if (getResponse.hits.total > 0) {
      this.logger.debug(getResponse.hits.hits[0]);
      const _id = getResponse.hits.hits[0]._id;
      await this.elasticsearchService.update({
        index: IdentityLinkElasticsearchDataService.DEFAULT_INDEX,
        id: _id,
        body: {
          doc: {
            usage,
            linkType,
            linkId,
            userId,
          },
        },
      });
    } else {
      await this.elasticsearchService.index({
        index: IdentityLinkElasticsearchDataService.DEFAULT_INDEX,
        body: {
          usage,
          linkType,
          linkId,
          userId,
        },
      });
    }
  }
}
