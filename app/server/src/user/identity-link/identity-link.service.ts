import { Inject, Injectable } from '@nestjs/common';
import { IdentityLinkUsageEnum } from './identity-link-usage.enum';
import { IdentityLinkDataServiceInterface } from './identity-link.data.service.interface';
import { IdentityLinkTypeEnum } from './identity-link-type.enum';

@Injectable()
export class IdentityLinkService {
  constructor(
    @Inject('IDENTITY_LINK_DATA_SERVICE')
    private readonly identityLinkDataService: IdentityLinkDataServiceInterface,
  ) {}

  public async getUserIdForIdentityLink(
    usage: IdentityLinkUsageEnum,
    linkType: IdentityLinkTypeEnum,
    linkId: string,
  ): Promise<string | null> {
    return this.identityLinkDataService.getUserIdForIdentityLink(
      usage,
      linkType,
      linkId,
    );
  }

  public async saveIdentityLink(
    usage: IdentityLinkUsageEnum,
    linkType: IdentityLinkTypeEnum,
    linkId: string,
    userId: string,
  ): Promise<void> {
    await this.identityLinkDataService.saveIdentityLink(
      usage,
      linkType,
      linkId,
      userId,
    );
  }
}
