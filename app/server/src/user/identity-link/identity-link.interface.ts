import { IdentityLinkTypeEnum } from './identity-link-type.enum';
import { IdentityLinkUsageEnum } from './identity-link-usage.enum';

export interface IdentityLinkInterface {
  type: IdentityLinkTypeEnum;
  usage: IdentityLinkUsageEnum;
  data: unknown;
}
