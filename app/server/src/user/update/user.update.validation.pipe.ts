import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import { UserUpdateInterface } from './user.update.interface';
import * as Joi from 'joi';

@Injectable()
export class UserUpdateValidationPipe implements PipeTransform {
  private _schema: Joi.ObjectSchema<UserUpdateInterface>;
  constructor() {
    this._schema = Joi.object({
      username: Joi.string().alphanum().required(),
      groups: Joi.array()
        .items(Joi.string().uuid({ version: 'uuidv4' }))
        .optional(),
    });
  }

  public transform(value: any, metadata: ArgumentMetadata): any {
    const { error } = this._schema.validate(value.data);
    if (error) {
      throw new BadRequestException(error.message);
    }
    return value;
  }
}
