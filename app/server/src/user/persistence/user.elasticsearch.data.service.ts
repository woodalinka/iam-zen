import { UserDataServiceInterface } from './user.data.service.interface';
import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import {
  Context,
  CustomLogger,
  ElasticsearchInitializer,
  PaginatedResults,
} from '@cryptexlabs/codex-nodejs-common';
import { UserPersistedInterface } from './user.persisted.interface';
import { IdentityLinkInterface } from '../identity-link/identity-link.interface';
import { UserPatchInterface } from '../update/user.patch.interface';
import { UserUpdateInterface } from '../update/user.update.interface';
import { QueryOptionsInterface } from '../../query.options.interface';

@Injectable()
export class UserElasticsearchDataService implements UserDataServiceInterface {
  private static get DEFAULT_INDEX() {
    return 'user';
  }

  private readonly elasticsearchInitializer: ElasticsearchInitializer;

  constructor(
    @Inject('LOGGER') private readonly logger: CustomLogger,
    private readonly elasticsearchService: ElasticsearchService,
  ) {
    this.elasticsearchInitializer = new ElasticsearchInitializer(
      logger,
      UserElasticsearchDataService.DEFAULT_INDEX,
      elasticsearchService,
      {
        id: {
          type: 'keyword',
        },
        groups: {
          type: 'keyword',
        },
        username: {
          type: 'keyword',
          normalizer: 'lowercase_normalizer',
        },
        usernameText: {
          type: 'text',
        },
      },
    );
  }

  async initializeIndex() {
    const exists = await this.elasticsearchService.indices.exists({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
    });

    if (!exists) {
      await this.elasticsearchService.indices.create({
        index: UserElasticsearchDataService.DEFAULT_INDEX,
        body: {
          settings: {
            analysis: {
              normalizer: {
                lowercase_normalizer: {
                  type: 'custom',
                  char_filter: [],
                  filter: ['lowercase'],
                },
              },
            },
          },
        },
      });
    }

    await this.elasticsearchInitializer.initializeIndex();
  }

  public async getUserWithUsername(
    username: string,
  ): Promise<UserPersistedInterface | null> {
    const response = await this.elasticsearchService.search({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                term: {
                  username,
                },
              },
            ],
          },
        },
      },
    });

    if (response.hits.total === 0) {
      return null;
    }

    const result = {
      ...(response.hits.hits[0] as any)._source,
    };
    delete result.usernameText;
    return result;
  }

  public async getUserWithId(userId: string): Promise<UserPersistedInterface> {
    const response = await this.elasticsearchService.search({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                term: {
                  id: userId,
                },
              },
            ],
          },
        },
      },
    });

    if (response.hits.total === 0) {
      return null;
    }

    const result = {
      ...(response.hits.hits[0] as any)._source,
    };
    delete result.usernameText;
    return result;
  }

  public async getUserWithIdOrName(
    userId: string,
    username: string,
  ): Promise<UserPersistedInterface | null> {
    const response = await this.elasticsearchService.search({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            should: [
              {
                term: {
                  id: userId,
                },
              },
              {
                term: {
                  username,
                },
              },
            ],
          },
        },
      },
    });

    if (response.hits.total === 0) {
      return null;
    }

    const result = {
      ...(response.hits.hits[0] as any)._source,
    };
    delete result.usernameText;
    return result;
  }

  public async createUser(
    context: Context,
    user: UserPersistedInterface,
  ): Promise<void> {
    await this.elasticsearchService.index({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      id: user.id,
      body: {
        ...user,
        usernameText: user.username,
      },
    });
  }

  public async updateTokenHashes(
    context: Context,
    userId: string,
    permissionsHash: string,
    expirationPolicyHash: string,
    extraHash: string,
  ): Promise<void> {
    await this.elasticsearchService.updateByQuery({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: 'ctx._source.hash = params.hash',
          params: {
            hash: {
              permissions: permissionsHash,
              tokenExpirationPolicy: expirationPolicyHash,
              extra: extraHash,
            },
          },
        },
        query: {
          match: {
            id: userId,
          },
        },
      },
    });
  }

  public async getUsers(
    context: Context,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<UserPersistedInterface>> {
    let query: { query?: any; sort?: any } = {};

    if (!options.search && options.searchName) {
      query = {
        query: {
          wildcard: {
            username: {
              value: `*${options.searchName}*`,
              boost: 1.0,
              rewrite: 'constant_score',
            },
          },
        },
      };
    }

    if (options.search) {
      query = {
        query: {
          query_string: {
            query: `*${options.search}*`,
            fields: ['usernameText', 'id'],
          },
        },
      };
    }

    const sortMap = {
      username: [{ username: options.orderDirection || 'asc' }, '_score'],
      groupCount: {
        _script: {
          type: 'number',
          script: {
            lang: 'painless',
            source: 'doc.groups.size()',
          },
          order: options.orderDirection || 'asc',
        },
      },
    };

    if (options.orderBy) {
      const sort = sortMap[options.orderBy];
      query = {
        ...query,
        sort,
      };
    }

    const response = await this.elasticsearchService.search({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      track_total_hits: true,
      body: {
        from: (page - 1) * pageSize,
        size: pageSize,
        ...query,
      },
    });

    return {
      total: (response.hits.total as any).value as number,
      results: response.hits.hits.map((message: { _source?: any }) => {
        const user = {
          ...message._source,
        };
        delete user.usernameText;
        return user;
      }),
    };
  }

  public async getUsersForGroups(
    context: Context,
    groupIds: string[],
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<UserPersistedInterface>> {
    let query: { query?: any; sort?: any } = {};

    if (!options.search && options.searchName) {
      query = {
        query: {
          wildcard: {
            username: {
              value: `*${options.searchName}*`,
              boost: 1.0,
              rewrite: 'constant_score',
            },
          },
        },
      };
    }

    if (options.search) {
      query = {
        query: {
          query_string: {
            query: `*${options.search}*`,
            fields: ['usernameText', 'id'],
          },
        },
      };
    }

    const sortMap = {
      username: [{ username: options.orderDirection || 'asc' }, '_score'],
      groupCount: {
        _script: {
          type: 'number',
          script: {
            lang: 'painless',
            source: 'doc.groups.size()',
          },
          order: options.orderDirection || 'asc',
        },
      },
    };

    query = {
      query: {
        ...query.query,
        bool: {
          must: [
            {
              terms: {
                groups: groupIds,
              },
            },
          ],
        },
      },
    };

    if (options.orderBy) {
      const sort = sortMap[options.orderBy];
      query = {
        ...query,
        sort,
      };
    }

    const response = await this.elasticsearchService.search({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      track_total_hits: true,
      body: {
        from: (page - 1) * pageSize,
        size: pageSize,
        ...query,
      },
    });

    return {
      total: (response.hits.total as any).value as number,
      results: response.hits.hits.map((message: { _source?: any }) => {
        const user = {
          ...message._source,
        };
        delete user.usernameText;
        return user;
      }),
    };
  }

  public async saveUsersFromConfig(
    users: UserPersistedInterface[],
  ): Promise<UserPersistedInterface[]> {
    if (users.length === 0) {
      return [];
    }

    const response = await this.elasticsearchService.search({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                terms: {
                  id: users.map((user) => user.id),
                },
              },
            ],
          },
        },
      },
    });

    const existingUsers = response.hits.hits;

    const existingUserIDs = existingUsers.map(
      (existingUser) => (existingUser._source as any).id,
    );

    const newUsers = users.filter((user) => !existingUserIDs.includes(user.id));
    const newUsersBulk = [];
    for (const newUser of newUsers) {
      newUsersBulk.push({
        index: {
          _index: UserElasticsearchDataService.DEFAULT_INDEX,
          _id: newUser.id,
        },
      });
      newUsersBulk.push({
        ...newUser,
        usernameText: newUser.username,
      });
    }

    const existingUsersBulk = [];
    for (const existingUser of existingUsers) {
      const updatedUser = users.find(
        (user) => user.id === (existingUser._source as any).id,
      );
      existingUsersBulk.push({
        index: {
          _index: UserElasticsearchDataService.DEFAULT_INDEX,
          _id: existingUser._id,
        },
      });
      existingUsersBulk.push({
        ...(existingUser._source as any),
        username: updatedUser.username,
        usernameText: updatedUser.username,
        groups: updatedUser.groups,
        hash: {
          ...(existingUser._source as any).hash,
          preConfiguredAuthentication:
            updatedUser.hash.preConfiguredAuthentication,
        },
      });
    }

    const bulk = [...newUsersBulk, ...existingUsersBulk];

    await this.elasticsearchService.bulk({ body: bulk });

    return newUsers;
  }

  public async saveIdentityLinks(
    context: Context,
    userId: string,
    identityLinks: IdentityLinkInterface[],
  ): Promise<void> {
    const finalIdentityLinks = [];
    let didChange = false;

    let user;
    while (!(user = await this.getUserWithId(userId))) {
      await new Promise((resolve) => setTimeout(resolve, 50));
    }

    for (const identityLink of user.identityLinks) {
      const newIdentityLink = identityLinks.find((findIdentityLink) => {
        return (
          findIdentityLink.type === identityLink.type &&
          findIdentityLink.usage === identityLink.usage
        );
      });
      if (newIdentityLink) {
        if (JSON.stringify(newIdentityLink.data) !== identityLink.data) {
          didChange = true;
        }
        finalIdentityLinks.push(newIdentityLink);
      } else {
        finalIdentityLinks.push(identityLink);
      }
    }

    for (const identityLink of identityLinks) {
      const existingIdentityLink = finalIdentityLinks.find(
        (findIdentityLink) => {
          return (
            findIdentityLink.type === identityLink.type &&
            findIdentityLink.usage === identityLink.usage
          );
        },
      );
      if (!existingIdentityLink) {
        didChange = true;
        finalIdentityLinks.push(identityLink);
      }
    }

    if (!didChange) {
      // No change to identity links.
      return;
    }

    await this.elasticsearchService.updateByQuery({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: 'ctx._source.identityLinks = params.identityLinks',
          params: {
            identityLinks: finalIdentityLinks,
          },
        },
        query: {
          match: {
            id: userId,
          },
        },
      },
    });
  }

  public async getUsersWithIds(
    userIds: string[],
  ): Promise<UserPersistedInterface[]> {
    const response = await this.elasticsearchService.search({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                terms: {
                  id: userIds,
                },
              },
            ],
          },
        },
      },
    });

    return response.hits.hits.map((message: { _source?: any }) => {
      const user = {
        ...message._source,
      };
      delete user.usernameText;
      return user;
    });
  }

  public async deleteUser(context: Context, userId: string): Promise<boolean> {
    const response = await this.elasticsearchService.deleteByQuery({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          match: {
            id: userId,
          },
        },
      },
    });
    return response.total === 1;
  }

  public async deleteUsers(
    context: Context,
    userIds: string[],
  ): Promise<boolean> {
    const response = await this.elasticsearchService.deleteByQuery({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                terms: {
                  id: userIds,
                },
              },
            ],
          },
        },
      },
    });
    return response.total > 0;
  }

  public async patchUser(
    context: Context,
    userId: string,
    userPatch: UserPatchInterface,
  ): Promise<boolean> {
    await this._validateUsernameNotTaken(context, userId, userPatch.username);
    const response = await this.elasticsearchService.updateByQuery({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
             if (params.username != null) {
               ctx._source.username = params.username
             }
            `,
          params: {
            username: userPatch.username,
          },
        },
        query: {
          match: {
            id: userId,
          },
        },
      },
    });

    return response.total === 1;
  }

  public async updateUser(
    context: Context,
    userId: string,
    userUpdate: UserUpdateInterface,
  ): Promise<boolean> {
    await this._validateUsernameNotTaken(context, userId, userUpdate.username);
    const response = await this.elasticsearchService.updateByQuery({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
             ctx._source.username = params.username;
             ctx._source.groups = params.groups;
            `,
          params: {
            username: userUpdate.username,
            groups: userUpdate.groups,
          },
        },
        query: {
          match: {
            id: userId,
          },
        },
      },
    });
    return response.total === 1;
  }

  private async _validateUsernameNotTaken(
    context: Context,
    userId: string,
    username: string,
  ) {
    if (username) {
      const existingUser = await this.getUserWithUsername(username);
      if (existingUser && existingUser.id !== userId) {
        throw new HttpException(
          `This username is in use by another user`,
          HttpStatus.CONFLICT,
        );
      }
    }
  }

  public async addGroups(
    context: Context,
    userId: string,
    groupIds: string[],
  ): Promise<boolean> {
    const response = await this.elasticsearchService.updateByQuery({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
           if (!(ctx._source.groups instanceof List)) {
             ctx._source.groups = new ArrayList();
           }
           ctx._source.groups.addAll(params.groups);
           ctx._source.groups = ctx._source.groups.stream().distinct().sorted().collect(Collectors.toList());
          `,
          params: {
            groups: groupIds,
          },
        },
        query: {
          match: {
            id: userId,
          },
        },
      },
    });
    return response.total === 1;
  }

  public async removeGroups(
    context: Context,
    userId: string,
    groupIds: string[],
  ): Promise<boolean> {
    const response = await this.elasticsearchService.updateByQuery({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
            if (!(ctx._source.groups instanceof List)) {
              ctx._source.groups = new ArrayList();
            }
            ctx._source.groups.removeAll(params.groups);
          `,
          params: {
            groups: groupIds,
          },
        },
        query: {
          match: {
            id: userId,
          },
        },
      },
    });

    return response.total === 1;
  }

  public async healthz() {
    await this.elasticsearchService.ping();
  }
}
