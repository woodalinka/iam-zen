import { UserInterface } from '../user.interface';
import { Context, PaginatedResults } from '@cryptexlabs/codex-nodejs-common';
import { UserPersistedInterface } from './user.persisted.interface';
import { IdentityLinkInterface } from '../identity-link/identity-link.interface';
import { UserUpdateInterface } from '../update/user.update.interface';
import { UserPatchInterface } from '../update/user.patch.interface';
import { QueryOptionsInterface } from '../../query.options.interface';

export interface UserDataServiceInterface {
  healthz(): Promise<void>;
  getUserWithId(userId: string): Promise<UserPersistedInterface | null>;
  getUsersWithIds(userIds: string[]): Promise<UserPersistedInterface[]>;
  getUserWithUsername(username: string): Promise<UserPersistedInterface | null>;
  getUserWithIdOrName(
    userId: string,
    username: string,
  ): Promise<UserPersistedInterface | null>;
  createUser(context: Context, user: UserInterface): Promise<void>;
  updateTokenHashes(
    context: Context,
    userId: string,
    permissionsHash: string,
    expirationPolicyHash: string,
    extraHash: string,
  ): Promise<void>;

  getUsers(
    context: Context,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<UserPersistedInterface>>;

  getUsersForGroups(
    context: Context,
    groupIds: string[],
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<UserPersistedInterface>>;

  /**
   * Returns list of newly created users
   *
   * @param {UserPersistedInterface[]} users
   */
  saveUsersFromConfig(
    users: UserPersistedInterface[],
  ): Promise<UserPersistedInterface[]>;

  saveIdentityLinks(
    context: Context,
    userId: string,
    identityLink: IdentityLinkInterface[],
  ): Promise<void>;

  deleteUser(context: Context, userId: string): Promise<boolean>;
  deleteUsers(context: Context, userIds: string[]): Promise<boolean>;

  updateUser(
    context: Context,
    userId: string,
    userUpdate: UserUpdateInterface,
  ): Promise<boolean>;

  patchUser(
    context: Context,
    userId: string,
    userPatch: UserPatchInterface,
  ): Promise<boolean>;

  addGroups(
    context: Context,
    userId: string,
    groupIds: string[],
  ): Promise<boolean>;

  removeGroups(
    context: Context,
    userId: string,
    groupIds: string[],
  ): Promise<boolean>;
}
