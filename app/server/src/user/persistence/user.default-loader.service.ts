import { Inject, Injectable } from '@nestjs/common';
import { UserDataServiceInterface } from './user.data.service.interface';
import { Config } from '../../config';
import { UserPersistedInterface } from './user.persisted.interface';
import { UserPermissionsProviderInterface } from '../permission/user.permissions-provider.interface';
import { ContextBuilder } from '@cryptexlabs/codex-nodejs-common';
import { IdentityLinkUsageEnum } from '../identity-link/identity-link-usage.enum';
import { v4 as uuidv4 } from 'uuid';
import { AuthenticationServiceInterface } from '../../authentication/authentication.service.interface';
import { IdentityLinkService } from '../identity-link/identity-link.service';
import { UserInterface } from '../user.interface';
import { PermissionUtil } from '../../permission/permission.util';
import { TokenUtil } from '../../token/token.util';
import { UserUtil } from '../user.util';
import { ExtraUtil } from '../identity-link/extra.util';
import { UserLoginExtraService } from '../identity-link/user.login.extra.service';

@Injectable()
export class UserDefaultLoaderService {
  constructor(
    @Inject('USER_DATA_SERVICE')
    private readonly userDataService: UserDataServiceInterface,
    @Inject('CONFIG') private readonly config: Config,
    @Inject('USER_PERMISSIONS_PROVIDER')
    private readonly userPermissionsProvider: UserPermissionsProviderInterface,
    @Inject('AUTHENTICATION_SERVICE')
    private readonly authenticationService: AuthenticationServiceInterface,
    @Inject('CONTEXT_BUILDER') private readonly contextBuilder: ContextBuilder,
    private readonly identityLinkService: IdentityLinkService,
    private readonly loginExtraService: UserLoginExtraService,
  ) {}

  public async load() {
    const context = this.contextBuilder.build().getResult();

    const permissionsMap = {};

    const persistedUsers: UserPersistedInterface[] = [];
    for (const configUser of this.config.defaultUsers) {
      const userForPermissions: UserInterface = {
        groups: configUser.groups || [],
        id: configUser.id,
        identityLinks: [],
        username: configUser.username,
      };
      const permissions =
        await this.userPermissionsProvider.getPermissionsForUser(
          context,
          userForPermissions,
        );
      permissionsMap[configUser.id] = permissions;

      const loginExtra = await this.loginExtraService.getLoginExtra(
        configUser.id,
      );
      persistedUsers.push({
        id: configUser.id,
        username: configUser.username,
        groups: configUser.groups,
        identityLinks: [
          {
            type: this.authenticationService.getIdentityLinkType(),
            usage: IdentityLinkUsageEnum.LOGIN,
            data: {
              authenticationId: uuidv4(),
            },
          },
        ],
        hash: {
          permissions: PermissionUtil.getPermissionsHash(permissions),
          tokenExpirationPolicy: TokenUtil.getTokenExpirationPolicyHash(
            this.config.tokenExpirationPolicy,
          ),
          preConfiguredAuthentication: UserUtil.getAuthenticationHash(
            configUser.authentication,
          ),
          extra: ExtraUtil.getExtraHash(loginExtra),
        },
      });
    }

    const existingUserIds = persistedUsers.map(
      (persistedUser) => persistedUser.id,
    );
    const existingUsers = await this.userDataService.getUsersWithIds(
      existingUserIds,
    );

    const newUsers = await this.userDataService.saveUsersFromConfig(
      persistedUsers,
    );

    for (const newUser of newUsers) {
      const configUser = this.config.defaultUsers.find(
        (configUser) => configUser.id === newUser.id,
      );
      const loginIdentityLink = newUser.identityLinks.find(
        (link) => link.usage === IdentityLinkUsageEnum.LOGIN,
      );
      await this.authenticationService.createUser(
        context,
        newUser.id,
        loginIdentityLink,
        {
          username: newUser.username,
          authentication: configUser.authentication,
        },
        PermissionUtil.mapSelfToUserId(permissionsMap[newUser.id], newUser.id),
        this.config.tokenExpirationPolicy,
      );

      await this.identityLinkService.saveIdentityLink(
        IdentityLinkUsageEnum.LOGIN,
        this.authenticationService.getIdentityLinkType(),
        this.authenticationService.getIdentityLinkId(loginIdentityLink),
        newUser.id,
      );
    }

    for (const existingUser of existingUsers) {
      const persistedUser = persistedUsers.find(
        (findUser) => findUser.id === existingUser.id,
      );

      if (
        existingUser.hash.preConfiguredAuthentication !==
        persistedUser.hash.preConfiguredAuthentication
      ) {
        const configUser = this.config.defaultUsers.find(
          (configApp) => configApp.id === existingUser.id,
        );

        const loginIdentityLink = existingUser.identityLinks.find(
          (findLink) => findLink.usage === IdentityLinkUsageEnum.LOGIN,
        );

        await this.authenticationService.updateUserAuthentication(
          context,
          null,
          loginIdentityLink,
          PermissionUtil.mapSelfToUserId(
            permissionsMap[configUser.id],
            configUser.id,
          ),
          this.config.tokenExpirationPolicy,
          existingUser.id,
          configUser.authentication,
        );
      }
    }
  }
}
