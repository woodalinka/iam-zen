import { UserService } from './user.service';
import { appConfig, elasticsearchService } from '../setup';
import { DatabaseTypeEnum } from '../data/database-type.enum';
import { UserElasticsearchDataService } from './persistence/user.elasticsearch.data.service';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { IdentityLinkService } from './identity-link/identity-link.service';
import { IdentityLinkElasticsearchDataService } from './identity-link/identity-link.elasticsearch.data.service';
import { UserDefaultLoaderService } from './persistence/user.default-loader.service';
import { UserDataServiceInterface } from './persistence/user.data.service.interface';
import { Config } from '../config';
import { UserPermissionsProviderInterface } from './permission/user.permissions-provider.interface';
import { AuthenticationServiceInterface } from '../authentication/authentication.service.interface';
import { ContextBuilder } from '@cryptexlabs/codex-nodejs-common';

import { UserLoginExtraService } from './identity-link/user.login.extra.service';

const providers = [];

if (appConfig.dbType === DatabaseTypeEnum.ELASTICSEARCH) {
  providers.push(
    {
      provide: ElasticsearchService,
      useValue: elasticsearchService,
    },
    UserElasticsearchDataService,
    IdentityLinkElasticsearchDataService,
    {
      provide: 'USER_DATA_SERVICE',
      useFactory: async (service: UserElasticsearchDataService) => {
        await service.initializeIndex();
        return service;
      },
      inject: [UserElasticsearchDataService],
    },
    {
      provide: 'IDENTITY_LINK_DATA_SERVICE',
      useFactory: async (service: IdentityLinkElasticsearchDataService) => {
        await service.initializeIndex();
        return service;
      },
      inject: [IdentityLinkElasticsearchDataService],
    },
  );
}

export const userProviders = [
  ...providers,
  UserService,
  {
    provide: UserDefaultLoaderService,
    useFactory: async (
      userDataService: UserDataServiceInterface,
      config: Config,
      userPermissionsProvider: UserPermissionsProviderInterface,
      authenticationService: AuthenticationServiceInterface,
      contextBuilder: ContextBuilder,
      identityLinkService: IdentityLinkService,
      loginExtraService: UserLoginExtraService,
    ) => {
      const userDefaultService = new UserDefaultLoaderService(
        userDataService,
        config,
        userPermissionsProvider,
        authenticationService,
        contextBuilder,
        identityLinkService,
        loginExtraService,
      );
      console.log();
      if (appConfig.autoLoadConfig) {
        await userDefaultService.load();
      }
      return userDefaultService;
    },
    inject: [
      'USER_DATA_SERVICE',
      'CONFIG',
      'USER_PERMISSIONS_PROVIDER',
      'AUTHENTICATION_SERVICE',
      'CONTEXT_BUILDER',
      IdentityLinkService,
      UserLoginExtraService,
    ],
  },
  IdentityLinkService,
  UserLoginExtraService,
];
