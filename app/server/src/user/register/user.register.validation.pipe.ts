import {
  ArgumentMetadata,
  BadRequestException,
  PipeTransform,
} from '@nestjs/common';
import { UserCreateInterface } from '../create/user.create.interface';
import * as Joi from 'joi';
import { userAuthenticationSchemas } from '../authentication/user.authentication.schemas';
import { AuthenticationProviderTypeEnum } from '@cryptexlabs/authf-data-model';

export class UserRegisterValidationPipe implements PipeTransform {
  private schema: Joi.ObjectSchema<UserCreateInterface>;

  constructor() {
    this.schema = Joi.object({
      authentication: userAuthenticationSchemas,
      username: Joi.string().optional(),
      groups: Joi.array().items(Joi.string()).optional(),
    });
  }

  transform(value: UserCreateInterface, metadata: ArgumentMetadata): any {
    const { error } = this.schema.validate(value);
    if (error) {
      throw new BadRequestException(error.message);
    }

    if (value.authentication) {
      if (value.authentication.type === AuthenticationProviderTypeEnum.BASIC) {
        if (value.username !== value.authentication.data.username) {
          throw new BadRequestException(
            `Username: ${value.username} does not match username: ${value.authentication.data.username}`,
          );
        }
      }
    }
    return value;
  }
}
