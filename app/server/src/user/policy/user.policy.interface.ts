export interface UserPolicyInterface {
  name: string;
  id: string;
  description: string;
  fromGroups: {
    id: string;
    name: string;
  }[];
}
