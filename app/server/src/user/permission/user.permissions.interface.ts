export interface UserPermissionsInterface {
  id: string;
  name: string;
  value: string;
  fromPolicies: {
    id: string;
    name: string;
  }[];
  fromGroups: {
    id: string;
    name: string;
  }[];
}
