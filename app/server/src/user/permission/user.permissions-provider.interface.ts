import { PermissionInterface } from '../../permission/permission.interface';
import { UserInterface } from '../user.interface';
import { Context } from '@cryptexlabs/codex-nodejs-common';

export interface UserPermissionsProviderInterface {
  getPermissionsForNewUser(context: Context): Promise<PermissionInterface[]>;
  getPermissionsForUser(
    context: Context,
    user: UserInterface,
  ): Promise<PermissionInterface[]>;
}
