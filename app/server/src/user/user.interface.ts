import { IdentityLinkInterface } from './identity-link/identity-link.interface';

export interface UserInterface {
  id: string;
  username: string | null;
  identityLinks: IdentityLinkInterface[];
  groups: string[];
}
