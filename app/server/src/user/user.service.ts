import {
  forwardRef,
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { UserDataServiceInterface } from './persistence/user.data.service.interface';
import {
  AuthenticateTypes,
  AuthenticationProviderTypeEnum,
  CreateTokenResponseDataInterface,
} from '@cryptexlabs/authf-data-model';
import { AuthenticationServiceInterface } from '../authentication/authentication.service.interface';
import { Config } from '../config';
import { AuthenticationServiceTypeEnum } from '../authentication/authentication-service.type.enum';
import { AuthfIdentityLinkInterface } from '../authentication/authf.identity-link.interface';
import { v4 as uuidv4 } from 'uuid';
import { IdentityLinkTypeEnum } from './identity-link/identity-link-type.enum';
import { IdentityLinkUsageEnum } from './identity-link/identity-link-usage.enum';
import { IdentityLinkInterface } from './identity-link/identity-link.interface';
import { AuthenticationServiceFactory } from '../authentication/authentication-service-factory';
import { UserCreateInterface } from './create/user.create.interface';
import {
  ArrayUtil,
  Context,
  PaginatedResults,
} from '@cryptexlabs/codex-nodejs-common';
import { IdentityLinkService } from './identity-link/identity-link.service';
import { UserPermissionsProviderInterface } from './permission/user.permissions-provider.interface';
import { UserPersistedInterface } from './persistence/user.persisted.interface';
import { UserDto } from './user.dto';
import { UserInListDto } from './read/user.in-list.dto';
import { UserGroupsProviderInterface } from './group/user.groups.provider.interface';
import * as jwt from 'jsonwebtoken';
import { PermissionUtil } from '../permission/permission.util';
import { TokenUtil } from '../token/token.util';
import { UserUpdateInterface } from './update/user.update.interface';
import { UserPatchInterface } from './update/user.patch.interface';
import { ImmutableObjectError } from '../error/immutable-object.error';
import { LocalesEnum } from '../locale/enum';
import { GroupService } from '../group/group.service';
import { UserLoginExtraService } from './identity-link/user.login.extra.service';
import { ExtraUtil } from './identity-link/extra.util';
import { BasicAuthAuthenticateInterface } from '@cryptexlabs/authf-data-model/src/authentication/provider/index';
import { QueryOptionsInterface } from '../query.options.interface';
import { PolicyService } from '../policy/policy.service';
import { GroupUtil } from '../group/group.util';

@Injectable()
export class UserService {
  constructor(
    @Inject('CONFIG') private readonly config: Config,
    @Inject('USER_DATA_SERVICE')
    private readonly userDataService: UserDataServiceInterface,
    @Inject('USER_PERMISSIONS_PROVIDER')
    private readonly permissionsProvider: UserPermissionsProviderInterface,
    @Inject('USER_GROUPS_PROVIDER')
    private readonly userGroupsProvider: UserGroupsProviderInterface,
    private readonly identityLinkService: IdentityLinkService,
    @Inject('AUTHENTICATION_SERVICE')
    private readonly authenticationService: AuthenticationServiceInterface,
    private readonly authenticationServiceFactory: AuthenticationServiceFactory,
    @Inject(forwardRef(() => 'GROUP_SERVICE'))
    private readonly groupService: GroupService,
    private readonly loginExtraService: UserLoginExtraService,
    @Inject('POLICY_SERVICE') private readonly policyService: PolicyService,
  ) {}

  public async loginUserWithId(
    context: Context,
    res: any,
    userId: string,
    userLogin: AuthenticateTypes,
  ) {
    const user = await this.userDataService.getUserWithId(userId);

    if (!user) {
      throw new HttpException(
        `User with id ${userId} not found`,
        HttpStatus.NOT_FOUND,
      );
    }

    return this._loginUser(context, res, user, userLogin);
  }

  public async loginUserWithUsername(
    context: Context,
    res: any,
    username: string,
    userLogin: BasicAuthAuthenticateInterface,
  ) {
    const user = await this.userDataService.getUserWithUsername(username);

    if (!user) {
      throw new HttpException(
        `User with username ${username} not found`,
        HttpStatus.NOT_FOUND,
      );
    }

    return this._loginUser(context, res, user, userLogin as AuthenticateTypes);
  }

  private async _loginUser(
    context: Context,
    res: any,
    user: UserPersistedInterface,
    userLogin: AuthenticateTypes,
  ) {
    let loginIdentityLink;

    if (userLogin.type === AuthenticationProviderTypeEnum.REFRESH) {
      const decodedAccessToken = jwt.decode(
        userLogin.data.token as any,
      ) as unknown as any;
      loginIdentityLink =
        this.authenticationService.getIdentityLinkForRefreshTokenSub(
          user.identityLinks,
          decodedAccessToken.sub,
        );
    } else {
      loginIdentityLink = user.identityLinks.find(
        (link) => link.usage === IdentityLinkUsageEnum.LOGIN,
      );
    }

    if (!loginIdentityLink) {
      throw new HttpException('Not authorized', HttpStatus.UNAUTHORIZED);
    }

    const authenticationService =
      this.authenticationServiceFactory.getAuthenticationServiceForLoginIdentityLinkType(
        loginIdentityLink.type,
      );
    if (!authenticationService) {
      throw new HttpException('Not authorized', HttpStatus.UNAUTHORIZED);
    }

    const tokenData = await this._refreshUserPermissionsWithPersistedUser(
      context,
      userLogin,
      user,
      loginIdentityLink,
    );

    if (!tokenData) {
      return await authenticationService.loginUser(
        context,
        res,
        user,
        loginIdentityLink,
        userLogin,
      );
    } else {
      return tokenData;
    }
  }

  public async createUser(
    context: Context,
    userId: string | null,
    newUser: UserCreateInterface,
    authenticationId?: string,
  ) {
    if (!userId && !newUser.username) {
      throw new HttpException(
        `Must provide either a userId or a username`,
        HttpStatus.BAD_REQUEST,
      );
    }

    let user;
    if (userId && newUser.username) {
      user = await this.userDataService.getUserWithIdOrName(
        userId,
        newUser.username,
      );
      if (user) {
        throw new HttpException(
          `User with username: ${newUser.username} or user Id: ${userId} already exists`,
          HttpStatus.CONFLICT,
        );
      }
    } else if (newUser.username) {
      user = await this.userDataService.getUserWithUsername(newUser.username);
      if (user) {
        throw new HttpException(
          `User with username: ${newUser.username} already exists`,
          HttpStatus.CONFLICT,
        );
      }
    } else if (userId) {
      user = await this.userDataService.getUserWithId(userId);
      if (user) {
        throw new HttpException(
          `User with id: ${userId} already exists`,
          HttpStatus.CONFLICT,
        );
      }
    }

    let loginIdentityLink: IdentityLinkInterface;
    const useUserId = userId || uuidv4();

    if (
      this.config.authenticationProviderType ===
      AuthenticationServiceTypeEnum.AUTHF
    ) {
      loginIdentityLink = {
        usage: IdentityLinkUsageEnum.LOGIN,
        type: IdentityLinkTypeEnum.AUTHF,
        data: {
          authenticationId: authenticationId || uuidv4(),
        },
      } as AuthfIdentityLinkInterface;
    }

    if (!loginIdentityLink) {
      throw new HttpException(
        `Unsupported authentication provider type: ${this.config.authenticationProviderType}`,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }

    const permissions = await this.permissionsProvider.getPermissionsForNewUser(
      context,
    );
    const tokenExpirationPolicy = this.config.tokenExpirationPolicy;

    await this.authenticationService.createUser(
      context,
      useUserId,
      loginIdentityLink,
      newUser,
      PermissionUtil.mapSelfToUserId(permissions, useUserId),
      tokenExpirationPolicy,
    );

    const defaultUserAttributes = this.config.newUserDefaultAttributes;
    const defaultIdentityLinks = defaultUserAttributes.identityLinks
      ? defaultUserAttributes.identityLinks
      : [];

    const loginExtra = await this.loginExtraService.getLoginExtra(useUserId);
    const persistUser: UserPersistedInterface = {
      id: useUserId,
      identityLinks: [loginIdentityLink, ...defaultIdentityLinks],
      username: newUser.username || null,
      groups: ArrayUtil.unique([
        ...(defaultUserAttributes.groups || []),
        ...(newUser.groups || []),
      ]),
      hash: {
        permissions: PermissionUtil.getPermissionsHash(permissions),
        tokenExpirationPolicy: TokenUtil.getTokenExpirationPolicyHash(
          this.config.tokenExpirationPolicy,
        ),
        extra: ExtraUtil.getExtraHash(loginExtra),
      },
    };

    await this.userDataService.createUser(context, persistUser);

    if (
      this.config.authenticationProviderType ===
      AuthenticationServiceTypeEnum.AUTHF
    ) {
      await this.identityLinkService.saveIdentityLink(
        IdentityLinkUsageEnum.LOGIN,
        IdentityLinkTypeEnum.AUTHF,
        (loginIdentityLink as AuthfIdentityLinkInterface).data.authenticationId,
        useUserId,
      );
    }
  }

  public async refreshUserPermissions(
    context: Context,
    userId: string,
    identityLink: IdentityLinkInterface,
    waitForUserToExist = false,
  ): Promise<void | CreateTokenResponseDataInterface> {
    let persistedUser;

    while (
      !(persistedUser = await this.userDataService.getUserWithId(userId)) &&
      waitForUserToExist
    ) {
      await new Promise((resolve) => setTimeout(resolve, 20));
    }
    return await this._refreshUserPermissionsWithPersistedUser(
      context,
      null,
      persistedUser,
      identityLink,
    );
  }

  public async getUsers(
    context: Context,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<UserInListDto>> {
    const result = await this.userDataService.getUsers(
      context,
      page,
      pageSize,
      options,
    );
    const users = result.results.map((persistedUser) => {
      const immutable = !!this.config.defaultUsers.find(
        (configUser) => configUser.id === persistedUser.id,
      );
      return new UserInListDto(persistedUser, immutable);
    });

    return {
      total: result.total,
      results: users,
    };
  }

  private async _refreshUserPermissionsWithPersistedUser(
    context: Context,
    userLogin: AuthenticateTypes | null,
    persistedUser: UserPersistedInterface,
    identityLink: IdentityLinkInterface,
  ): Promise<void | CreateTokenResponseDataInterface> {
    const currentPermissions =
      await this.permissionsProvider.getPermissionsForUser(
        context,
        persistedUser,
      );
    const currentExtra = await this.loginExtraService.getLoginExtra(
      persistedUser.id,
    );
    const currentExtraHash = ExtraUtil.getExtraHash(currentExtra);
    const currentPermissionsHash =
      PermissionUtil.getPermissionsHash(currentPermissions);
    const tokenExpirationPolicy = this.config.tokenExpirationPolicy;
    const currentTokenExpirationPolicyHash =
      TokenUtil.getTokenExpirationPolicyHash(tokenExpirationPolicy);
    let tokenData;
    if (
      persistedUser.hash.permissions !== currentPermissionsHash ||
      persistedUser.hash.tokenExpirationPolicy
    ) {
      tokenData = await this.authenticationService.updateTokenPayload(
        context,
        userLogin,
        persistedUser.id,
        PermissionUtil.mapSelfToUserId(currentPermissions, persistedUser.id),
        tokenExpirationPolicy,
        [identityLink],
        true,
      );
      let succeeded = false;
      while (!succeeded) {
        try {
          await this.userDataService.updateTokenHashes(
            context,
            persistedUser.id,
            currentPermissionsHash,
            currentTokenExpirationPolicyHash,
            currentExtraHash,
          );
          succeeded = true;
        } catch (e) {
          await new Promise((resolve) => setTimeout(resolve, 100));
        }
      }
    }

    if (persistedUser.hash.extra !== currentExtraHash) {
      const loginIdentityLink = persistedUser.identityLinks.find(
        (findLink) => findLink.usage === IdentityLinkUsageEnum.LOGIN,
      );

      if (!loginIdentityLink) {
        return;
      }

      tokenData = await this.authenticationService.updateUserAuthentication(
        context,
        userLogin,
        loginIdentityLink,
        PermissionUtil.mapSelfToUserId(currentPermissions, persistedUser.id),
        this.config.tokenExpirationPolicy,
        persistedUser.id,
      );
      let succeeded = false;
      while (!succeeded) {
        try {
          await this.userDataService.updateTokenHashes(
            context,
            persistedUser.id,
            currentPermissionsHash,
            currentTokenExpirationPolicyHash,
            currentExtraHash,
          );
          succeeded = true;
        } catch (e) {
          await new Promise((resolve) => setTimeout(resolve, 100));
        }
      }
    }
    return tokenData;
  }

  public async getUserById(context: Context, userId: string): Promise<UserDto> {
    const user = await this.userDataService.getUserWithId(userId);
    if (!user) {
      throw new HttpException('User does not exist', HttpStatus.NOT_FOUND);
    }
    const immutable = !!this.config.defaultUsers.find(
      (configUser) => configUser.id === user.id,
    );

    const groups = await this.userGroupsProvider.getGroupsForUser(
      context,
      user.groups,
    );
    const groupsWithPolicies = await this.groupService.getGroupsByIds(
      context,
      groups.map((group) => group.id),
    );

    const policyIds = GroupUtil.getUniquePolicyIdsForGroups(groupsWithPolicies);

    const policies = await this.policyService.getPoliciesByIds(
      context,
      policyIds,
    );

    const userPolicies = policies.map((policy) => {
      const fromGroups = [];
      const policyIds = policies.map((policy) => policy.id);
      for (const group of groups) {
        const intersect = group.policies.filter((policyId) =>
          policyIds.includes(policyId),
        );
        if (intersect.length > 0) {
          fromGroups.push({
            id: group.id,
            name: group.name,
          });
        }
      }

      return {
        id: policy.id,
        name: policy.name,
        description: policy.description,
        fromGroups,
      };
    });

    const permissions = await this.permissionsProvider.getPermissionsForUser(
      context,
      user,
    );

    const userPermissions = permissions.map((permission) => {
      const fromPolicies = [];
      for (const policy of policies) {
        if (policy.permissions.includes(permission.id)) {
          fromPolicies.push({
            id: policy.id,
            name: policy.name,
          });
        }
      }

      const fromGroups = [];
      const policyIds = fromPolicies.map((policy) => policy.id);
      for (const group of groups) {
        const intersect = group.policies.filter((policyId) =>
          policyIds.includes(policyId),
        );
        if (intersect.length > 0) {
          fromGroups.push({
            id: group.id,
            name: group.name,
          });
        }
      }

      return {
        id: permission.id,
        name: permission.name,
        value: permission.value,
        description: permission.description,
        fromPolicies,
        fromGroups,
      };
    });

    const groupsForResponse = groups.map((group) => ({
      id: group.id,
      name: group.name,
      description: group.description,
    }));

    return new UserDto(
      user,
      groupsForResponse,
      userPolicies,
      userPermissions,
      immutable,
    );
  }

  public async deleteUser(context: Context, userId: string): Promise<void> {
    this._immutabilityCheck(context, userId);
    const didModify = await this.userDataService.deleteUser(context, userId);
    if (!didModify) {
      throw new NotFoundException();
    }
  }

  public async deleteUsers(context: Context, userIds: string[]): Promise<void> {
    for (const userId of userIds) {
      this._immutabilityCheck(context, userId);
    }

    const didModify = await this.userDataService.deleteUsers(context, userIds);
    if (!didModify) {
      throw new NotFoundException();
    }
  }

  public async updateUser(
    context: Context,
    userId: string,
    userUpdate: UserUpdateInterface,
  ): Promise<void> {
    this._immutabilityCheck(context, userId);
    const didModify = await this.userDataService.updateUser(
      context,
      userId,
      userUpdate,
    );
    if (!didModify) {
      throw new NotFoundException();
    }
  }

  public async patchUser(
    context: Context,
    userId: string,
    userPatch: UserPatchInterface,
  ): Promise<void> {
    this._immutabilityCheck(context, userId);
    const didModify = await this.userDataService.patchUser(
      context,
      userId,
      userPatch,
    );
    if (!didModify) {
      throw new NotFoundException();
    }
  }

  public async addGroups(
    context: Context,
    userId: string,
    groupIds: string[],
  ): Promise<void> {
    const uniqueGroups = ArrayUtil.unique(groupIds);

    const foundGroups = await this.groupService.getGroupsByIds(
      context,
      uniqueGroups,
    );

    if (foundGroups.length !== uniqueGroups.length) {
      throw new HttpException(`Invalid groups`, HttpStatus.BAD_REQUEST);
    }

    const didModify = await this.userDataService.addGroups(
      context,
      userId,
      uniqueGroups,
    );
    if (!didModify) {
      throw new NotFoundException();
    }
  }

  public async removeGroups(
    context: Context,
    userId: string,
    groupIds: string[],
  ): Promise<void> {
    const immutableUser = this.config.defaultUsers.find(
      (findUser) => findUser.id === userId,
    );

    if (immutableUser) {
      if (ArrayUtil.intersection(groupIds, immutableUser.groups).length > 0) {
        const locale =
          groupIds.length === 1
            ? LocalesEnum.ERROR_IMMUTABLE_USER_GROUP_DELETE_SINGLE
            : LocalesEnum.ERROR_IMMUTABLE_USER_GROUP_DELETE_MULTIPLE;
        throw new ImmutableObjectError(context, locale);
      }
    }

    const didModify = await this.userDataService.removeGroups(
      context,
      userId,
      groupIds,
    );

    if (!didModify) {
      throw new NotFoundException();
    }
  }

  public async existsByUsername(
    context: Context,
    username: string,
  ): Promise<boolean> {
    const user = await this.userDataService.getUserWithUsername(username);
    return !!user;
  }

  private _immutabilityCheck(context: Context, userId: string) {
    const immutableUser = this.config.defaultUsers.find(
      (findUser) => findUser.id === userId,
    );
    if (immutableUser) {
      throw new ImmutableObjectError(context, LocalesEnum.ERROR_IMMUTABLE_USER);
    }
  }

  public async getUsersForGroup(
    context: Context,
    groupId: string,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<UserInListDto>> {
    const result = await this.userDataService.getUsersForGroups(
      context,
      [groupId],
      page,
      pageSize,
      options,
    );
    const users = result.results.map((persistedUser) => {
      const immutable = !!this.config.defaultUsers.find(
        (configUser) => configUser.id === persistedUser.id,
      );
      return new UserInListDto(persistedUser, immutable);
    });

    return {
      total: result.total,
      results: users,
    };
  }

  public async getUsersForPolicy(
    context: Context,
    policyId: string,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<UserInListDto>> {
    const groups = await this.groupService.getGroupsForPolicyId(
      context,
      policyId,
    );

    const result = await this.userDataService.getUsersForGroups(
      context,
      groups.map((g) => g.id),
      page,
      pageSize,
      options,
    );
    const users = result.results.map((persistedUser) => {
      const immutable = !!this.config.defaultUsers.find(
        (configUser) => configUser.id === persistedUser.id,
      );
      return new UserInListDto(persistedUser, immutable);
    });

    return {
      total: result.total,
      results: users,
    };
  }

  public async getUsersForPermission(
    context: Context,
    permissionId: string,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<UserInListDto>> {
    const policies = await this.policyService.getPoliciesForPermission(
      context,
      permissionId,
    );

    const groups = await this.groupService.getGroupsByPoliciesForPermissions(
      context,
      policies.map((p) => p.id),
    );

    const result = await this.userDataService.getUsersForGroups(
      context,
      groups.map((g) => g.id),
      page,
      pageSize,
      options,
    );

    const users = result.results.map((persistedUser) => {
      const immutable = !!this.config.defaultUsers.find(
        (configUser) => configUser.id === persistedUser.id,
      );
      return new UserInListDto(persistedUser, immutable);
    });

    return {
      total: result.total,
      results: users,
    };
  }
}
