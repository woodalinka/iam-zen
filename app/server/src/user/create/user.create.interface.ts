import { AuthenticationTypes } from '@cryptexlabs/authf-data-model';

export interface UserCreateInterface {
  authentication?: AuthenticationTypes;
  username?: string;
  groups?: string[];
}
