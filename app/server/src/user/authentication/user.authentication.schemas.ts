import * as Joi from 'joi';
import { AuthenticationProviderTypeEnum } from '@cryptexlabs/authf-data-model';

export const userAuthenticationSchemas = Joi.alternatives()
  .try(
    Joi.object({
      type: Joi.string().valid(AuthenticationProviderTypeEnum.BASIC).required(),
      data: Joi.object({
        username: Joi.string().required(),
        password: Joi.string().required(),
      }).required(),
    }),
    Joi.object({
      type: Joi.string().valid(AuthenticationProviderTypeEnum.API).required(),
      data: Joi.object({
        apiKey: Joi.string().required(),
        secret: Joi.string().required(),
      }).required(),
    }),
    Joi.object({
      type: Joi.string()
        .valid(AuthenticationProviderTypeEnum.SAMSUNG_LEGACY)
        .required(),
      data: Joi.object({
        appId: Joi.string().required(),
        appSecret: Joi.string().required(),
        userId: Joi.string().required(),
      }).required(),
    }),
  )
  .optional();
