import { UserPersistedInterface } from '../persistence/user.persisted.interface';

export class UserInListDto {
  public readonly id: string;
  public readonly groupCount: number;
  public readonly username: string;

  constructor(user: UserPersistedInterface, public readonly immutable) {
    this.id = user.id;
    this.groupCount = (user.groups || []).length;
    this.username = user.username;
  }
}
