import { AuthenticationTypes } from '@cryptexlabs/authf-data-model';

export interface UserConfigInterface {
  username: string;
  id: string;
  groups: string[];
  authentication: AuthenticationTypes;
}
