import {
  Body,
  Controller,
  Delete,
  Get,
  Headers,
  HttpCode,
  HttpStatus,
  Inject,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Put,
  Query,
  Response,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  ExampleAuthTokenPairResponse,
  ExampleBasicAuth,
  exampleNewBasicAuthUser,
  examplePatchUser,
  ExampleRefreshAuth,
  exampleUpdateUser,
  exampleUser,
  exampleUserId,
  exampleUsers,
} from './example';
import { UserService } from './user.service';
import { UserCreateInterface } from './create/user.create.interface';
import {
  ApiMetaHeaders,
  ApiPagination,
  ContextBuilder,
  NotEmptyPipe,
  RestPaginatedResponse,
  RestResponse,
  SimpleHttpResponse,
} from '@cryptexlabs/codex-nodejs-common';
import { LocalesEnum } from '../locale/enum';
import {
  AuthenticateTypes,
  AuthfMetaTypeEnum,
} from '@cryptexlabs/authf-data-model';
import { UserRegisterValidationPipe } from './register/user.register.validation.pipe';
import { UserRegisterWithUsernameValidationPipe } from './register/user.register.with-username.validation.pipe';
import { ExampleSimpleResponse } from '../example/example.simple.response';
import { ExampleRestResponse } from '../example/example.rest.response';
import { UserUpdateInterface } from './update/user.update.interface';
import { UserPatchInterface } from './update/user.patch.interface';
import { appConfig } from '../setup';
import { UserModifyGroupsGuard } from './group/user.modify.groups.guard';
import { UserListAuthzGuard } from './read/user.list.authz.guard';
import { UserUpdateAuthzGuard } from './update/user.update.authz.guard';
import { UserDeleteAuthzGuard } from './delete/user.delete.authz.guard';
import { UserGetAuthzGuard } from './read/user.get.authz.guard';
import { UserCreateAuthzGuard } from './create/user.create.authz.guard';
import { MessageInterface } from '@cryptexlabs/codex-data-model';
import { UserInListDto } from './read/user.in-list.dto';
import { UserPatchValidationPipe } from './update/user.patch.validation.pipe';
import { UserUpdateValidationPipe } from './update/user.update.validation.pipe';
import { Uuidv4ArrayValidationPipe } from '@cryptexlabs/codex-nodejs-common/lib/src/pipe/uuidv4.array.validation.pipe';
import { BasicAuthAuthenticateInterface } from '@cryptexlabs/authf-data-model/src/authentication/provider/index';
import { OrderDirectionEnum } from '../query.options.interface';
import { OrderDirectionValidationPipe } from '../order.direction.validation.pipe';

@Controller(`/${appConfig.appPrefix}/${appConfig.apiVersion}/user`)
@ApiTags('user')
@ApiMetaHeaders()
export class UserController {
  constructor(
    private readonly userService: UserService,
    @Inject('CONTEXT_BUILDER') private readonly contextBuilder: ContextBuilder,
  ) {}

  @Post(':userId/token')
  @ApiOperation({
    summary: 'Create a new token for a user (Login)',
  })
  @ApiBody({
    examples: {
      'Basic Auth': {
        value: new ExampleBasicAuth(),
      },
      Refresh: {
        value: new ExampleRefreshAuth(),
      },
    },
    schema: {},
    required: true,
  })
  @ApiParam({
    name: 'userId',
    example: exampleUserId,
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleAuthTokenPairResponse(),
    },
    status: HttpStatus.OK,
  })
  public async login(
    @Response() res,
    @Headers() headers,
    @Body(NotEmptyPipe) body: BasicAuthAuthenticateInterface,
    @Param('userId', NotEmptyPipe) userId: string,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();
    if (userId === 'any') {
      return this.loginWithUsername(res, headers, body);
    }

    const result = await this.userService.loginUserWithId(
      context,
      res,
      userId,
      body as AuthenticateTypes,
    );
    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    res.send(
      new RestResponse(
        responseContext,
        HttpStatus.CREATED,
        AuthfMetaTypeEnum.AUTHENTICATION_TOKEN_PAIR,
        result,
      ),
    );
  }

  @Post('any/token')
  @ApiOperation({
    summary: 'Create a new token for a user (Login) with username',
  })
  @ApiBody({
    examples: {
      'Basic Auth': {
        value: new ExampleBasicAuth(),
      },
      Refresh: {
        value: new ExampleRefreshAuth(),
      },
    },
    schema: {},
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleAuthTokenPairResponse(),
    },
    status: HttpStatus.OK,
  })
  public async loginWithUsername(
    @Response() res,
    @Headers() headers,
    @Body(NotEmptyPipe) body: BasicAuthAuthenticateInterface,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();
    const result = await this.userService.loginUserWithUsername(
      context,
      res,
      body.data.username.toLowerCase(),
      body,
    );

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    res.send(
      new RestResponse(
        responseContext,
        HttpStatus.CREATED,
        AuthfMetaTypeEnum.AUTHENTICATION_TOKEN_PAIR,
        result,
      ),
    );
  }

  @Post(':userId')
  @ApiOperation({
    summary: 'Create new user (Register)',
  })
  @ApiParam({
    name: 'userId',
    example: exampleUserId,
    required: true,
  })
  @ApiBody({
    schema: {
      example: exampleNewBasicAuthUser,
    },
    required: true,
  })
  @UseGuards(UserCreateAuthzGuard)
  public async register(
    @Headers() headers,
    @Body(NotEmptyPipe, UserRegisterValidationPipe) body: UserCreateInterface,
    @Param('userId', NotEmptyPipe) userId: string,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    await this.userService.createUser(context, userId, {
      ...body,
      username: body.username.toLowerCase(),
    });

    return new SimpleHttpResponse(
      this.contextBuilder
        .build()
        .setMetaFromHeadersForNewMessage(headers)
        .getResult(),
      HttpStatus.CREATED,
      LocalesEnum.SUCCESS,
    );
  }

  @Post()
  @ApiOperation({
    summary: 'Create new user (Register) with username',
  })
  @ApiBody({
    schema: {
      example: exampleNewBasicAuthUser,
    },
    required: true,
  })
  @UseGuards(UserCreateAuthzGuard)
  public async registerWithUsername(
    @Headers() headers,
    @Body(NotEmptyPipe, UserRegisterWithUsernameValidationPipe)
    body: UserCreateInterface,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    await this.userService.createUser(context, null, {
      ...body,
      username: body.username.toLowerCase(),
    });

    return new SimpleHttpResponse(
      this.contextBuilder
        .build()
        .setMetaFromHeadersForNewMessage(headers)
        .getResult(),
      HttpStatus.CREATED,
      LocalesEnum.SUCCESS,
    );
  }

  @Get()
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Get list of users',
  })
  @ApiPagination()
  @ApiQuery({
    name: 'orderBy',
    required: false,
    schema: {
      example: 'name',
    },
  })
  @ApiQuery({
    name: 'orderDirection',
    required: false,
    schema: {
      example: 'asc',
    },
  })
  @ApiQuery({
    name: 'searchName',
    required: false,
  })
  @ApiQuery({
    name: 'search',
    required: false,
  })
  @ApiResponse({
    schema: {
      example: new ExampleRestResponse(
        HttpStatus.OK,
        'cryptexlabs.iam-zen.user.list',
        exampleUsers,
      ),
    },
  })
  @UseGuards(UserListAuthzGuard)
  public async getUsers(
    @Query('page', NotEmptyPipe, ParseIntPipe) page: number,
    @Query('pageSize', NotEmptyPipe, ParseIntPipe) pageSize: number,
    @Query('searchName') searchName: string,
    @Query('search') search: string,
    @Query('orderBy') orderBy: string,
    @Query('orderDirection', OrderDirectionValidationPipe)
    orderDirection: OrderDirectionEnum,
    @Headers() headers: Record<string, string>,
  ): Promise<MessageInterface<UserInListDto[]>> {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const result = await this.userService.getUsers(context, page, pageSize, {
      searchName,
      search,
      orderBy,
      orderDirection,
    });

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestPaginatedResponse(
      responseContext,
      HttpStatus.OK,
      result.total,
      'cryptexlabs.iam-zen.user.list',
      result.results,
    );
  }

  @Get(':userId')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Get a user by user ID',
  })
  @ApiParam({
    name: 'userId',
    example: '278c6ec0-777c-43ad-83db-18b7cdfe2314',
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleRestResponse(
        HttpStatus.OK,
        'cryptexlabs.iam-zen.user',
        exampleUser,
      ),
    },
    status: HttpStatus.OK,
  })
  @UseGuards(UserGetAuthzGuard)
  public async getUser(
    @Param('userId', NotEmptyPipe) userId: string,
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const user = await this.userService.getUserById(context, userId);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestResponse(
      responseContext,
      HttpStatus.OK,
      'cryptexlabs.iam-zen.user',
      user,
    );
  }

  @Delete(':userId')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Delete user by user ID',
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @ApiParam({
    name: 'userId',
    example: '16b327c5-af12-4f02-a772-005c6b92ba6d',
    required: true,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(UserDeleteAuthzGuard)
  public async deleteUser(
    @Param('userId', NotEmptyPipe) userId: string,
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    await this.userService.deleteUser(context, userId);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Delete()
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Delete users by user ID',
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @ApiQuery({
    name: 'ids',
    example: '16b327c5-af12-4f02-a772-005c6b92ba6d',
    required: true,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(UserDeleteAuthzGuard)
  public async deleteUsers(
    @Query('ids', NotEmptyPipe) userIds: string[],
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const useIds = Array.isArray(userIds) ? userIds : [userIds];

    await this.userService.deleteUsers(context, useIds);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Put(':userId')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Update user by user ID',
  })
  @ApiParam({
    name: 'userId',
    example: '16b327c5-af12-4f02-a772-005c6b92ba6d',
    required: true,
  })
  @ApiBody({
    schema: {
      example: exampleUpdateUser,
    },
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(UserUpdateAuthzGuard)
  public async updateUser(
    @Param('userId', NotEmptyPipe) userId: string,
    @Headers() headers,
    @Body(NotEmptyPipe, UserUpdateValidationPipe)
    userUpdate: UserUpdateInterface,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    await this.userService.updateUser(context, userId, userUpdate);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Patch(':userId')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Partially update user by user ID',
  })
  @ApiParam({
    name: 'userId',
    example: '16b327c5-af12-4f02-a772-005c6b92ba6d',
    required: true,
  })
  @ApiBody({
    schema: {
      example: examplePatchUser,
    },
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(UserUpdateAuthzGuard)
  public async patchUser(
    @Param('userId', NotEmptyPipe) userId: string,
    @Body(NotEmptyPipe, UserPatchValidationPipe) userPatch: UserPatchInterface,
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    await this.userService.patchUser(context, userId, {
      ...userPatch,
      username: userPatch?.username.toLowerCase(),
    });

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Post(':userId/group')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Add user to groups',
  })
  @ApiParam({
    name: 'userId',
    example: '16b327c5-af12-4f02-a772-005c6b92ba6d',
    required: true,
  })
  @ApiBody({
    schema: {
      example: ['613b6c12-d9c1-47cf-b401-0b5b03ef33d0'],
    },
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(new UserModifyGroupsGuard('create'))
  public async addGroups(
    @Param('userId', NotEmptyPipe) userId: string,
    @Body(NotEmptyPipe, Uuidv4ArrayValidationPipe) groups: string[],
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    await this.userService.addGroups(context, userId, groups);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Delete(':userId/group')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Remove user from groups',
  })
  @ApiParam({
    name: 'userId',
    example: '16b327c5-af12-4f02-a772-005c6b92ba6d',
    required: true,
  })
  @ApiQuery({
    name: 'ids',
    example: [
      '613b6c12-d9c1-47cf-b401-0b5b03ef33d0',
      'cf24881f-0983-40e2-a6d1-2e560863e89e',
    ],
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(new UserModifyGroupsGuard('delete'))
  public async removeGroups(
    @Param('userId', NotEmptyPipe) userId: string,
    @Query('ids', NotEmptyPipe) ids: string[],
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const groupIds = typeof ids === 'string' ? [ids] : ids;

    await this.userService.removeGroups(context, userId, groupIds);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Get('any/exists')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Checks if user exists',
  })
  @ApiQuery({
    name: 'username',
    example: 'johndoe',
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleRestResponse(
        HttpStatus.OK,
        'cryptexlabs.iam-zen.user.exists',
        true,
      ),
    },
  })
  @UseGuards(UserCreateAuthzGuard)
  public async getUserNameExists(
    @Headers() headers,
    @Query('username') username: string,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const exists = await this.userService.existsByUsername(context, username);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestResponse(
      responseContext,
      HttpStatus.OK,
      'cryptexlabs.iam-zen.user.exists',
      exists,
    );
  }
}
