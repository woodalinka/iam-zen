import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { HttpAuthzGuardUtil } from '@cryptexlabs/codex-nodejs-common';

@Injectable()
export class UserDeleteAuthzGuard implements CanActivate {
  public canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const util = new HttpAuthzGuardUtil(context);
    return util.isAuthorized(
      {
        object: 'iam-zen',
        objectId: '',
        action: '',
      },
      {
        object: 'user',
        objectId: util.params.userId,
        action: 'delete',
      },
    );
  }
}
