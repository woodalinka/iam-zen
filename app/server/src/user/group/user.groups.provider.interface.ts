import { UserGroupDetailedInterface } from './user.group.interface';
import { Context } from '@cryptexlabs/codex-nodejs-common';

export interface UserGroupsProviderInterface {
  getGroupsForUser(
    context: Context,
    groupIds: string[],
  ): Promise<UserGroupDetailedInterface[]>;
}
