export interface UserGroupInterface {
  id: string;
  name: string;
  description: string;
}

export interface UserGroupDetailedInterface extends UserGroupInterface {
  policies: string[];
}
