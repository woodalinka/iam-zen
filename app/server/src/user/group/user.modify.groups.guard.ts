import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { HttpAuthzGuardUtil } from '@cryptexlabs/codex-nodejs-common';
import { AuthzNamespace } from '../../constants';

@Injectable()
export class UserModifyGroupsGuard implements CanActivate {
  constructor(private readonly action: string) {}

  public canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const util = new HttpAuthzGuardUtil(context);

    const groups = this.action === 'delete' ? util.query.ids : util.body;

    for (const id of groups) {
      if (
        !util.isAuthorized(
          AuthzNamespace,
          {
            action: '',
            object: 'user',
            objectId: util.params.userId,
          },
          {
            action: this.action,
            object: 'group',
            objectId: id,
          },
        )
      ) {
        return false;
      }
    }
    return true;
  }
}
