import {
  ContextBuilder,
  RestPaginatedResponse,
  ServiceClient,
} from '@cryptexlabs/codex-nodejs-common';
import { HttpStatus } from '@nestjs/common';
import { appConfig, baseLogger } from '../setup';
import { MessageContext } from '@cryptexlabs/codex-data-model';
import { i18nData } from '../locale/locales';

const exampleContext = new ContextBuilder(
  baseLogger,
  appConfig,
  new ServiceClient(appConfig),
  new MessageContext('default', null),
)
  .setI18nApi(i18nData)
  .build()
  .getResult();

export class ExampleRestPaginatedResponse extends RestPaginatedResponse {
  constructor(status: HttpStatus, type: string, data: any) {
    super(exampleContext, status, 1, type, data);
  }
}
