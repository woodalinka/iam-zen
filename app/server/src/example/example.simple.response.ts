import {
  ContextBuilder,
  ServiceClient,
  SimpleHttpResponse,
} from '@cryptexlabs/codex-nodejs-common';
import { LocalesEnum } from '../locale/enum';
import { HttpStatus } from '@nestjs/common';
import { appConfig, baseLogger } from '../setup';
import { MessageContext } from '@cryptexlabs/codex-data-model';
import { i18nData } from '../locale/locales';

const exampleContext = new ContextBuilder(
  baseLogger,
  appConfig,
  new ServiceClient(appConfig),
  new MessageContext('default', null),
)
  .setI18nApi(i18nData)
  .build()
  .getResult();

export class ExampleSimpleResponse extends SimpleHttpResponse {
  constructor() {
    super(exampleContext, HttpStatus.OK, LocalesEnum.SUCCESS);
  }
}
