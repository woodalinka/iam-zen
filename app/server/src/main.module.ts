import { Module } from '@nestjs/common';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { APP_FILTER } from '@nestjs/core';
import { AppHttpExceptionFilter } from '@cryptexlabs/codex-nodejs-common';
import { appConfig, baseLogger, setupProviders } from './setup';
import { Locale } from '@cryptexlabs/codex-data-model';
import { appProviders } from './app/app.providers';
import { authenticationProviders } from './authentication/authentication.providers';
import { groupProviders } from './group/group.providers';
import { permissionProviders } from './permission/permission.providers';
import { policyProviders } from './policy/policy.providers';
import { reauthProviders } from './reauth/reauth.providers';
import { roleProviders } from './role/role.providers';
import { userProviders } from './user/user.providers';
import { AppController } from './app/app.controller';
import { GroupController } from './group/group.controller';
import { PermissionController } from './permission/permission.controller';
import { PolicyController } from './policy/policy.controller';
import { ReAuthController } from './reauth/reauth.controller';
import { RoleController } from './role/role.controller';
import { UserController } from './user/user.controller';
import { HealthzController } from './healthz.controller';
import { EnvController } from './env.controller';
import { RootController } from './root.controller';
import { LoadConfigService } from './load-config.service';

const defaultLocale = new Locale(
  appConfig.fallbackLanguage,
  appConfig.fallbackCountry,
);

const appFilter = new AppHttpExceptionFilter(
  defaultLocale,
  baseLogger,
  appConfig,
);

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', '..', '..', 'ui', 'build'),
      serveRoot: process.env.UI_PREFIX || '',
    }),
  ],
  controllers: [
    RootController,
    AppController,
    GroupController,
    PermissionController,
    PolicyController,
    ReAuthController,
    RoleController,
    UserController,
    HealthzController,
    EnvController,
  ],
  providers: [
    LoadConfigService,
    {
      provide: Locale,
      useValue: defaultLocale,
    },
    ...appProviders,
    ...authenticationProviders,
    ...groupProviders,
    ...permissionProviders,
    ...policyProviders,
    ...reauthProviders,
    ...roleProviders,
    ...userProviders,
    {
      provide: APP_FILTER,
      useValue: appFilter,
    },
    ...setupProviders,
  ],
})
export class MainModule {}
