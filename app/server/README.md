# IAM Zen Development Configuration

Configuration settings for setting up development environment

## Setup

### Localise.biz
You need an account with [localize.biz](https://localise.biz) to commit language file updates to this repository

If you haven't done so already add the following to your `~/.bash_profile`

```bash
export IAM_ZEN_LOCALISE_BIZ_API_KEY=your_api_key_goes_here
```

Reload your bash profile
```bash
source ~/.bash_profile
```

Verify your api key has been set as an environment variable
```bash
env | grep IAM_ZEN_LOCALISE_BIZ_API_KEY
```

### Database Setup

#### Copy overrides file

##### Sqlite (todo)
```shell
cp docker-compose.override.sqlite.example.yml docker-compose.override.yml
```

##### Elasticsearch (preferred)
```shell
cp docker-compose.override.elasticsearch.example.yml docker-compose.override.yml
```

##### MySQL
```shell
cp docker-compose.override.elasticsearch.example.yml docker-compose.override.yml
```

### Docker Setup (preferred)

#### Docker Build
```bash
cd ../docker
docker-compose build
```

### Localhost Setup

#### Installation

```bash
yarn install
```

## Running the app

### Docker (preferred)

```
cd ../docker
docker-compose up
```

### Localhost

#### development
```shell
yarn start
```

#### watch mode
```shell
yarn start:dev
```

#### incremental rebuild (webpack)
```shell
yarn webpack
yarn start:hmr
```

# production mode
```shell
yarn start:prod
```

## Test

# Unit test
```bash
yarn test
```

# e2e tests
```shell
yarn test:e2e
```

# test coverage
```shell
yarn test:cov
```

## Support

No Support

## License

MIT
